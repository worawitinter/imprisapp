<?php
require_once '../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
?>
<!-- main_login.php -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="../folder_script/favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $lbl_login_header;?></title>
    <link rel="stylesheet" type="text/css" href="../folder_script/css_utility.css" />
    <link rel="stylesheet" href="../folder_script/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="../folder_script/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="../folder_script/jquery-3.2.1.min.js"></script>
    <script src="../folder_script/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src='../folder_script/bootstrap-select-1.12.4/dist/js/bootstrap-select.min.js'></script>
    <script src="../folder_script/jquery-loading-overlay-1.6.0/src/loadingoverlay.min.js"></script>
    <script src="../folder_script/jquery-loading-overlay-1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>
    <script src='../folder_script/base64.js'></script>
    <script language="javascript">
        $(document).ready(function(){
                 $("#btn_submit").click(function(){
                        $.LoadingOverlay("show");
                          username=Base64.encode($("#fmi_username").val());
                          password=Base64.encode($("#fmi_password").val());
                          set_language=$("#set_language").val();
                          $.ajax({
                           type: "POST",
                           cache: false,
                           url: "login_authentication<?php echo $url_file_ext; ?>",
                                data: "username="+username+"&password="+password+"&set_language="+set_language,
                           success: function(data){
                                if(data==='true'){
                                     $("#add_err").html('<div class="alert alert-success"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;<?php echo $ntf_login_success;?>...</div>');
                                     window.location="../contentadmin/page/login_routine<?php echo $url_file_ext; ?>";
                                     $.LoadingOverlay("hide");
                                 }else if(data==='expired'){
                                     $("#add_err").css('display', 'inline', 'important');
                                     $("#add_err").html('<div class="alert alert-danger"><i class="fa fa-warning" aria-hidden="true"></i> บัญชีดังกล่าวหมดอายุ โปรดติดต่อผู้ดูแลระบบ!</div>');
                                     $.LoadingOverlay("hide");
                                 }else if(data==='exist'){
                                     $("#add_err").css('display', 'inline', 'important');
                                     $("#add_err").html('<div class="alert alert-danger"><i class="fa fa-warning" aria-hidden="true"></i> ชื่อผู้ใช้ดังกล่าว กำลังอยู่ในระบบ!</div>');
                                     $.LoadingOverlay("hide");
                                 }else{
                                    $("#add_err").css('display', 'inline', 'important');
                                    $("#add_err").html('<div class="alert alert-danger"><i class="fa fa-warning" aria-hidden="true"></i> <?php echo $ntf_login_error;?>!</div>');
                                    $.LoadingOverlay("hide");
                                }
                           },
                           beforeSend:function()
                           {
                                $("#add_err").css('display', 'inline', 'important');
                                $("#add_err").html('<div class="alert alert-success"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;<?php echo $ntf_login_success;?>...</div>');
                           }
                          });

                        return false;
                });
                $("#set_language").change(function(){
                    if(this.value != ''){
                        window.open("main_login.php?set_language=" + this.value,"_self");
                    }
                });
        });
    </script>
  </head>
  <body>
<br><br><br><br>
        <div class="container" style="width:70%;">
        <div class="card card-container">
            <div class="row">
                <div class="main_login_header col-lg-6">
                    <div class="img_box_login"><img class="resize_horizontal" src="<?php echo SERVER_URL_ROOT . '/folder_script/'.SYS_LOGO;?>" /></div>
                    <h1 class="text-center"><b><?php echo $lbl_app_name;?></b></h1>
                    <h2 class="text-center"><b>Consular Case Management System</b></h2>
                    <h3 class="text-center"><b><?php echo $lbl_version." ".VERSION;?></b></h3>
                </div>
                  <div class="col-lg-6">
                      <div class="well">
                          <h2 style="border-bottom: 6px solid #c9302c;">ยินดีต้อนรับ</h2>
                          <form name="AppForm" enctype="multipart/form-data">
                              <span id="reauth-email" class="reauth-email"></span>
                              <div class="form-group input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                              <input class="form-control" type="text" id="fmi_username" name='fmi_username' placeholder="<?php echo $lbl_username;?>"/>
                              </div>
                              <div class="form-group input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                              <input class="form-control" type="password" id="fmi_password" name='fmi_password' placeholder="<?php echo $lbl_password;?>"/>
                              </div>
                              <div class="form-group input-group" style="display:none">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                              <select id="set_language" name="set_language" class="form-control">
                                  <option value="" "selected"><?php echo $lbl_lang_pref;?></option>
                                  <option value="th" <?php if($set_language == 'th'){ echo "selected";}?>><?php echo $lbl_lang_th;?></option>
                                  <option value="en" <?php if($set_language == 'en'){ echo "selected";}?>><?php echo $lbl_lang_en;?></option>
                              </select>
                              </div>
                              <div class="err" id="add_err"></div>
                              <div id="remember" class="checkbox">
                                  <label>

                                  </label>
                              </div>
                              <button id="btn_submit" class="btn btn-success" type="submit"><i class="fa fa-key" aria-hidden="true"></i> <?php echo $lbl_login_submit;?></button>
                          </form><!-- /form -->
                      </div>
                  </div>
              </div>
        </div>
        </div><!-- /card-container -->
        <h6 class="welcome text-center"><?php echo POWERBY;?></h6>
    </div><!-- /container -->
  </body>
</html>
