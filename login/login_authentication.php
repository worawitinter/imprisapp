<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/
require_once '../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/Authentication.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();
$Authentication = new Authentication();

$result = 'false';
$log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
$tablename = "sys_user_main";
$log_tablename = "sys_activity_main";
$log_fromtrans = "usraccess";
$login_username = base64_decode($_POST["username"]);
$login_password = base64_decode($_POST["password"]);
$date_created = $TransactionGenUnique->generate_trans_datetime();
$location = LOCATION;
$qs_login = $Authentication->verifyUser($tablename,$DatabaseOperation,$login_username);
if($qs_login->rowCount() > 0){
    while($row=$qs_login->fetch(PDO::FETCH_OBJ)) {
        $verify_result = $Authentication->decryptPassword($login_password,$row->password);
        if($verify_result == true && $row->var_50_004 == 0 || ($row->uniquenum_pri == 'sysupri' || $row->username == 'admin')){
            session_start();
            $_SESSION['login_session'] = time();
            $_SESSION["cookies_sys_admin_yn"] = "n";
            $_SESSION["cookies_sys_create_yn"] = "n";
            $_SESSION["cookies_sys_view_yn"] = "n";
            $_SESSION["cookies_sys_delete_yn"] = "n";
            $_SESSION["cookies_username"] = $row->username;
            $_SESSION["cookies_last_access"] = $row->date_001;
            $_SESSION["cookies_sys_role"] = $row->sys_role;
            $_SESSION["cookies_sys_acc_right"] = $row->sys_acc_right;
            $_SESSION["cookies_set_language"] = $_POST["set_language"];
            $_SESSION["cookies_log_uniquenum_pri"] = $log_uniquenum_pri;
            $_SESSION["cookies_usr_uniquenum_pri"] = $row->uniquenum_pri;

            $sql = "SELECT usr.idcode, dept.uniquenum_pri as dept_uniq, dept.desc_lang01 as dept_desc, usr.usr_firstname as firstname, usr.usr_lastname as lastname"
                    . " FROM sys_gencode_main as dept"
                    . " INNER JOIN sys_user_data as usr"
                    . " ON usr.var_25_001 = dept.uniquenum_pri"
                    . " AND dept.tag_table_usage = 'sys_user_dept'"
                    . " AND dept.tag_deleted_yn = 'n'"
                    . " AND dept.tag_active_yn = 'y'"
                    . "WHERE usr.uniquenum_pri = '".$row->uniquenum_pri."'"
                    . " AND usr.tag_table_usage = 'sys_user'"
                    . " AND usr.tag_deleted_yn = 'n'";
            $qs_result = $DatabaseOperation->getRecordFromTable($sql);
            while($rows=$qs_result->fetch(PDO::FETCH_OBJ)) {
                $_SESSION["cookies_sys_user_dept_desc"] = $rows->dept_desc;
                $_SESSION["cookies_sys_user_dept_uniq"] = $rows->dept_uniq;
                $_SESSION["cookies_sys_user_firstname"] = $rows->firstname;
                $_SESSION["cookies_sys_user_lastname"] = $rows->lastname;
            }

            $sys_acc_right = $SysConversion->convertListToArray($row->sys_acc_right);
            if(count($sys_acc_right) > 0){
                foreach ($sys_acc_right as $usr_right) {
                    if($usr_right == 'create'){
                        $_SESSION["cookies_sys_create_yn"] = "y";
                    }else if($usr_right == 'view'){
                        $_SESSION["cookies_sys_view_yn"] = "y";
                    }else if($usr_right == 'delete'){
                        $_SESSION["cookies_sys_delete_yn"] = "y";
                    }
                }
            }
            if($row->sys_role == 'admin'){
                $_SESSION["cookies_sys_admin_yn"] = "y";
                $_SESSION["cookies_sys_create_yn"] = "y";
                $_SESSION["cookies_sys_view_yn"] = "n";
                $_SESSION["cookies_sys_delete_yn"] = "y";
            }
            $condition = "tag_table_usage = 'sys_user' and tag_deleted_yn = 'n' and tag_active_yn = 'y' and uniquenum_pri = '".$row->uniquenum_pri."'";
            $data = array("date_001"=>$TransactionGenUnique->generate_trans_datetime(),
                          "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                          "var_25_002"=>$log_uniquenum_pri
                         );
            $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);

            $log_data = array   (   "tag_table_usage"=>$log_fromtrans,
                                "uniquenum_pri"=>$log_uniquenum_pri,
                                "sys_link"=>"login",
                                "uniquenum_sec"=>$row->uniquenum_pri,
                                "userid_cookie"=>$row->username,
                                "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                                "date_created"=>$date_created,
                                "desc_lang01"=>$_SERVER['HTTP_USER_AGENT'],
                                "notes_memo"=>$location,
                                "var_25_001"=>$_POST["set_language"],
                                "date_001"=>$row->date_001,
                                "var_100_001"=>$row->sys_role,
                                "var_100_002"=>$row->sys_acc_right,
                                "desc_lang10"=>getHostByName(getHostName())
                            );
           $DatabaseOperation->insertRecordIntoTable($log_tablename,$log_data);
           $result = 'true';
       }elseif(($row->uniquenum_pri <> 'sysupri' && $row->username <> 'admin') && $row->var_50_004 > 0){
           $result = "exist";
       }elseif($row->date_002 < $TransactionGenUnique->generate_trans_datetime()){
           $result = 'expired';
       }
    }
}else{
    $log_data = array   (   "tag_table_usage"=>$log_fromtrans,
                        "uniquenum_pri"=>$log_uniquenum_pri,
                        "sys_link"=>"faile",
                        "userid_cookie"=>$_POST["username"],
                        "setgen_code"=>$_POST["password"],
                        "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                        "date_created"=>$date_created,
                        "desc_lang01"=>$_SERVER['HTTP_USER_AGENT'],
                        "notes_memo"=>$location,
                        "var_25_001"=>$_POST["set_language"],
                        "desc_lang10"=>getHostByName(getHostName())
                    );
   $DatabaseOperation->insertRecordIntoTable($log_tablename,$log_data);
}
echo $result;
?>
