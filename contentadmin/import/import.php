<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/import/parsecsv-for-php/parsecsv.lib.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysAutoNum.php';

$csv = new parseCSV();
$csv->encoding('UTF-16', 'UTF-8');
$csv->delimiter = "\t";
$csv->parse('csv/impris_import_24102017.csv');
//print_r($csv->data);
$array_result = $csv->data;
for($i=0; $i<=count($array_result); $i++){
    $uniquenum_pri = TransactionGenUnique::autogen_uniquenum_pri();
    $date_created = TransactionGenUnique::generate_trans_datetime();
    $userid = "admin";
    $fromtrans = "impris";
    $imp_autonum = Import::getLastNumber($fromtrans); //Last used running number
    $tablename = "tran_record_main";
    $fromlink = "imp";

    $name = Import::getPersonName($array_result[$i]["name"]);
    $imp_title = $name["title"];
    $imp_firstname = $name["fname"];
    $imp_lastname = $name["lname"];
    $imp_gender = $name["gender"];

    $person_id = Import::getPersonalID($array_result[$i]["passport"]);
    $imp_idcard = $person_id["ic"];
    $imp_passport = $person_id["passport"];

    $imp_station = Import::getStation($array_result[$i]["station"]);
    $state = $imp_station["state"];
    $station = str_replace(".", "", $imp_station["station"]);

    $imp_province = Import::getProvince($array_result[$i]["address"]);
    $imp_refdoc = $array_result[$i]["reference"];
    $status = $array_result[$i]["status"];
    $imp_prisdate = Import::getDateConversion($array_result[$i]["date"]);

    $imp_laws = Import::chkExistRecord($array_result[$i]["regulation_01"],"reg_list");
    if($imp_laws == ""){
        $insert_yn = "n";
        $error_msg = "Regulation not found!";
    }
    $imp_det = Import::chkExistRecord($station,"pris_det");
    if($imp_det == ""){
        $insert_yn = "n";
        $error_msg = "Station not found!";
    }else{
        $insert_yn = "y";
        $error_msg = "imported!";
    }
    $imp_remark = $array_result[$i]["station_01"];

    if($insert_yn == "y"){
        $data = array   (   "tag_table_usage"=>$fromtrans,
                            "uniquenum_pri"=>$uniquenum_pri,
                            "sys_link"=>$fromlink,
                            "userid_cookie"=>$userid,
                            "date_created"=>$date_created,
                            "date_lastupdate"=>TransactionGenUnique::generate_trans_datetime(),
                            "desc_lang01"=>$imp_firstname,
                            "desc_lang02"=>$imp_lastname,
                            "desc_lang04"=>$imp_gender,
                            "desc_lang08"=>$imp_province,
                            "desc_lang10"=>$imp_idcard,
                            "var_25_005"=>$imp_det,
                            "var_25_001"=>$imp_telephone,
                            "date_002"=>$imp_prisdate,
                            "var_25_002"=>$imp_laws,
                            "var_25_003"=>$imp_refdoc,
                            "var_25_004"=>$imp_stat,
                            "notes_memo"=>$imp_remark,
                            "var_50_001"=>$imp_title,
                            "var_50_002"=>$imp_passport,
                            "var_50_003"=>$imp_autonum
                        );
        $result = DatabaseOperation::insertRecordIntoTable($tablename,$data);
        SysAutoNum::setSysNum($fromtrans,$imp_autonum);
    }
}

class Import{
    public function getTitle($name){
        $title = "";
        if(strpos($name, 'นาย') !== false){
            $title = "mr";
        }else if(strpos($name, 'นาง') !== false){
            $title = "mrs";
        }else if(strpos($name, 'น.ส.') !== false){
            $title = "ms";
        }else if(strpos($name, 'ด.ช.') !== false){
            $title = "master";
        }else if(strpos($name, 'ด.ญ.') !== false){
            $title = "miss";
        }
        return $title;
    }

    public function getGender($title){
        if($title == "m" || $title == "master"){
            $gender = "m";
        }else{
            $gender = "f";
        }

        return $gender;
    }

    public function getPersonName($name){
        $title = Import::getTitle($name);
        $gender = Import::getGender($title);
        $array_title = array("นาย", "นาง", "น.ส.", "ด.ช.", "ด.ญ.");
        $new_name = str_replace($array_title, "", $name);
        $array_name = explode(" ", $new_name);
        $impris_name = array("title"=>$title,"fname"=>trim($array_name[0]),"lname"=>trim($array_name[1]),"gender"=>$gender);

        return $impris_name;
    }

    public function getPersonalID($passport){
        $array_passpost = explode("/", $passport);
        $impris_id = array("ic"=>trim($array_passpost[0]),"passport"=>trim($array_passpost[1]));

        return $impris_id;
    }

    public function getStation($station){
        $array_station = explode("#", $station);
        $impris_station = array("state"=>trim($array_station[0]),"station"=>trim($array_station[1]));

        return $impris_station;
    }

    public function getProvince($address){
        $province = str_replace("จ.", "", $address);

        return trim($province);
    }

    public function getDateConversion($date){
        $array_date = explode(" ", $date);
        $month_short = array('ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
        $month_number = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        if(trim(strlen($array_date[0])) == 1){
            $dd = "0".$array_date[0];
        }else{
            $dd = $array_date[0];
        }

        $mm = mb_substr(str_replace($month_short, $month_number, $array_date[1]), 0, 2);
        if(trim(substr($array_date[2], -4)) - 543 == -543){
            $yyyy = "1999";
        }else{
            $yyyy = trim(substr($array_date[2], -4)) - 543;
        }

        $new_date = $yyyy."-".$mm."-".$dd." 00:00:00";

        return $new_date;
    }

    public function chkExistRecord($string,$fromtrans){
        $query = "SELECT uniquenum_pri as setgen_uniq, setgen_code, desc_lang01 as setgen_desc
                    FROM sys_gencode_main
                    WHERE tag_table_usage = '".$fromtrans."'
                    AND tag_deleted_yn = 'n'
                    AND tag_active_yn = 'y'
                    AND lower(desc_lang01) = '".trim(strtolower($string))."'";
        $qs_result = DatabaseOperation::getRecordFromTable($query);
        while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $uniquenum_pri = $rows["setgen_uniq"];
        }

        return $uniquenum_pri;
    }

    public function getLastNumber($fromtrans){
        $query = "SELECT CONCAT(setgen_code,desc_lang03,desc_lang01,desc_lang02) as sys_dnum
                    FROM sys_gencode_main
                    WHERE tag_table_usage = '".$fromtrans."_sys_autonum'
                    AND tag_deleted_yn = 'n'
                    AND tag_active_yn = 'y'";
        $qs_result = DatabaseOperation::getRecordFromTable($query);
        while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $sys_dnum = $rows["sys_dnum"];
        }

        return $sys_dnum;
    }
}
?>
