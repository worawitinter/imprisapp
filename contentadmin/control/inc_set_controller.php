<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of inc_set_controller
 * To declare class object
 * @author Worawit
 */

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/FileUpload.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysAutoNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();
$GencodeSboxList = new GencodeSboxList();
$FileUpload = new FileUpload();
$AuditTrailLog = new AuditTrailLog();
$SysReport = new SysReport();
$SysListMain = new SysListMain();
$SysMainCookies = new SysMainCookies();
$SysAutoNum = new SysAutoNum();
$SysTransNum = new SysTransNum();
?>
