<?php
session_start();

unset($_SESSION["cookies_acpt_mth_amt"]);
unset($_SESSION["cookies_acpt_paymt_method"]);
unset($_SESSION["cookies_acpt_issue"]);
unset($_SESSION["cookies_acpt_recv"]);
unset($_SESSION["cookies_acpt_witness1"]);
unset($_SESSION["cookies_acpt_witness2"]);
unset($_SESSION["cookies_pymt_trnsprt"]);
unset($_SESSION["cookies_pymt_lodg"]);
unset($_SESSION["cookies_pymt_accmd"]);
unset($_SESSION["cookies_pymt_oths"]);
unset($_SESSION["cookies_pymt_issue"]);
unset($_SESSION["cookies_pymt_officer"]);
unset($_SESSION["cookies_pymt_witness1"]);
unset($_SESSION["cookies_pymt_witness2"]);

$_SESSION["cookies_acpt_mth_amt"] = $_POST["fmi_acpt_mth_amt"];
$_SESSION["cookies_acpt_paymt_method"] = $_POST["fmi_acpt_paymt_method"];
$_SESSION["cookies_acpt_issue"] = $_POST["fmi_acpt_issue"];
$_SESSION["cookies_acpt_recv"] = $_POST["fmi_acpt_recv"];
$_SESSION["cookies_acpt_witness1"] = $_POST["fmi_acpt_witness1"];
$_SESSION["cookies_acpt_witness2"] = $_POST["fmi_acpt_witness2"];
$_SESSION["cookies_pymt_trnsprt"] = $_POST["fmi_pymt_trnsprt"];
$_SESSION["cookies_pymt_lodg"] = $_POST["fmi_pymt_lodg"];
$_SESSION["cookies_pymt_accmd"] = $_POST["fmi_pymt_accmd"];
$_SESSION["cookies_pymt_oths"] = $_POST["fmi_pymt_oths"];
$_SESSION["cookies_pymt_issue"] = $_POST["fmi_pymt_issue"];
$_SESSION["cookies_pymt_officer"] = $_POST["fmi_pymt_officer"];
$_SESSION["cookies_pymt_witness1"] = $_POST["fmi_pymt_witness1"];
$_SESSION["cookies_pymt_witness2"] = $_POST["fmi_pymt_witness2"];
?>