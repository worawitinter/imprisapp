<!DOCTYPE html>
<!-- inc_parse_modal_header_desc.php -->
<script type="text/javascript">
    function parseModalHeaderDesc(){
        if(typeof toggleAuditLogs === 'function'){toggleAuditLogs();}
        $("#wrapper").toggleClass("toggled");
        var frommode = document.getElementById('fmi_frommode').value;
        if(frommode == 'new'){
            document.getElementById('header_label').innerHTML = 'เพิ่ม';
            document.getElementById('menu-toggle').style.display = "none";
        }else if(frommode == 'edit'){
            document.getElementById('header_label').innerHTML = 'แก้ไข';
            document.getElementById('menu-toggle').style.display = "";
        }else if(frommode == 'view'){
            document.getElementById('header_label').innerHTML = 'เรียกดู';
            document.getElementById('menu-toggle').style.display = "";
        }else if(frommode == 'search'){
            document.getElementById('header_label').innerHTML = 'ค้นหา';
            document.getElementById('menu-toggle').style.display = "none";
        }else if(frommode == 'auditlog'){
            document.getElementById('header_label').innerHTML = 'ประวัติการแก้ไขรายการ';
            document.getElementById('menu-toggle').style.display = "";
        }
    }
</script>
