<?php
@session_start();
ini_set('display_errors', 0);
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_language.php';
$url_file_ext = URL_FILE_EXT;

if(!strpos($_SERVER['REQUEST_URI'], 'login')){
    require_once DOCUMENT_ROOT . '/contentadmin/control/class/Authentication.php';
    require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
    $Authentication = new Authentication();
    $DatabaseOperation = new DatabaseOperation();
    $usr_uniquenum_pri = $_SESSION["cookies_usr_uniquenum_pri"];
    $tablename = "sys_user_main";
    $qs_login = $Authentication->verifyUser($tablename,$DatabaseOperation,$_SESSION["cookies_usr_uniquenum_pri"]);
    if($qs_login->rowCount() > 0){
        while($row=$qs_login->fetch(PDO::FETCH_OBJ)) {
            if(!strpos($_SERVER['REQUEST_URI'], 'welcome_screen') && (empty($_SESSION["cookies_log_uniquenum_pri"]) || $row->var_50_004 == 0)){
                echo "<script>alert('คุณถูกตัดออกจากระบบ! โปรดเข้าสู่ระบบใหม่อีกครั้ง')</script>";
                echo "<script>window.open('logout.php','_self');</script>";
            }
        }
    }

    if(time() - $_SESSION['login_session'] > SESSION_EXPIRY) { //subtract new timestamp from the old one
        echo "<script>alert('Session หมดอายุ! โปรดเข้าสู่ระบบใหม่อีกครั้ง')</script>";
        unset($_SESSION['login_session']);
        echo "<script>window.open('logout.php','_self');</script>";
    }else{
        require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';
        $SysMainCookies = new SysMainCookies();
        $SysMainCookies->setSysCookies($_SESSION['login_session'],$_SESSION["cookies_usr_uniquenum_pri"]);
        $_SESSION['login_session'] = time(); //set new timestamp
    }
}
?>
