<?php

/*
 * To parse label descript base on fromtrans values.
 */
$label_header = "";
$ext_label = "";
if(isset($_GET["fromtrans"])){
    if($_GET["fromlink"] == 'sys_autonum'){
        $fromtrans = substr($_GET["fromtrans"], 0, strpos($_GET["fromtrans"], '_sys_autonum'));
    }else{
        $fromtrans = $_GET["fromtrans"];
    }

    if($fromtrans == "pris_det"){
        $label_header = "สถานที่ควบคุมตัว";
    }else if($fromtrans == "reg_list"){
        $label_header = "ข้อหาที่ถูกจับกุม";
    }else if($fromtrans == "imp_stat"){
        $label_header = "สถานะผู้ตกทุกข์";
    }else if($fromtrans == "impris"){
        $label_header = "ลงทะเบียนผู้ตกทุกข์";
    }else if($fromtrans == "sys_user"){
        $label_header = "ผู้ใช้งานระบบ";
    }else if($fromtrans == "sys_user_dept"){
        $label_header = "สังกัด";
    }else if($fromtrans == "impr_doc"){
        $label_header = "จัดการเอกสาร";
    }else if($fromtrans == "impris_rep"){
        $label_header = "ออกรายงาน";
    }else if($fromtrans == "imp_cate"){
        $label_header = "ประเภทงาน";
    }else if($fromtrans == "pris_cate"){
        $label_header = "ประเภทสถานที่ควบคุมตัว";
    }else if($fromtrans == "reg_cate"){
        $label_header = "ประเภทข้อหาที่ถูกจับกุม";
    }else if($fromtrans == "imp_vist"){
        $label_header = "ประวัติการเยี่ยมผู้ตกทุกข์";
    }

    if($_GET["fromlink"] == 'sys_autonum'){
        $ext_label = "ตั้งค่าหมายเลขอัตโนมัติ";
    }
}
