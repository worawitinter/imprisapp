<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$SysConversion = new SysConversion();
$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();
$TransactionGenUnique = new TransactionGenUnique();

if(isset($_REQUEST["fromlink"])){$fromlink = $_REQUEST["fromlink"];}
if(isset($_REQUEST["tag_usage"])){$tag_usage = $_REQUEST["tag_usage"];}
if(isset($_REQUEST["uniquenum_pri"])){$uniquenum_pri = $_REQUEST["uniquenum_pri"];}
if(isset($_REQUEST["fld_name"])){$fld_name = $_REQUEST["fld_name"];}
if(isset($_REQUEST["datefrom"])){$datefrom = $SysConversion->convertDate($_REQUEST["datefrom"]);}
if(isset($_REQUEST["dateto"])){$dateto = $SysConversion->convertDate($_REQUEST["dateto"]);}
if(isset($_REQUEST["search_query"])){$search_query = $_REQUEST["search_query"];}
if(isset($_REQUEST["fromtrans"])){$fromtrans = $_REQUEST["fromtrans"];}
if(isset($_REQUEST["js_function"])){$js_function = $_REQUEST["js_function"];}
if(isset($_REQUEST["tablename"])){$tablename = $_REQUEST["tablename"];}
$ajax_text = "";
$ajax_array = array();
$ext_conditon = "";
if(isset($_REQUEST["imp_cate_uniq"]) && $_REQUEST["imp_cate_uniq"] != ""){
    $ext_conditon = " AND imp.var_100_003 = '".$_REQUEST["imp_cate_uniq"]."'";
}
switch ($tag_usage) {
    case 'autogen_upri':
        $uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
        $ajax_text = $uniquenum_pri;
        $return_result = $ajax_text;
    break;
    case 'sys_autonum':
        $fmi_autonum = "fmi_".$fld_name."_autonum";
        $js_function = 'chkExistField("'.$tablename.'",document.all.fmi_fromtrans.value,document.all.'.$fmi_autonum.'.value,"setgen_code")';
        $sql = "SELECT CONCAT(setgen_code,desc_lang03,desc_lang01,desc_lang02) as sys_autonum, uniquenum_pri"
                . " FROM sys_gencode_main"
                . " WHERE tag_table_usage = '".$fromlink."'"
                . " AND tag_deleted_yn = 'n'"
                . " AND tag_active_yn = 'y'"
                . " ORDER BY FIELD(tag_other01_yn, 'y') DESC";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        if($qs_result->rowCount() > 0){
            $ajax_text = "<select name='".$fmi_autonum."' id='".$fmi_autonum."' class='selectpicker'>";
            foreach($qs_result as $value) {
                $ajax_text .= "<option value='".$value["sys_autonum"]."@@@".$value["uniquenum_pri"]."'>".$value["sys_autonum"]."</option>";
            }
            $ajax_text .= "</select>";
        }else{
            $ajax_text .= "<input type='text' name='".$fmi_autonum."' id='".$fmi_autonum."' maxlength='50' class='form-control required' placeholder='รหัส' onblur='".$js_function."'/>";
        }

        $return_result = $ajax_text;
    break;
    case 'dshbrd_total_record':
        $sql = "SELECT IFNULL(COUNT(imp.idcode), 0) as total_record"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . $ext_conditon
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . " GROUP BY imp.tag_table_usage";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text .= $value["total_record"];
        }
        $return_result = $ajax_text;
    break;
    case 'dshbrd_total_today':
        $sql = "SELECT IFNULL(COUNT(imp.idcode), 0) as total_record"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND DATE_FORMAT(imp.date_created,'%Y-%m-%d') = '".$SysConversion->convertDateFormat($TransactionGenUnique->generate_trans_datetime(),("Y-m-d"))."'"
                . $ext_conditon
                . " GROUP BY imp.tag_table_usage";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text .= $value["total_record"];
        }
        $return_result = $ajax_text;
    break;
    case 'dshbrd_total_male':
        $sql = "SELECT IFNULL(COUNT(imp.idcode), 0) as total_record"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.desc_lang04 = 'm'"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY imp.desc_lang04";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text .= $value["total_record"];
        }
        $return_result = $ajax_text;
    break;
    case 'dshbrd_total_female':
        $sql = "SELECT IFNULL(COUNT(imp.idcode), 0) as total_record"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.desc_lang04 = 'f'"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY imp.desc_lang04";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text .= $value["total_record"];
        }
        $return_result = $ajax_text;
    break;
    case 'dshbrd_list_pris_det':
        $sql = "SELECT det.desc_lang01 as det_desc, count(imp.idcode) as total_pris"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " and sgm.tag_other01_yn = 'y'"
                . " LEFT OUTER JOIN sys_gencode_main as det"
                . " on det.uniquenum_pri = imp.var_25_005"
                   . " and det.tag_table_usage = 'pris_det'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY imp.var_25_005"
                . " ORDER BY count(imp.idcode) DESC limit 10";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['det_desc'] = $row['det_desc'];
            $row_array['total_pris'] = $row['total_pris'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_list_state':
        $sql = "SELECT det.var_100_001 as state_desc, count(imp.idcode) as total_pris"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " and sgm.tag_other01_yn = 'y'"
                . " LEFT OUTER JOIN sys_gencode_main as det"
                . " on det.uniquenum_pri = imp.var_25_005"
                   . " and det.tag_table_usage = 'pris_det'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND det.var_100_001 <> ''"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY det.var_100_001"
                . " ORDER BY count(imp.idcode) DESC limit 10";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['state_desc'] = $row['state_desc'];
            $row_array['total_pris'] = $row['total_pris'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_list_reg':
        $sql = "SELECT sgm.desc_lang01 as imp_cate, reg_cate.desc_lang01 as reg_cate, reg.desc_lang01 as reg_desc, count(imp.idcode) as total_pris"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " and sgm.tag_other01_yn = 'y'"
                . " LEFT OUTER JOIN sys_gencode_main as reg"
                . " on reg.uniquenum_pri = imp.var_25_002"
                . " and reg.tag_table_usage = 'reg_list'"
                . " LEFT OUTER JOIN sys_gencode_main as reg_cate"
                . " on reg_cate.uniquenum_pri = reg.var_25_002"
                . " and reg_cate.tag_table_usage = 'reg_cate'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.var_25_002 <> ''"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY reg.desc_lang01"
                . " ORDER BY sgm.desc_lang01, reg_cate.desc_lang01, count(imp.idcode) DESC limit 10";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['imp_cate_desc'] = $row['imp_cate'];
            $row_array['reg_cate_desc'] = $row['reg_cate'];
            $row_array['reg_desc'] = $row['reg_desc'];
            $row_array['total_pris'] = $row['total_pris'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_list_status':
        $sql = "SELECT sgm.desc_lang01 as imp_cate, stat.desc_lang01 as state_desc, count(imp.idcode) as total_pris"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " RIGHT OUTER JOIN sys_gencode_main as stat"
                . " on stat.uniquenum_pri = imp.var_25_004"
                   . " and stat.tag_table_usage = 'imp_stat'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' "
                . $ext_conditon
                . " GROUP BY imp.var_25_004"
                . " ORDER BY sgm.desc_lang01, count(imp.idcode) DESC limit 10";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['imp_cate_desc'] = $row['imp_cate'];
            $row_array['state_desc'] = $row['state_desc'];
            $row_array['total_pris'] = $row['total_pris'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_chart_age_group':
        $sql = "SELECT SUM(IF(imp.desc_lang03 < 20,1,0)) as '< 20',
                    SUM(IF(imp.desc_lang03 BETWEEN 20 and 29,1,0)) as '20 - 29',
                    SUM(IF(imp.desc_lang03 BETWEEN 30 and 39,1,0)) as '30 - 39',
                    SUM(IF(imp.desc_lang03 BETWEEN 40 and 49,1,0)) as '40 - 49',
                    SUM(IF(imp.desc_lang03 BETWEEN 50 and 59,1,0)) as '50 - 59',
                    SUM(IF(imp.desc_lang03 BETWEEN 60 and 69,1,0)) as '60 - 69',
                    SUM(IF(imp.desc_lang03 BETWEEN 70 and 79,1,0)) as '70 - 79',
                    SUM(IF(imp.desc_lang03 >=80, 1, 0)) as '> 80',
                    SUM(IF(imp.desc_lang03 is null, 1, 0)) as 'na'"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . $ext_conditon
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' ";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['below_20'] = $row['< 20'];
            $row_array['_20_29'] = $row['20 - 29'];
            $row_array['_30_39'] = $row['30 - 39'];
            $row_array['_40_49'] = $row['40 - 49'];
            $row_array['_50_59'] = $row['50 - 59'];
            $row_array['_60_69'] = $row['60 - 69'];
            $row_array['_70_79'] = $row['70 - 79'];
            $row_array['above_80'] = $row['> 80'];
            $row_array['na'] = $row['na'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_chart_prist_date_monthly':
        $sql = "SELECT imp.desc_lang04 as gender,
                    SUM(IF(MONTH(imp.date_002) = 01,1,0)) as 'jan',
                    SUM(IF(MONTH(imp.date_002) = 02,1,0)) as 'feb',
                    SUM(IF(MONTH(imp.date_002) = 03,1,0)) as 'mar',
                    SUM(IF(MONTH(imp.date_002) = 04,1,0)) as 'apr',
                    SUM(IF(MONTH(imp.date_002) = 05,1,0)) as 'may',
                    SUM(IF(MONTH(imp.date_002) = 06,1,0)) as 'jun',
                    SUM(IF(MONTH(imp.date_002) = 07,1,0)) as 'jul',
                    SUM(IF(MONTH(imp.date_002) = 08,1,0)) as 'aug',
                    SUM(IF(MONTH(imp.date_002) = 09,1,0)) as 'sep',
                    SUM(IF(MONTH(imp.date_002) = 10,1,0)) as 'oct',
                    SUM(IF(MONTH(imp.date_002) = 11,1,0)) as 'nov',
                    SUM(IF(MONTH(imp.date_002) = 12,1,0)) as 'dec',
                    SUM(IF(MONTH(imp.date_002) = 00,1,0)) as 'na'"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.desc_lang04 = 'f'"
                . $ext_conditon
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' ";
        $sql .= " UNION ALL ";
        $sql .= "SELECT imp.desc_lang04 as gender,
                    SUM(IF(MONTH(imp.date_002) = 01,1,0)) as 'jan',
                    SUM(IF(MONTH(imp.date_002) = 02,1,0)) as 'feb',
                    SUM(IF(MONTH(imp.date_002) = 03,1,0)) as 'mar',
                    SUM(IF(MONTH(imp.date_002) = 04,1,0)) as 'apr',
                    SUM(IF(MONTH(imp.date_002) = 05,1,0)) as 'may',
                    SUM(IF(MONTH(imp.date_002) = 06,1,0)) as 'jun',
                    SUM(IF(MONTH(imp.date_002) = 07,1,0)) as 'jul',
                    SUM(IF(MONTH(imp.date_002) = 08,1,0)) as 'aug',
                    SUM(IF(MONTH(imp.date_002) = 09,1,0)) as 'sep',
                    SUM(IF(MONTH(imp.date_002) = 10,1,0)) as 'oct',
                    SUM(IF(MONTH(imp.date_002) = 11,1,0)) as 'nov',
                    SUM(IF(MONTH(imp.date_002) = 12,1,0)) as 'dec',
                    SUM(IF(MONTH(imp.date_002) = 00,1,0)) as 'na'"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . " AND imp.desc_lang04 = 'm'"
                . $ext_conditon
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' ";
        $sql .= " UNION ALL ";
        $sql .= "SELECT 't' as gender,
                    SUM(IF(MONTH(imp.date_002) = 01,1,0)) as 'jan',
                    SUM(IF(MONTH(imp.date_002) = 02,1,0)) as 'feb',
                    SUM(IF(MONTH(imp.date_002) = 03,1,0)) as 'mar',
                    SUM(IF(MONTH(imp.date_002) = 04,1,0)) as 'apr',
                    SUM(IF(MONTH(imp.date_002) = 05,1,0)) as 'may',
                    SUM(IF(MONTH(imp.date_002) = 06,1,0)) as 'jun',
                    SUM(IF(MONTH(imp.date_002) = 07,1,0)) as 'jul',
                    SUM(IF(MONTH(imp.date_002) = 08,1,0)) as 'aug',
                    SUM(IF(MONTH(imp.date_002) = 09,1,0)) as 'sep',
                    SUM(IF(MONTH(imp.date_002) = 10,1,0)) as 'oct',
                    SUM(IF(MONTH(imp.date_002) = 11,1,0)) as 'nov',
                    SUM(IF(MONTH(imp.date_002) = 12,1,0)) as 'dec',
                    SUM(IF(MONTH(imp.date_002) = 00,1,0)) as 'na'"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " WHERE imp.tag_table_usage = 'impris'"
                . " AND imp.tag_deleted_yn = 'n'"
                . $ext_conditon
                . " AND imp.date_002 between '".$datefrom."' and '".$dateto."' ";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['gender'] = $row['gender'];
            $row_array['jan'] = $row['jan'];
            $row_array['feb'] = $row['feb'];
            $row_array['mar'] = $row['mar'];
            $row_array['apr'] = $row['apr'];
            $row_array['may'] = $row['may'];
            $row_array['jun'] = $row['jun'];
            $row_array['jul'] = $row['jul'];
            $row_array['aug'] = $row['aug'];
            $row_array['sep'] = $row['sep'];
            $row_array['oct'] = $row['oct'];
            $row_array['nov'] = $row['nov'];
            $row_array['dec'] = $row['dec'];
            $row_array['na'] = $row['na'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'dshbrd_chart_prist_state':
        $state_str = file_get_contents("../../folder_script/countries/countries/malaysia.json");
        $json_state = json_decode($state_str, true);
        $json_state1 = json_decode($state_str, true);
        $sql = "SELECT";
                foreach ($json_state as $state_name => $json_state) {
                 $sql .= " '".$json_state['name']."' as 'fld_".$json_state['code']."',";
                 $sql .= " SUM(IF(det.var_25_001 = '".$json_state['code']."',1,0)) as '".$json_state['code']."',";
                }
                $sql .= " imp.desc_lang04 as gender"
                . " FROM tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " left outer join sys_gencode_main as det
                    	on det.uniquenum_pri = imp.var_25_005
                    		and det.tag_table_usage = 'pris_det'
                            and det.tag_deleted_yn = 'n'
                            and det.tag_active_yn = 'y'
                    where imp.tag_table_usage = 'impris'
                    and imp.tag_deleted_yn = 'n'
                    and det.var_100_001 <> ''";
                $sql .= $ext_conditon;
                $sql .= " AND imp.date_002 between '".$datefrom."' and '".$dateto."'";
                $sql .= " GROUP BY imp.desc_lang04";
        $sql .= " UNION ALL ";
        $sql .= "SELECT";
                foreach ($json_state1 as $state_name => $json_state1) {
                 $sql .= " '".$json_state1['name']."' as 'fld_".$json_state1['code']."',";
                 $sql .= " SUM(IF(det.var_25_001 = '".$json_state1['code']."',1,0)) as '".$json_state1['code']."',";
                }
                $sql .= " 't' as gender";
                $sql .= " from tran_record_main as imp"
                . " INNER JOIN sys_gencode_main as sgm"
                . " on sgm.uniquenum_pri = imp.var_100_003"
                . " and sgm.tag_table_usage = 'imp_cate'"
                . " and sgm.tag_deleted_yn = 'n'"
                . " and sgm.tag_active_yn = 'y'"
                . " left outer join sys_gencode_main as det
                    	on det.uniquenum_pri = imp.var_25_005
                    		and det.tag_table_usage = 'pris_det'
                            and det.tag_deleted_yn = 'n'
                            and det.tag_active_yn = 'y'
                    where imp.tag_table_usage = 'impris'
                    and imp.tag_deleted_yn = 'n'
                    and det.var_100_001 <> ''";
                $sql .= $ext_conditon;
                $sql .= " AND imp.date_002 between '".$datefrom."' and '".$dateto."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['gender'] = $row['gender'];
            foreach($row as $key => $value)
            {
                $new_key = str_replace("-","_",$key);
                $row_array[$new_key] = $row[$key];
            }
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'imp_attchmt':
        $init_prev = array();
        $init_prev_conf = array();
        $str_init_prev = "";
        $str_init_prev_conf = "";
        $path = SERVER_URL_ROOT . '/folder_upload/impr_doc/';
        $sql = "SELECT CONCAT('".$path."', uniquenum_pri, '/', notes_memo) as file_url,   notes_memo as file_name,"
                . " uniquenum_pri,      userid_cookie,      date_created,    var_25_002 as file_size,   var_25_003 as file_ext"
                . " FROM ".$tablename
                . " WHERE tag_table_usage = '".$fromtrans."'"
                . " AND tag_deleted_yn = 'n'"
                . " AND var_25_001 = 'imp_attchmt'"
                . " AND uniquenum_sec = '".$uniquenum_pri."'"
                . " ORDER BY idcode";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);

        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            if(trim($row['file_ext']) == 'pdf'){
                $type = 'pdf';
                $download_url = "false";
            }elseif(trim($row['file_ext']) == 'txt'){
                $type = 'text';
                $download_url = "true";
            }elseif(trim($row['file_ext']) == 'jpg' || trim($row['file_ext']) == 'jpeg' || trim($row['file_ext']) == 'gif' || trim($row['file_ext']) == 'png'){
                $type = 'image';
                $download_url = "true";
            }else{
                $type = 'office';
                $download_url = "true";
            }
            $str_init_prev_conf .= "{'type':'".$type."','caption':'".$row['file_name']."','size':'".$row['file_size']."','url':'imp_attchmt_oup.php?delete_file=y&del_uniquenum_pri=".$row['uniquenum_pri']."&tablename=".$tablename."','key':'".$row['uniquenum_pri']."','downloadUrl':'".$download_url."'},";
            $str_init_prev .= "'".$row['file_url']."',";

        }
        $row_array1['init_prev_conf'] = substr($str_init_prev_conf,0,strlen($str_init_prev_conf)-1);
        $row_array2['init_prev'] = substr($str_init_prev,0,strlen($str_init_prev)-1);
        $str_resutl = substr($str_init_prev,0,strlen($str_init_prev)-1)."@@@".substr($str_init_prev_conf,0,strlen($str_init_prev_conf)-1);
        $return_result = $str_resutl;
    break;
    case 'imp_exist':
        $sql = "SELECT uniquenum_pri"
                . " FROM tran_record_main"
                . " WHERE tag_table_usage = 'impris'"
                . " AND tag_deleted_yn = 'n'"
                . " AND (desc_lang10 = '".$search_query."'"
                . " OR var_50_002 = '".$search_query."'"
                . " OR var_100_001 = '".$search_query."'"
                . " OR var_100_002 = '".$search_query."')";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text = $value["uniquenum_pri"];
        }
        $return_result = $ajax_text;
    break;
    case 'gencode_exist':
        $sql = "SELECT uniquenum_pri"
                . " FROM ".$tablename
                . " WHERE tag_table_usage = '".$fromtrans."'"
                . " AND tag_deleted_yn = 'n'"
                . " AND ".$fld_name." = '".$search_query."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text = $value["uniquenum_pri"];
        }
        $return_result = $ajax_text;
    break;
    case 'imp_refdoc':
        $path = SERVER_URL_ROOT . '/folder_upload/impr_doc/';
        $sql = "SELECT uniquenum_pri,       setgen_code,     desc_lang01 as setgen_desc,     var_100_002 as file_name,    notes_memo"
                . " FROM sys_gencode_main"
                . " WHERE tag_table_usage = 'impr_doc'"
                . " AND tag_deleted_yn = 'n'"
                . " AND var_100_002 <> ''"
                . " AND uniquenum_pri = '".$uniquenum_pri."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        foreach($qs_result as $value) {
            $ajax_text = $value["setgen_code"]."@@@".$value["setgen_desc"]."@@@".$path.$value["uniquenum_pri"]."/".$value["file_name"]."@@@".$value["notes_memo"]."@@@".$value["file_name"];
        }
        $return_result = $ajax_text;
    break;
    case 'chk_exist_fld':
        $sql = "SELECT idcode"
                . " FROM ".$tablename
                . " WHERE tag_table_usage = '".$fromtrans."'"
                . " AND tag_deleted_yn = 'n'"
                . " AND ".$fld_name." = '".$search_query."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        $return_result = $qs_result->rowCount();
    break;
    case 'chk_lockrecord':
        $qs_result = $SysMainActivity->checkRecord($fromtrans,$uniquenum_pri,'');
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $ajax_text = $row["userid_cookie"]."@@@".$row["date_lastupdate"];
        }
        $return_result = $ajax_text;
    break;
    case 'unlockrecord':
        $qs_result = $SysMainActivity->releaseRecord($fromtrans,$uniquenum_pri);
        $return_result = $qs_result->rowCount();
    break;
    case 'lockrecord':
        $qs_result = $SysMainActivity->lockRecord($fromlink,$fromtrans,$uniquenum_pri,$fld_name);
        $return_result = $qs_result->rowCount();
    break;
    case 'sbox_list':
        $sql = "SELECT uniquenum_pri as list_uniq,   setgen_code as list_code,     desc_lang01 as list_desc"
            . " FROM    sys_gencode_main"
            . " WHERE   tag_table_usage = '".$fromtrans."'"
            . " AND tag_deleted_yn = 'n'"
            . " AND tag_active_yn = 'y'"
            . " AND ".$fld_name." = '".$uniquenum_pri."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $row_array['list_uniq'] = $row['list_uniq'];
            $row_array['list_desc'] = $row['list_desc'];
            array_push($ajax_array,$row_array);
        }
        $return_result = json_encode($ajax_array);
    break;
    case 'chk_imps':
        $sql = "SELECT tag_other01_yn as chk_imps"
            . " FROM    sys_gencode_main"
            . " WHERE   tag_table_usage = 'imp_cate'"
            . " AND tag_deleted_yn = 'n'"
            . " AND tag_active_yn = 'y'"
            . " AND uniquenum_pri = '".$uniquenum_pri."'";
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $ajax_text = $row['chk_imps'];
        }
        $return_result = $ajax_text;
    break;
    default:
    # code...
    break;
}
echo $return_result;
 ?>
