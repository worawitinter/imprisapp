<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$SysConversion = new SysConversion();
$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();
$TransactionGenUnique = new TransactionGenUnique();

if(isset($_REQUEST["fromlink"])){$fromlink = $_REQUEST["fromlink"];}
if(isset($_REQUEST["tag_usage"])){$tag_usage = $_REQUEST["tag_usage"];}
if(isset($_REQUEST["uniquenum_pri"])){$uniquenum_pri = $_REQUEST["uniquenum_pri"];}
if(isset($_REQUEST["fld_name"])){$fld_name = $_REQUEST["fld_name"];}
if(isset($_REQUEST["datefrom"])){$datefrom = $SysConversion->convertDate($_REQUEST["datefrom"]);}
if(isset($_REQUEST["dateto"])){$dateto = $SysConversion->convertDate($_REQUEST["dateto"]);}
if(isset($_REQUEST["search_query"])){$search_query = $_REQUEST["search_query"];}
if(isset($_REQUEST["fromtrans"])){$fromtrans = $_REQUEST["fromtrans"];}
if(isset($_REQUEST["js_function"])){$js_function = $_REQUEST["js_function"];}
if(isset($_REQUEST["tablename"])){$tablename = $_REQUEST["tablename"];}

switch ($tag_usage) {
    case 'chk_lockrecord':
        $qs_result = $SysMainActivity->checkRecord($fromtrans,$uniquenum_pri,'');
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $lock_msg = "รายการนี้ถูกล็อคโดย ".$row["userid_cookie"]." เมื่อวันที่/เวลา ".$row["date_lastupdate"]." โปรดดำเนินการปลดล็อค";
            $return_result = "<script type='text/javascript'>";
            if($fld_name != $row["userid_cookie"]){
                $return_result .= "if($('#chk_lock_record').val() != 'y'){";
                $return_result .= "$('#div_lock_msg').show();$('#lock_msg').html('".$lock_msg."');";
                $return_result .= "Push.create('ระบบแจ้งเตือน', {";
                $return_result .= "    body: '".$lock_msg."',";
                $return_result .= "    icon: '../../folder_script/".$notif_logo."',";
                $return_result .= "    timeout: 9000,";
                $return_result .= "    onClick: function () {";
                $return_result .= "        window.focus();";
                $return_result .= "        this.close();";
                $return_result .= "    }";
                $return_result .= "});}";
                $return_result .= "$('#chk_lock_record').val('y');";
                $return_result .= "$('#frm_unlock').show();";
                $return_result .= "$('#frm_lock').hide();";
                $return_result .= "$('#frm_submit').prop('disabled', true);";
                $return_result .= "$('#frm_cancel').prop('disabled', true);";
            }else{
                $return_result .= "$('#frm_unlock').hide();";
                $return_result .= "$('#frm_lock').show();";
                $return_result .= "$('#frm_submit').attr('disabled',false)";
                $return_result .= "$('#frm_cancel').attr('disabled',false)";
                $return_result .= "$('#chk_lock_record').val('n');";
            }
            $return_result .= "</script>";
        }
    break;
    default:
    # code...
    break;
}
if(isset($return_result)){
    echo $return_result;
}
?>
