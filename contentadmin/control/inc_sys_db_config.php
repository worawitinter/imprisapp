<?php
/*
 * To configure database table operation base on fromtrans values.
 */
$tablename = "sys_gencode_main";
$tablename_data = "sys_gencode_data";
if(isset($_REQUEST["fromtrans"])){
    if($_REQUEST["fromlink"] == 'sys_autonum'){
        $fromtrans = substr($_REQUEST["fromtrans"], 0, strpos($_REQUEST["fromtrans"], '_sys_autonum'));
    }else{
        $fromtrans = $_REQUEST["fromtrans"];
    }
    if($fromtrans == "pris_det" || $fromtrans == "reg_list" || $fromtrans == "imp_stat" || $fromtrans == "sys_user_dept" || $fromtrans == "impr_doc" || $fromtrans == "imp_cate" || $fromtrans == "pris_cate" || $fromtrans == "reg_cate"){
        $tablename = "sys_gencode_main";
        $tablename_data = "sys_gencode_data";
    }else if($fromtrans == "impris" || $fromtrans == "impris_rep"){
        $tablename = "tran_record_main";
        $tablename_data = "tran_record_data";
    }else if($fromtrans == "sys_user"){
        $tablename = "sys_user_main";
        $tablename_data = "sys_user_data";
    }
}
 ?>
