<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
$SysListMain = new SysListMain();

$fromlink = $_GET["fromlink"];
$tag_usage = $_GET["tag_usage"];
$uniquenum_pri = $_GET["uniquenum_pri"];
$tag_audit_yn = $_GET["tag_audit_yn"];
$page_start = 0;
$limit = 0;

switch ($fromlink) {
    case 'adm_gencode':
        $qs_result = $SysListMain->getRowGencodeResult($tag_usage,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn);
        break;
    case 'imp':
        $qs_result = $SysListMain->getRowImprisResult($tag_usage,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn);
        break;
    case 'usr':
        $qs_result = $SysListMain->getRowUserResult($tag_usage,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn);
        break;
    case 'sys_autonum':
        $qs_result = $SysListMain->getRowSysAutoNumResult($tag_usage,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn);
        break;
  default:
    break;
}
    if(isset($qs_result)){
        $array_data = $qs_result->fetchAll();
        echo json_encode($array_data);
    }
?>
