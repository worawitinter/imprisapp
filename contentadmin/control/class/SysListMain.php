<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysListMain
 *
 * @author Worawit
 */

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';

class SysListMain {
    public function getRowGencodeResult($fromtrans,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn){
        $query = "SELECT sgm.uniquenum_pri as gencode_uniq,     sgm.setgen_code as gencode_code,        sgm.desc_lang01 as gencode_desc, "
                . "sgm.uniquenum_pri,                           sgm.userid_cookie,                      sgm.date_lastupdate,"
                . "sgm.notes_memo as gencode_remark,            sgm.date_created";
                if($fromtrans == 'pris_det'){
                    $query = $query . ", sgm.var_25_001 as state_code, sgm.var_100_001 as state_desc, sgm.var_25_003 as pris_cate_uniq";
                }else{
                    $query = $query . ", '' as state_code, '' as state_desc, '' as pris_cate_uniq";
                }
                if($fromtrans == 'impr_doc'){
                    $query = $query . ", sgm.var_100_002 as file_name";
                }else{
                    $query = $query . ", '' as file_name";
                }
                if($fromtrans == 'imp_cate'){
                    $query = $query . ", sgm.tag_other01_yn as chk_imps";
                }else{
                    $query = $query . ", 'n' as chk_imps";
                }
                if($fromtrans == 'reg_list'){
                    $query = $query . ", sgm.var_25_002 as reg_cate_uniq";
                }else{
                    $query = $query . ", '' as reg_cate_uniq";
                }
                if($fromtrans == 'imp_vist'){
                    $query = $query . ", sgm.num_02_d_001 as imp_tot_male,  sgm.num_02_d_002 as imp_tot_female, sgm.date_001 as imp_vist_date,        sgm.var_25_005 as imp_det_uniq,   det.desc_lang01 as imp_det_desc";
                }else{
                    $query = $query . ", '0.00' as imp_tot_male,  '0.00' as imp_tot_female, '0000-00-00' as imp_vist_date,        '' as imp_det_uniq,   '' as imp_det_desc";
                }
                if($fromtrans == 'imp_stat'){
                    $query = $query . ", sgm.var_25_004 as imp_cate_uniq";
                }else{
                    $query = $query . ", '' as imp_cate_uniq";
                }
                $query = $query . " FROM sys_gencode_main as sgm";
                if($fromtrans == 'imp_vist'){
                    $query = $query . " LEFT OUTER JOIN sys_gencode_main as det"
                                . " on det.uniquenum_pri = sgm.var_25_005"
                                   . " and det.tag_table_usage = 'pris_det'";
                }
                $query = $query . " WHERE sgm.tag_table_usage = '".$fromtrans."' ";
                if(isset($uniquenum_pri) && $uniquenum_pri != ''){
                    $query = $query . " AND sgm.uniquenum_pri = '".$uniquenum_pri."'";
                }
                if(isset($search_query) && $search_query != ''){
                    $query = $query . " AND (lower(sgm.setgen_code) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(sgm.desc_lang01) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(sgm.notes_memo) like '%".trim(strtolower($search_query))."%')";
                }
                $query = $query . " AND sgm.tag_deleted_yn = '".$tag_audit_yn."'";
                if($tag_audit_yn == "y"){
                    $query = $query . " AND sgm.tag_active_yn = 'n'";
                }else{
                    $query = $query . " AND sgm.tag_active_yn = 'y'";
                }
                $query = $query . " ORDER BY sgm.date_created DESC";
                if(isset($page_start) && $limit > 0){
                    $query = $query . " LIMIT ".$page_start.",".$limit;
                }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getRowImprisResult($fromtrans,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn){
        $query = "SELECT imp.uniquenum_pri,         imp.desc_lang01 as imp_firstname,   imp.desc_lang02 as imp_lastname,"
                . "imp.date_001 as imp_birthdate,   imp.desc_lang03 as imp_age,         imp.desc_lang04 as imp_gender,"
                . "imp.desc_lang05 as imp_addrr,    imp.desc_lang06 as imp_subdistrict, imp.desc_lang07 as imp_district,"
                . "imp.desc_lang08 as imp_province, imp.desc_lang09 as imp_zipcode,     imp.var_25_001 as imp_telephone,"
                . "imp.date_002 as imp_prisdate,    imp.var_25_002 as imp_reg_uniq,    laws.desc_lang01 as imp_reg_desc,"
                . "doc.setgen_code as imp_refdoc,    imp.desc_lang10 as imp_idcard,       imp.notes_memo as imp_remark,"
                . "imp.var_25_004 as imp_stat_uniq, stat.desc_lang01 as imp_stat_desc,  imp.var_50_002 as imp_passport,"
                . "imp.var_25_005 as imp_det_uniq,  det.desc_lang01 as imp_det_desc,    imp.date_created as imp_date_created,               imp_cate.desc_lang01 as imp_cate_desc,  det.var_100_001 as state_desc,                      imp.tag_other01_yn as chk_drf,"
                . "CASE WHEN imp.var_50_001 = 'mr' THEN 'นาย' "
                . "WHEN imp.var_50_001 = 'ms' THEN 'นางสาว' "
                . "WHEN imp.var_50_001 = 'mrs' THEN 'นาง' "
                . "WHEN imp.var_50_001 = 'master' THEN 'เด็กชาย' "
                . "WHEN imp.var_50_001 = 'miss' THEN 'เด็กหญิง' "
                . "ELSE '' END as imp_title,        "
                . "imp.var_50_001 as imp_title_2,"
                . "imp.var_50_003 as sys_autonum,   imp.date_created,"
                . "imp.var_50_004 as imp_firstname_2,   imp.var_50_005 as imp_lastname_2,   imp.var_100_001 as imp_temp_passport,"
                . "imp.var_100_002 as imp_ci,           imp.var_100_003 as imp_cate_uniq,   doc.setgen_code as imp_refdoc_desc, doc.uniquenum_pri as imp_refdoc_uniq,"
                . "imp.var_100_004 as imp_bdr_pass,     imp.date_003 as imp_cshbrw_date,    imp.num_02_d_001 as imp_cshbrw_amt,"
                . "imp.tag_other01_yn as imp_cshbrw_stat,   imp.var_100_005 as imp_cshbrw_ref,"
                . "pris_cate.desc_lang01 as pris_cate_desc, pris_cate.uniquenum_pri as pris_cate_uniq,"
                . "laws_cate.desc_lang01 as laws_cate_desc, laws_cate.uniquenum_pri as laws_cate_uniq,"
                . "imp_dept.desc_lang01 as imp_dept_desc, imp_dept.uniquenum_pri as imp_dept_uniq";
                $query = $query . " FROM tran_record_main as imp"
                        . " LEFT OUTER JOIN sys_gencode_main as laws"
                            . " on laws.uniquenum_pri = imp.var_25_002"
                               . " and laws.tag_table_usage = 'reg_list'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as laws_cate "
                        . " on laws.var_25_002 = laws_cate.uniquenum_pri "
                        . " and laws_cate.tag_table_usage = 'reg_cate'";
               $query = $query . " LEFT OUTER JOIN sys_gencode_main as imp_cate"
                           . " on imp_cate.uniquenum_pri = imp.var_100_003"
                              . " and imp_cate.tag_table_usage = 'imp_cate'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as stat "
                            . " on stat.uniquenum_pri = imp.var_25_004"
                               . " and stat.tag_table_usage = 'imp_stat'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as det"
                            . " on det.uniquenum_pri = imp.var_25_005"
                               . " and det.tag_table_usage = 'pris_det'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as pris_cate"
                                . " on det.var_25_003 = pris_cate.uniquenum_pri"
                                . " and pris_cate.tag_table_usage = 'pris_cate'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as imp_dept"
                                . " on imp.var_25_006 = imp_dept.uniquenum_pri"
                                . " and imp_dept.tag_table_usage = 'sys_user_dept'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as doc"
                           . " on doc.uniquenum_pri = imp.var_25_003"
                              . " and doc.tag_table_usage = 'impr_doc'";
                $query = $query . " WHERE imp.tag_table_usage = '".$fromtrans."' ";
                if(isset($uniquenum_pri) && $uniquenum_pri != ''){
                    $query = $query . " AND imp.uniquenum_pri = '".$uniquenum_pri."'";
                }
                if(isset($search_query) && $search_query != ''){
                    $query = $query . " AND (lower(imp.desc_lang01) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang02) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_25_003) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.notes_memo) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang05) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang06) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang07) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang08) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang09) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_50_002) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_50_003) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_50_004) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_50_005) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_100_001) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(imp.var_100_002) like '%".trim(strtolower($search_query))."%')";
                }
                $query = $query . " AND imp.tag_deleted_yn = '".$tag_audit_yn."'";
                $query = $query . " ORDER BY imp.date_created DESC";
                if(isset($page_start) && $limit > 0){
                    $query = $query . " LIMIT ".$page_start.",".$limit;
                }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        return $qs_result;
    }

    public function getRowUserResult($fromtrans,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn){
        $query = "SELECT usr_main.uniquenum_pri,        usr_main.username as usr_username,          usr_main.password as usr_password,"
                . "usr_main.sys_role as usr_role,       usr_main.sys_acc_right as usr_right,        usr_main.date_002 as usr_enddate,"
                . "CASE WHEN usr_data.usr_title = 'mr' THEN 'นาย' "
                . "WHEN usr_data.usr_title = 'ms' THEN 'นางสาว' "
                . "WHEN usr_data.usr_title = 'mrs' THEN 'นาง' "
                . "WHEN usr_data.usr_title = 'master' THEN 'เด็กชาย' "
                . "WHEN usr_data.usr_title = 'miss' THEN 'เด็กหญิง' "
                . "ELSE '' END as usr_title,                  usr_data.usr_firstname,                     usr_data.usr_lastname,"
                . "usr_data.var_25_001 as usr_dept_uniq,    dept.desc_lang01 as usr_dept_desc,      usr_data.usr_tel_01 as usr_contact,"
                . "usr_data.usr_email_01 as usr_email,      usr_data.notes_memo as usr_remark,      usr_main.var_50_003 as sys_autonum,"
                . "usr_main.date_created,               usr_main.date_001 as user_last_access,      usr_main.var_50_004 as login_session";
                $query = $query . " FROM sys_user_main as usr_main"
                        . " INNER JOIN sys_user_data as usr_data"
                            . " on usr_data.uniquenum_pri = usr_main.uniquenum_pri"
                            . " and usr_main.uniquenum_pri <> 'sysupri'";
                $query = $query . " LEFT OUTER JOIN sys_gencode_main as dept "
                            . " on dept.uniquenum_pri = usr_data.var_25_001"
                               . " and dept.tag_table_usage = 'sys_user_dept'";
                $query = $query . " WHERE usr_main.tag_table_usage = '".$fromtrans."' ";
                if(isset($uniquenum_pri) && $uniquenum_pri != ''){
                    $query = $query . " AND usr_main.uniquenum_pri = '".$uniquenum_pri."'";
                }
                if(isset($search_query) && $search_query != ''){
                    $query = $query . " AND (lower(usr_main.username) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(usr_data.usr_firstname) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(usr_data.usr_lastname) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(usr_data.usr_tel_01) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(usr_data.usr_email_01) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(usr_data.notes_memo) like '%".trim(strtolower($search_query))."%')";
                }
                $query = $query . " AND usr_main.tag_deleted_yn = '".$tag_audit_yn."'";
                if($tag_audit_yn == "y"){
                    $query = $query . " AND usr_main.tag_active_yn = 'n'";
                }else{
                    $query = $query . " AND usr_main.tag_active_yn = 'y'";
                }
                $query = $query . " ORDER BY usr_main.date_created DESC";
                if(isset($page_start) && $limit > 0){
                    $query = $query . " LIMIT ".$page_start.",".$limit;
                }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getRowSysAutoNumResult($fromtrans,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn){
        $query = "SELECT uniquenum_pri,             setgen_code as prefix_code,              desc_lang01 as run_number, "
                . "tag_other02_yn as chk_mth_year,          userid_cookie,                      date_lastupdate,"
                . "desc_lang02 as suffix_code,                   tag_active_yn as chk_active,        tag_other01_yn as chk_default,"
                . "desc_lang03 as mth_year,         date_created";
                $query = $query . " FROM sys_gencode_main "
                . " WHERE tag_table_usage = '".$fromtrans."' ";
                if(isset($uniquenum_pri) && $uniquenum_pri != ''){
                    $query = $query . " AND uniquenum_pri = '".$uniquenum_pri."'";
                }
                if(isset($search_query) && $search_query != ''){
                    $query = $query . " AND (lower(setgen_code) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(desc_lang01) like '%".trim(strtolower($search_query))."%'";
                    $query = $query . " OR lower(desc_lang02) like '%".trim(strtolower($search_query))."%')";
                }
                $query = $query . " AND tag_deleted_yn = '".$tag_audit_yn."'";
                $query = $query . " ORDER BY date_created DESC";
                if(isset($page_start) && $limit > 0){
                    $query = $query . " LIMIT ".$page_start.",".$limit;
                }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getRowRemarkResult($fromtrans,$uniquenum_pri){
        $query = "SELECT tran.uniquenum_sec,   tran.userid_cookie,              usr_data.usr_firstname,"
                . "usr_data.usr_lastname,          tran.date_created,               tran.notes_memo"
                . " FROM tran_record_data as tran"
                . " LEFT OUTER JOIN sys_user_main as usr_main"
                . " ON usr_main.username = tran.userid_cookie"
                . " INNER JOIN sys_user_data as usr_data"
                . " ON usr_data.uniquenum_pri = usr_main.uniquenum_pri"
                . " WHERE tran.tag_table_usage = '".$fromtrans."'";
                $query = $query . " AND tran.var_25_001 = 'remark_list'";
                $query = $query . " AND tran.uniquenum_sec = '".$uniquenum_pri."'";
                $query = $query . " AND tran.tag_deleted_yn = 'n'";
                $query = $query . " ORDER BY date_created DESC";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getRowAuditLogResult($fromtrans,$uniquenum_pri){
        $query = "SELECT tran.uniquenum_pri,    tran.userid_cookie,     usr_data.usr_firstname,"
                . "usr_data.usr_lastname,                   tran.date_created,      tran.notes_memo"
                . " FROM sys_activity_main as tran"
                . " LEFT OUTER JOIN sys_user_main as usr_main"
                . " ON usr_main.username = tran.userid_cookie"
                . " INNER JOIN sys_user_data as usr_data"
                . " ON usr_data.uniquenum_pri = usr_main.uniquenum_pri"
                . " WHERE tran.tag_table_usage = '".$fromtrans."'"
                . " AND tran.var_25_001 = 'auditlog'";
                $query = $query . " AND tran.uniquenum_sec = '".$uniquenum_pri."'";
                $query = $query . " ORDER BY date_created DESC";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }
}
