<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170719             wrwt                   create new file
###############################################################################*/

class TransactionGenUnique {
    public function autogen_uniquenum_pri(){
        $uniquenum_pri = 'p'.$this->generate_datetime().$this->generate_milliseconds();

        return $uniquenum_pri;
    }

    public function autogen_uniquenum_uniq(){
        $uniquenum_uniq = 'u'.$this->generate_datetime().$this->generate_milliseconds();

        return $uniquenum_uniq;
    }

    public function create_del_uniquenum_pri($uniquenum_pri){
        $del_uniquenum_pri = str_replace("p","x",$uniquenum_pri);

        return $del_uniquenum_pri;
    }

    public function generate_datetime(){
        $datetime = (new \DateTime())->format('Ymds');

        return $datetime;
    }

    public function generate_milliseconds() {
        $setmil = explode(' ', microtime());
        $milisec = ((int)$setmil[1]) * 1000 + ((int)round($setmil[0] * 1000));

        return $milisec;
    }

    public function generate_trans_datetime(){
        $datetime = (new \DateTime())->format('Y-m-d H:i:s');

        return $datetime;
    }
}
