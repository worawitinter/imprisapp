<?php

class SysConversion {
    public function convertDate($date){
        if($date <> '' && $date <> 'Invalid date'){
            $array_date = explode('/',$date);
            $parse_date = $array_date[2]."-".$array_date[1]."-".$array_date[0];
        }else{
            $parse_date = "00-00-0000";
        }

        return $parse_date;
    }

    public function convertArrayToList($array){
        $stringList = rtrim(implode(',', $array), ',');

        return $stringList;
    }

    public function convertListToArray($list){
        $arrayList = [];
        $arrayList = explode(',', $list);

        return $arrayList;
    }

    public function convertDateFormat($date,$format){
        if($date == '0000-00-00 00:00:00'){
            $date_format = "00-00-0000";
        }else{
            $dtime = new DateTime($date);
            $date_format = $dtime->format($format);
        }
        return $date_format;
    }

    public function insertWhiteSpace($str,$len){
        return (str_repeat($str, $len));
    }

    public function setWordWrap($string, $width = 75, $break = "\n") {
        // split on problem words over the line length
        $pattern = sprintf('/([^ ]{%d,})/', $width);
        $output = '';
        $words = preg_split($pattern, $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        foreach ($words as $word) {
            if (false !== strpos($word, ' ')) {
                // normal behaviour, rebuild the string
                $output .= $word;
            } else {
                // work out how many characters would be on the current line
                $wrapped = explode($break, wordwrap($output, $width, $break));
                $count = $width - (strlen(end($wrapped)) % $width);

                // fill the current line and add a break
                $output .= substr($word, 0, $count) . $break;

                // wrap any remaining characters from the problem word
                $output .= wordwrap(substr($word, $count), $width, $break, true);
            }
        }

        // wrap the final output
        return wordwrap($output, $width, $break);
    }

    public function convertThYear($year){
        $bd_year = $year + 543;
        return $bd_year;
    }

    function convertThMonth($month){
        $thai_month_arr=array(
            "00"=>"...",
            "01"=>"มกราคม",
            "02"=>"กุมภาพันธ์",
            "03"=>"มีนาคม",
            "04"=>"เมษายน",
            "05"=>"พฤษภาคม",
            "06"=>"มิถุนายน",
            "07"=>"กรกฎาคม",
            "08"=>"สิงหาคม",
            "09"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม"
        );
        return $thai_month_arr[$month];
    }

    public function convertNumberToThaiBaht($number,$currency){ 
        $currency_short = 'บาท';
        $decimal_short = 'สตางค์';
        if($currency == "MYR"){
            $currency_short = 'ริงกิต';
            $decimal_short = 'เซ็นต์';
        }
        $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ'); 
        $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน'); 
        $number = str_replace(",","",$number); 
        $number = str_replace(" ","",$number); 
        $number = str_replace($currency_short,"",$number); 
        $number = explode(".",$number); 
        if(sizeof($number)>2){ 
            return 'ทศนิยมหลายตัว'; 
        exit; 
        } 
        $strlen = strlen($number[0]); 
        $convert = ''; 
        for($i=0;$i<$strlen;$i++){ 
            $n = substr($number[0], $i,1); 
            if($n!=0){ 
                if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; } 
                elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; } 
                elseif($i==($strlen-2) AND $n==1){ $convert .= ''; } 
                else{ $convert .= $txtnum1[$n]; } 
                $convert .= $txtnum2[$strlen-$i-1]; 
            } 
        } 
        
        $convert .= $currency_short; 
        if($number[1]=='0' OR $number[1]=='00' OR 
            $number[1]==''){ 
            $convert .= 'ถ้วน'; 
        }else{ 
            $strlen = strlen($number[1]); 
            for($i=0;$i<$strlen;$i++){ 
                $n = substr($number[1], $i,1); 
                if($n!=0){ 
                    if($i==($strlen-1) AND $n==1){
                        $convert .= 'เอ็ด';
                    }elseif($i==($strlen-2) AND $n==2){
                        $convert .= 'ยี่';
                    }elseif($i==($strlen-2) AND $n==1){
                        $convert .= '';
                    }else{ 
                        $convert .= $txtnum1[$n];
                    } 
                    $convert .= $txtnum2[$strlen-$i-1]; 
                } 
            } 
            $convert .= $decimal_short; 
        } 
        return $convert; 
    } 
}
