<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';

class SysReport{
    public function getImprisRecord($condition){
        $query = "SELECT imp.uniquenum_pri,         imp.desc_lang01 as imp_firstname,   imp.desc_lang02 as imp_lastname,"
                . "imp.date_001 as imp_birthdate,   imp.desc_lang03 as imp_age,         imp.desc_lang04 as imp_gender,"
                . "imp.desc_lang05 as imp_addrr,    imp.desc_lang06 as imp_subdistrict, imp.desc_lang07 as imp_district,"
                . "imp.desc_lang08 as imp_province, imp.desc_lang09 as imp_zipcode,     imp.var_25_001 as imp_telephone,"
                . "imp.date_002 as imp_prisdate,    imp.var_25_002 as imp_reg_uniq,    laws.desc_lang01 as imp_reg_desc,"
                . "ref_doc.setgen_code as imp_refdoc,    imp.desc_lang10 as imp_idcard,       imp.notes_memo as imp_remark,"
                . "imp.var_25_004 as imp_stat_uniq, stat.desc_lang01 as imp_stat_desc,  imp.var_50_002 as imp_passport,"
                . "imp.var_25_005 as imp_det_uniq,  det.desc_lang01 as imp_det_desc,    imp.var_50_003 as imp_autonum,"
                . "CASE WHEN imp.var_50_001 = 'mr' THEN 'นาย' "
                . "WHEN imp.var_50_001 = 'ms' THEN 'นางสาว' "
                . "WHEN imp.var_50_001 = 'mrs' THEN 'นาง' "
                . "WHEN imp.var_50_001 = 'master' THEN 'เด็กชาย' "
                . "WHEN imp.var_50_001 = 'miss' THEN 'เด็กหญิง' "
                . "ELSE '' END as imp_title,"
                . "CASE WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'mr' THEN 'Mr.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'ms' THEN 'Ms.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'mrs' THEN 'Mrs.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'miss' THEN 'Miss' "
                . "ELSE '' END as imp_title_2,"
                . "imp.var_50_003 as imp_autonum,   imp.var_50_004 as imp_firstname_2,  imp.var_50_005 as imp_lastname_2,"
                . "reg_cate.desc_lang01 as reg_cate_desc,   pris_cate.desc_lang01 as pris_cate_desc,"
                . "imp.date_created as imp_date_created,    imp_cate.desc_lang01 as imp_cate_desc,"
                . "det.var_100_001 as imp_det_state,"
                . "imp.date_003 as imp_cshbrw_date,         imp.num_02_d_001 as imp_cshbrw_amt,"
                . "imp.tag_other01_yn as imp_cshbrw_stat,   imp.var_100_005 as imp_cshbrw_ref,"
                . "CASE WHEN imp.desc_lang04 = 'm' THEN 'ชาย' WHEN imp.desc_lang04 = 'f' THEN 'หญิง' ELSE '' END as imp_gender";
        $query = $query . " FROM tran_record_main as imp"
                . " LEFT JOIN sys_gencode_main as laws"
                    . " on laws.uniquenum_pri = imp.var_25_002"
                       . " and laws.tag_table_usage = 'reg_list'";
       $query = $query . " LEFT JOIN sys_gencode_main as imp_cate"
                   . " on imp_cate.uniquenum_pri = imp.var_100_003"
                      . " and imp_cate.tag_table_usage = 'imp_cate'";
       $query = $query . " LEFT JOIN sys_gencode_main as reg_cate"
                   . " on reg_cate.uniquenum_pri = laws.var_25_002"
                      . " and reg_cate.tag_table_usage = 'reg_cate'";
        $query = $query . " LEFT JOIN sys_gencode_main as stat "
                    . " on stat.uniquenum_pri = imp.var_25_004"
                       . " and stat.tag_table_usage = 'imp_stat'";
        $query = $query . " LEFT JOIN sys_gencode_main as det"
                    . " on det.uniquenum_pri = imp.var_25_005"
                       . " and det.tag_table_usage = 'pris_det'";
        $query = $query . " LEFT JOIN sys_gencode_main as pris_cate"
                   . " on pris_cate.uniquenum_pri = det.var_25_003"
                      . " and pris_cate.tag_table_usage = 'pris_cate'";
        $query = $query . " LEFT JOIN sys_gencode_main as ref_doc"
                      . " on ref_doc.uniquenum_pri = imp.var_25_003"
                         . " and ref_doc.tag_table_usage = 'impr_doc'";
        $query = $query . " WHERE imp.tag_table_usage = 'impris' and imp.tag_deleted_yn = 'n'";
        $query = $query . $condition;
        $query = $query . " order by imp.date_002";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisByRegCate($field_condition,$condition,$fmi_provn_class){
        $query = "select imp.reg_cate_unique, imp.reg_cate_desc, 
                    MAX(CASE WHEN imp.imp_gender_code = 'm' and imp.imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as imp_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' and imp.imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as imp_female, 
                    MAX(CASE WHEN imp.imp_gender_code = 'm' and imp.imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province not in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as oth_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' and imp.imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province not in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as oth_female, 
                    (select COUNT(imp_gender_code) from tran_record_main_all where imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) as total 
                    from tran_record_main_all as imp 
                    where imp.tag_deleted_yn = 'n'";
        $query = $query . $condition;
        $query = $query . " group by imp.reg_cate_unique order by imp.reg_cate_desc";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisByReg($field_condition,$condition,$fmi_provn_class,$reg_cate_unique){
        $query = "select imp.imp_reg_desc as reg_list, 
                    MAX(CASE WHEN imp.imp_gender_code = 'm' and imp.imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as imp_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' and imp.imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as imp_female, 
                    MAX(CASE WHEN imp.imp_gender_code = 'm' and imp.imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province not in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as oth_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' and imp.imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province not in (".$fmi_provn_class.") and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as oth_female, 
                    (select COUNT(imp_gender_code) from tran_record_main_all where imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) as total 
                    from tran_record_main_all as imp  
                    where imp.tag_deleted_yn = 'n'
                    and imp.reg_cate_unique = '".$reg_cate_unique."'";
        $query = $query . $condition;
        $query = $query . " group by imp.imp_reg_uniq  order by imp.imp_reg_desc";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisByState($field_condition,$condition,$fmi_provn_class){
        $query = "select imp.imp_det_state_desc as state_desc, 
                    MAX(CASE WHEN imp_gender_code = 'm' and imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province in (".$fmi_provn_class.") ".$field_condition." and imp_det_state_code = imp.imp_det_state_code and imp.imp_rec_type = imp_rec_type) ELSE 0 END) as imp_male, 
                    MAX(CASE WHEN imp_gender_code = 'f' and imp_province in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province in (".$fmi_provn_class.") ".$field_condition." and imp_det_state_code = imp.imp_det_state_code and imp.imp_rec_type = imp_rec_type) ELSE 0 END) as imp_female, 
                    MAX(CASE WHEN imp_gender_code = 'm' and imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp_det_state_code = imp.imp_det_state_code and imp.imp_rec_type = imp_rec_type) ELSE 0 END) as oth_male, 
                    MAX(CASE WHEN imp_gender_code = 'f' and imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp_province not in (".$fmi_provn_class.") ".$field_condition." and imp_det_state_code = imp.imp_det_state_code and imp.imp_rec_type = imp_rec_type) ELSE 0 END) as oth_female, 
                    (select COUNT(imp_gender_code) from tran_record_main_all where imp.imp_rec_type = imp_rec_type".$field_condition." and imp_det_state_code = imp.imp_det_state_code) as total 
                    from tran_record_main_all as imp
                    where imp.tag_deleted_yn = 'n'";
        $query = $query . $condition;
        $query = $query . " group by imp.imp_det_state_code";
        $query = $query . " order by imp.imp_det_state_code";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        return $qs_result;
    }

    public function getImprisWithinReg($condition){
        $query = "SELECT imp.uniquenum_pri,         imp.desc_lang01 as imp_firstname,   imp.desc_lang02 as imp_lastname,"
                . "imp.date_001 as imp_birthdate,   imp.desc_lang03 as imp_age,         imp.desc_lang04 as imp_gender,"
                . "imp.desc_lang05 as imp_addrr,    imp.desc_lang06 as imp_subdistrict, imp.desc_lang07 as imp_district,"
                . "imp.desc_lang08 as imp_province, imp.desc_lang09 as imp_zipcode,     imp.var_25_001 as imp_telephone,"
                . "imp.date_002 as imp_prisdate,    imp.var_25_002 as imp_reg_uniq,    laws.desc_lang01 as imp_reg_desc,"
                . "imp.var_25_003 as imp_refdoc,    imp.desc_lang10 as imp_idcard,       imp.notes_memo as imp_remark,"
                . "imp.var_25_004 as imp_stat_uniq, stat.desc_lang01 as imp_stat_desc,  imp.var_50_002 as imp_passport,"
                . "imp.var_25_005 as imp_det_uniq,  det.desc_lang01 as imp_det_desc,"
                . "CASE WHEN imp.var_50_001 = 'mr' THEN 'นาย' "
                . "WHEN imp.var_50_001 = 'ms' THEN 'นางสาว' "
                . "WHEN imp.var_50_001 = 'mrs' THEN 'นาง' "
                . "WHEN imp.var_50_001 = 'master' THEN 'เด็กชาย' "
                . "WHEN imp.var_50_001 = 'miss' THEN 'เด็กหญิง' "
                . "ELSE '' END as imp_title,"
                . "CASE WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'mr' THEN 'Mr.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'ms' THEN 'Ms.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'mrs' THEN 'Mrs.' "
                . "WHEN imp.var_50_004 <> '' and imp.var_50_001 = 'miss' THEN 'Miss' "
                . "ELSE '' END as imp_title_2,"
                . "imp.var_50_003 as imp_autonum,   imp.var_50_004 as imp_firstname_2,  imp.var_50_005 as imp_lastname_2,"
                . "imp.var_100_001 as imp_temp_passport,    imp.var_100_002 as imp_ci,  imp.notes_memo,"
                . "imp.var_100_004 as imp_bdr_pass,         imp.num_02_d_001 as imp_cshbrw_amt,     imp_dept.desc_lang01 as usr_dept_desc,  imp.date_003 as imp_cshbrw_date";
        $query = $query . " FROM tran_record_main as imp"
                . " LEFT JOIN sys_gencode_main as laws"
                    . " on laws.uniquenum_pri = imp.var_25_002"
                       . " and laws.tag_table_usage = 'reg_list'";
       $query = $query . " LEFT JOIN sys_gencode_main as imp_cate"
                   . " on imp_cate.uniquenum_pri = imp.var_100_003"
                      . " and imp_cate.tag_table_usage = 'imp_cate'"
                      . " and imp_cate.tag_other01_yn = 'y'";
        $query = $query . " LEFT JOIN sys_gencode_main as stat "
                    . " on stat.uniquenum_pri = imp.var_25_004"
                       . " and stat.tag_table_usage = 'imp_stat'";
        $query = $query . " LEFT JOIN sys_gencode_main as det"
                    . " on det.uniquenum_pri = imp.var_25_005"
                       . " and det.tag_table_usage = 'pris_det'";
       $query = $query . " LEFT JOIN sys_gencode_main as imp_dept"
                       . " on imp.var_25_006 = imp_dept.uniquenum_pri"
                       . " and imp_dept.tag_table_usage = 'sys_user_dept'";
        $query = $query . " WHERE imp.tag_table_usage = 'impris'";
        $query = $query . $condition;
        $query = $query . " order by imp.date_002";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisBenfiByRegCate($field_condition,$condition){
        $query = "select imp.reg_cate_unique, imp.reg_cate_desc,
                    MAX(CASE WHEN imp.imp_gender_code = 'm' ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as imp_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp.imp_rec_type = imp_rec_type ".$field_condition." and reg_cate_unique = imp.reg_cate_unique) ELSE 0 END) as imp_female 
                    from tran_record_main_all as imp
                    where imp.tag_deleted_yn = 'n'";
        $query = $query . $condition;
        $query = $query . " group by imp.reg_cate_unique order by imp.reg_cate_desc";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        return $qs_result;
    }

    public function getImprisBenfiByReg($field_condition,$condition,$reg_cate_unique){
        $query = "select imp.imp_reg_desc as reg_list, 
                    MAX(CASE WHEN imp.imp_gender_code = 'm' ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'm' and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as imp_male, 
                    MAX(CASE WHEN imp.imp_gender_code = 'f' ".$field_condition." and imp.imp_rec_type = imp_rec_type THEN (select count(imp_gender_code) from tran_record_main_all where imp_gender_code = 'f' and imp.imp_rec_type = imp_rec_type ".$field_condition." and imp_reg_uniq = imp.imp_reg_uniq) ELSE 0 END) as imp_female 
                    from tran_record_main_all as imp
                    where imp.tag_deleted_yn = 'n'
                    and imp.reg_cate_unique = '".$reg_cate_unique."'";
        $query = $query . $condition;
        $query = $query . " group by imp.imp_reg_uniq  order by imp.imp_reg_desc";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisVistHistByState($condition){
        $query = "select det.var_25_001 as state_code, det.var_100_001 as state_desc, sum(vist.num_02_d_001) as tot_male, sum(vist.num_02_d_002) as tot_female
                from sys_gencode_main as vist
                LEFT JOIN sys_gencode_main as det
                    on det.uniquenum_pri = vist.var_25_005
                        and det.tag_table_usage = 'pris_det'
                        and det.tag_deleted_yn = 'n'
                        and det.tag_active_yn = 'y'
                    where vist.tag_table_usage = 'imp_vist'
                    and vist.tag_deleted_yn = 'n'";
        $query = $query . $condition;
        $query = $query . " group by det.var_25_001 order by det.var_100_001";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }

    public function getImprisVistHist($condition,$state_code){
        $query = "select vist.num_02_d_001 as imp_tot_male,      vist.num_02_d_002 as imp_tot_female,
                    vist.date_001 as imp_vist_date,              vist.var_25_005 as imp_det_uniq,
                    det.desc_lang01 as imp_det_desc,             det.var_100_001 as state_desc,
                    det.var_25_001 as state_code
                from sys_gencode_main as vist
                LEFT JOIN sys_gencode_main as det
                    on det.uniquenum_pri = vist.var_25_005
                        and det.tag_table_usage = 'pris_det'
                        and det.tag_deleted_yn = 'n'
                        and det.tag_active_yn = 'y'
                    where vist.tag_table_usage = 'imp_vist'
                    and vist.tag_deleted_yn = 'n'
                    and det.var_25_001  = '".$state_code."'";
        $query = $query . $condition;
        $query = $query . " order by det.var_100_001, det.desc_lang01, vist.date_001";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);

        return $qs_result;
    }
}
?>
