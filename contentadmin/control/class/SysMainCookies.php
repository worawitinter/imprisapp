<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170717             wrwt                  create new file
###############################################################################*/
@session_start();
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/Authentication.php';

class SysMainCookies {
    function setSysCookies($login_session,$cookies_usr_uniquenum_pri){
        $DatabaseOperation = new DatabaseOperation();
        $condition = "tag_table_usage = 'sys_user' and tag_deleted_yn = 'n' and tag_active_yn = 'y' and uniquenum_pri = '".$cookies_usr_uniquenum_pri."'";
        $data = array("var_50_004"=>$login_session);
        $DatabaseOperation->updateRecordToTable('sys_user_main',$data,$condition);
    }

    function clearUserCookies($uniquenum_pri){
        $DatabaseOperation = new DatabaseOperation();
        $condition = "tag_table_usage = 'sys_user' and tag_deleted_yn = 'n' and tag_active_yn = 'y' and uniquenum_pri = '".$uniquenum_pri."'";
        $data = array("var_50_004"=>"0");
        $DatabaseOperation->updateRecordToTable('sys_user_main',$data,$condition);
    }

    function clearSessionGarbage(){
        $TransactionGenUnique = new TransactionGenUnique();
        $DatabaseOperation = new DatabaseOperation();
        $SysMainCookies = new SysMainCookies();
        $Authentication = new Authentication();
        $SysMainActivity = new SysMainActivity();
        $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
        $log_tablename = "sys_activity_main";
        $log_fromtrans = "usraccess";
        $date_created = $TransactionGenUnique->generate_trans_datetime();
        $location = LOCATION;
        $query = "select usr.var_25_002 as uniquenum_pri, usr.uniquenum_pri as usr_uniquenum_pri, usr.username, usr.var_50_004 as log_time
                    from sys_user_main as usr
                    left join sys_activity_main as uout
                    	on uout.uniquenum_sec <> usr.var_25_002
                        and uout.tag_table_usage = 'usraccess'
                        and uout.sys_link in ('logout','sys_logout')
                    where usr.tag_table_usage = 'sys_user'
                    and usr.tag_deleted_yn = 'n'
                    and usr.tag_active_yn = 'y'
                    and usr.var_50_004 > 0";
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        if($qs_result->rowCount() > 0){
            while($row=$qs_result->fetch(PDO::FETCH_OBJ)) {
                if(time() - $row->log_time > SESSION_EXPIRY){
                    $log_data = array   (   "tag_table_usage"=>$log_fromtrans,
                                        "uniquenum_pri"=>$log_uniquenum_pri,
                                        "uniquenum_sec"=>$row->uniquenum_pri,
                                        "sys_link"=>"auto_logout",
                                        "userid_cookie"=>$row->username,
                                        "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                                        "date_created"=>$date_created,
                                        "desc_lang01"=>$_SERVER['HTTP_USER_AGENT'],
                                        "notes_memo"=>$location,
                                        "desc_lang10"=>getHostByName(getHostName()),
                                        "setgen_code"=>"sysupri"
                                    );
                    $DatabaseOperation->insertRecordIntoTable($log_tablename,$log_data);
                    $SysMainCookies->clearUserCookies($row->usr_uniquenum_pri);
                }
            }
        }
    }
}
