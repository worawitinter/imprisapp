<?php

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';

class SysAutoNum
{
    public function setSysNum($fromtrans,$sysnum,$uniquenum_pri,$fromlink,$userid,$date_created){
        self::insertTransSysAutoNumList($fromtrans,$sysnum,$uniquenum_pri,$fromlink,$userid,$date_created);
        $new_sysnum = self::checkLastNum($fromtrans,$uniquenum_pri);
        self::updateNewNum($fromtrans,$new_sysnum,$uniquenum_pri);
    }

    public function checkLastNum($fromtrans,$uniquenum_pri){
        $extract_val = self::extractSysNum($fromtrans,$uniquenum_pri);
        $new_sysnum = str_pad($extract_val[0]["run_number"] + 1, strlen($extract_val[0]["run_number"]), 0, STR_PAD_LEFT);
        return $new_sysnum;
    }

    public function extractSysNum($fromtrans,$uniquenum_pri){
        $query = "SELECT  setgen_code as prefix,    desc_lang03 as mth_year,    desc_lang01 as run_number,    desc_lang02 as suffix
                FROM    sys_gencode_main
                WHERE   tag_table_usage = '".$fromtrans."_sys_autonum'
                AND     uniquenum_pri = '".$uniquenum_pri."'
                AND     tag_deleted_yn = 'n'
                AND     tag_active_yn = 'y'";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $result[] = $row;
        }
        return $result;
    }

    public function checkDuplicateSysNum($fromtrans,$sysnum,$uniquenum_pri){
        $new_sysnum = "";
        $query = "SELECT  setgen_code as prefix,    desc_lang01 as mth_year,    desc_lang02 as run_number,    desc_lang03 as suffix
                FROM    sys_activity_main
                WHERE   tag_table_usage = '".$fromtrans."'
                AND     var_25_001 = 'sys_autonum'
                AND     uniquenum_sec = '".$uniquenum_pri."'
                AND     var_50_003 = '".$sysnum."'
                AND     tag_active_yn = 'y'
                AND     tag_deleted_yn = 'n'";
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($query);
        if($qs_result->rowCount() >= 1){
            $row = $qs_result->fetch(PDO::FETCH_ASSOC);
            $run_number = str_pad($row["run_number"] + 1, strlen($row["run_number"]), 0, STR_PAD_LEFT);
            $new_sysnum = $row["prefix"].$row["mth_year"].$run_number.$row["suffix"];
        }
        return $new_sysnum;
    }

    public function insertTransSysAutoNumList($fromtrans,$sysnum,$uniquenum_pri,$fromlink,$userid,$date_created){
        $SysMainActivity = new SysMainActivity();
        $TransactionGenUnique = new TransactionGenUnique();
        $tablename = "sys_activity_main";
        $tag_tran_type = "sys_autonum";
        $extract_val = self::extractSysNum($fromtrans,$uniquenum_pri);
        $data = array   (   "tag_table_usage"=>$fromtrans,
                            "uniquenum_pri"=>$TransactionGenUnique->autogen_uniquenum_pri(),
                            "uniquenum_sec"=>$uniquenum_pri,
                            "sys_link"=>$fromlink,
                            "userid_cookie"=>$userid,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                            "date_created"=>$date_created,
                            "var_25_001"=>$tag_tran_type,
                            "var_50_003"=>$extract_val[0]["prefix"].$extract_val[0]["mth_year"].$extract_val[0]["run_number"].$extract_val[0]["suffix"],
                            "setgen_code"=>$extract_val[0]["prefix"],
                            "desc_lang01"=>$extract_val[0]["mth_year"],
                            "desc_lang02"=>$extract_val[0]["run_number"],
                            "desc_lang03"=>$extract_val[0]["suffix"]
                        );
        $DatabaseOperation = new DatabaseOperation();
        $DatabaseOperation->insertRecordIntoTable($tablename,$data);
    }

    public function updateNewNum($fromtrans,$new_sysnum,$uniquenum_pri){
        $SysMainActivity = new SysMainActivity();
        $tablename = "sys_gencode_main";
        $data = array   (   "desc_lang01"=>$new_sysnum,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                        );
        $condition = "uniquenum_pri = '".$uniquenum_pri."'";
        $DatabaseOperation = new DatabaseOperation();
        $result = $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);
    }
}

 ?>
