<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170719             wrwt                   create new file
###############################################################################*/
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';

class AuditTrailLog {
    public function resetRecord($frommode,$fromtrans,$fromlink,$imp_autonum,$tablename,$uniquenum_pri,$userid){
        $TransactionGenUnique = new TransactionGenUnique();
        $DatabaseOperation = new DatabaseOperation();
        $SysMainActivity = new SysMainActivity();
        $date_created = $TransactionGenUnique->generate_trans_datetime();
        $del_uniquenum_pri = $TransactionGenUnique->create_del_uniquenum_pri($uniquenum_pri);
        $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
        $tag_deleted_yn = "y";
        $tag_active_yn = "n";
        $data = array   (   "uniquenum_pri"=>$log_uniquenum_pri,
                            "uniquenum_sec"=>$uniquenum_pri,
                            "tag_deleted_yn"=>$tag_deleted_yn,
                            "tag_active_yn"=>$tag_active_yn,
                            "userid_cookie"=>$userid,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                            "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime()
                        );
        $condition = "uniquenum_pri = '".$uniquenum_pri."'";
        $result = $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);
        if(substr($tablename,-4) == "main"){
            $this->createActivityLogs($frommode,$fromtrans,$fromlink,$imp_autonum,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created);
        }
    }

    public function createActivityLogs($frommode,$fromtrans,$fromlink,$imp_autonum,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created){
        $DatabaseOperation = new DatabaseOperation();
        $SysMainActivity = new SysMainActivity();
        $tablename = "sys_activity_main";
        $tag_tran_type = "auditlog";
        if($frommode == "edit"){
            $activity_desc = "ถูกแก้ไข";
        }elseif($frommode == "delete"){
            $activity_desc = "ถูกลบออกจากระบบ";
        }else{
            $activity_desc = "ถูกสร้างใหม่";
            $log_uniquenum_pri = $uniquenum_pri;
        }
        $data = array   (   "tag_table_usage"=>$fromtrans,
                            "uniquenum_pri"=>$log_uniquenum_pri,
                            "sys_link"=>$fromlink,
                            "uniquenum_sec"=>$uniquenum_pri,
                            "userid_cookie"=>$userid,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                            "date_created"=>$date_created,
                            "notes_memo"=>$activity_desc,
                            "setgen_code"=>$frommode,
                            "var_50_003"=>$imp_autonum,
                            "var_25_001"=>$tag_tran_type
                        );
        $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
    }
}
