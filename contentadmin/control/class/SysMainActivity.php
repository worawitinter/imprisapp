<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysMainActivity
 *
 * @author Worawit
 */
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';

class SysMainActivity {
    function getClientIpAddress() {
        $ip = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
        }

    function lockRecord($fromlink,$fromtrans,$uniquenum_pri,$userid){
        $tablename = "sys_activity_main";
        $DatabaseOperation = new DatabaseOperation();
        $TransactionGenUnique = new TransactionGenUnique();

        $result = $this->checkRecord($fromtrans,$uniquenum_pri,'');
        $count = $result->fetch(PDO::FETCH_NUM);
        if($count > 0){
            $data = array   (   "userid_cookie"=>$userid,
                                "ip_addr"=>$this->getClientIpAddress(),
                                "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime()
                            );
            $condition = "uniquenum_sec = '".$uniquenum_pri."' and tag_table_usage = 'lockrecord' and var_25_001 = '".$fromtrans."'";
            $qs_result = $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);
        }else{
            $data = array   (   "tag_table_usage"=>'lockrecord',
                                "uniquenum_pri"=>$TransactionGenUnique->autogen_uniquenum_pri(),
                                "sys_link"=>$fromlink,
                                "uniquenum_sec"=>$uniquenum_pri,
                                "userid_cookie"=>$userid,
                                "ip_addr"=>$this->getClientIpAddress(),
                                "date_created"=>$TransactionGenUnique->generate_trans_datetime(),
                                "var_25_001"=>$fromtrans,
                                "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime()
                            );
            $qs_result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
        }
        return $qs_result;
    }

    function releaseRecord($fromtrans,$uniquenum_pri){
        $tablename = "sys_activity_main";
        $del_uniquenum_pri = "";
        $qs_result = $this->checkRecord($fromtrans,$uniquenum_pri,'');
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $del_uniquenum_pri = $row["uniquenum_pri"];
        }
        if($del_uniquenum_pri != ""){
            $DatabaseOperation = new DatabaseOperation();
            $qs_result = $DatabaseOperation->deleteRecordFromTable($tablename,$del_uniquenum_pri);
            return $qs_result;
        }
    }

    function checkRecord($fromtrans,$uniquenum_pri,$userid){
        $sql = "SELECT uniquenum_pri, userid_cookie, date_lastupdate"
                . " FROM sys_activity_main"
                . " WHERE tag_table_usage = 'lockrecord'"
                . " AND uniquenum_sec = '".$uniquenum_pri."'"
                . " AND var_25_001 = '".$fromtrans."'";
        if($userid != ''){
            $sql .= " AND userid_cookie = '".$userid."'";
        }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);

        return $qs_result;
    }
}
