<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170716             wrwt                   create new file
2.      20170719             wrwt                   add delete statment
###############################################################################*/

require_once DOCUMENT_ROOT . '/contentadmin/config/PDOConfig.php';

class DatabaseOperation {
    public function insertRecordIntoTable($tablename,$data){
        $conn = new PDOConfig;
        $columns = $this->prepareColumnValues($data);
        $query = "INSERT INTO ".$tablename." SET ";
        $query .= implode(', ', $columns);
        $result = $conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function updateRecordToTable($tablename,$data,$condition){
        $conn = new PDOConfig;
        $columns = $this->prepareColumnValues($data);
        $query = "UPDATE ".$tablename." SET ";
        $query .= implode(', ', $columns) . " WHERE ".$condition;
        $result = $conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function deleteRecordFromTable($tablename,$uniquenum_pri){
        $conn = new PDOConfig;
        $query = "DELETE FROM ".$tablename." WHERE uniquenum_pri = '".$uniquenum_pri."'";
        $result = $conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function getRecordAllFromTable($tablename,$condition){
        $conn = new PDOConfig;
        $query = "SELECT * FROM ".$tablename." WHERE ".$condition;
        $result = $conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function getRecordFromTable($query){
        $conn = new PDOConfig;
        $result = $conn->prepare($query);
        $result->execute();

        return $result;
    }

    public function prepareColumnValues($data){
        array_walk($data, create_function('&$a', 'if(isset($a)): $a = "\'".AddSlashes($a)."\'"; else: $a = "NULL"; endif;'));
        array_walk($data, create_function('&$val, $key', '$val = "$key = $val";'));

        return $data;
    }

    public function getTotalRecords($tablename,$fromtrans){
        $query = "SELECT COUNT(idcode) as total"
                . " FROM ".$tablename.""
                . " WHERE tag_table_usage = '".$fromtrans."'"
                . " AND tag_deleted_yn = 'n' AND uniquenum_pri <> 'sysupri'";
        $qs_result = $this->getRecordFromTable($query);
        $rows = $qs_result->fetch(PDO::FETCH_ASSOC);

        return $rows["total"];
    }
}
