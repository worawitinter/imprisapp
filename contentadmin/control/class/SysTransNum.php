<script type="text/javascript">
    function getAutoSysNum(fld_name) {
        $.ajax({
            type: "GET",
            url: '../control/inc_ajax_text.php?tag_usage=sys_autonum&fromlink=' + fld_name + '_sys_autonum&fld_name=' + fld_name,
            data: {},
            success: function (response) {
                $('#fmi_'+ fld_name +'_autonum_list').html(response);
                console.log((response));
                $('.selectpicker').selectpicker({
                    size: 2
                });
                populate_sysnum();
            },
            error: function () {
                $('#fmi_'+ fld_name +'_autonum_list').html('There was an error!');
            }
        });
    }
</script>
<?php
class SysTransNum{
    public $fld_name = null;

    function __construct($fromtrans){
        $this->fld_name = $fromtrans;

        $sbox = $this->setSboxList();
        echo $sbox;
    }

    function setSboxList(){
        $div = "<div style=display:none' id='fmi_".$this->fld_name."_autonum_list' class='fmi_".$this->fld_name."_autonum_list' data-container='body'></div>";
        $textbox = "<input style='display:' type='text' id='fmi_ori_".$this->fld_name."_autonum' name='fmi_ori_".$this->fld_name."_autonum' class='form-control' value='' readonly>";

        return $div.$textbox;
    }
}
 ?>
