<?php

/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170806             wrwt                   create new file
###############################################################################*/
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/folder_script/inc_src_script.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';

class GencodeSboxList {
    public $fld_name = null;
    public $fld_usage = null;
    public $fld_label = "กรุณาเลือก";
    public $fld_unique = null;
    public $fld_size = 30;
    public $fld_readmode = null;
    public $fld_onclick = null;
    public $fld_onblur = null;
    public $fld_list = "desc";
    public $fld_disp = "desc";
    public $fld_js = null;
    public $fld_ref_uniq = null;
    private $unique = null;
    private $code = null;
    private $desc = null;

    function __construct($fld_name,$fld_usage,$fld_label,$fld_unique,$fld_size,$fld_readmode,$fld_onclick,$fld_onblur,$fld_list,$fld_disp,$fld_js,$fld_ref_uniq){
        $this->fld_name = $fld_name;
        $this->fld_usage = $fld_usage;
        $this->fld_label = $fld_label;
        $this->fld_unique = $fld_unique;
        $this->fld_size = $fld_size;
        $this->fld_readmode = $fld_readmode;
        $this->fld_onclick = $fld_onclick;
        $this->fld_onblur = $fld_onblur;
        $this->fld_list = $fld_list;
        $this->fld_disp = $fld_disp;
        $this->fld_js = $fld_js;
        $this->fld_ref_uniq = $fld_ref_uniq;
        $sbox = $this->setSboxList();
        echo $sbox;
    }

    public function setSboxList(){
        $qs_result = self::getValueList();
        $select_list = "<select class='selectpicker' data-hide-disabled='true' data-live-search='true' name='".$this->fld_name."' id='".$this->fld_name."' onchange='".$this->fld_js."' title='".$this->fld_label."'>";
        $select_list = $select_list."<option value=''>กรุณาเลือก</option>";
        while($row = $qs_result->fetch(PDO::FETCH_ASSOC)){
            $select_list = $select_list."<option value='".$row["gencode_uniq"]."' ".self::setSelectedList($this->unique = $row["gencode_uniq"]).">".self::sortSboxDisp($this->code = $row["gencode_code"],$this->desc = $row["gencode_desc"])."</option>";
        }
        $select_list = $select_list."</select>";
        return $select_list;
    }

    public function getValueList(){
        $string = str_replace($this->fld_usage, '', '_sys_autonum');
        if($string == "sys_autonum"){
            $sql = "SELECT CONCAT(setgen_code,desc_lang03,desc_lang01,desc_lang02) as gencode_uniq, CONCAT(setgen_code,desc_lang03,desc_lang01,desc_lang02) as gencode_code, CONCAT(setgen_code,desc_lang03,desc_lang01,desc_lang02) as gencode_desc"
                    . " FROM sys_gencode_main"
                    . " WHERE tag_table_usage = '".$this->fld_usage."'"
                    . " AND tag_deleted_yn = 'n'"
                    . " AND tag_active_yn = 'y'"
                    . " ORDER BY ".self::sortSboxList()
                    . " DESC";
        }else{
            $sql = "SELECT setgen_unique as gencode_uniq, setgen_code as gencode_code, desc_lang01 as gencode_desc"
                    . " FROM sys_gencode_main"
                    . " WHERE tag_table_usage = '".$this->fld_usage."'"
                    . " AND tag_deleted_yn = 'n'"
                    . " AND tag_active_yn = 'y'"
                    . " ORDER BY ".self::sortSboxList()
                    . " DESC";
        }
        $DatabaseOperation = new DatabaseOperation();
        $qs_result = $DatabaseOperation->getRecordFromTable($sql);

        return $qs_result;
    }

    public function sortSboxList(){
        switch ($this->fld_list){
            case 'codedesc':
                $srt_list = "setgen_code, desc_lang01";
                break;
            case 'desc':
                $srt_list = "desc_lang01";
                break;
            case 'desccode':
                $srt_list = "desc_lang01, set_code";
                break;
            default:
                $srt_list = "setgen_code";
        }

        return $srt_list;
    }

    public function sortSboxDisp($code,$desc){
        switch ($this->fld_disp){
            case 'codedesc':
                $srt_disp = $code." ".$desc;
                break;
            case 'desc':
                $srt_disp = $desc;
                break;
            case 'desccode':
                $srt_disp = $desc." ".$code;
                break;
            default:
                $srt_disp = $code;
        }

        return $srt_disp;
    }

    public function setSelectedList($row_unique){
        if($this->fld_unique != "" && $this->fld_unique == $row_unique){
            $fld_selected = "selected";
        }else{
            $fld_selected = "";
        }

        return $fld_selected;
    }

    public function setFldReadMode(){
        if($this->$fld_readmode == "view"){
            $readmode = "readonly";
        }else{
            $readmode = "";
        }

        return $readmode;
    }
}
