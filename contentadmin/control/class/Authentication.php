<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170719             wrwt                   create new file
###############################################################################*/
class Authentication {
    public function verifyUser($tablename,$conn,$login_username){
        $condition = "tag_table_usage = 'sys_user' and tag_deleted_yn = 'n' and tag_active_yn = 'y' and (username = '".$login_username."' or uniquenum_pri = '".$login_username."')";
        $qs_result = $conn->getRecordAllFromTable($tablename,$condition);
        return $qs_result;
    }

    public function encryptPassword($login_password){
        $ency_password = password_hash($login_password, PASSWORD_DEFAULT);
        return $ency_password;
    }

    public function decryptPassword($login_password,$ency_password){
        $result = false;
        if(password_verify($login_password,$ency_password)){
            $result = true;
        }
        return $result;
    }
}
