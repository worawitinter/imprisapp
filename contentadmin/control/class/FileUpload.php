<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170916             wrwt                  create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';

class FileUpload {
    function uploadFile($file,$uniquenum_pri){
        $path = DOCUMENT_ROOT . '/folder_upload/impr_doc/'.$uniquenum_pri;
        $this->deleteFileIfExist($uniquenum_pri,$file['name'][0]);
        $error = $this->verifyFile($path,$file);
        if(!empty(array_filter($error))){
            print_r($error);
        }else{
            $this->prepareFile($path,$file);
            //echo "{success}";
        }
    }

    function prepareFile($path,$file){
        $file_tmp = $file['tmp_name'][0];
        $file_name = $file['name'][0];
        $this->createFolderIfNotExist($path);
        $upload_path = $path.'/'.$file_name;
        move_uploaded_file($file_tmp,$upload_path);
    }

    function verifyFile($path,$file){
        $error = array();
        $file_name = $file['name'][0];
        $file_size = $file['size'][0];
        $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        $error[] = $this->getFileExtension($file_ext);
        $error[] = $this->getFileSize($file_size);
        $error[] = $this->checkFileExist($path,$file);
        return $error;
    }

    function getFileExtension($file_ext){
        $result = "";
        $extension = array("jpg","png","gif","pdf","doc","docx","xls","xlsx");
        if(in_array($file_ext,$extension) === false){
            $result = "error_file_extension";
        }
        return $result;
    }

    function getFileSize($file_size){
        $result = "";
        if($file_size > 104857600){
            $result = "error_file_size";
        }
        return $result;
    }

    function checkFileExist($path,$file){
        $result = "";
        if(file_exists($path.'/'.$file['name'][0])){
            $result = "error_file_exist";
        }
         return $result;
    }

    function createFolderIfNotExist($path){
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }

    function deleteFileIfExist($uniquenum_pri,$file){
      $path = DOCUMENT_ROOT . '/folder_upload/impr_doc/'.$uniquenum_pri;
      if(is_dir($path)){
          array_map('unlink', glob("$path/*.*"));
          rmdir($path);
      }
    }
}
