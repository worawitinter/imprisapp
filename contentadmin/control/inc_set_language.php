<?php
if(isset($_GET["set_language"])){
    $set_language = $_GET["set_language"];
}else if(isset($_SESSION["cookies_set_language"])){
    $set_language = $_SESSION["cookies_set_language"];
}else{
    $set_language = DEF_LANGUAGE;
}

//main_login.php
if($set_language == 'en'){
    $lbl_app_name = "ImprisApp";
    $lbl_version = "Version";
    $lbl_login_header = "Welcome";
    $lbl_username = "Username";
    $lbl_password = "Password";
    $lbl_login_submit = "Login";
    $lbl_powerby = "Power by";
    $lbl_lang_pref = "Language Preference";
    $lbl_lang_th = "Thai";
    $lbl_lang_en = "English";

    $ntf_login_error = "Username or password incorrect";
    $ntf_login_success = "Logging In";
}else{
    $lbl_app_name = "ระบบฐานข้อมูลงานคุ้มครองคนไทยในมาเลเซีย";
    $lbl_version = "เวอร์ชั่น";
    $lbl_login_header = "ยินดีต้อนรับ";
    $lbl_username = "ชื่อผู้ใช้";
    $lbl_password = "รหัสผ่าน";
    $lbl_login_submit = "เข้าสู่ระบบ";
    $lbl_powerby = "พัฒนาโดย";
    $lbl_lang_pref = "กรุณาเลือกภาษา";
    $lbl_lang_th = "ภาษาไทย";
    $lbl_lang_en = "ภาษาอังกฤษ";

    $ntf_login_error = "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง";
    $ntf_login_success = "กำลังเข้าสู่ระบบ";
}

//inc_menu_topmain.php
if($set_language == 'en'){
    $lbl_main_menu = "Dashboard";
    $lbl_setting = "Settings";
    $lbl_entry = "Entry";
    $lbl_entry_impris = "Registration";
    $lbl_entry_refdoc = "Document Management";
    $lbl_rept = "Report";
    $lbl_usr_welcome = "Welcome";
    $lbl_usr_title = "User";
    $lbl_usr_role = "Role";
    $lbl_last_access = "Last access on";
    $lbl_usr_logout = "Logout";
}else{
    $lbl_main_menu = "แดชบอร์ด";
    $lbl_setting = "จัดการระบบ";
    $lbl_entry = "จัดการข้อมูล";
    $lbl_entry_impris = "ลงทะเบียนผู้ตกทุกข์";
    $lbl_entry_refdoc = "จัดการเอกสาร";
    $lbl_rept = "รายงาน";
    $lbl_usr_welcome = "ยินดีต้อนรับ";
    $lbl_usr_title = "ผู้ใช้";
    $lbl_usr_role = "สถานะ";
    $lbl_last_access = "เข้าใช้งานครั้งล่าสุดเมื่อ";
    $lbl_usr_logout = "ออกจากระบบ";
}
?>
