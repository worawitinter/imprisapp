<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
2.      20180601            wrwt                   set $page_limit = 50
###############################################################################*/

$host = "127.0.0.1";
$username = "root";
$password = "123456";
$charset = "utf8";
$database = "imprisapp";
$application = "imprisapp";
$version = "2.0";
$powerby = "";
$document_root = substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['SCRIPT_NAME']));
$server_url_root = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
//$location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
$location = "";
$def_panel_width = "auto";
$def_panel_height = "auto";
$page_limit = 50;
$def_language = "th";
$sys_logo = "thai-embassy-logo.png";
$notif_logo = $sys_logo;
$notif_msg = "ระบบแจ้งเตือน";
if($application <> ""){
    $document_root = $document_root."/".$application;
    $server_url_root = $server_url_root."/".$application;
}
$url_file_ext = ".html";
$session_expiry = 3600; //default 2 hours

define("HOST",$host);
define("USER",$username);
define("PASS",$password);
define("CHARSET",$charset);
define("DB",$database);
define("VERSION",$version);
define("POWERBY", $powerby);
define("DOCUMENT_ROOT", $document_root);
define("APPLICATION", $application);
define("SERVER_URL_ROOT", $server_url_root);
define("PANEL_WIDTH", $def_panel_width);
define("PANEL_HEIGHT", $def_panel_height);
define("PAGE_LIMIT", $page_limit);
define("DEF_LANGUAGE", $def_language);
define("SYS_LOGO", $sys_logo);
define("LOCATION", $location);
define("URL_FILE_EXT", $url_file_ext);
define("SESSION_EXPIRY", $session_expiry);
?>
