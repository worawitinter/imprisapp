<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/

require_once 'config.ini.php';

class PDOConfig extends PDO {

    private $engine;
    private $host;
    private $database;
    private $user;
    private $pass;
    private $charset;

    private static $pdo;

    public function __construct(){
        $this->engine = 'mysql';
        $this->host = HOST;
        $this->database = DB;
        $this->user = USER;
        $this->pass = PASS;
        $this->charset = CHARSET;
        $dns = $this->engine.':dbname='.$this->database.";host=".$this->host.";charset=".$this->charset;
        parent::__construct( $dns, $this->user, $this->pass );
    }

    public static function getInstance(){
    	$dsn="mysql:dbname=".DB.";host=".HOST;
    	$user=USER;
    	$pass=PASS;
    	self::$pdo = SPDO::connect($dsn, $user, $pass, array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));

		return self::$pdo;

    }
}
?>
