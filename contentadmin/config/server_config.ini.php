<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/

$host = "localhost";
$username = "id2801551_root";
$password = "123456";
$charset = "utf8";
$database = "id2801551_imprisapp";
$version = "1.0";
$powerby = "Royal Thai Embassy, Kuala Lumpur";
$document_root = substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['SCRIPT_NAME']));
$def_panel_width = "auto";
$def_panel_height = "auto";
$page_limit = 10;
$def_language = "th";

define("HOST",$host);
define("USER",$username);
define("PASS",$password);
define("CHARSET",$charset);
define("DB",$database);
define("VERSION",$version);
define("POWERBY", $powerby);
define("DOCUMENT_ROOT", $document_root);
define("PANEL_WIDTH", $def_panel_width);
define("PANEL_HEIGHT", $def_panel_height);
define("PAGE_LIMIT", $page_limit);
define("DEF_LANGUAGE", $def_language);
?>
