<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170712             wrwt                  create new file
###############################################################################*/

class MyPDO extends PDO
{

	/**
	 * Usefull for easy table modification (insert or update line)
	 * all variables are added into slashes and dangerous characters are escaped
	 * NULL pointers are re-transformed to NULL in SQL language
	 * Does not support inner SQL functions! such as NOW() etc.	 
	 *
	 * @uses InvalidArgumentException	 
	 * @param string $table       target SQL table
	 * @param array $set          array with items to update/insert
	 * @param string $condition   SQL condition if UPDATE table 
	 *
	 * @return void
	 */
	
	public function modifyTable($table, array $set, $condition = "")
	{
		
		if(Empty($table))
		{
			throw new InvalidArgumentException("invalid table name");
		}
		
		if(Empty($set))
		{
			throw new InvalidArgumentException("variable set can not be empty");
		}
		
		// insert string into slashes and re-transform NULL
		array_walk($set, create_function('&$a', 'if(isset($a)): $a = "\'".AddSlashes($a)."\'"; else: $a = "NULL"; endif;'));
	
	    if(!Empty($condition))
		{
			array_walk($set, create_function('&$val, $key', '$val = "$key = $val";'));
			$this->exec("UPDATE `$table` SET " . implode(", ", $set) . " WHERE $condition");
		}
		else
		{
			$this->exec("INSERT INTO `$table` (" . implode(", ", array_keys($set)) . ") VALUES (" . implode(", ", $set) . ")");
		}
	}
}

/**
 * Singleton implementation of PDO class
 * 
 * @version 1.00
 * @license http://www.gnu.org/copyleft/gpl.html GPL
 * @author Michal "Techi" Vrchota <michal.vrchota@seznam.cz>
 * @package Database
 * @category Database
 */   

class SPDO extends MyPDO
{
	/**
	 * @var SPDO $instance
	 */
	 	 	
	private static $instance = FALSE;
	
	/**
	 * returns instance from anywhere
	 *
	 * @return SPDO
	 */	 	 	 	
	
	public static function getInstance()
	{
		if(!self::$instance)
		{
			// init your connection here to design singleton implementation
			throw new Exception("PDO instance was not initialized");
		}
		else
		{
			return self::$instance;
		}
	}
	
	/**
	 * Connect to DB - same as PDO constructor
	 * 
	 * @param string $dsn
	 * @param string $username
	 * @param string $password
	 * @param array $driver_options
	 * @return SPDO
	 */	 	 	 	 	 	 	 	
	
	public static function connect($dsn, $username = "root", $password = "root", array $driver_options = NULL)
	{
		self::$instance = new MyPDO($dsn, $username, $password, $driver_options);
		return self::$instance;
	}
}

?>
