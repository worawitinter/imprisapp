<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$DatabaseOperation = new DatabaseOperation();
$SysListMain = new SysListMain();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table class="table table-striped table-bordered table-list">
            <tr>
                <th class="text-center"><em class="fa fa-cog"></em></th>
                <th class="hidden-xs">ลำดับ</th>
                <th>ชื่อ-สกุล</th>
                <th>สังกัด</th>
                <th>ชื่อผู้ใช้</th>
                <th>ระดับสิทธิ์</th>
                <th>วันที่สิ้นสุด</th>
            </tr>
        <?php
            $uniquenum_pri = '';
            if(isset($_GET["page"])){
                $page = $_GET["page"];
            }else{
                $page = 1;
            }
            if(isset($_GET["search_query"])){
                $search_query = $_GET["search_query"];
            }else{
                $search_query = "";
            }
            $limit = PAGE_LIMIT;
            $page_start = ($page-1) * $limit;
            $cnt = 0;
            $qs_result = $SysListMain->getRowUserResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
            if($search_query != ""){
                $total_records = $qs_result->rowCount();
            }else{
                $total_records = $DatabaseOperation->getTotalRecords($tablename,$_GET['fromtrans']);
            }
            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                $cnt++;
        ?>
        <tr>
            <td align="center">
                <input type="hidden" id="r_sys_autonum<?php echo $cnt;?>" name="r_sys_autonum<?php echo $cnt;?>" value="<?php echo $rows["sys_autonum"];?>">
                <a data-toggle="modal" data-target="#frm_usr" class="row_upd btn btn-success" title="แก้ไขหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="edit" href="#frm_usr"><em class="fa fa-pencil" style="color: #FFFFFF"></em></a>
                <a class="row_del btn btn-danger" title="ลบหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="delete" onclick="confirmDelete('<?php echo $cnt;?>','<?php echo $rows["uniquenum_pri"];?>');"><em class="fa fa-trash"></em></a>
                <a id="row_logout" <?php if($rows["login_session"] == 0){?>class="row_logout btn btn-danger"<?php }else{ ?> class="row_logout btn btn-success" <?php } ?> <?php if($rows["login_session"] == 0){?>title="ชื่อผู้ใช้ <?php echo $rows["usr_username"];?> อยู่ในสถานะออฟไลน์"<?php }else{ ?> title="ชื่อผู้ใช้ <?php echo $rows["usr_username"];?> อยู่ในสถานะออนไลน์" <?php } ?> data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="logout" <?php if($rows["login_session"] > 0){?>onclick="confirmLogout('<?php echo $rows["usr_username"];?>','<?php echo $rows["uniquenum_pri"];?>');"<?php }else{ ?>onclick="toggleAlertBox('','ชื่อผู้ใช้ <?php echo $rows["usr_username"];?> อยู่ในสถานะออฟไลน์!','fa fa-warning','orange');"<?php } ?>><em class="fa fa-power-off"></em></a>
            </td>
            <td class="hidden-xs"><?php echo $cnt;?></td>
            <td><?php echo $rows["usr_title"]." ".$rows["usr_firstname"]." ".$rows["usr_lastname"];?></td>
            <td><?php echo $rows["usr_dept_desc"];?></td>
            <td><?php echo $rows["usr_username"];?></td>
            <td><?php echo $rows["usr_role"]." (".$rows["usr_right"].")";?></td>
            <td><?php echo $rows["usr_enddate"];?></td>
        </tr>
        <?php
            } /* End while loop $qs_result */
        ?>
        </table>
    </body>
</html>
