<!DOCTYPE html>
<!-- inc_imp_main_form.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .modal-dialog{
                position: relative;
                display: table; /* This is important */
                overflow-y: auto;
                overflow-x: auto;
                width: auto;
                min-width: 1000px;
            }

            #imp_refdoc{
                width:70%;
                height:70%;
                position: relative;
                padding: 0px;
                left: 0px;
                top: 10px;
            }

            #imp_tab .nav-pills > li > a {
              border-radius: 4px 4px 0 0 ;
            }

            #imp_tab .tab-content {
              padding : 5px 15px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#frm_imp_main').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        fmi_imp_firstname: {
                            validators: {
                                    stringLength: {
                                    min: 2,
                                },
                                    notEmpty: {
                                    message: 'โปรดระบุชื่อ (ภาษาไทย)'
                                }
                            }
                        },
                        fmi_imp_lastname: {
                           validators: {
                                stringLength: {
                                   min: 2,
                               },
                               notEmpty: {
                                   message: 'โปรดระบุนามสกุล (ภาษาไทย)'
                               }
                           }
                        },
                        fmi_imp_firstname_2: {
                            validators: {
                                    stringLength: {
                                    min: 2,
                                },
                                    notEmpty: {
                                    message: 'โปรดระบุชื่อ (ภาษาอังกฤษ)'
                                }
                            }
                        },
                        fmi_imp_lastname_2: {
                           validators: {
                                stringLength: {
                                   min: 2,
                               },
                               notEmpty: {
                                   message: 'โปรดระบุนามสกุล (ภาษาอังกฤษ)'
                               }
                           }
                        },
                        fmi_imp_passport: {
                           validators: {
                                stringLength: {
                                   min: 2,
                               },
                               notEmpty: {
                                   message: 'โปรดระบุหมายเลขหนังสือเดินทาง'
                               }
                           }
                        },
                        fmi_imp_birthdate: {
                           validators: {
                               date: {
                                   format: 'DD/MM/YYYY',
                                   message: 'รูปแบบวันที่ไม่ถูกต้อง (DD/MM/YYYY)'
                               }
                           }
                        },
                        fmi_imp_age: {
                           validators: {
                                integer: {
                                   message: 'โปรดระบุเฉพาะตัวเลข',
                               }
                           }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                    $('#frm_imp_main').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.LoadingOverlay("show");
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        var value=$.trim($('#fmi_fromtrans').val());
                        if(value.length == 0)
                        {
                            //Error Code: 001 value missing in fromtrans url parameter
                            toggleAlertBox('','เกิดความผิดพลาดของระบบ [Error Code: 001]','fa fa-warning','red');
                        }else{
                            if(validateForm(value)){
                                $.ajax({
                                    type: 'post',
                                    url: 'imp_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        $('#frm_imp').modal('hide');
                                        $('.modal-body input').val('');
                                        $('.modal-body textarea').val('');
                                        document.getElementById('chk_drf').checked = false;
                                        jQuery("#target-content").load("inc_imp_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    });
                });
            });

            function validateForm(fromtrans){
                var result = true;
                if(document.getElementById('chk_drf').checked == false){
                    if($("#fmi_imp_dept").val() == ""){
                        toggleAlertBox('','กรุณาเลือก สังกัด','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#fmi_imp_cate").val() == ""){
                        toggleAlertBox('','กรุณาเลือก ประเภทงาน','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#fmi_imp_title").val() == ""){
                        toggleAlertBox('','กรุณาเลือก คำนำหน้า','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#fmi_imp_gender").val() == ""){
                        toggleAlertBox('','กรุณาเลือก เพศ','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#fmi_imp_stat").val() == ""){
                        toggleAlertBox('','กรุณาเลือก สถานะ','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#chk_imps").val() == "y"){
                        if($("#fmi_imp_laws").val() == ""){
                            toggleAlertBox('','กรุณาเลือก ข้อหาที่ถูกจับกุม','fa fa-warning','orange');
                            $.LoadingOverlay("hide");
                            return false;
                        }else if($("#fmi_imp_det").val() == ""){
                            toggleAlertBox('','กรุณาเลือก สถานที่ควบคุมตัว','fa fa-warning','orange');
                            $.LoadingOverlay("hide");
                            return false;
                        }
                    }
                }
                return result;
            }

            function confirmCloseModal(){
                $.confirm({
                    title: '',
                    content: 'ต้องการยกเลิกรายการนี้?',
                    icon: 'fa fa-times-circle',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก'
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-red',
                            action: function(){
                                $('#frm_imp').modal('hide');
                                $(".modal-body input").val("");
                                $(".modal-body textarea").val("");
                                $('#frm_imp_main').bootstrapValidator('resetForm', true);
                                document.getElementById('chk_drf').checked = false;
                            }
                        }
                    }
                });
            }

            function calculateAge(obj){
                var stringDate = obj.value.split("/");
                var today = new Date();
                var birthDate = new Date(stringDate[2], stringDate[1] - 1, stringDate[0]);
                var age = today.getFullYear() - birthDate.getFullYear();
                $('#fmi_imp_age').val(age);
            }

            $('document').ready(function(){
            	var wHeight = 300;
                $(".remark_page").height(wHeight);
                $('#remark_content').slimScroll({
                    height: wHeight + 'px',
                    width: 790 + 'px'
                });

                $("#fmi_imp_refdoc").change(function() {
                    var uniquenum_pri = $("#fmi_imp_refdoc").val();
                    toggleRefDoc(uniquenum_pri);
                });
            });

            function updateRemark(){
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                var fmi_remark = $("#fmi_imp_remark").val();
                // Show full page LoadingOverlay

                if(fmi_remark.length > 1){
                    $.LoadingOverlay("show");
                    $.ajax({
                        type: 'post',
                        url: 'imp_update_remark_oup.php',
                        data: $('form').serialize(),
                        success: function () {
                            $('#fmi_imp_remark').val('');
                            jQuery("#remark_content").load("inc_imp_remark_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&uniquenum_pri=" + uniquenum_pri);
                            $.LoadingOverlay("hide");
                        }
                    });
                }else{
                    toggleAlertBox('','กรุณากรอกข้อมูล','fa fa-warning','red');
                }
            }

            function toggleRefDoc(uniquenum_pri){
                var tag_usage = 'imp_refdoc';
                if(uniquenum_pri != ''){
                    $.LoadingOverlay("show");
                    $.ajax({
                        url: '../control/inc_ajax_text.php',
                        data: "tag_usage=" + tag_usage + "&uniquenum_pri=" + uniquenum_pri,
                        dataType: 'text',
                        success: function(data)
                        {
                            if(data){
                                var str = data.split("@@@");
                                var setgen_code = str[0];
                                var setgen_desc = str[1];
                                var file_url = str[2];
                                var notes_memo = str[3];
                                var file_name = str[4];
                                var js_function = "window.open('" + file_url + "');";
                                var url = "download.php?uniquenum_pri=" + uniquenum_pri + "&file_name=" + file_name;
                                $("#imp_refdoc").html('<p><a href="' + url + '" title="' + notes_memo + '"><i class="fa fa-cloud-download" aria-hidden="true"></i> ' + file_name + '</a><p>');
                            }else{
                                $("#imp_refdoc").html('');
                            }

                        }
                    });
                    $.LoadingOverlay("hide");
                }
            }

            function format(input, format, sep) {
                var output = "";
                var idx = 0;
                for (var i = 0; i < format.length && idx < input.length; i++) {
                    output += input.substr(idx, format[i]);
                    if (idx + format[i] < input.length) output += sep;
                    idx += format[i];
                }

                output += input.substr(idx);

                return output;
            }
            $(document).ready(function() {
                $('#fmi_imp_idcard').keyup(function() {
                    var str = $(this).val().replace(/-/g, ""); // remove hyphens
                    // You may want to remove all non-digits here
                    // var foo = $(this).val().replace(/\D/g, "");
                    if (str.length > 0) {
                        str = format(str, [1, 4, 5, 2, 1], "-");
                    }
                    $(this).val(str);
                });
            });

            function chkImprisonCategory(){
                var fmi_imp_cate_unique = $('#fmi_imp_cate').val();
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=chk_imps&uniquenum_pri=" + fmi_imp_cate_unique,
                     dataType: 'text',
                     success: function(data){
                          if(data){
                              $("#chk_imps").val(data);
                          }
                    }
                });
            }
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_imp" tabindex="-1" role="dialog" aria-labelledby="frm_imp_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <?php require_once 'inc_main_form_heading.php';?>
                    <form class="form-horizontal" role="form" id="frm_imp_main" enctype="multipart/form-data">
                        <fieldset>
                        <!-- Modal Body -->
                        <div id="modal-body" class="modal-body">
                            <div id="wrapper">
                                <!-- Sidebar -->
                                <?php require_once 'inc_toggle_side_nav_main.php';?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="fmi_imp_cate">ประเภทงาน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-3">
                                        <?php $gencode = new GencodeSboxList(
                                                                              $gencode->fld_name = "fmi_imp_cate",
                                                                              $gencode->fld_usage = "imp_cate",
                                                                              $gencode->fld_label = "ประเภทงาน",
                                                                              $gencode->fld_unique = "",
                                                                              $gencode->fld_size = "",
                                                                              $gencode->fld_readmode = "",
                                                                              $gencode->fld_onclick = "",
                                                                              $gencode->fld_onblur = "",
                                                                              $gencode->fld_list = "desc",
                                                                              $gencode->fld_disp = "desc",
                                                                              $gencode->fld_js = 'toggleSboxList("fmi_imp_stat","var_25_004","imp_stat","fmi_imp_cate");chkImprisonCategory();',
                                                                              $gencode->fld_ref_uniq = ""
                                                                          );
                                        ?>
                                        <input type="hidden" id="chk_imps" name="chk_imps" value="n">
                                    </div>
                                    <label class="col-sm-2 control-label" for="fmi_imp_cate">สังกัด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-3">
                                        <?php $gencode = new GencodeSboxList(
                                                                              $gencode->fld_name = "fmi_imp_dept",
                                                                              $gencode->fld_usage = "sys_user_dept",
                                                                              $gencode->fld_label = "สังกัด",
                                                                              $gencode->fld_unique = "",
                                                                              $gencode->fld_size = "",
                                                                              $gencode->fld_readmode = "",
                                                                              $gencode->fld_onclick = "",
                                                                              $gencode->fld_onblur = "",
                                                                              $gencode->fld_list = "desc",
                                                                              $gencode->fld_disp = "desc",
                                                                              $gencode->fld_js = 'toggleSboxList("fmi_imp_stat","var_25_004","imp_stat","fmi_imp_cate")',
                                                                              $gencode->fld_ref_uniq = ""
                                                                          );
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div id="imp_tab" class="container">
                                        <ul  class="nav nav-pills">
                                    		<li class="active">
                                                <a  href="#1b" data-toggle="tab">
                                                    <i class="fa fa-address-card-o" aria-hidden="true" style=" vertical-align: middle;"></i>&nbsp;<b>ข้อมูลส่วนตัว >>></b>
                                                </a>
                                    		</li>
                                    		<li>
                                                <a href="#2b" data-toggle="tab">
                                                    <i class="fa fa-universal-access" aria-hidden="true" style=" vertical-align: middle;"></i>&nbsp;<b>รายละเอียดการถูกจับกุม >>></b>
                                                </a>
                                    		</li>
                                    		<li>
                                                <a href="#3b" data-toggle="tab">
                                                    <i class="fa fa-paper-plane" aria-hidden="true" style=" vertical-align: middle;"></i>&nbsp;<b>สถานะและหมายเหตุ >>></b>
                                                </a>
                                    		</li>
                                    		<li>
                                                <a href="#4b" data-toggle="tab">
                                                    <i class="fa fa-money" aria-hidden="true" style=" vertical-align: middle;"></i>
                                                    &nbsp;<b>เงินทดรองราชการ >>></b>
                                                </a>
                                    		</li>
                                    	</ul>
                                        <div class="tab-content clearfix">
                                            <div class="tab-pane active" id="1b">
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_title">คำนำหน้า <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <select name="fmi_imp_title" id="fmi_imp_title" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                                        <option value="">กรุณาเลือก</option>
                                                        <option value="mr">นาย</option>
                                                        <option value="ms">นางสาว</option>
                                                        <option value="mrs">นาง</option>
                                                        <option value="master">เด็กชาย</option>
                                                        <option value="miss">เด็กหญิง</option>
                                                    </select>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_<?php echo $_GET["fromtrans"];?>_autonum">หมายเลขรายการ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <?php $trans_autonum = new SysTransNum($trans_autonum->fromtrans = $_GET["fromtrans"]);?>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_firstname">ชื่อ (ภาษาไทย) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_firstname" maxlength="50" class="form-control required" id="fmi_imp_firstname" placeholder="ชื่อ (ภาษาไทย)"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_lastname">นามสกุล (ภาษาไทย) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_lastname" maxlength="50" class="form-control required" id="fmi_imp_lastname" placeholder="นามสกุล (ภาษาไทย)"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_firstname_2">ชื่อ (ภาษาอังกฤษ) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_firstname_2" maxlength="50" class="form-control required" id="fmi_imp_firstname_2" placeholder="ชื่อ (ภาษาอังกฤษ)"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_lastname_2">นามสกุล (ภาษาอังกฤษ) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_lastname_2" maxlength="50" class="form-control required" id="fmi_imp_lastname_2" placeholder="นามสกุล (ภาษาอังกฤษ)"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_birthdate">วัน/เดือน/ปี เกิด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                        <div class='input-group date' id='fmi_imp_birthdate_sel'>
                                                            <input type='text' class="form-control" id="fmi_imp_birthdate" name="fmi_imp_birthdate" placeholder="วัน/เดือน/ปี" onblur="calculateAge(this);"/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                        <script type="text/javascript">
                                                            $(function () {
                                                                $('#fmi_imp_birthdate_sel').datetimepicker({
                                                                    format: 'DD/MM/YYYY',
                                                                    defaultDate: '1976-01-01'
                                                                });
                                                            });
                                                        </script>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_age">อายุ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_age" maxlength="50" class="form-control required" id="fmi_imp_age" placeholder="อายุ"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_gender">เพศ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <select name="fmi_imp_gender" id="fmi_imp_gender" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                                        <option value="">กรุณาเลือก</option>
                                                        <option value="m">ชาย</option>
                                                        <option value="f">หญิง</option>
                                                    </select>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_idcard">หมายเลขประจำตัวประชาชน<i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_idcard" maxlength="17" class="form-control required" id="fmi_imp_idcard" placeholder="หมายเลขประจำตัวประชาชน" onblur="chkExistImpris(this);"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_imp_bdr_pass">หนังสือผ่านแดน (Border Pass) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="fmi_imp_bdr_pass" maxlength="50" class="form-control required" id="fmi_imp_bdr_pass" placeholder="หนังสือผ่านแดน (Border Pass)" onblur="chkExistImpris(this);"/>
                                                    </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_passport">หมายเลขหนังสือเดินทาง <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_passport" maxlength="50" class="form-control required" id="fmi_imp_passport" placeholder="หมายเลขหนังสือเดินทาง" onblur="chkExistImpris(this);"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_imp_ci">หมายเลขหนังสือสำคัญประจำตัว (C.I.) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="fmi_imp_ci" maxlength="50" class="form-control required" id="fmi_imp_ci" placeholder="หมายเลขหนังสือสำคัญประจำตัว (C.I.)" onblur="chkExistImpris(this);"/>
                                                    </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_temp_passport">หมายเลขหนังสือเดินทางชั่วคราว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_temp_passport" maxlength="50" class="form-control required" id="fmi_imp_temp_passport" placeholder="หมายเลขหนังสือเดินทางชั่วคราว" onblur="chkExistImpris(this);"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_addrr">ภูมิลำเนา <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <textarea class="form-control required" name="fmi_imp_addrr" id="fmi_imp_addrr" rows="2" placeholder="ภูมิลำเนา"></textarea>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_subdistrict">ตำบล <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" id="fmi_imp_subdistrict" name="fmi_imp_subdistrict" maxlength="50" class="form-control required" placeholder="ตำบล"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_district">อำเภอ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" id="fmi_imp_district" name="fmi_imp_district" maxlength="50" class="form-control required" placeholder="อำเภอ"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_province">จังหวัด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" id="fmi_imp_province" name="fmi_imp_province" maxlength="50" class="form-control required" placeholder="จังหวัด"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_zipcode">รหัสไปรษณีย์ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" id="fmi_imp_zipcode" name="fmi_imp_zipcode" maxlength="50" class="form-control required" placeholder="รหัสไปรษณีย์"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_telephone">หมายเลขติดต่อญาติ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" id="fmi_imp_telephone" name="fmi_imp_telephone" maxlength="50" class="form-control required" placeholder="หมายเลขติดต่อญาติ"/>
                                                  </div>
                                                </div>
                                                <script type="text/javascript">
                                                        $.Thailand({
                                                            database: '../../folder_script/jquery.Thailand.js/database/db.json',
                                                            $district: $('#fmi_imp_subdistrict'),
                                                            $amphoe: $('#fmi_imp_district'),
                                                            $province: $('#fmi_imp_province'),
                                                            $zipcode: $('#fmi_imp_zipcode')
                                                        });
                                                </script>
                                            </div>
                                            <div class="tab-pane fade" id="2b">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_imp_prisdate">วันที่ถูกจับกุม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                        <div class='input-group date' id='fmi_imp_prisdate_sel'>
                                                              <input type='text' class="form-control" id="fmi_imp_prisdate" name="fmi_imp_prisdate" placeholder="วัน/เดือน/ปี"/>
                                                              <span class="input-group-addon">
                                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                              </span>
                                                          </div>
                                                          <script type="text/javascript">
                                                              $(function () {
                                                                  $('#fmi_imp_prisdate_sel').datetimepicker({
                                                                      format: 'DD/MM/YYYY'
                                                                  });
                                                              });
                                                          </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_reg_cate">ประเภทข้อหาที่ถูกจับกุม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                        <?php $gencode = new GencodeSboxList(
                                                                                              $gencode->fld_name = "fmi_reg_cate",
                                                                                              $gencode->fld_usage = "reg_cate",
                                                                                              $gencode->fld_label = "ประเภทข้อหาที่ถูกจับกุม",
                                                                                              $gencode->fld_unique = "",
                                                                                              $gencode->fld_size = "",
                                                                                              $gencode->fld_readmode = "",
                                                                                              $gencode->fld_onclick = "",
                                                                                              $gencode->fld_onblur = "",
                                                                                              $gencode->fld_list = "desc",
                                                                                              $gencode->fld_disp = "desc",
                                                                                              $gencode->fld_js = 'toggleSboxList("fmi_imp_laws","var_25_002","reg_list","fmi_reg_cate")',
                                                                                              $gencode->fld_ref_uniq = ""
                                                                                          );
                                                        ?>
                                                    </div>
                                                    <label class="col-sm-2 control-label" for="fmi_imp_laws">ข้อหาที่ถูกจับกุม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                      <?php $gencode = new GencodeSboxList(
                                                                                            $gencode->fld_name = "fmi_imp_laws",
                                                                                            $gencode->fld_usage = "reg_list",
                                                                                            $gencode->fld_label = "ข้อหาที่ถูกจับกุม",
                                                                                            $gencode->fld_unique = "",
                                                                                            $gencode->fld_size = "",
                                                                                            $gencode->fld_readmode = "",
                                                                                            $gencode->fld_onclick = "",
                                                                                            $gencode->fld_onblur = "",
                                                                                            $gencode->fld_list = "desc",
                                                                                            $gencode->fld_disp = "desc",
                                                                                            $gencode->fld_js = "",
                                                                                            $gencode->fld_ref_uniq = ""
                                                                                        );
                                                      ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_pris_cate">ประเภทสถานที่ควบคุมตัว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                        <?php $gencode = new GencodeSboxList(
                                                                                              $gencode->fld_name = "fmi_pris_cate",
                                                                                              $gencode->fld_usage = "pris_cate",
                                                                                              $gencode->fld_label = "ประเภทสถานที่ควบคุมตัว",
                                                                                              $gencode->fld_unique = "",
                                                                                              $gencode->fld_size = "",
                                                                                              $gencode->fld_readmode = "",
                                                                                              $gencode->fld_onclick = "",
                                                                                              $gencode->fld_onblur = "",
                                                                                              $gencode->fld_list = "desc",
                                                                                              $gencode->fld_disp = "desc",
                                                                                              $gencode->fld_js = 'toggleSboxList("fmi_imp_det","var_25_003","pris_det","fmi_pris_cate")',
                                                                                              $gencode->fld_ref_uniq = ""
                                                                                          );
                                                        ?>
                                                    </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_det">สถานที่ควบคุมตัว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <?php $gencode = new GencodeSboxList(
                                                                                            $gencode->fld_name = "fmi_imp_det",
                                                                                            $gencode->fld_usage = "pris_det",
                                                                                            $gencode->fld_label = "สถานที่ควบคุมตัว",
                                                                                            $gencode->fld_unique = "",
                                                                                            $gencode->fld_size = "",
                                                                                            $gencode->fld_readmode = "",
                                                                                            $gencode->fld_onclick = "",
                                                                                            $gencode->fld_onblur = "",
                                                                                            $gencode->fld_list = "desc",
                                                                                            $gencode->fld_disp = "desc",
                                                                                            $gencode->fld_js = "",
                                                                                            $gencode->fld_ref_uniq = ""
                                                                                        );
                                                      ?>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_refdoc">เลขที่เอกสาร <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <?php $gencode = new GencodeSboxList(
                                                                                            $gencode->fld_name = "fmi_imp_refdoc",
                                                                                            $gencode->fld_usage = "impr_doc",
                                                                                            $gencode->fld_label = "เลขที่เอกสาร",
                                                                                            $gencode->fld_unique = "",
                                                                                            $gencode->fld_size = "",
                                                                                            $gencode->fld_readmode = "",
                                                                                            $gencode->fld_onclick = "",
                                                                                            $gencode->fld_onblur = "",
                                                                                            $gencode->fld_list = "code",
                                                                                            $gencode->fld_disp = "code",
                                                                                            $gencode->fld_js = "",
                                                                                            $gencode->fld_ref_uniq = ""
                                                                                        );
                                                      ?>
                                                  </div>
                                                  <div class="col-sm-7">
                                                    <div id="imp_refdoc"></div>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="3b">
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_stat">สถานะ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <?php $gencode = new GencodeSboxList(
                                                                                          $gencode->fld_name = "fmi_imp_stat",
                                                                                          $gencode->fld_usage = "imp_stat",
                                                                                          $gencode->fld_label = "กรุณาเลือก",
                                                                                          $gencode->fld_unique = "",
                                                                                          $gencode->fld_size = "",
                                                                                          $gencode->fld_readmode = "",
                                                                                          $gencode->fld_onclick = "",
                                                                                          $gencode->fld_onblur = "",
                                                                                          $gencode->fld_list = "desc",
                                                                                          $gencode->fld_disp = "desc",
                                                                                          $gencode->fld_js = "",
                                                                                          $gencode->fld_ref_uniq = ""
                                                                                      );
                                                    ?>
                                                  </div>
                                                  <div class="col-sm-3">
                                                  </div>
                                                </div>
                                                <div class="form-group remark_page" id="imp_remark1">
                                                    <label class="col-sm-2 control-label" for="fmi_imp_remark">หมายเหตุ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                      <div class="col-sm-6 content" id="remark_content"></div>
                                                </div>
                                                <div class="form-group" id="imp_remark2">
                                                    <div class="col-sm-6"></div>
                                                  <div class="col-sm-3">
                                                      <textarea class="form-control" rows="5" id="fmi_imp_remark" name="fmi_imp_remark" placeholder="อัพเดทหมายเหตุที่นี่..."></textarea>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <button type="button" class="btn btn-warning" onclick="updateRemark();return false;" title="บันทึกหมายเหตุ"><i class="fa fa-paper-plane" aria-hidden="true"></i> บันทึกหมายเหตุ</button>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="4b">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="fmi_imp_cshbrw_date">วันที่ยืม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                    <div class="col-sm-3">
                                                          <div class='input-group date' id='fmi_imp_cshbrw_date_sel'>
                                                              <input type='text' class="form-control" id="fmi_imp_cshbrw_date" name="fmi_imp_cshbrw_date" placeholder="วัน/เดือน/ปี"/>
                                                              <span class="input-group-addon">
                                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                              </span>
                                                          </div>
                                                          <script type="text/javascript">
                                                              $(function () {
                                                                  $('#fmi_imp_cshbrw_date_sel').datetimepicker({
                                                                      format: 'DD/MM/YYYY'
                                                                  });
                                                              });
                                                          </script>
                                                    </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_cshbrw_amt">จำนวนเงินทดรอง (RM) <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_cshbrw_amt" maxlength="50" class="form-control required" id="fmi_imp_cshbrw_amt" placeholder="0.00"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_imp_cshbrw_stat">สถานะการชดใช้เงิน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <select name="fmi_imp_cshbrw_stat" id="fmi_imp_cshbrw_stat" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                                          <option value="n">กรุณาเลือก</option>
                                                          <option value="s">ยังไม่ได้รับคืนจากกระทรวงฯ</option>
                                                          <option value="y">ได้รับคืนจากกระทรวงฯ แล้ว</option>
                                                      </select>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_imp_cshbrw_ref">หมายเลขอ้างอิง <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_imp_cshbrw_ref" maxlength="50" class="form-control required" id="fmi_imp_cshbrw_ref" placeholder="หมายเลขอ้างอิง"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <div class="col-sm-12">
                                                    <label class="col-sm-2 control-label badge-icon-text">ฟอร์มสัญญารับสภาพหนี้</label>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_mth_amt">จำนวนเงินผ่อนชำระรายเดือน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_acpt_mth_amt" maxlength="50" class="form-control required" id="fmi_acpt_mth_amt" placeholder="จำนวนเงินผ่อนชำระรายเดือน"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_paymt_method">ช่องทางการชำระเงิน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_acpt_paymt_method" maxlength="50" class="form-control required" id="fmi_acpt_paymt_method" placeholder="ช่องทางการชำระเงิน"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_issue">ผู้ให้สัญญา <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_acpt_issue" maxlength="50" class="form-control required" id="fmi_acpt_issue" placeholder="ผู้ให้สัญญา"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_recv">ผู้รับสัญญา <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_acpt_recv" maxlength="50" class="form-control required" id="fmi_acpt_recv" placeholder="ผู้รับสัญญา"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_witness1">พยาน 1 <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_acpt_witness1" maxlength="50" class="form-control required" id="fmi_acpt_witness1" placeholder="พยาน 1"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_acpt_witness2">พยาน 2 <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_acpt_witness2" maxlength="50" class="form-control required" id="fmi_acpt_witness2" placeholder="พยาน 2"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <div class="col-sm-12">
                                                    <label class="col-sm-2 control-label badge-icon-text">ฟอร์มสัญญารับรองการชดใช้เงินคืน</label>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_trnsprt">ค่ายานพาหนะ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_pymt_trnsprt" maxlength="50" class="form-control required" id="fmi_pymt_trnsprt" placeholder="ค่ายานพาหนะ"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_lodg">ค่าอาหาร/การเดินทาง <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_pymt_lodg" maxlength="50" class="form-control required" id="fmi_pymt_lodg" placeholder="ค่าอาหาร/การเดินทาง"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_accmd">ค่าเช่าที่พัก <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_pymt_accmd" maxlength="50" class="form-control required" id="fmi_pymt_accmd" placeholder="ค่าเช่าที่พัก"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_oths">ค่าใช้จ่ายอื่น ๆ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_pymt_oths" maxlength="50" class="form-control required" id="fmi_pymt_oths" placeholder="ค่าใช้จ่ายอื่น ๆ"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_issue">ผู้ทำสัญญารับรอง <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_pymt_issue" maxlength="50" class="form-control required" id="fmi_pymt_issue" placeholder="ผู้ทำสัญญารับรอง"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_officer">เจ้าหน้าที่ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_pymt_officer" maxlength="50" class="form-control required" id="fmi_pymt_officer" placeholder="เจ้าหน้าที่"/>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_witness1">พยาน 1 <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                    <input type="text" name="fmi_pymt_witness1" maxlength="50" class="form-control required" id="fmi_pymt_witness1" placeholder="พยาน 1"/>
                                                  </div>
                                                  <label class="col-sm-2 control-label" for="fmi_pymt_witness2">พยาน 2 <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                                  <div class="col-sm-3">
                                                      <input type="text" name="fmi_pymt_witness2" maxlength="50" class="form-control required" id="fmi_pymt_witness2" placeholder="พยาน 2"/>
                                                  </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 material-switch pull-right">
                                    รายการร่าง <i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;
                                <input id="chk_drf" name="chk_drf" type="checkbox"/>
                                <label for="chk_drf" class="label-info"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <!-- Modal Footer -->
                        <?php require_once 'inc_main_form_footer.php';?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
