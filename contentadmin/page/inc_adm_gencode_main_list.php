<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$SysListMain = new SysListMain();
$DatabaseOperation = new DatabaseOperation();
$TransactionGenUnique = new TransactionGenUnique();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table width="100%" class="table table-striped table-bordered table-list">
        <tr>
            <th class="text-center"><em class="fa fa-cog"></em></th>
            <th class="hidden-xs">ลำดับ</th>
            <th <?php if($_GET["fromtrans"] == 'imp_vist'){ echo "style='display:none'";}?>>รหัส</th>
            <th <?php if($_GET["fromtrans"] == 'imp_vist'){ echo "style='display:none'";}?>>คำอธิบาย</th>
            <th <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>วันที่เข้าเยี่ยม</th>
            <th <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>สถานที่ควบคุมตัว</th>
            <th <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>รวมเพศชาย</th>
            <th <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>รวมเพศหญิง</th>
            <th <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>รวมทั้งหมด</th>
        </tr>
        <?php
            $uniquenum_pri = '';
            if(isset($_GET["page"])){
                $page = $_GET["page"];
            }else{
                $page = 1;
            }
            if(isset($_GET["search_query"])){
                $search_query = $_GET["search_query"];
            }else{
                $search_query = "";
            }
            $limit = PAGE_LIMIT;
            $page_start = ($page-1) * $limit;
            $cnt = 0;

            $qs_result = $SysListMain->getRowGencodeResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
            if($search_query != ""){
                $total_records = $qs_result->rowCount();
            }else{
                $total_records = $DatabaseOperation->getTotalRecords($tablename,$_GET['fromtrans']);
            }
            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                $cnt++;
        ?>
        <tr>
            <td width="140" align="center">
                <input type="hidden" id="r_sys_autonum<?php echo $cnt;?>" name="r_sys_autonum<?php echo $cnt;?>" value="<?php echo $rows["gencode_code"];?>">
                <a data-toggle="modal" data-target="#frm_adm_gencode" class="row_upd btn btn-success" title="แก้ไขหมายเลขรายการ <?php echo $rows["gencode_code"];?>" data-id="<?php echo $rows["gencode_uniq"];?>" data-frommode="edit" href="#frm_adm_gencode"><em class="fa fa-pencil" style="color: #FFFFFF"></em></a>
                <a class="row_del btn btn-danger" title="ลบหมายเลขรายการ <?php echo $rows["gencode_code"];?>" data-id="<?php echo $rows["gencode_uniq"];?>" data-frommode="delete" onclick="confirmDelete('<?php echo $cnt;?>','<?php echo $rows["gencode_uniq"];?>');"><em class="fa fa-trash"></em></a>
                <a data-toggle="modal" data-target="#frm_imp_attchmt" class="row_upd btn btn-warning" title="แนบเอกสารหมายเลขรายการ <?php echo $rows["gencode_code"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="upload" data-impname="<?php echo $rows["gencode_code"]." ".$rows["gencode_desc"];?>" <?php if($fromtrans != 'imp_vist'){ echo "style='display:none'"; } ?>><em class="fa fa-paperclip" style="color: #FFFFFF"></em></a>
            </td>
            <td class="hidden-xs"><?php echo $cnt;?></td>
            <td <?php if($_GET["fromtrans"] == 'imp_vist'){ echo "style='display:none'";}?>><?php echo $rows["gencode_code"];?></td>
            <td <?php if($_GET["fromtrans"] == 'imp_vist'){ echo "style='display:none'";}?>><?php echo $rows["gencode_desc"];?></td>
            <td <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>><?php echo $rows["imp_vist_date"];?></td>
            <td <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>><?php echo $rows["imp_det_desc"];?></td>
            <td <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>><?php echo number_format($rows["imp_tot_male"]);?></td>
            <td <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>><?php echo number_format($rows["imp_tot_female"]);?></td>
            <td <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>><?php echo $rows["imp_tot_male"] + $rows["imp_tot_female"];?></td>
        </tr>
        <?php
            } /* End while loop $qs_result */
        ?>
        </table>
    </body>
</html>
