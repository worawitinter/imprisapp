<!-- inc_rep_listmain.php -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6"></div>
                <div class="col col-xs-6 text-right">
                    <select class="selectpicker" data-dropup-auto="false" data-style="btn-default btn-xs" id="fmi_rep_export" name="fmi_rep_export">
                        <option value="" selected>เลือกประเภทเอกสาร</option>
                        <option value="excel">Excel</option>
                        <option value="pdf">PDF</option>
                    </select>
                </div>
            </div>
        </div>
    </body>
</html>
