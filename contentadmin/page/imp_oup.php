<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170813             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysAutoNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';

$DatabaseOperation = new DatabaseOperation();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();
$SysAutoNum = new SysAutoNum();
$AuditTrailLog = new AuditTrailLog();
$TransactionGenUnique = new TransactionGenUnique();

$tablename = "tran_record_main";
$imp_firstname = $_POST["fmi_imp_firstname"];
$imp_lastname = $_POST["fmi_imp_lastname"];
$imp_firstname_2 = $_POST["fmi_imp_firstname_2"];
$imp_lastname_2 = $_POST["fmi_imp_lastname_2"];
$imp_birthdate = $SysConversion->convertDate($_POST["fmi_imp_birthdate"]);
$imp_age = $_POST["fmi_imp_age"];
$imp_gender = $_POST["fmi_imp_gender"];
$imp_title = $_POST["fmi_imp_title"];
$imp_idcard = $_POST["fmi_imp_idcard"];
$imp_passport = $_POST["fmi_imp_passport"];
$imp_addrr = $_POST["fmi_imp_addrr"];
$imp_subdistrict = $_POST["fmi_imp_subdistrict"];
$imp_district = $_POST["fmi_imp_district"];
$imp_province = $_POST["fmi_imp_province"];
$imp_zipcode = $_POST["fmi_imp_zipcode"];
$imp_telephone = $_POST["fmi_imp_telephone"];
$imp_det = $_POST["fmi_imp_det"];
$imp_prisdate = $SysConversion->convertDate($_POST["fmi_imp_prisdate"]);
$imp_laws = $_POST["fmi_imp_laws"];
$imp_refdoc = $_POST["fmi_imp_refdoc"];
$imp_stat = $_POST["fmi_imp_stat"];
$imp_remark = $_POST["fmi_imp_remark"];
$imp_temp_passport = $_POST["fmi_imp_temp_passport"];
$imp_ci = $_POST["fmi_imp_ci"];
$imp_cate_uniq = $_POST["fmi_imp_cate"];
$imp_bdr_pass = $_POST["fmi_imp_bdr_pass"];
$imp_cshbrw_date = $SysConversion->convertDate($_POST["fmi_imp_cshbrw_date"]);
$imp_dept = $_POST["fmi_imp_dept"];
if($_POST["fmi_imp_cshbrw_amt"] <> ''){
    $imp_cshbrw_amt = $_POST["fmi_imp_cshbrw_amt"];
}else{
    $imp_cshbrw_amt = 0;
}
$imp_cshbrw_stat = $_POST["fmi_imp_cshbrw_stat"];
$imp_cshbrw_ref = $_POST["fmi_imp_cshbrw_ref"];

if($frommode == "new"){
    $date_created = $TransactionGenUnique->generate_trans_datetime();
}else{
    $date_created = $_POST["fmi_date_created"];
}
if($frommode == "new"){
    $sys_autonum = explode("@@@", $_POST["fmi_".$fromtrans."_autonum"]);
    $sys_auto_num_upri = $sys_autonum[1];
    $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
    $new_sysnum = $SysAutoNum->checkDuplicateSysNum($fromtrans,$sys_autonum[0],$sys_auto_num_upri);
    if($new_sysnum == ""){
        $imp_autonum = $sys_autonum[0];
    }else{
        $imp_autonum = $new_sysnum;
    }
}else{
    $imp_autonum = $_POST["fmi_ori_".$fromtrans."_autonum"];
}
if(isset($_POST["chk_drf"])){
    $chk_drf = "y";
}else{
    $chk_drf = "n";
}

$data = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_pri"=>$uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                    "desc_lang01"=>$imp_firstname,
                    "desc_lang02"=>$imp_lastname,
                    "date_001"=>$imp_birthdate,
                    "desc_lang03"=>$imp_age,
                    "desc_lang04"=>$imp_gender,
                    "desc_lang05"=>$imp_addrr,
                    "desc_lang06"=>$imp_subdistrict,
                    "desc_lang07"=>$imp_district,
                    "desc_lang08"=>$imp_province,
                    "desc_lang09"=>$imp_zipcode,
                    "desc_lang10"=>$imp_idcard,
                    "var_25_005"=>$imp_det,
                    "var_25_001"=>$imp_telephone,
                    "date_002"=>$imp_prisdate,
                    "var_25_002"=>$imp_laws,
                    "var_25_003"=>$imp_refdoc,
                    "var_25_004"=>$imp_stat,
                    "notes_memo"=>$imp_remark,
                    "var_50_001"=>$imp_title,
                    "var_50_002"=>$imp_passport,
                    "var_50_003"=>$imp_autonum,
                    "var_50_004"=>$imp_firstname_2,
                    "var_50_005"=>$imp_lastname_2,
                    "var_100_001"=>$imp_temp_passport,
                    "var_100_002"=>$imp_ci,
                    "var_100_003"=>$imp_cate_uniq,
                    "var_100_004"=>$imp_bdr_pass,
                    "date_003"=>$imp_cshbrw_date,
                    "num_02_d_001"=>$imp_cshbrw_amt,
                    "tag_other01_yn"=>$imp_cshbrw_stat,
                    "var_100_005"=>$imp_cshbrw_ref,
                    "var_25_006"=>$imp_dept,
                    "tag_other01_yn"=>$chk_drf
                );

if($frommode == "new"){
    $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
    $SysAutoNum->setSysNum($fromtrans,$imp_autonum,$sys_auto_num_upri,$fromlink,$userid,$date_created);
    $AuditTrailLog->createActivityLogs($frommode,$fromtrans,$fromlink,$imp_autonum,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created);
}elseif($frommode == "edit"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$imp_autonum,$tablename,$uniquenum_pri,$userid);
    $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
    $SysMainActivity->lockRecord($fromlink,$fromtrans,$uniquenum_pri,$userid);
}elseif($frommode == "delete"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$imp_autonum,$tablename,$uniquenum_pri,$userid);
    $SysMainActivity->releaseRecord($fromtrans,$uniquenum_pri);
}
?>
