<!DOCTYPE html>
<!-- inc_sys_autonum_main_from.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .modal-dialog{
                position: relative;
                display: table; /* This is important */
                overflow-y: auto;
                overflow-x: auto;
                width: auto;
                min-width: 600px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#frm_sys_autonum_main').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        fmi_run_number: {
                            validators: {
                                    stringLength: {
                                    min: 2,
                                },
                                    notEmpty: {
                                    message: 'โปรดระบุหมายเลขเริ่มต้น'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                    $('#frm_sys_autonum_main').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        var value=$.trim($('#fmi_fromtrans').val());
                        if(value.length == 0)
                        {
                            //Error Code: 001 value missing in fromtrans url parameter
                            toggleAlertBox('','เกิดความผิดพลาดของระบบ [Error Code: 001]','fa fa-warning','red');
                        }else{
                            $.ajax({
                                type: 'post',
                                url: 'sys_autonum_oup.php',
                                data: $('form').serialize(),
                                success: function () {
                                    $('#frm_sys_autonum').modal('hide');
                                    $('.modal-body input').val('');
                                    jQuery("#target-content").load("inc_sys_autonum_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                    jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                }
                            });
                        }
                    });
                });
            });

            function confirmCloseModal(){
                $.confirm({
                    title: '',
                    content: 'ต้องการยกเลิกรายการนี้?',
                    icon: 'fa fa-times-circle',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก'
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-red',
                            action: function(){
                                $('#frm_sys_autonum').modal('hide');
                                $(".modal-body input").val("");
                                $('#frm_sys_autonum_main').bootstrapValidator('resetForm', true);
                            }
                        }
                    }
                });
            }
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_sys_autonum" tabindex="-1" role="dialog" aria-labelledby="frm_sys_autonum_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <?php require_once 'inc_main_form_heading.php';?>
                    <form class="form-horizontal" role="form" id="frm_sys_autonum_main" enctype="multipart/form-data">
                        <fieldset>
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <div id="wrapper">
                                <!-- Sidebar -->
                                <?php require_once 'inc_toggle_side_nav_main.php';?>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label" for="fmi_prefix_code">คำขึ้นต้น <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-8">
                                      <input type="text" name="fmi_prefix_code" size="15" maxlength="50" class="form-control required"
                                      id="fmi_prefix_code" placeholder="คำขึ้นต้น" onblur="dispSysAutoNumFormat();"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label" for="fmi_run_number">หมายเลขเริ่มต้น <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-8">
                                      <input type="text" name="fmi_run_number" maxlength="100" class="form-control required"
                                          id="fmi_run_number" placeholder="หมายเลขเริ่มต้น" onblur="dispSysAutoNumFormat();"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label" for="fmi_suffix_code">คำลงท้าย <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-8">
                                      <input type="text" name="fmi_suffix_code" maxlength="100" class="form-control required"
                                          id="fmi_suffix_code" placeholder="คำลงท้าย" onblur="dispSysAutoNumFormat();"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">ตัวอย่าง <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-8">
                                      <input id="rs_autonum" name="rs_autonum" type="text" class="form-control required" value="" placeholder="ตัวอย่าง" readonly disabled/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">ตั้งค่าเพิ่มเติม <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-4">
                                      <div class="material-switch pull-right">
                                        แทรกลำดับเดือน/ปี&nbsp;
                                        <input id="chk_mth_year" name="chk_mth_year" type="checkbox" onclick="dispSysAutoNumFormat();"/>
                                        <label for="chk_mth_year" class="label-info"></label>
                                      </div><br><br>
                                      <div class="material-switch pull-right">
                                        สถานะใช้งาน&nbsp;
                                        <input id="chk_active" name="chk_active" type="checkbox"/>
                                        <label for="chk_active" class="label-info"></label>
                                      </div><br><br>
                                      <div class="material-switch pull-right">
                                        ค่าเริ่มต้น&nbsp;
                                        <input id="chk_default" name="chk_default" type="checkbox"/>
                                        <label for="chk_default" class="label-info"></label>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Footer -->
                        <?php require_once 'inc_main_form_footer.php';?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
