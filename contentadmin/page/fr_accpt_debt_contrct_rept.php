<!-- fr_accpt_debt_contrct_rept.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#frm_rep').modal('show');
            });

            $(document).ready(function() {
                var frommode = "search";
                var fromtrans = $("#fmi_fromtrans").val();
                $(".modal-content #fmi_frommode").val( frommode );
                $('.modal-body input').val('');
                $('.modal-body textarea').val('');
                $("#menu-toggle").hide();
                $("#sidebar-wrapper").hide();
                $('#frm_submit').text('ตกลง');
                parseModalHeaderDesc();
                document.getElementById('frm_footer').style.display = '';

                $('.selectpicker').selectpicker({
                    iconBase: 'fa'
                });
            });
        </script>
    </head>
    <body>
        <?php require_once 'inc_rep_main_form.php';?>
        <div id="pnl_list" class="card" style="display:none;width: <?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <?php
                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
            ?>
            <div class="container" style="width:auto;">
                <div class="row">
                    <div class="col-md-12">
                        <?php require_once 'inc_rep_listmain.php';?>
                        <div id="target-content" />
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
