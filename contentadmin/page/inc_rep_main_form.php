<!DOCTYPE html>
<!-- inc_rep_main_from.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .modal-dialog{
                position: relative;
                display: table; /* This is important */
                overflow-y: auto;
                overflow-x: auto;
                width: auto;
                min-width: 600px;
            }
        </style>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#frm_rep').modal('show');
                $("#frm_print").hide();
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_lock").hide();
                $("#frm_unlock").hide();
                $("#frm_cancel").hide();
                $("#frm_submit").hide();
                $('#frm_submit').attr('disabled',false);
                $('#frm_cancel').attr('disabled',false);
            });

            $(document).ready(function() {
                $('#frm_rep_main').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    }
                })
                .on('success.form.bv', function(e) {
                    $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                    $('#frm_rep_main').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.LoadingOverlay("show");
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        var value=$.trim($('#fmi_fromtrans').val());
                        var datefrom=$.trim($('#fmi_date_from').val());
                        var dateto=$.trim($('#fmi_date_to').val());
                        var state_code=$.trim($('#fmi_state_code').val());
                        var imp_laws=$.trim($('#fmi_imp_laws').val());
                        var imp_gender=$.trim($('#fmi_imp_gender').val());
                        var imp_det=$.trim($('#fmi_imp_det').val());
                        var imp_stat=$.trim($('#fmi_stat').val());
                        var search_string=$.trim($('#fmi_search_string').val());
                        var rept_output = $.trim($('#fmi_rept_output').val());
                        var provn_class = $.trim($('#fmi_provn_class_disp').val());
                        var date_type = $.trim($('#fmi_date_type').val());
                        var multi_state = $.trim($('#fmi_multi_state_disp').val());
                        var imp_cate = $.trim($('#fmi_imp_cate').val());
                        var imp_cshbrw_stat = $.trim($('#fmi_imp_cshbrw_stat').val());
                        var date_debt_from=$.trim($('#fmi_date_debt_from').val());
                        var date_debt_to=$.trim($('#fmi_date_debt_to').val());
                        if($('#chk_rec_drf').prop("checked") == true){
                            var chk_rec_drf = 'y';
                        }else{
                            var chk_rec_drf = 'n';
                        }
                        
                        var target_url = 'inc_<?php echo $_GET["fromlink"];?><?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&datefrom=' + datefrom + '&dateto=' + dateto + '&state_code=' + state_code + '&imp_laws=' + imp_laws + '&imp_gender=' + imp_gender + '&imp_det=' + imp_det + '&imp_stat=' + imp_stat + '&search_string=' + search_string + '&provn_class=' + provn_class + '&date_type=' + date_type + '&multi_state=' + multi_state + '&imp_cate=' + imp_cate + '&imp_cshbrw_stat=' + imp_cshbrw_stat + '&date_debt_from=' + date_debt_from + '&date_debt_to=' + date_debt_to + '&chk_rec_drf=' + chk_rec_drf;
                        if(value.length == 0)
                        {
                            //Error Code: 001 value missing in fromtrans url parameter
                            toggleAlertBox('','เกิดความผิดพลาดของระบบ [Error Code: 001]','fa fa-warning','red');
                        }else{
                            $.ajax({
                                type: 'get',
                                url: target_url,
                                success: function () {
                                    $('#frm_rep').modal('hide');
                                    $('#reshow_modal').hide();
                                    $('#frm_rep').on('hidden.bs.modal', function () {
                                        jQuery("#target-content").load(target_url);
                                    });
                                    $('#pnl_list').show();
                                    if(rept_output == 'pdf'){
                                        window.open(target_url + '&rept_output=pdf');
                                    }
                                    $.LoadingOverlay("hide");
                                }

                            });
                        }
                    });
                });
            });

            function confirmCloseModal(){
                $.confirm({
                    title: '',
                    content: 'ต้องการยกเลิกรายการนี้?',
                    icon: 'fa fa-times-circle',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก'
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-red',
                            action: function(){
                                $('#frm_rep').modal('hide');
                                $(".modal-body input").val("");
                                $('#frm_rep_main').bootstrapValidator('resetForm', true);
                            }
                        }
                    }
                });
            }

            function convertDate(inputFormat) {
                function pad(s) { return (s < 10) ? '0' + s : s; }
                var d = new Date(inputFormat);
                return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
            }

            $(window).on('load',function(){
                var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                var firstDay = convertDate(new Date(y, m, 1));
                var lastDay = convertDate(new Date(y, m + 1, 0));
                if($("#fmi_date_from") != 'undefined'){
                    setTimeout(function(){ document.getElementById("fmi_date_from").value = firstDay; }, 60);
                }
                if($("#fmi_date_to") != 'undefined'){
                    setTimeout(function(){ document.getElementById("fmi_date_to").value = lastDay; }, 60);
                }
                if($("#fmi_date_debt_from") != 'undefined'){
                    setTimeout(function(){ document.getElementById("fmi_date_debt_from").value = firstDay; }, 60);
                }
                if($("#fmi_date_debt_to") != 'undefined'){
                    setTimeout(function(){ document.getElementById("fmi_date_debt_to").value = lastDay; }, 60);
                }
            });
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_rep" tabindex="-1" role="dialog" aria-labelledby="frm_rep_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <?php require_once 'inc_main_form_heading.php';?>
                    <form class="form-horizontal" role="form" id="frm_rep_main" enctype="multipart/form-data">
                        <fieldset>
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <div class="row">
                                  <div class="col-xs"><label class="col-sm-3 control-label">วันที่ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4"><input type='text' class="form-control" id="fmi_date_from" name="fmi_date_from" placeholder="วัน/เดือน/ปี เริ่มต้น"/></div>
                                  <div class="col-md-4"><input type='text' class="form-control" id="fmi_date_to" name="fmi_date_to" placeholder="วัน/เดือน/ปี สิ้นสุด"/></div>
                                  <script type="text/javascript">
                                      $(function () {
                                          $('#fmi_date_from').datetimepicker({
                                              format: 'DD/MM/YYYY'
                                          });
                                          $('#fmi_date_to').datetimepicker({
                                              format: 'DD/MM/YYYY'
                                          });
                                      });
                                  </script>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-xs"><label class="col-sm-3 control-label">แสดงตามวันที่ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                <div class="col-md-4">
                                    <select class="selectpicker" data-dropup-auto="false" data-style="btn-default btn-xs" id="fmi_date_type" name="fmi_date_type">
                                        <option value="date_impris" selected>วันที่ถูกจับกุม</option>
                                        <option value="date_trans">วันที่รับแจ้ง</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_debt_contrct_status_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">วันที่ยืม <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4"><input type='text' class="form-control" id="fmi_date_debt_from" name="fmi_date_debt_from" placeholder="วัน/เดือน/ปี ที่ยืม เริ่มต้น"/></div>
                                  <div class="col-md-4"><input type='text' class="form-control" id="fmi_date_debt_to" name="fmi_date_debt_to" placeholder="วัน/เดือน/ปี ที่ยืม สิ้นสุด"/></div>
                                  <script type="text/javascript">
                                      $(function () {
                                          $('#fmi_date_debt_from').datetimepicker({
                                              format: 'DD/MM/YYYY'
                                          });
                                          $('#fmi_date_debt_to').datetimepicker({
                                              format: 'DD/MM/YYYY'
                                          });
                                      });
                                  </script>
                            </div>
                            <div class="row top-buffer">
                                  <div class="col-xs"><label class="col-sm-3 control-label">ประเภทงาน <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_imp_cate",
                                                                            $gencode->fld_usage = "imp_cate",
                                                                            $gencode->fld_label = "ประเภทงาน",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_rec_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">เพศ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select name="fmi_imp_gender" id="fmi_imp_gender" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                          <option value="">กรุณาเลือก</option>
                                          <option value="m">ชาย</option>
                                          <option value="f">หญิง</option>
                                      </select>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] == "fr_impris_by_reg_rept" || $_GET["fromlink"] == "fr_impris_vist_hist_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">รัฐ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select id="fmi_state_disp" class="selectpicker" data-hide-disabled="true" data-live-search="true" onchange="selectList(this);">
                                          <option value="">กรุณาเลือก</option>
                                        <?php
                                          $state_str = file_get_contents("../../folder_script/countries/countries/malaysia.json");
                                          $json_state = json_decode($state_str, true);
                                          foreach ($json_state as $state_name => $json_state) {
                                              $state_value = $json_state['code']."@@@".$json_state['name'];
                                        ?>
                                          <option value="<?php echo $state_value?>"><?php echo $json_state['name']?></option>
                                        <?php
                                          }
                                        ?>
                                      </select>
                                      <input type="hidden" class="form-control required" id="fmi_state_code" name="fmi_state_code" value="">
                                      <input type="hidden" class="form-control required" id="fmi_state_desc" name="fmi_state_desc" value="">
                                      <script type="text/javascript">
                                          function selectList(list){
                                              var val_list = list.value;
                                              var str = val_list.split('@@@');
                                              document.getElementById('fmi_state_code').value = str[0];
                                              document.getElementById('fmi_state_desc').value = str[1];
                                          }
                                      </script>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] == "fr_impris_by_state_rept" || $_GET["fromlink"] == "fr_impris_vist_hist_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">ข้อหาที่ถูกจับกุม <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_imp_laws",
                                                                            $gencode->fld_usage = "reg_list",
                                                                            $gencode->fld_label = "ข้อหาที่ถูกจับกุม",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_rec_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">สถานที่ควบคุมตัว <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_imp_det",
                                                                            $gencode->fld_usage = "pris_det",
                                                                            $gencode->fld_label = "สถานที่ควบคุมตัว",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_rec_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">สถานะ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_stat",
                                                                            $gencode->fld_usage = "imp_stat",
                                                                            $gencode->fld_label = "สถานะ",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_protn_benft_rept" && $_GET["fromlink"] != "fr_impris_vist_hist_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">รัฐ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select name="fmi_multi_state_disp[]" id="fmi_multi_state_disp" class="selectpicker" multiple data-done-button="true">
                                          <?php
                                            $state_str = file_get_contents("../../folder_script/countries/countries/malaysia.json");
                                            $json_state = json_decode($state_str, true);
                                            foreach ($json_state as $state_name => $json_state) {
                                                $state_value = $json_state['code'];
                                          ?>
                                            <option value="'<?php echo $state_value?>'"><?php echo $json_state['name']?></option>
                                          <?php
                                            }
                                          ?>
                                      </select>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_by_reg_rept" && $_GET["fromlink"] != "fr_impris_by_state_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">คนในพื้นที่ ศอ.บต. <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select name="fmi_provn_class_disp[]" id="fmi_provn_class_disp" class="selectpicker" multiple data-done-button="true">
                                        <?php
                                          $provn_str = file_get_contents("../../folder_script/jquery.Thailand.js/database/raw_database/TH_province.json");
                                          $json_provn = json_decode($provn_str, true);
                                          foreach ($json_provn as $provn_name => $json_provn) {
                                                $provn_value = $json_provn['name'];
                                        ?>
                                          <option value="'<?php echo $provn_value?>'"><?php echo $json_provn['name']?></option>
                                        <?php
                                          }
                                        ?>
                                      </select>
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_impris_rec_rept" && $_GET["fromlink"] != "fr_impris_within_reg_rept" && $_GET["fromlink"] != "fr_debt_paymt_contrct_rept" && $_GET["fromlink"] != "fr_accpt_debt_contrct_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">คำค้นหา <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <input type="text" name="fmi_search_string" id="fmi_search_string" class="form-control" placeholder="ป้อนคำค้นหา...">
                                  </div>
                            </div>
                            <div class="row top-buffer" <?php if($_GET["fromlink"] != "fr_debt_contrct_status_rept"){ echo "style='display:none'";}?>>
                                  <div class="col-xs"><label class="col-sm-3 control-label">สถานะการชดใช้เงิน <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select name="fmi_imp_cshbrw_stat" id="fmi_imp_cshbrw_stat" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                          <option value="">แสดงทั้งหมด</option>
                                          <option value="s">ยังไม่ได้รับคืนจากกระทรวงฯ</option>
                                          <option value="y">ได้รับคืนจากกระทรวงฯ แล้ว</option>
                                      </select>
                                  </div>
                            </div>
                            <div class="row top-buffer">
                                  <div class="col-xs"><label class="col-sm-3 control-label">การแสดงผล <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4">
                                      <select class="selectpicker" data-dropup-auto="false" data-style="btn-default btn-xs" id="fmi_rept_output" name="fmi_rept_output">
                                          <option value="" selected>เลือกประเภทเอกสาร</option>
                                          <option value="excel">Excel</option>
                                          <option value="pdf">PDF</option>
                                      </select>
                                  </div>
                            </div>
                            <div class="row top-buffer">
                                  <div class="col-xs"><label class="col-sm-3 control-label">
                                  <?php if($_GET["fromlink"] == "fr_impris_rec_rept"){ echo "แสดงเฉพาะรายการร่าง";}else{ echo "รวมรายการร่าง";}?>
                                    <i class="fa fa-angle-double-left" aria-hidden="true"></i></label></div>
                                  <div class="col-md-4 material-switch">
                                  <input id="chk_rec_drf" name="chk_rec_drf" type="checkbox"/>
                                        <label for="chk_rec_drf" class="label-info"></label>
                                  </div>
                            </div>
                        </div>
                        <!-- Modal Footer -->
                        <?php require_once 'inc_main_form_footer.php';?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
