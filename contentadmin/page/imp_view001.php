<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
ob_start();

$SysListMain = new SysListMain();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$stly_lt = "border-left: 1px solid #000000;border-top: 1px solid #000000;";
$stly_lb = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lbr = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltr = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lrtb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000;";

$fromlink = $_GET["fromlink"];
$fromtrans = $_GET["fromtrans"];
$uniquenum_pri = $_GET["uniquenum_pri"];
$tag_audit_yn = $_GET["tag_audit_yn"];
$page_start = 0;
$limit = 0;

$qs_result = $SysListMain->getRowImprisResult($fromtrans,$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn);
$qs_remark = $SysListMain->getRowRemarkResult($fromtrans,$uniquenum_pri);

$rept_footer = $_SESSION["cookies_username"]." : ".$SysMainActivity->getClientIpAddress()."|หน้า {PAGENO} / {nb}|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding=0 cellspacing=0 autosize="1">
                <?php
                    while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                    $rept_filename = $rows["sys_autonum"];
                    $rept_name = "ประวัติผู้กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ : ".$rept_filename;
                    $rept_header = "|".$rept_name."|";
                ?>
                <tr>
                    <td width="100%" colspan="2" align="center" style="<?php echo $stly_lrtb;?>"><b><?php echo $rept_name;?></b></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>ชื่อ-นามสกุล (ภาษาไทย) :</b> <?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>ชื่อ-นามสกุล (ภาษาอังกฤษ) :</b> <?php echo strtoupper($rows["imp_title_2"])." ".strtoupper($rows["imp_firstname_2"])." ".strtoupper($rows["imp_lastname_2"]);?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>วัน/เดือน/ปี เกิด :</b> <?php echo $SysConversion->convertDateFormat($rows["imp_birthdate"],"d/m/Y");?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>อายุ :</b> <?php if(!empty($rows["imp_age"])){ echo $rows["imp_age"]." ปี";}?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>เพศ :</b> <?php if($rows["imp_gender"] == 'm'){echo "ชาย";}else{echo "หญิง";}?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>ประเภทงาน :</b> <?php echo $rows["imp_cate_desc"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>หมายเลขประจำตัวประชาชน :</b> <?php echo $rows["imp_idcard"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>หมายเลขหนังสือเดินทาง :</b> <?php echo $rows["imp_passport"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>หมายเลขหนังสือสำคัญประจำตัว (C.I.) :</b> <?php echo $rows["imp_ci"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>หมายเลขหนังสือเดินทางชั่วคราว :</b> <?php echo $rows["imp_temp_passport"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>หนังสือผ่านแดน (Border Pass) :</b> <?php echo $rows["imp_bdr_pass"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>วันที่รับแจ้ง :</b> <?php echo $SysConversion->convertDateFormat($rows["imp_date_created"],"d/m/Y");?></td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="left" style="height:100; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>ภูมิลำเนา</b> <?php echo $rows["imp_addrr"]." ".$rows["imp_subdistrict"]." ".$rows["imp_district"]." ".$rows["imp_province"]." ".$rows["imp_zipcode"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>หมายเลขติดต่อญาติ :</b> <?php echo $rows["imp_telephone"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>วันที่ถูกจับกุม :</b> <?php echo $SysConversion->convertDateFormat($rows["imp_prisdate"],"d/m/Y");?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>สถานที่ควบคุมตัว :</b> <?php echo $rows["imp_det_desc"].", ".$rows["state_desc"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>ข้อหาที่ถูกจับกุม :</b> <?php echo $rows["imp_reg_desc"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>เลขที่เอกสาร :</b> <?php echo $rows["imp_refdoc_desc"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>สถานะ :</b> <?php echo $rows["imp_stat_desc"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>วันที่ยืม :</b> <?php echo $SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"d/m/Y");?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>จำนวนเงินทดรอง (RM) :</b> <?php echo number_format($rows["imp_cshbrw_amt"],2);?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="<?php echo $stly_lb;?>"><b>หมายเลขอ้างอิง :</b> <?php echo $rows["imp_cshbrw_ref"];?></td>
                    <td width="50%" align="left" style="<?php echo $stly_lbr;?>"><b>สถานะการชดใช้เงิน :</b>
                        <?php
                            if($rows["imp_cshbrw_stat"] == "y"){
                                echo 'ได้รับคืนจากกระทรวงฯ แล้ว';
                            }elseif($rows["imp_cshbrw_stat"] == "s"){
                                echo 'ยังไม่ได้รับคืนจากกระทรวงฯ';
                            }else{
                                echo 'ไม่พบข้อมูลเงินทดรองราชการ';
                            }
                        ?>
                    </td>
                </tr>
                <?php
                    } /* End while loop $qs_result */
                ?>
                <tr>
                    <td width="100%" colspan="2" align="left" style="height:600; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>หมายเหตุ :</b><br><br>
                        <?php
                            while($rows = $qs_remark->fetch(PDO::FETCH_ASSOC)){
                                echo $rows["notes_memo"]." (โดย ".$rows["usr_firstname"]." ".$rows["usr_lastname"]." : ".$SysConversion->convertDateFormat($rows["date_created"],"d/m/Y h:i:s").")"."<br><br>";
                            }
                        ?>
                    </td>
                </tr>
                </table>
            </div>
        </div>
    </body>
</html>
<?php
$html = ob_get_contents();
$pdf = new mPDF('th', 'A4-P', '10', 'THSaraban','15','15','15','10');
$pdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('../../folder_script/css_printform_generator.css'); // external css
$pdf->SetTitle($rept_name);
$pdf->defaultfooterfontstyle='I';
$pdf->setFooter($rept_footer);
$pdf->WriteHTML($stylesheet,1);
$pdf->WriteHTML($html, 2);
$pdf->SetJS('window.print(); window.close();');
ob_end_clean();
$pdf->Output($rept_filename.'.pdf', 'I');
?>
