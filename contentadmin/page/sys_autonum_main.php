<!-- sys_autonum_main.php -->
<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
        <script type="text/javascript">
            $(document).on("click", ".parse-frommode", function () {
                var frommode = $(this).data('frommode');
                $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                $('.modal-body').find('button').removeClass( "btn dropdown-toggle disabled btn-default" ).addClass( "btn dropdown-toggle btn-default" );
                $(".modal-content #fmi_frommode").val( frommode );
                $('.modal-body input').val('');
                $('.modal-body checkbox').val('');
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_lock").hide();
                $("#frm_unlock").hide();
                $("#frm_cancel").show();
                $("#frm_submit").show();
                $('#frm_submit').attr('disabled',false);
                $('#frm_cancel').attr('disabled',false);
                $("#frm_print").hide();
                parseModalHeaderDesc();
                getNewUniqueID();
                setTimeout(function(){ dispSysAutoNumFormat(); }, 1000);
            });

            $(document).on("click", ".row_upd", function () {
                var frommode = $(this).data('frommode');
                var upd_id = $(this).data('id');
                $(".modal-content #fmi_frommode").val( frommode );
                $(".modal-content #fmi_uniquenum_pri").val( upd_id );
                //alert(document.getElementById('fmi_frommode').value);
                if(frommode == 'view'){
                    var tag_audit_yn = "y";
                }else{
                    var tag_audit_yn = "n";
                }
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_print").hide();
                getRowUpdate(tag_audit_yn);
                parseModalHeaderDesc();
                chkLockRecord();
                autoChkLockRecord();
                setTimeout(function(){ dispSysAutoNumFormat(); }, 1000);
                document.getElementById('fmi_prefix_code').autofocus;
                if(frommode == 'view'){
                    $("#frm_footer").hide();
                    $("#menu-toggle").hide();
                    $("#sidebar-wrapper").hide();
                    $('.modal-body').find('input, textarea, button, select').attr('disabled','disabled');
                }else{
                    $("#frm_footer").show();
                    $("#sidebar-wrapper").show();
                    $("#menu-toggle").show();
                    $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                }
            });

            function confirmDelete(row,id){
                $.confirm({
                    title: '',
                    content: 'ต้องการลบรายการที่ ' + row + '?',
                    icon: 'fa fa-trash',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก',
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-danger',
                            action: function(){
                                $(".modal-content #fmi_frommode").val('delete');
                                $(".modal-content #fmi_uniquenum_pri").val(id);
                                $.LoadingOverlay("show");
                                $.ajax({
                                    type: 'post',
                                    url: 'sys_autonum_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        toggleAlertBox('','ลบรายการสำเร็จ!','fa fa-trash','green');
                                        jQuery("#target-content").load("inc_sys_autonum_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function getNewUniqueID(){
                var tag_usage = 'autogen_upri';
                $.ajax({
                    url: '../control/inc_ajax_text.php',
                    data: "tag_usage=" + tag_usage,
                    dataType: 'text',
                    success: function(data)
                    {
                        document.getElementById('fmi_uniquenum_pri').value = data;
                    }
                });
            }

            function getRowUpdate(tag_audit_yn){
                var fromlink = document.getElementById('fmi_fromlink').value;
                var tag_usage = document.getElementById('fmi_fromtrans').value;
                var uniquenum_pri = document.getElementById('fmi_uniquenum_pri').value;
                $.ajax({
                    url: '../control/inc_ajax_fetch_row_update.php',
                    data: "fromlink=" + fromlink + "&tag_usage=" + tag_usage + "&uniquenum_pri=" + uniquenum_pri + "&tag_audit_yn=" + tag_audit_yn,
                    dataType: 'json',
                    success: function(data)
                    {
                        document.getElementById('fmi_prefix_code').value = data[0].prefix_code;
                        document.getElementById('fmi_run_number').value = data[0].run_number;
                        document.getElementById('fmi_suffix_code').value = data[0].suffix_code;
                        document.getElementById('fmi_date_created').value = data[0].date_created;
                        if(data[0].chk_mth_year == 'y'){
                            document.getElementById('chk_mth_year').checked = true;
                        }else{
                            document.getElementById('chk_mth_year').checked = false;
                        }

                        if(data[0].chk_active == 'y'){
                            document.getElementById('chk_active').checked = true;
                        }else{
                            document.getElementById('chk_active').checked = false;
                        }

                        if(data[0].chk_default == 'y'){
                            document.getElementById('chk_default').checked = true;
                        }else{
                            document.getElementById('chk_default').checked = false;
                        }
                    }
                });
            }

            function formSearch(){
                if(typeof document.getElementById('form_search') === 'object'){
                    var fmi_form_search = document.getElementById('form_search').value;
    				jQuery("#target-content").load("inc_sys_autonum_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1&search_query="+fmi_form_search);
                    jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                }
            }

            $(document).ready(function() {
                $('#frm_sys_autonum').on('hidden.bs.modal', function () {
                    if(IntChkLock != null){
                        clearInterval(IntChkLock);
                    }
                });
            });

            function dispSysAutoNumFormat(){
                var prefix_code = document.getElementById('fmi_prefix_code').value;
                var run_number = document.getElementById('fmi_run_number').value;
                var suffix_code = document.getElementById('fmi_suffix_code').value;

                if(document.getElementById('chk_mth_year').checked == true){
                    if((new Date()).getMonth() <= 9){
                        var month = "0" + (new Date()).getMonth();
                    }
                    document.getElementById('rs_autonum').value = prefix_code + month + (new Date()).getFullYear() + run_number + suffix_code;
                }else{
                    document.getElementById('rs_autonum').value = prefix_code + run_number + suffix_code;
                }
            }
        </script>
    </head>
    <body>
        <div class="card" style="width:<?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <?php require_once 'inc_sys_autonum_main_form.php';?>
            <div class="container">
                <?php
                    require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6"></div>
                                    <div class="col col-xs-6 text-right">
                                        <a data-toggle="modal" data-frommode="new" data-target="#frm_sys_autonum" title="เพิ่ม" class="parse-frommode btn btn-primary" href="#frm_sys_autonum" style="color:#ffffff"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" id="target-content"></div>
                            <?php
                                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_pagination_main.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
