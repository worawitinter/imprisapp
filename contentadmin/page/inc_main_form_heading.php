<!-- inc_main_form_heading.php --->
<?php
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';
?>
<div class="modal-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <button type="button" class="close"
                        data-dismiss="modal" title="ปิดหน้าต่าง">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="modal-title" id="frm_label">
                    <h2><?php if($_GET["fromtarget"] == "report"){ echo "<i class='fa fa-search fa-2x' aria-hidden='true'>";}else{ echo "<i class='fa fa-pencil-square-o fa-2x' aria-hidden='true'>";}?></i>&nbsp;<?php if($ext_label != ""){echo $ext_label;}else{echo $label_header;}?> <?php if($rep_header_desc != ""){echo " <i class='fa fa-angle-double-right' aria-hidden='true'></i> ".$rep_header_desc;}?> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <span class="label label-success"><span id="header_label"></span></span></h2>
                </h4>
            </div>
        </div>
        <div id="div_menu-toggle" class="row">
            <div class="col-lg-12 pull-right">
                <div class="pull-right">
                    <a id="menu-toggle" title="" data-toggle="collapse" href="#collapseLog" role="button" aria-expanded="false" aria-controls="collapseLog">
                        <i class="fa fa-history fa-2x" aria-hidden="true" style=" vertical-align: middle;color:#ffffff" title="เรียกดูประวัติการแก้ไขรายการ"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
