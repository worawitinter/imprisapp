<?php
@session_start();
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="../folder_script/favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $lbl_app_name." ".$lbl_version." ".$version?></title>
    <?php require_once DOCUMENT_ROOT . '/folder_script/inc_src_script.php';?>
    <script language="javascript">
        $(document).ready(function(){
            $.LoadingOverlay("show");
            setTimeout(function(){
                window.location="main_screen<?php echo $url_file_ext; ?>?fromtrans=&fromlink=home&fromtarget=&frommode=view";
            }, 3000);
        });
    </script>
  </head>
  <body>
    <br><br><br><br><br><br><br>
    <div class="container">
        <div class="card card-container">
          <div class="row">
              <div>
                  <div class="img_box_login"><img class="resize_horizontal" src="<?php echo SERVER_URL_ROOT . '/folder_script/'.SYS_LOGO;?>" /></div>
                  <h1 class="text-center"><b><?php echo $lbl_app_name;?></b></h1>
                  <h2 class="text-center"><b>Consular Case Management System</b></h2>
                  <h3 class="text-center"><b><?php echo $lbl_version." ".VERSION;?></b></h3>
              </div>
          </div>
        </div>
    </div>
  </body>
</html>
