<?php
@session_start();
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';
$SysMainCookies = new SysMainCookies();
$SysMainCookies->clearSessionGarbage();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="../folder_script/favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <?php require_once DOCUMENT_ROOT . '/folder_script/inc_src_script.php';?>
    <script language="javascript">
        $(document).ready(function(){
            $.LoadingOverlay("show");
            window.location="welcome_screen<?php echo $url_file_ext; ?>";
        });
    </script>
  </head>
  <body>
  </body>
</html>
