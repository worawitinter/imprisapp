<?php
@ob_start();
@session_start();
require_once '../config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysMainCookies = new SysMainCookies();
$SysMainActivity = new SysMainActivity();

$log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
$log_tablename = "sys_activity_main";
$log_fromtrans = "usraccess";
$date_created = $TransactionGenUnique->generate_trans_datetime();
$location = LOCATION;
$log_data = array   (   "tag_table_usage"=>$log_fromtrans,
                    "uniquenum_pri"=>$log_uniquenum_pri,
                    "sys_link"=>"logout",
                    "uniquenum_sec"=>$_SESSION["cookies_log_uniquenum_pri"],
                    "userid_cookie"=>$_SESSION["cookies_username"],
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "desc_lang01"=>$_SERVER['HTTP_USER_AGENT'],
                    "notes_memo"=>$location,
                    "var_25_001"=>$_SESSION["cookies_set_language"],
                    "var_100_001"=>$_SESSION["cookies_sys_role"],
                    "var_100_002"=>$_SESSION["cookies_sys_acc_right"],
                    "desc_lang10"=>getHostByName(getHostName())
                );
$DatabaseOperation->insertRecordIntoTable($log_tablename,$log_data);
$SysMainCookies->setSysCookies("0",$_SESSION["cookies_usr_uniquenum_pri"]);
@session_unset();
@session_destroy();
header("location:../../login/");
?>
