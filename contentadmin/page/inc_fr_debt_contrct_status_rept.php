<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';

$SysReport = new SysReport();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$stly_lt = "border-left: 1px solid #000000;border-top: 1px solid #000000;";
$stly_lb = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lbr = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltr = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lrtb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000;";

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
    ob_start();
}

$uniquenum_pri = '';
if(isset($_GET["fromtrans"])){
    $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
}

$fmi_imp_date_from = "";
$fmi_imp_date_to = "";
$fmi_state_code = "";
$fmi_imp_gender = "";
$fmi_imp_laws = "";
$fmi_imp_det = "";
$fmi_search_string = "";
$fmi_imp_stat = "";
$fmi_date_type = "";
$fmi_imp_cate = "";
$fmi_imp_cshbrw_stat = "";
$fmi_imp_date_debt_from = "";
$fmi_imp_date_debt_to = "";
if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
if(!empty($_GET["state_code"])){$fmi_state_code = $_GET["state_code"];}
if(!empty($_GET["imp_laws"])){$fmi_imp_laws = $_GET["imp_laws"];}
if(!empty($_GET["imp_det"])){$fmi_imp_det = $_GET["imp_det"];}
if(!empty($_GET["imp_gender"])){$fmi_imp_gender = $_GET["imp_gender"];}
if(!empty($_GET["search_string"])){$fmi_search_string = $_GET["search_string"];}
if(!empty($_GET["imp_stat"])){$fmi_imp_stat = $_GET["imp_stat"];}
if(!empty($_GET["date_type"])){$fmi_date_type = $_GET["date_type"];}
if(!empty($_GET["imp_cate"])){$fmi_imp_cate = $_GET["imp_cate"];}
if(!empty($_GET["imp_cshbrw_stat"])){$fmi_imp_cshbrw_stat = $_GET["imp_cshbrw_stat"];}
if(!empty($_GET["date_debt_from"])){$fmi_imp_date_debt_from = $SysConversion->convertDate($_GET["date_debt_from"]);}
if(!empty($_GET["date_debt_to"])){$fmi_imp_date_debt_to = $SysConversion->convertDate($_GET["date_debt_to"]);}

$search_query = "";
if($fmi_date_type == 'date_impris'){
    $fld_date_col = "date_002";
}else{
    $fld_date_col = "date_created";
}
if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
}else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
}
if(!empty($fmi_state_code)){
    $search_query = $search_query . " AND det.var_25_001 = '".$fmi_state_code."'";
}
if(!empty($fmi_imp_laws)){
    $search_query = $search_query . " AND imp.var_25_002 = '".$fmi_imp_laws."'";
}
if(!empty($fmi_imp_det)){
    $search_query = $search_query . " AND imp.var_25_005 = '".$fmi_imp_det."'";
}
if(!empty($fmi_imp_gender)){
    $search_query = $search_query . " AND imp.desc_lang04 = '".$fmi_imp_gender."'";
}
if(!empty($fmi_imp_stat)){
    $search_query = $search_query . " AND imp.var_25_004 = '".$fmi_imp_stat."'";
}
if(!empty($fmi_imp_cate)){
    $search_query = $search_query . " AND imp.var_100_003 = '".$fmi_imp_cate."'";
}
if(!empty($fmi_imp_cshbrw_stat)){
    $search_query = $search_query . " AND imp.tag_other01_yn = '".$fmi_imp_cshbrw_stat."'";
}
if(!empty($fmi_search_string)){
    $search_query = $search_query . " AND (lower(imp.desc_lang01) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang02) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.var_25_003) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.notes_memo) like '%".trim(strtolower($fmi_search_string))."%')";
}

if(isset($_GET["chk_rec_drf"]) && $_GET["chk_rec_drf"] == "n"){     
    $search_query = $search_query . "  and imp.tag_other01_yn = 'n'"; 
}

if($fmi_imp_date_debt_from !== "" && $fmi_imp_date_debt_to == ""){
    $search_query = $search_query . " AND imp.date_003 between '".$fmi_imp_date_debt_from."' AND '".$fmi_imp_date_debt_from."'";
}else if($fmi_imp_date_debt_from !== "" && $fmi_imp_date_debt_to !== ""){
    $search_query = $search_query . " AND imp.date_003 between '".$fmi_imp_date_debt_from."' AND '".$fmi_imp_date_debt_to."'";
}

$qs_result = $SysReport->getImprisRecord($search_query);

$rept_name = $rep_header_desc;
$rept_range = "ระหว่างวันที่ ".$_GET["datefrom"]." จนถึง ".$_GET["dateto"];
$rept_filename = $rept_name."_".$_GET["datefrom"]."_".$_GET["dateto"];
$rept_header = "|".$rept_name."<br>".$rept_range."|";
$rept_footer = $_SESSION["cookies_username"]." : ".$SysMainActivity->getClientIpAddress()."|หน้า {PAGENO} / {nb}|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding=0 cellspacing=0>
                    <thead>
                        <tr>
                            <th style="<?php echo $stly_ltb;?>">ลำดับ</th>
                            <th style="<?php echo $stly_ltb;?>">วันที่รับแจ้ง</th>
                            <th style="<?php echo $stly_ltb;?>">ชื่อ-สกุล</th>
                            <th style="<?php echo $stly_ltb;?>">อายุ</th>
                            <th style="<?php echo $stly_ltb;?>">หมายเลขประจำตัวประชาชน</th>
                            <th style="<?php echo $stly_ltb;?>">ภูมิลำเนา</th>
                            <th style="<?php echo $stly_ltb;?>">วันที่ยืม</th>
                            <th style="<?php echo $stly_ltb;?>">หมายเลขอ้างอิง</th>
                            <th style="<?php echo $stly_ltb;?>">สถานะการชดใช้เงิน</th>
                            <th align="right" style="<?php echo $stly_lrtb;?>">จำนวนเงิน (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $tot_amt = 0;
                        while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                            $cnt++;
                            $tot_amt = $tot_amt + $rows["imp_cshbrw_amt"];
                        ?>
                            <tr>
                                <td class="hidden-xs" style="<?php echo $stly_lb;?>"><?php echo $cnt;?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo $rows["imp_date_created"];?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?></td>
                                <td align="right" style="<?php echo $stly_lb;?>"><?php echo $rows["imp_age"];?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo $rows["imp_idcard"];?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo wordwrap($rows["imp_addrr"]." ".$rows["imp_subdistrict"]." ".$rows["imp_district"]." ".$rows["imp_province"]." ".$rows["imp_zipcode"]);?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo $SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"d-m-Y");?></td>
                                <td style="<?php echo $stly_lb;?>"><?php echo $rows["imp_cshbrw_ref"];?></td>
                                <td style="<?php echo $stly_lb;?>">
                                    <?php
                                        if($rows["imp_cshbrw_stat"] == "y"){
                                            echo "ได้รับคืนจากกระทรวงฯ แล้ว";
                                        }elseif($rows["imp_cshbrw_stat"] == "s"){
                                                echo "ได้รับคืนจากกระทรวงฯ แล้ว";
                                        }else{
                                            echo "ไม่พบข้อมูลเงินทดรองราชการ";
                                        }
                                    ?>
                                </td>
                                <td align="right" style="<?php echo $stly_lbr;?>"><?php echo number_format($rows["imp_cshbrw_amt"], 2);?></td>
                            </tr>
                            <?php
                                } /* End while loop $qs_result */
                            ?>
                            <tr style="background-color:#A9A9A9;">
                                <td align="right" colspan="9" style="<?php echo $stly_lb;?>"><b>รวมทั้งหมด (RM)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="right" style="<?php echo $stly_lbr;?>"><b><?php echo number_format($tot_amt, 2);?></b></td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_hidden_values.php';?>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    $pdf = new mPDF('th', 'A2-L', '0', 'THSaraban','15','15','22','15');
    $pdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('../../folder_script/css_report_generator.css'); // external css
    $pdf->SetTitle($rept_name);
    $pdf->defaultheaderfontsize=10;
    $pdf->defaultheaderfontstyle='B';
    $pdf->SetHeader($rept_header);
    $pdf->defaultfooterfontstyle='I';
    $pdf->setFooter($rept_footer);
    $pdf->WriteHTML($stylesheet,1);
    $pdf->WriteHTML($html, 2);
    ob_end_clean();
    $pdf->Output($rept_filename.'.pdf', 'I');
}
?>
