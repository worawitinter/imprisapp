<!-- main_screen.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="../../folder_script/favicon-16x16.png" sizes="16x16" />
    <title><?php echo $lbl_app_name." ".$lbl_version." ".$version?></title>
    <?php require_once DOCUMENT_ROOT . '/folder_script/inc_src_script.php';?>
    <?php require_once DOCUMENT_ROOT . '/contentadmin/control/inc_parse_modal_header_desc.php';?>
    <style>
        .navbar-topmain{
             border-bottom: 6px solid #c9302c;
             /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e2e2e2+0,dbdbdb+50,d1d1d1+51,fefefe+100;Grey+Gloss+%231 */
             background: rgb(226,226,226); /* Old browsers */
             background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6-15 */
             background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10-25,Safari5.1-6 */
             background: linear-gradient(to bottom, rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
             filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            if(!$('#elemId').length){
                $('#reshow_modal').show();
                $("#reshow_modal").on('click',function(){
                    $('#frm_rep').modal('show');
                });
            }
        });
    </script>
  </head>
  <body>
        <!-- /#sidebar-wrapper -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <?php require_once 'inc_menu_topmain.php';?>
                <h6 class="welcome text-center" style="padding-top:50px">&nbsp;</h6>
                <div id="reshow_modal" class="watermark" style="cursor: pointer;display:none"><?php if(isset($rep_header_desc) && $rep_header_desc != ''){ echo $rep_header_desc;}?></div>
                <?php
                    if(isset($_GET["fromlink"]) && (isset($_GET["fromtarget"]) && $_GET["fromtarget"] != "report")){
                        require_once $_GET["fromlink"].'_main.php';
                    }else{
                        require_once $_GET["fromlink"].'.php';
                    }
                ?>
            </div>
            <div id="page_content_footer" <?php if(isset($_GET["fromlink"]) && (isset($_GET["fromtarget"]) && $_GET["fromtarget"] != "report")){ echo "style='display:'";}else{echo "style='display:none'";}?>>
                <h6 class="welcome text-center"><?php echo POWERBY;?></h6>
            </div>
        </div>
  </body>
</html>
