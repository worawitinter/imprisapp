<!-- inc_imp_attchmt_file_reload_form.php -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="file-loading">
          <input id="fmi_imp_file_upload" name="fmi_imp_file_upload[]" type="file" multiple>
        </div>
        <input type="hidden" name="fmi_frommode" id="fmi_frommode" value="">
        <input type="hidden" name="fmi_uniquenum_pri" id="fmi_uniquenum_pri" value="">
        <input type="hidden" name="fmi_fromtrans" id="fmi_fromtrans" value="<?php echo $fromtrans; ?>">
        <input type="hidden" name="fmi_fromlink" id="fmi_fromlink" value="<?php echo $fromlink; ?>">
        <input type="hidden" name="fmi_userid" id="fmi_userid" value="<?php echo $_SESSION["cookies_username"]; ?>">
        <input type="hidden" name="fmi_date_created" id="fmi_date_created" value="">
        <div id="kartik-file-errors"></div>
    </body>
</html>
