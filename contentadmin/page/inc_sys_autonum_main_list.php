<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysListMain = new SysListMain();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table class="table table-striped table-bordered table-list">
            <tr>
                <th class="text-center"><em class="fa fa-cog"></em></th>
                <th class="hidden-xs">ลำดับ</th>
                <th>ชุดหมายเลข</th>
                <th>สถานะใช้งาน</th>
                <th>ค่าเริ่มต้น</th>
            </tr>
        <?php
            $uniquenum_pri = '';
            if(isset($_GET["page"])){
                $page = $_GET["page"];
            }else{
                $page = 1;
            }
            if(isset($_GET["search_query"])){
                $search_query = $_GET["search_query"];
            }else{
                $search_query = "";
            }
            $limit = PAGE_LIMIT;
            $page_start = ($page-1) * $limit;
            $cnt = 0;
            $qs_result = $SysListMain->getRowSysAutoNumResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
            if($search_query != ""){
                $total_records = $qs_result->rowCount();
            }else{
                $total_records = $DatabaseOperation->getTotalRecords($tablename,$_GET['fromtrans']);
            }
            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                $cnt++;
        ?>
        <tr>
            <td align="center">
                <a data-toggle="modal" data-target="#frm_sys_autonum" class="row_upd btn btn-success" title="แก้ไขรายการที่ <?php echo $cnt;?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="edit" href="#frm_sys_autonum"><em class="fa fa-pencil" style="color: #FFFFFF"></em></a>
                <a class="row_del btn btn-danger" title="ลบรายการที่ <?php echo $cnt;?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="delete" onclick="confirmDelete('<?php echo $cnt;?>','<?php echo $rows["uniquenum_pri"];?>');"><em class="fa fa-trash"></em></a>
            </td>
            <td class="hidden-xs"><?php echo $cnt;?></td>
            <td>
                <?php
                    if($rows["prefix_code"] != ""){
                        echo $rows["prefix_code"];
                    }
                    if($rows["mth_year"] != ""){
                        echo $rows["mth_year"];
                    }
                    if($rows["run_number"] != ""){
                        echo $rows["run_number"];
                    }
                    if($rows["suffix_code"] != ""){
                        echo $rows["suffix_code"];
                    }
                ?>
            </td>
            <td><?php echo $rows["chk_active"];?></td>
            <td><?php echo $rows["chk_default"];?></td>
        </tr>
        </table>
        <?php
            } /* End while loop $qs_result */
        ?>
    </body>
</html>
