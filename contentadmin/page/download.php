<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';

$file_name = $_GET['file_name'];
$uniquenum_pri = $_GET['uniquenum_pri'];
$path = DOCUMENT_ROOT . '/folder_upload/impr_doc/' . $uniquenum_pri . '/' . $file_name;

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment;  filename="'.basename($path).'"');
header('Content-Length: ' . filesize($path));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

//stream file
ob_get_clean();
readfile($path);
ob_end_flush();
 ?>
