<!DOCTYPE html>
<!-- inc_pagination_main.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';

$DatabaseOperation = new DatabaseOperation();

$limit = PAGE_LIMIT;
$total_records = $DatabaseOperation->getTotalRecords($tablename,$_GET['fromtrans']);
$total_pages = ceil($total_records / $limit);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript">
            $(document).ready(function(){
                $.LoadingOverlay("show");
                $('.pagination').pagination({
                    items: <?php echo $total_records;?>,
                    itemsOnPage: <?php echo $limit;?>,
                    cssStyle: 'light-theme',
                    currentPage : 1,
                    onPageClick : function(pageNumber) {
                        jQuery("#target-content").load("inc_<?php echo $_GET["fromlink"];?>_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=" + pageNumber);
                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");

                    },
                    onInit :function() {
                        jQuery("#target-content").load("inc_<?php echo $_GET["fromlink"];?>_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                    }
                });
                $.LoadingOverlay("hide");
            });
        </script>
    </head>
    <body>
        <div class="panel-footer">
            <div class="row">
                    <div id="target-pagination">
                    </div>
            </div>
        </div>
    </body>
</html>
