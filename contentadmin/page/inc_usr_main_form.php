<!DOCTYPE html>
<!-- inc_usr_main_from.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .modal-dialog{
                position: relative;
                display: table; /* This is important */
                overflow-y: auto;
                overflow-x: auto;
                width: auto;
                min-width: 1000px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#frm_usr_main').bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        fmi_usr_firstname: {
                            validators: {
                                    stringLength: {
                                    min: 2,
                                },
                                    notEmpty: {
                                    message: 'โปรดระบุชื่อ'
                                }
                            }
                        },
                        fmi_usr_lastname: {
                           validators: {
                                stringLength: {
                                   min: 2,
                               },
                               notEmpty: {
                                   message: 'โปรดระบุนามสกุล'
                               }
                           }
                        },
                        fmi_usr_username: {
                           validators: {
                                stringLength: {
                                   min: 6,
                               },
                               notEmpty: {
                                   message: 'โปรดกำหนดชื่อผู้ใช้งาน'
                               }
                           }
                        },
                        fmi_usr_enddate: {
                           validators: {
                               date: {
                                   format: 'DD/MM/YYYY',
                                   message: 'รูปแบบวันที่ไม่ถูกต้อง (DD/MM/YYYY)'
                               }
                           }
                        },
                        fmi_usr_email: {
                           validators: {
                               emailAddress: {
                                    message: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                                }
                           }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                    $('#frm_usr_main').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.LoadingOverlay("show");
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        var value=$.trim($('#fmi_fromtrans').val());
                        if(value.length == 0)
                        {
                            //Error Code: 001 value missing in fromtrans url parameter
                            toggleAlertBox('','เกิดความผิดพลาดของระบบ [Error Code: 001]','fa fa-warning','red');
                        }else{
                            if(validateForm(value)){
                                $.ajax({
                                    type: 'post',
                                    url: 'usr_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        $('#frm_usr').modal('hide');
                                        $('.modal-body input').val('');
                                        $('.modal-body textarea').val('');
                                        jQuery("#target-content").load("inc_usr_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    });
                });
            });

            function validateForm(fromtrans){
                var result = true;
                if($("#fmi_usr_dept").val() == ""){
                    toggleAlertBox('','กรุณาเลือก สังกัด','fa fa-warning','orange');
                    $.LoadingOverlay("hide");
                    return false;
                }else if($("#fmi_usr_right").val() == ""){
                    toggleAlertBox('','กรุณากำหนด สิทธิ์การใช้งาน','fa fa-warning','orange');
                    $.LoadingOverlay("hide");
                    return false;
                }
                return result;
            }

            function confirmCloseModal(){
                $.confirm({
                    title: '',
                    content: 'ต้องการยกเลิกรายการนี้?',
                    icon: 'fa fa-times-circle',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก'
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-red',
                            action: function(){
                                $('#frm_usr').modal('hide');
                                $(".modal-body input").val("");
                                $(".modal-body textarea").val("");
                                $('#frm_usr_main').bootstrapValidator('resetForm', true);
                            }
                        }
                    }
                });
            }

        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_usr" tabindex="-1" role="dialog" aria-labelledby="frm_usr_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <?php require_once 'inc_main_form_heading.php';?>
                    <form class="form-horizontal" role="form" id="frm_usr_main" enctype="multipart/form-data">
                        <fieldset>
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <div id="wrapper">
                                <!-- Sidebar -->
                                <?php require_once 'inc_toggle_side_nav_main.php';?>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_title">คำนำหน้า <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                    <select name="fmi_usr_title" id="fmi_usr_title" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                        <option value="mr">นาย</option>
                                        <option value="ms">นางสาว</option>
                                        <option value="mrs">นาง</option>
                                        <option value="master">เด็กชาย</option>
                                        <option value="miss">เด็กหญิง</option>
                                    </select>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_<?php echo $_GET["fromtrans"];?>_autonum">หมายเลขรายการ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                    <?php $trans_autonum = new SysTransNum($trans_autonum->fromtrans = $_GET["fromtrans"]);?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_firstname">ชื่อ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type="text" name="fmi_usr_firstname" maxlength="50" class="form-control required" id="fmi_usr_firstname" placeholder="ชื่อ"/>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_lastname">นามสกุล <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type="text" name="fmi_usr_lastname" maxlength="50" class="form-control required" id="fmi_usr_lastname" placeholder="นามสกุล"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_dept">สังกัด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_usr_dept",
                                                                            $gencode->fld_usage = "sys_user_dept",
                                                                            $gencode->fld_label = "สังกัด",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_contact">หมายเลขติดต่อ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type="text" name="fmi_usr_contact" maxlength="50" class="form-control required" id="fmi_usr_contact" placeholder="หมายเลขติดต่อ"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_email">อีเมล์ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type="text" name="fmi_usr_email" maxlength="50" class="form-control required" id="fmi_usr_email" placeholder="อีเมล์"/>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_remark">หมายเหตุ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <textarea class="form-control" name="fmi_usr_remark" id="fmi_usr_remark" rows="3" placeholder="หมายเหตุ"></textarea>
                                  </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_username">ชื่อผู้ใช้ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type="text" name="fmi_usr_username" maxlength="50" class="form-control required" id="fmi_usr_username" placeholder="ชื่อผู้ใช้"/>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_password">รหัสผ่าน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <div class="password">
                                        <input type="password" name="fmi_usr_password" maxlength="50" class="form-control required" id="fmi_usr_password" placeholder="รหัสผ่าน"/>
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                      </div>
                                      <script type="text/javascript">
                                            $("#fmi_usr_password").on("keyup",function(){
                                                if($(this).val())
                                                    $(".glyphicon-eye-open").show();
                                                else
                                                    $(".glyphicon-eye-open").hide();
                                                });
                                            $(".glyphicon-eye-open").mousedown(function(){
                                                            $("#fmi_usr_password").attr('type','text');
                                                        }).mouseup(function(){
                                                            $("#fmi_usr_password").attr('type','password');
                                                        }).mouseout(function(){
                                                            $("#fmi_usr_password").attr('type','password');
                                                        });
                                      </script>

                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_role">ระดับสิทธิ์ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <select name="fmi_usr_role" id="fmi_usr_role" class="selectpicker" data-hide-disabled="true" data-live-search="false">
                                        <option value="user">ผู้ใช้งาน</option>
                                        <option value="admin">ผู้ดูแลระบบ</option>
                                    </select>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_right">สิทธิ์การใช้งาน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <select name="fmi_usr_right[]" id="fmi_usr_right" class="selectpicker" multiple data-done-button="true">
                                        <option value="create">สร้าง/แก้ไข</option>
                                        <option value="view">เรียกดู</option>
                                        <option value="delete">ลบ</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_usr_enddate">วันที่สิ้นสุด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                        <div class='input-group date' id='fmi_usr_enddate_sel'>
                                            <input type='text' class="form-control" id="fmi_usr_enddate" name="fmi_usr_enddate" placeholder="วัน/เดือน/ปี"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#fmi_usr_enddate_sel').datetimepicker({
                                                    format: 'DD/MM/YYYY'
                                                });
                                            });
                                        </script>
                                  </div>
                                  <label class="col-sm-2 control-label" for="fmi_usr_last_access">เข้าใช้งานครั้งล่าสุด <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-3">
                                      <input type='text' class="form-control" id="fmi_usr_last_access" name="fmi_usr_last_access" placeholder="วัน/เดือน/ปี" readonly="readonly"/>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Footer -->
                        <?php require_once 'inc_main_form_footer.php';?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
