<!DOCTYPE html>
<!-- inc_imp_attchmt_main_form.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script>
            $(document).on("click", ".row_upd", function () {
                jQuery("#file_reload").load("inc_imp_attchmt_file_reload_form.php");
                var imp_name = $(this).data('impname');
                $("#imp_name").text(imp_name);
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                var tablename = $("#fmi_tablename_data").val();
                var fromtrans = $("#fmi_fromtrans").val();
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=imp_attchmt&uniquenum_pri=" + uniquenum_pri + "&tablename=" + tablename + "&fromtrans=" + fromtrans,
                     success: function(data){
                         if(data != ""){
                             var str = data.split('@@@');
                             var init_prev_obj = eval('[' + str[0] + ']');
                             var init_prev_conf_obj = eval('[' + str[1] + ']');
                             $("#fmi_imp_file_upload").fileinput({
                                 theme: 'fa',
                                 language: 'th',
                                 uploadUrl: "imp_attchmt_oup.php", // server upload action
                                 uploadAsync: true,
                                 browseOnZoneClick: true,
                                 allowedFileExtensions: ['jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'],
                                 showUpload: true,
                                 showCaption: true,
                                 browseClass: "btn btn-primary btn-lg",
                                 previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                                 overwriteInitial: false,
                                 initialPreviewAsData: true,
                                 initialPreview: init_prev_obj,
                                 initialPreviewConfig: init_prev_conf_obj,
                                 purifyHtml: true,
                                 uploadExtraData: function() {
                                     return {
                                         fmi_uniquenum_pri: $("#fmi_uniquenum_pri").val(),
                                         fmi_fromtrans: $("#fmi_fromtrans").val(),
                                         fmi_fromlink: $("#fmi_fromlink").val(),
                                         fmi_frommode: $("#fmi_frommode").val(),
                                         fmi_userid: $("#fmi_userid").val(),
                                         fmi_tablename: $("#fmi_tablename_data").val()
                                     };
                                 }
                             });
                         }else{
                             $("#fmi_imp_file_upload").fileinput({
                                 theme: 'fa',
                                 language: 'th',
                                 uploadUrl: "imp_attchmt_oup.php", // server upload action
                                 uploadAsync: true,
                                 maxFileCount: 5,
                                 browseOnZoneClick: true,
                                 allowedFileExtensions: ['jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'],
                                 uploadExtraData: function() {
                                     return {
                                         fmi_uniquenum_pri: $("#fmi_uniquenum_pri").val(),
                                         fmi_fromtrans: $("#fmi_fromtrans").val(),
                                         fmi_fromlink: $("#fmi_fromlink").val(),
                                         fmi_frommode: $("#fmi_frommode").val(),
                                         fmi_userid: $("#fmi_userid").val(),
                                         fmi_tablename: $("#fmi_tablename_data").val()
                                     };
                                 }
                             });
                         }
                    }
                });
            });
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_imp_attchmt" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <div class="row">
                      <div class="col-lg-12">
                          <button type="button" class="close"
                                  data-dismiss="modal" title="ปิดหน้าต่าง">
                                 <span aria-hidden="true">&times;</span>
                                 <span class="sr-only">Close</span>
                          </button>
                      </div>
                  </div>
                <h2 class="modal-title" id="ModalLabel">
                    <i class="fa fa-paperclip fa-2x" style="color: #FFFFFF" aria-hidden="true"></i>
                    &nbsp;แนบรูปภาพ / เอกสาร&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span id="imp_name"></span>
                </h2>
              </div>
              <div class="modal-body">
                <div id="file_reload"></div>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
