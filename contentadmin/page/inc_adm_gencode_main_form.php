<!DOCTYPE html>
<!-- inc_adm_gencode_main_from.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .modal-dialog{
                position: relative;
                display: table; /* This is important */
                overflow-y: auto;
                overflow-x: auto;
                width: auto;
                min-width: 600px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                var fromtrans = $("#fmi_fromtrans").val();
                $('#frm_adm_gencode_main')
                .find('[name="fmi_'+ fromtrans +'_autonum"]')
                    .selectpicker()
                        .change(function(e) {
                         $('#frm_adm_gencode_main').bootstrapValidator('revalidateField', 'fmi_'+ fromtrans +'_autonum');
                })
                .end()
                .bootstrapValidator({
                    // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        fmi_gencode_desc: {
                           validators: {
                                stringLength: {
                                   min: 2,
                               },
                               notEmpty: {
                                   message: 'โปรดระบุคำอธิบาย'
                               }
                           }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                    $('#frm_adm_gencode_main').data('bootstrapValidator').resetForm();
                    // Prevent form submission
                    e.preventDefault();

                    // Get the form instance
                    var $form = $(e.target);

                    // Get the BootstrapValidator instance
                    var bv = $form.data('bootstrapValidator');

                    // Use Ajax to submit form data
                    $.LoadingOverlay("show");
                    $.post($form.attr('action'), $form.serialize(), function(result) {
                        var value=$.trim($('#fmi_fromtrans').val());
                        if(value.length == 0)
                        {
                            //Error Code: 001 value missing in fromtrans url parameter
                            toggleAlertBox('','เกิดความผิดพลาดของระบบ [Error Code: 001]','fa fa-warning','red');
                        }else{
                            if(value == 'impr_doc'){frm_fileupload();}
                            if(validateForm(value)){
                                $.ajax({
                                    type: 'post',
                                    url: 'adm_gencode_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        $('#frm_adm_gencode').modal('hide');
                                        $('.modal-body input').val('');
                                        $('.modal-body textarea').val('');
                                        jQuery("#target-content").load("inc_adm_gencode_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    });
                });
            });

            function validateForm(fromtrans){
                var result = true;
                if(fromtrans == "pris_det"){
                    if($("#fmi_pris_cate").val() == ""){
                        toggleAlertBox('','กรุณาเลือก ประเภทสถานที่ควบคุมตัว','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }else if($("#fmi_state_code").val() == ""){
                        toggleAlertBox('','กรุณาเลือก ภูมิภาค/รัฐ','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }
                }else if(fromtrans == "reg_list"){
                    if($("#fmi_reg_cate").val() == ""){
                        toggleAlertBox('','กรุณาเลือก ประเภทข้อหาที่ถูกจับกุม','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }
                }else if(fromtrans == "imp_stat"){
                    if($("#fmi_imp_cate").val() == ""){
                        toggleAlertBox('','กรุณาเลือก ประเภทงาน','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }
                }else if(fromtrans == "imp_vist"){
                    if($("#fmi_imp_det").val() == ""){
                        toggleAlertBox('','กรุณาเลือก สถานที่ควบคุมตัว','fa fa-warning','orange');
                        $.LoadingOverlay("hide");
                        return false;
                    }
                }
                return result;
            }

            function confirmCloseModal(){
                $.confirm({
                    title: '',
                    content: 'ต้องการยกเลิกรายการนี้?',
                    icon: 'fa fa-times-circle',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก'
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-red',
                            action: function(){
                                $('#frm_adm_gencode').modal('hide');
                                $(".modal-body input").val("");
                                $(".modal-body textarea").val("");
                                $('#frm_adm_gencode_main').bootstrapValidator('resetForm', true);
                            }
                        }
                    }
                });
            }

            function frm_fileupload(){
                $.LoadingOverlay("show");
                var file_data = $('[type=file]').prop('files')[0];
                var form_data = new FormData();
                var file_name = document.getElementById("fmi_file_name").value;
                var gencode_desc = document.getElementById("fmi_gencode_desc").value;
                form_data.append('fmi_imp_doc', file_data);
                form_data.append('fmi_file_name', file_name);
                form_data.append('fmi_gencode_desc', gencode_desc);
                //alert(form_data);
                $.ajax({
                            url: 'adm_gencode_oup.php?frommode=' + document.getElementById("fmi_frommode").value + '&userid=' + document.getElementById("fmi_userid").value + '&uniquenum_pri=' + document.getElementById("fmi_uniquenum_pri").value, // point to server-side PHP script
                            dataType: 'text',  // what to expect back from the PHP script, if anything
                            cache: false,
                            enctype: 'multipart/form-data',
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function(php_script_response){
                                if(php_script_response.trim() == ""){
                                    toggleAlertBox('','อัพโหลดไฟล์เสร็จสิ้น!','fa fa-warning','green');
                                }else{
                                    toggleAlertBox('','เกิดความผิดพลาด อัพโหลดไฟล์ไม่สำเร็จ!','fa fa-warning','red');
                                }
                                //alert(php_script_response); // display response from the PHP script, if any
                                $.LoadingOverlay("hide");
                            }
                 });
            }

            function populate_filename(){
              var fullPath = document.getElementById('fmi_imp_doc').value
              if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
                document.getElementById("fmi_gencode_desc").value = filename;
                document.getElementById("fmi_file_name").value = filename;
              }
            }

            function openFile(){
              var url = "download.php?uniquenum_pri=" + document.getElementById('fmi_uniquenum_pri').value + "&file_name=" + document.getElementById("fmi_file_name").value;
              //$("#ico_dowload").attr("href", url);
              if(document.getElementById("fmi_file_name").value != ""){
                   window.location.href=url;
              }
            }
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="frm_adm_gencode" tabindex="-1" role="dialog" aria-labelledby="frm_adm_gencode_label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <?php require_once 'inc_main_form_heading.php';?>
                    <form class="form-horizontal" role="form" id="frm_adm_gencode_main" enctype="multipart/form-data">
                        <fieldset>
                        <!-- Modal Body -->
                        <div id="model-body" class="modal-body">
                            <div id="wrapper">
                                <!-- Sidebar -->
                                <?php require_once 'inc_toggle_side_nav_main.php';?>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_<?php echo $_GET["fromtrans"];?>_autonum">รหัส <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                        <?php $trans_autonum = new SysTransNum($trans_autonum->fromtrans = $_GET["fromtrans"]);?>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'reg_list'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label" for="fmi_reg_cate">ประเภทข้อหาที่ถูกจับกุม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_reg_cate",
                                                                            $gencode->fld_usage = "reg_cate",
                                                                            $gencode->fld_label = "ประเภทข้อหาที่ถูกจับกุม",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'pris_det'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label" for="fmi_pris_cate">ประเภทสถานที่ควบคุมตัว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_pris_cate",
                                                                            $gencode->fld_usage = "pris_cate",
                                                                            $gencode->fld_label = "ประเภทสถานที่ควบคุมตัว",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>
                                    <label class="col-sm-2 control-label" for="fmi_imp_vist_date">วันที่เข้าเยี่ยม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-3">
                                          <div class='input-group date' id='fmi_imp_vist_date_sel'>
                                              <input type='text' class="form-control" id="fmi_imp_vist_date" name="fmi_imp_vist_date" placeholder="วัน/เดือน/ปี"/>
                                              <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                              </span>
                                          </div>
                                          <script type="text/javascript">
                                              $(function () {
                                                  $('#fmi_imp_vist_date_sel').datetimepicker({
                                                      format: 'DD/MM/YYYY'
                                                  });
                                              });
                                          </script>
                                    </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>
                                    <label class="col-sm-2 control-label" for="fmi_imp_det">สถานที่ควบคุมตัว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-3">
                                        <?php $gencode = new GencodeSboxList(
                                                                              $gencode->fld_name = "fmi_imp_det",
                                                                              $gencode->fld_usage = "pris_det",
                                                                              $gencode->fld_label = "สถานที่ควบคุมตัว",
                                                                              $gencode->fld_unique = "",
                                                                              $gencode->fld_size = "",
                                                                              $gencode->fld_readmode = "",
                                                                              $gencode->fld_onclick = "",
                                                                              $gencode->fld_onblur = "",
                                                                              $gencode->fld_list = "desc",
                                                                              $gencode->fld_disp = "desc",
                                                                              $gencode->fld_js = "",
                                                                              $gencode->fld_ref_uniq = ""
                                                                          );
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'imp_stat'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label" for="fmi_reg_cate">ประเภทงาน <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <?php $gencode = new GencodeSboxList(
                                                                            $gencode->fld_name = "fmi_imp_cate",
                                                                            $gencode->fld_usage = "imp_cate",
                                                                            $gencode->fld_label = "ประเภทงาน",
                                                                            $gencode->fld_unique = "",
                                                                            $gencode->fld_size = "",
                                                                            $gencode->fld_readmode = "",
                                                                            $gencode->fld_onclick = "",
                                                                            $gencode->fld_onblur = "",
                                                                            $gencode->fld_list = "desc",
                                                                            $gencode->fld_disp = "desc",
                                                                            $gencode->fld_js = "",
                                                                            $gencode->fld_ref_uniq = ""
                                                                        );
                                      ?>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'imp_vist'){ echo "style='display:none'";}?>>
                                    <label class="col-sm-2 control-label">จำนวนผู้ถูกควบคุมตัว <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-2">
                                        <div class='input-group gender'>
                                            <span class="input-group-addon">
                                                <span class="fa fa-male"></span>
                                            </span>
                                            <input type='text' class="form-control" id="fmi_imp_tot_male" name="fmi_imp_tot_male" placeholder="0" title="รวมเพศชาย"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class='input-group gender'>
                                            <span class="input-group-addon">
                                                <span class="fa fa-female"></span>
                                            </span>
                                            <input type='text' class="form-control" id="fmi_imp_tot_female" name="fmi_imp_tot_female" placeholder="0" title="รวมเพศหญิง"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] == 'imp_vist'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label" for="fmi_gencode_desc">คำอธิบาย <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <input type="text" name="fmi_gencode_desc" maxlength="100" class="form-control required"
                                          id="fmi_gencode_desc" placeholder="คำอธิบาย"/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label" for="fmi_gencode_remark">หมายเหตุ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <textarea class="form-control" name="fmi_gencode_remark" id="fmi_gencode_remark" rows="3" placeholder="หมายเหตุ"></textarea>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'pris_det'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label" for="fmi_state_disp">ภูมิภาค/รัฐ <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                  <div class="col-sm-10">
                                      <select id="fmi_state_disp" class="selectpicker" data-hide-disabled="true" data-live-search="true" onchange="selectList(this);">
                                        <option value="">กรุณาเลือก</option>
                                        <?php
                                          $state_str = file_get_contents("../../folder_script/countries/countries/malaysia.json");
                                          $json_state = json_decode($state_str, true);
                                          foreach ($json_state as $state_name => $json_state) {
                                              $state_value = $json_state['code']."@@@".$json_state['name'];
                                        ?>
                                          <option value="<?php echo $state_value?>"><?php echo $json_state['name']?></option>
                                        <?php
                                          }
                                        ?>
                                      </select>
                                      <input type="hidden" class="form-control required" id="fmi_state_code" name="fmi_state_code" value="">
                                      <input type="hidden" class="form-control required" id="fmi_state_desc" name="fmi_state_desc" value="">
                                      <script type="text/javascript">
                                          function selectList(list){
                                              var val_list = list.value;
                                              var str = val_list.split('@@@');
                                              document.getElementById('fmi_state_code').value = str[0];
                                              document.getElementById('fmi_state_desc').value = str[1];
                                          }
                                      </script>
                                  </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'impr_doc'){ echo "style='display:none'";}?>>
                                    <label class="col-sm-2 control-label" for="fmi_imp_doc">เอกสาร <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                                    <div class="col-sm-10">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-warning btn-file"><span class="fileupload-new"><i class="fa fa-paperclip" aria-hidden="true"></i> แนบเอกสาร</span>
                                            <span class="fileupload-exists"><i class="fa fa-paperclip" aria-hidden="true"></i> แนบเอกสาร</span><input name="fmi_imp_doc" id="fmi_imp_doc" type="file" onchange="populate_filename();"/></span>
                                            <button type="button" id="upload" class="btn btn-primary" onclick="frm_fileupload();" style="display:none"><i class="fa fa-cloud-upload" aria-hidden="true"></i> อัพโหลดไฟล์</button>
                                            <button type="button" id="ico_dowload" class="btn btn-success" onclick="openFile();"><i class="fa fa-cloud-download" aria-hidden="true"></i> ดาวน์โหลดไฟล์</button>
                                            <br/><br/>
                                            <a href="#" id="ico_del" title="ลบไฟล์" onclick="frm_deletefile();" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"><i class="fa fa-times" aria-hidden="true"></i></a> <span id="file_name" class="fileupload-preview"></span>
                                            <input type="hidden" id="fmi_file_name" name="fmi_file_name" value="">
                                        </div>
                                        <script type="text/javascript">
                                            !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
                                        </script>
                                    </div>
                                </div>
                                <div class="form-group" <?php if($_GET["fromtrans"] <> 'imp_cate'){ echo "style='display:none'";}?>>
                                  <label class="col-sm-2 control-label">ตั้งค่าเพิ่มเติม <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                                  <div class="col-sm-10 material-switch pull-right">
                                    เกี่ยวข้องคดีถูกจับกุม&nbsp;
                                    <input id="chk_imps" name="chk_imps" type="checkbox"/>
                                    <label for="chk_imps" class="label-info"></label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Footer -->
                        <?php require_once 'inc_main_form_footer.php';?>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
