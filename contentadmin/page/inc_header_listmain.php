<!DOCTYPE html>
<!-- inc_header_listmain.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';

$enable_def_search_listmain_yn = "n";
if((isset($_GET["fromtarget"]) && $_GET["fromtarget"] <> "report") && (isset($_GET["fromlink"]) && $_GET["fromlink"] <> "rep")){
    $enable_def_search_listmain_yn = "y";
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script>
            document.onkeydown=function(evt){
                var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
                if(keyCode == 13)
                {
                    formSearch();
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="row">&nbsp;&nbsp;&nbsp;<br>
            <div class="col-sm-8 text-left">
                <div class="chip">
                    <h2><i class="fa fa-th-large" aria-hidden="true"></i>&nbsp;<?php echo $label_header;if($ext_label != ""){?> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $ext_label;}?> <?php if($rep_header_desc != ""){echo " <i class='fa fa-angle-double-right' aria-hidden='true'></i> ".$rep_header_desc;}?></h2>
                </div>
            </div>
            <div class="col-sm-3 text-right" <?php if($enable_def_search_listmain_yn == "n"){?>style="display:none"<?php }?>>
                <form name="frm_gencode_search" class="navbar-form navbar-centre" enctype="multipart/form-data">
                    <div class="form-group">
                      <input type="text" name="form_search" id="form_search" class="form-control" placeholder="คำค้นหา...">
                    </div>
                    <button type="button" class="btn btn-success" onclick="formSearch();return false;"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</button>
                </form>
            </div>
        </div>
        <br>
    </body>
</html>
