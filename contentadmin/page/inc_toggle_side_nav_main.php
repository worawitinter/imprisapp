<!DOCTYPE html>
<!-- inc_toggle_side_nav_main.php -->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .page {
                padding: 0 5px;
            }
            .content {
                padding:0 5px 5px;
                position:relative;
                background: rgb(248, 249, 232); /* Fallback for older browsers without RGBA-support */
                background: rgba(248, 249, 232, 0.5);
            }

        </style>
        <script type="text/javascript">
            $("#menu-toggle").click(function(e) {
                var frommode = $("#fmi_frommode").val();
                if(frommode != "new"){
                    $.LoadingOverlay("show");
                    toggleAuditLogs();
                    e.preventDefault();
                }
            });

            $('document').ready(function(){
                var frommode = $("#fmi_frommode").val();
                if(frommode != "new"){
                    toggleAuditLogs();
                	var wHeight = $("#sidebar_height").val();
                    var wWidth = 790;
                    $(".page").height(wHeight);
                    $(".page").width(wWidth);
                    $('#content').slimScroll({
                        height: wHeight + 'px',
                        width: wWidth + 'px'
                    });
                }
            });

            function toggleAuditLogs(){
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                jQuery("#content").load("inc_audit_log_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&uniquenum_pri=" + uniquenum_pri);
                $.LoadingOverlay("hide");
            }
        </script>
    </head>
    <body>
        <div id="div_collapseLog" class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-7 collapse" id="collapseLog">
                <h4><span class="label label-danger">
                    <i class="fa fa-history" aria-hidden="true" style=" vertical-align: middle;"></i>
                    &nbsp;<b>ประวัติการแก้ไขรายการ >>></b>
                </span></h4>
                <input type="hidden" id="sidebar_height" name="sidebar_height" value="300">
                <div class="content" id="content"></div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </body>
</html>
