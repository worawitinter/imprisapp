<!-- inc_form_search_panel.php -->
<div id="pnl_imp_search" class="card" style="width: <?php echo PANEL_WIDTH;?>;height: 150px">
    <form name="frm_imp_search" enctype="multipart/form-data"><br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <label class="control-label" for="fmi_imp_laws">ข้อหาที่ถูกจับกุม <i class="fa fa-angle-double-right" aria-hidden="true"></i></label>
                    <?php $gencode = new GencodeSboxList(
                                                        $gencode->fld_name = "fmi_imp_laws",
                                                        $gencode->fld_usage = "reg_list",
                                                        $gencode->fld_label = "ข้อหาที่ถูกจับกุม",
                                                        $gencode->fld_unique = "",
                                                        $gencode->fld_size = "",
                                                        $gencode->fld_readmode = "",
                                                        $gencode->fld_onclick = "",
                                                        $gencode->fld_onblur = "",
                                                        $gencode->fld_list = "desc",
                                                        $gencode->fld_disp = "desc",
                                                        $gencode->fld_js = "",
                                                        $gencode->fld_ref_uniq = ""
                                                    );
                    ?>
                </div>
                <div class="col-lg-3">
                    <label class="control-label">สถานที่ควบคุมตัว <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                    <?php $gencode = new GencodeSboxList(
                                                          $gencode->fld_name = "fmi_imp_det",
                                                          $gencode->fld_usage = "pris_det",
                                                          $gencode->fld_label = "สถานที่ควบคุมตัว",
                                                          $gencode->fld_unique = "",
                                                          $gencode->fld_size = "",
                                                          $gencode->fld_readmode = "",
                                                          $gencode->fld_onclick = "",
                                                          $gencode->fld_onblur = "",
                                                          $gencode->fld_list = "desc",
                                                          $gencode->fld_disp = "desc",
                                                          $gencode->fld_js = "",
                                                          $gencode->fld_ref_uniq = ""
                                                      );
                    ?>
                </div>
                <div class="col-lg-3">
                    <label class="control-label">สถานะ <i class="fa fa-angle-double-left" aria-hidden="true"></i></label>
                    <?php $gencode = new GencodeSboxList(
                                                          $gencode->fld_name = "fmi_stat",
                                                          $gencode->fld_usage = "imp_stat",
                                                          $gencode->fld_label = "สถานะ",
                                                          $gencode->fld_unique = "",
                                                          $gencode->fld_size = "",
                                                          $gencode->fld_readmode = "",
                                                          $gencode->fld_onclick = "",
                                                          $gencode->fld_onblur = "",
                                                          $gencode->fld_list = "desc",
                                                          $gencode->fld_disp = "desc",
                                                          $gencode->fld_js = "",
                                                          $gencode->fld_ref_uniq = ""
                                                      );
                    ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-2">
                    <input type='text' class="form-control" id="fmi_date_from" name="fmi_date_from" placeholder="วัน/เดือน/ปี เริ่มต้น"/>
                    <script type="text/javascript">
                        $(function () {
                            $('#fmi_date_from').datetimepicker({
                                format: 'DD/MM/YYYY'
                            });
                        });
                    </script>
                </div>
                <div class="col-lg-2">
                    <input type='text' class="form-control" id="fmi_date_to" name="fmi_date_to" placeholder="วัน/เดือน/ปี สิ้นสุด"/>
                    <script type="text/javascript">
                        $(function () {
                            $('#fmi_date_to').datetimepicker({
                                format: 'DD/MM/YYYY'
                            });
                        });
                    </script>
                </div>
                <div class="col-lg-3">
                    <span class="pull-right">
                        <input type="text" name="fmi_imp_search" id="fmi_imp_search" size="50" class="form-control" placeholder="ค้นหา">
                    </span>
                </div>
                <div class="col-lg">
                    <button type="button" class="btn btn-info" onclick="formSearch();">ค้นหา</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- inc_form_search_panel.php -->
