<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/FileUpload.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';

$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();
$FileUpload = new FileUpload();
$TransactionGenUnique = new TransactionGenUnique();
$AuditTrailLog = new AuditTrailLog();

$tablename = $_POST["fmi_tablename"];
$tag_type = "imp_attchmt";
$file_upload = $_FILES["fmi_imp_file_upload"];
$file_name = $file_upload["name"][0];
$file_type = $file_upload["type"][0];
$file_path = $file_upload["tmp_name"][0];
$file_error = $file_upload["error"][0];
$file_size = $file_upload["size"][0];
$file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
if(isset($_GET["delete_file"]) && $_GET["delete_file"] == "y"){
    $uniquenum_pri = $_GET["del_uniquenum_pri"];
    $tablename = $_GET["tablename"];
}else{
    $uniquenum_pri = $_POST["fmi_uniquenum_pri"];
}
$new_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
$fromtrans = $_POST["fmi_fromtrans"];
$fromlink = $_POST["fmi_fromlink"];
$frommode = $_POST["fmi_frommode"];
$userid = $_POST["fmi_userid"];
$date_created = $TransactionGenUnique->generate_trans_datetime();
$json_output = '{
                    "name":"'.$file_name.'",
                    "type":"'.$file_type.'",
                    "tmp_name":"'.$file_path.'",
                    "error":"'.$file_error.'",
                    "size":"'.$file_size.'",
                    "uniquenum_pri":"'.$uniquenum_pri.'",
                    "fromtrans":"'.$fromtrans.'",
                    "fromlink":"'.$fromlink.'",
                    "frommode":"'.$frommode.'",
                    "userid":"'.$userid.'"
                }';

if(isset($file_upload)){
    $FileUpload->uploadFile($file_upload,$new_uniquenum_pri);
    $data = array   (   "tag_table_usage"=>$fromtrans,
                        "uniquenum_sec"=>$uniquenum_pri,
                        "uniquenum_pri"=>$new_uniquenum_pri,
                        "sys_link"=>$fromlink,
                        "var_25_001"=>$tag_type,
                        "var_25_002"=>$file_size,
                        "var_25_003"=>$file_ext,
                        "userid_cookie"=>$userid,
                        "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                        "date_created"=>$date_created,
                        "notes_memo"=>$file_name
                    );
    $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
}else if(isset($_GET["delete_file"]) && $_GET["delete_file"] == "y" && $uniquenum_pri != ""){
    $filename = "";
    $gencode_desc = "";
    $FileUpload->deleteFileIfExist($uniquenum_pri,$file_upload);
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$imp_autonum,$tablename,$uniquenum_pri,$userid);
}
echo "{}";
?>
