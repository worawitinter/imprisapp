<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170813             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysAutoNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/Authentication.php';

$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();
$SysConversion = new SysConversion();
$SysAutoNum = new SysAutoNum();
$AuditTrailLog = new AuditTrailLog();
$TransactionGenUnique = new TransactionGenUnique();
$Authentication = new Authentication();

$usr_title = $_POST["fmi_usr_title"];
$usr_firstname = $_POST["fmi_usr_firstname"];
$usr_lastname = $_POST["fmi_usr_lastname"];
$usr_enddate = $SysConversion->convertDate($_POST["fmi_usr_enddate"]);
$usr_dept = $_POST["fmi_usr_dept"];
$usr_contact = $_POST["fmi_usr_contact"];
$usr_email = $_POST["fmi_usr_email"];
$usr_username = $_POST["fmi_usr_username"];
$usr_password = $Authentication->encryptPassword($_POST["fmi_usr_password"]);
$usr_right = $SysConversion->convertArrayToList($_POST["fmi_usr_right"]);
$usr_role = $_POST["fmi_usr_role"];
$usr_remark = $_POST["fmi_usr_remark"];
if($frommode == "new"){
    $date_created = $TransactionGenUnique->generate_trans_datetime();
}else{
    $date_created = $_POST["fmi_date_created"];
}
if($frommode == "new"){
    $sys_autonum = explode("@@@", $_POST["fmi_".$fromtrans."_autonum"]);
    $sys_auto_num_upri = $sys_autonum[1];
    $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
    $new_sysnum = $SysAutoNum->checkDuplicateSysNum($fromtrans,$sys_autonum[0],$sys_auto_num_upri);
    if($new_sysnum == ""){
        $usr_autonum = $sys_autonum[0];
    }else{
        $usr_autonum = $new_sysnum;
    }
}else{
    $usr_autonum = $_POST["fmi_ori_".$fromtrans."_autonum"];
}

$tablename_main = "sys_user_main";
$tablename_data = "sys_user_data";

$data_main = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_pri"=>$uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                    "date_002"=>$usr_enddate,
                    "username"=>$usr_username,
                    "password"=>$usr_password,
                    "sys_role"=>$usr_role,
                    "sys_acc_right"=>$usr_right,
                    "var_50_003"=>$usr_autonum
                );

                if($_POST["fmi_usr_password"] <> ''){
                    $index = array_search('password',$data_main);
                    if($index !== FALSE){
                        unset($array[$index]);
                    }
                }


$data_data = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_pri"=>$uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                    "usr_title"=>$usr_title,
                    "usr_firstname"=>$usr_firstname,
                    "usr_lastname"=>$usr_lastname,
                    "var_25_001"=>$usr_dept,
                    "usr_tel_01"=>$usr_contact,
                    "usr_email_01"=>$usr_email,
                    "notes_memo"=>$usr_remark
                );
if($frommode == "new"){
    $result_main = $DatabaseOperation->insertRecordIntoTable($tablename_main,$data_main);
    $SysAutoNum->setSysNum($fromtrans,$usr_autonum,$sys_auto_num_upri,$fromlink,$userid,$date_created);
    $result_data = $DatabaseOperation->insertRecordIntoTable($tablename_data,$data_data);
    $AuditTrailLog->createActivityLogs($frommode,$fromtrans,$fromlink,$usr_autonum,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created);
}elseif($frommode == "edit"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$usr_autonum,$tablename_main,$uniquenum_pri,$userid);
    $result_main = $DatabaseOperation->insertRecordIntoTable($tablename_main,$data_main);

    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$usr_autonum,$tablename_data,$uniquenum_pri,$userid);
    $result_data = $DatabaseOperation->insertRecordIntoTable($tablename_data,$data_data);
    $SysMainActivity->lockRecord($fromlink,$fromtrans,$uniquenum_pri,$userid);
}elseif($frommode == "delete"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$usr_autonum,$tablename_main,$uniquenum_pri,$userid);
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$usr_autonum,$tablename_data,$uniquenum_pri,$userid);
    $SysMainActivity->releaseRecord($fromtrans,$uniquenum_pri);
}
?>
