<!DOCTYPE html>
<!-- inc_imp_remark_main_list.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';

$SysListMain = new SysListMain();

$uniquenum_pri = $_GET["uniquenum_pri"];
$fromtrans = $_GET["fromtrans"];

$qs_result = $SysListMain->getRowRemarkResult($fromtrans,$uniquenum_pri);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
        .chat
        {
        list-style: none;
        margin: 0;
        padding: 0;
        }

        .chat li
        {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
        }

        .chat li.left .chat-body
        {
        margin-left: 60px;
        }

        .chat li.right .chat-body
        {
        margin-right: 60px;
        }

        .chat li .chat-body p
        {
        margin: 0;
        color: #777777;
        }

        .panel .slidedown .glyphicon, .chat .glyphicon
        {
        margin-right: 5px;
        }
        </style>
        <script>

        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <?php
                    if($qs_result->rowcount() == 0){
                        echo "<i class='fa fa-frown-o' aria-hidden='true'></i> <i>ไม่พบประวัติการบันทึกหมายเหตุ...</i>";
                    }else{
                ?>
                    <ul class="chat">
                        <?php
                            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                        ?>
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=<?php echo substr($rows["userid_cookie"],0,1);?>" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"><?php echo $rows["usr_firstname"]." ".$rows["usr_lastname"];?></strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span><?php echo $rows["date_created"];?></small>
                                </div>
                                <p>
                                    <?php echo $rows["notes_memo"];?>
                                </p>
                            </div>
                        </li>
                        <?php
                            }
                        ?>
                    </ul>
                <?php
                    }
                ?>
                </div>
            </div>
        </div>
    </body>
</html>
