<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170813             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/FileUpload.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysAutoNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';

$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();
$FileUpload = new FileUpload();
$TransactionGenUnique = new TransactionGenUnique();
$SysAutoNum = new SysAutoNum();
$AuditTrailLog = new AuditTrailLog();
$TransactionGenUnique = new TransactionGenUnique();
$SysConversion = new SysConversion();

if(isset($_FILES['fmi_imp_doc']['name'])){
    $imp_doc = array("name"=>array("0"=>$_FILES['fmi_imp_doc']['name']),
                        "type"=>array("0"=>$_FILES['fmi_imp_doc']['type']),
                        "tmp_name"=>array("0"=>$_FILES['fmi_imp_doc']['tmp_name']),
                        "error"=>array("0"=>$_FILES['fmi_imp_doc']['error']),
                        "size"=>array("0"=>$_FILES['fmi_imp_doc']['size']),
                    );
}

$tablename = "sys_gencode_main";
$gencode_desc = $_POST["fmi_gencode_desc"];
$gencode_remark = $_POST["fmi_gencode_remark"];
$state_code = $_POST["fmi_state_code"];
$state_desc = $_POST["fmi_state_desc"];
$filename = $_POST["fmi_file_name"];
$reg_cate_uniq = $_POST["fmi_reg_cate"];
$pris_cate_uniq = $_POST["fmi_pris_cate"];
if($frommode == "new"){
    $date_created = $TransactionGenUnique->generate_trans_datetime();
}else{
    $date_created = $_POST["fmi_date_created"];
}
$userid = $_POST["fmi_userid"];
if($frommode == "new"){
    $sys_autonum = explode("@@@", $_POST["fmi_".$fromtrans."_autonum"]);
    $sys_auto_num_upri = $sys_autonum[1];
    $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
    $new_sysnum = $SysAutoNum->checkDuplicateSysNum($fromtrans,$sys_autonum[0],$sys_auto_num_upri);
    if($new_sysnum == ""){
        $gencode_code = $sys_autonum[0];
    }else{
        $gencode_code = $new_sysnum;
    }
}else{
    $gencode_code = $_POST["fmi_ori_".$fromtrans."_autonum"];
}
$imp_vist_date = $SysConversion->convertDate($_POST["fmi_imp_vist_date"]);
$imp_det_uniq = $_POST["fmi_imp_det"];
$imp_cate_uniq = $_POST["fmi_imp_cate"];

if($_POST["fmi_imp_tot_male"] <> '' && $_POST["fmi_imp_tot_male"] <> 'undefined'){
    $imp_tot_male = $_POST["fmi_imp_tot_male"];
}else{
    $imp_tot_male = 0;
}

if($_POST["fmi_imp_tot_female"] <> '' && $_POST["fmi_imp_tot_female"] <> 'undefined'){
    $imp_tot_female = $_POST["fmi_imp_tot_female"];
}else{
    $imp_tot_female = 0;
}

if(isset($_POST["chk_imps"])){
    $sys_chk_imps = "y";
}else{
    $sys_chk_imps = "n";
}

$data = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_pri"=>$uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "setgen_unique"=>$uniquenum_pri,
                    "setgen_code"=>$gencode_code,
                    "desc_lang01"=>$gencode_desc,
                    "var_25_001"=>$state_code,
                    "var_100_001"=>$state_desc,
                    "notes_memo"=>$gencode_remark,
                    "userid_cookie"=>$userid,
                    "date_created"=>$date_created,
                    "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                    "var_100_002"=>$filename,
                    "var_25_002"=>$reg_cate_uniq,
                    "var_25_003"=>$pris_cate_uniq,
                    "num_02_d_001"=>$imp_tot_male,
                    "num_02_d_002"=>$imp_tot_female,
                    "date_001"=>$imp_vist_date,
                    "var_25_005"=>$imp_det_uniq,
                    "var_25_004"=>$imp_cate_uniq,
                    "tag_other01_yn"=>$sys_chk_imps
                );

if(isset($imp_doc)){
    $FileUpload->uploadFile($imp_doc,$_GET["uniquenum_pri"]);
    if(isset($_GET["frommode"]) && $_GET["frommode"] == "edit"){
        $data = array   (   "userid_cookie"=>$userid,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                            "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                            "desc_lang01"=>$gencode_desc,
                            "var_100_002"=>$filename
                        );
        $condition = "uniquenum_pri = '".$_GET["uniquenum_pri"]."'";
        $result = $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);
    }
}else if(isset($_GET["delete_file"]) && $_GET["delete_file"] == "y" && $_POST["fmi_file_name"] != ""){
    $filename = "";
    $gencode_desc = "";
    $FileUpload->deleteFileIfExist($_POST["uniquenum_pri"],$_POST["fmi_file_name"]);
    $data = array   (   "userid_cookie"=>$userid,
                        "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                        "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                        "desc_lang01"=>$gencode_desc,
                        "var_100_002"=>$filename
                    );
    $condition = "uniquenum_pri = '".$_POST["uniquenum_pri"]."'";
    $result = $DatabaseOperation->updateRecordToTable($tablename,$data,$condition);
}else{
    if($frommode == "new" && $_POST["fmi_".$fromtrans."_autonum"] != ""){
        $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
        $SysAutoNum->setSysNum($fromtrans,$gencode_code,$sys_auto_num_upri,$fromlink,$userid,$date_created);
        $AuditTrailLog->createActivityLogs($frommode,$fromtrans,$fromlink,$gencode_code,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created);
    }elseif($frommode == "edit"){
        $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$gencode_code,$tablename,$uniquenum_pri,$userid);
        $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
        $SysMainActivity->lockRecord($fromlink,$fromtrans,$uniquenum_pri,$userid);
    }elseif($frommode == "delete"){
        $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$gencode_code,$tablename,$uniquenum_pri,$userid);
        $FileUpload->deleteFileIfExist($uniquenum_pri,$_POST["fmi_file_name"]);
        $SysMainActivity->releaseRecord($fromtrans,$uniquenum_pri);
    }
}
?>
