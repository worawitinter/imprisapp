<!-- inc_rep_hidden_values.php -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#fmi_rep_export').on('change', function() {
                    var datefrom=$.trim($('#datefrom').val());
                    var dateto=$.trim($('#dateto').val());
                    var state_code=$.trim($('#state_code').val());
                    var imp_laws=$.trim($('#imp_laws').val());
                    var imp_gender=$.trim($('#imp_gender').val());
                    var imp_det=$.trim($('#imp_det').val());
                    var imp_stat=$.trim($('#imp_stat').val());
                    var search_string=$.trim($('#search_string').val());
                    var rept_output = $.trim($('#rept_output').val());
                    var rept_output = $.trim($('#rept_output').val());
                    var provn_class = $.trim($('#fmi_provn_class_disp').val());
                    var date_type = $.trim($('#fmi_date_type').val());
                    var multi_state = $.trim($('#fmi_multi_state_disp').val());
                    var imp_cate = $.trim($('#fmi_imp_cate').val());
                    var imp_cshbrw_stat = $.trim($('#fmi_imp_cshbrw_stat').val());
                    var date_debt_from=$.trim($('#fmi_date_debt_from').val());
                    var date_debt_to=$.trim($('#fmi_date_debt_to').val());
                    var target_url = 'inc_<?php echo $_GET["fromlink"];?><?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&datefrom=' + datefrom + '&dateto=' + dateto + '&state_code=' + state_code + '&imp_laws=' + imp_laws + '&imp_gender=' + imp_gender + '&imp_det=' + imp_det + '&imp_stat=' + imp_stat + '&search_string=' + search_string + '&provn_class=' + provn_class + '&date_type=' + date_type + '&multi_state=' + multi_state + '&imp_cate=' + imp_cate + "&imp_cshbrw_stat=" + imp_cshbrw_stat + '&date_debt_from=' + date_debt_from + '&date_debt_to=' + date_debt_to;
                    if(this.value == 'pdf'){
                        window.open(target_url + '&rept_output=pdf');
                    }else{
                        $('#rept_style').tableExport({type:this.value,escape:'false'});
                    }
                });
            });
        </script>
    </head>
    <body>
        <input type="hidden" id="datefrom" name="datefrom" value="<?php echo $_GET["datefrom"];?>">
        <input type="hidden" id="dateto" name="dateto" value="<?php echo $_GET["dateto"];?>">
        <input type="hidden" id="state_code" name="state_code" value="<?php echo $_GET["state_code"];?>">
        <input type="hidden" id="imp_laws" name="imp_laws" value="<?php echo $_GET["imp_laws"];?>">
        <input type="hidden" id="imp_gender" name="imp_gender" value="<?php echo $_GET["imp_gender"];?>">
        <input type="hidden" id="imp_det" name="imp_det" value="<?php echo $_GET["imp_det"];?>">
        <input type="hidden" id="imp_stat" name="imp_stat" value="<?php echo $_GET["imp_stat"];?>">
        <input type="hidden" id="search_string" name="search_string" value="<?php echo $_GET["search_string"];?>">
        <input type="hidden" id="rept_output" name="rept_output" value="<?php echo $_GET["rept_output"];?>">
        <input type="hidden" id="provn_class" name="provn_class" value="<?php echo $_GET["provn_class"];?>">
        <input type="hidden" id="date_type" name="date_type" value="<?php echo $_GET["date_type"];?>">
        <input type="hidden" id="multi_state" name="multi_state" value="<?php echo $_GET["multi_state"];?>">
        <input type="hidden" id="imp_cate" name="imp_cate" value="<?php echo $_GET["imp_cate"];?>">
        <input type="hidden" id="imp_cshbrw_stat" name="imp_cshbrw_stat" value="<?php echo $_GET["imp_cshbrw_stat"];?>">
        <input type="hidden" id="date_debt_from" name="datefrom" value="<?php echo $_GET["date_debt_from"];?>">
        <input type="hidden" id="date_debt_to" name="dateto" value="<?php echo $_GET["date_debt_to"];?>">
    </body>
</html>
