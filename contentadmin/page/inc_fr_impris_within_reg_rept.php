<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';

$SysReport = new SysReport();
$SysListMain = new SysListMain();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$stly_lt = "border-left: 1px solid #000000;border-top: 1px solid #000000;";
$stly_lb = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lbr = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltr = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lrtb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000;";

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
    ob_start();
}

if(isset($_GET["fromtrans"])){
    $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
}
$fmi_imp_date_from = "";
$fmi_imp_date_to = "";
$fmi_imp_laws = "";
$fmi_search_string = "";
$uniquenum_pri = "";
$tag_audit_yn = "n";
$fmi_date_type = "";
$fmi_imp_cate = "";
if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
if(!empty($_GET["imp_laws"])){$fmi_imp_laws = $_GET["imp_laws"];}
if(!empty($_GET["search_string"])){$fmi_search_string = $_GET["search_string"];}
if(!empty($_GET["uniquenum_pri"])){$uniquenum_pri = $_GET["uniquenum_pri"];}
if(isset($_GET["tag_audit_yn"])){$tag_audit_yn = $_GET["tag_audit_yn"];}
if(!empty($_GET["date_type"])){$fmi_date_type = $_GET["date_type"];}
if(!empty($_GET["imp_cate"])){$fmi_imp_cate = $_GET["imp_cate"];}

$search_query = "";
if($fmi_date_type == 'date_impris'){
    $fld_date_col = "date_002";
}else{
    $fld_date_col = "date_created";
}
if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
}else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
}
if(!empty($fmi_imp_laws)){
    $search_query = $search_query . " AND det.var_25_002 = '".$fmi_imp_laws."'";
}
if(!empty($uniquenum_pri)){
    $search_query = $search_query . " AND imp.uniquenum_pri = '".$uniquenum_pri."'";
}
if($tag_audit_yn == "y"){
    $search_query = $search_query . " AND imp.tag_deleted_yn = 'ed'";
}else{
    $search_query = $search_query . " AND imp.tag_deleted_yn = 'n'";
}

if(!empty($fmi_search_string)){
    $search_query = $search_query . " AND (lower(imp.desc_lang01) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang02) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.var_25_003) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.notes_memo) like '%".trim(strtolower($fmi_search_string))."%')";
}

if(isset($_GET["chk_rec_drf"]) && $_GET["chk_rec_drf"] == "n"){     
    $search_query = $search_query . "  and imp.tag_other01_yn = 'n'"; 
}

$qs_result = $SysReport->getImprisWithinReg($search_query);

$rept_name = $rep_header_desc;
$rept_range = "ระหว่างวันที่ ".$_GET["datefrom"]." จนถึง ".$_GET["dateto"];
$rept_filename = $rept_name."_".$_GET["datefrom"]."_".$_GET["dateto"];
$rept_header = "|".$rept_name."<br>".$rept_range."|";
$rept_footer = $_SESSION["cookies_username"]."|".$SysMainActivity->getClientIpAddress()."|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding=0 cellspacing=0 autosize="1">
                <?php
                    $cnt = 0;
                    while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                        $cnt = $cnt + 1;
                ?>
                    <tr>
                        <td width="100%" colspan="2" align="center" style="<?php echo $stly_lrtb;?>"><b>ประวัติผู้กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ</b></td>
                    </tr>
                <tr>
                    <td width="100%" colspan="2" align="left" style="<?php echo $stly_lbr;?>"><b>สอท./สกญ./สนง. การค้าฯ</b></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lb;?>"><b>ชื่อ-นามสกุล (ภาษาไทย)</b><br><?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?></td>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>ชื่อ-นามสกุล (ภาษาอังกฤษ)</b><br><?php echo $rows["imp_title_2"]." ".$rows["imp_firstname_2"]." ".$rows["imp_lastname_2"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lb;?>"><b>หมายเลขประจำตัวประชาชนประชาชน</b><br><?php echo $rows["imp_idcard"];?></td>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>หมายเลขหนังสือเดินทาง</b><br><?php echo $rows["imp_passport"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lb;?>"><b>หมายเลขหนังสือสำคัญประจำตัว (C.I.)</b><br><?php echo $rows["imp_ci"];?></td>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>หมายเลขหนังสือเดินทางชั่วคราว</b><br><?php echo $rows["imp_temp_passport"];?></td>
                </tr>
                <tr>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lb;?>"><b>หนังสือผ่านแดน (Border Pass)</b><br><?php echo $rows["imp_bdr_pass"];?></td>
                    <td width="50%" align="left" style="height:50; vertical-align: text-top;<?php echo $stly_lbr;?>">&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="left" style="height:100; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>ภูมิลำเนา</b><br><?php echo $rows["imp_addrr"]." ".$rows["imp_subdistrict"]." ".$rows["imp_district"]." ".$rows["imp_province"]." ".$rows["imp_zipcode"];?></td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="left" style="height:100; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>เรื่องที่กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ</b><br><?php echo $rows["imp_reg_desc"];?></td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="left" style="height:550; vertical-align: text-top;<?php echo $stly_lbr;?>"><b>ความเห็น สอท./สกญ./สนง. การค้าฯ และ ความเห็นของหน่วยงานท้องถิ่น (ตำรวจ/อัยการ/ศาล)</b><br><br>
                        <?php
                        $qs_remark = $SysListMain->getRowRemarkResult($fromtrans,$rows["uniquenum_pri"]);
                        while($list_remark = $qs_remark->fetch(PDO::FETCH_ASSOC)){
                            echo $list_remark["notes_memo"]." (โดย ".$list_remark["usr_firstname"]." ".$list_remark["usr_lastname"]." : ".$SysConversion->convertDateFormat($list_remark["date_created"],"d/m/Y h:i:s").")"."<br><br>";
                        }
                        ?>
                    </td>
                </tr>
                <?php
                    } /* End while loop $qs_result */
                ?>
                </table>
            </div>
        </div>
        <?php require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_hidden_values.php';?>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    $pdf = new mPDF('th', 'A4-P', '10', 'THSaraban','15','15','15','10');
    $pdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('../../folder_script/css_report_generator.css'); // external css
    $pdf->SetTitle($rept_name);
    $pdf->defaultfooterfontstyle='I';
    $pdf->setFooter($rept_footer);
    $pdf->WriteHTML($stylesheet,1);
    $pdf->WriteHTML($html, 2);
    $pdf->SetJS('window.print(); window.close();');
    ob_end_clean();
    $pdf->Output($rept_filename.'.pdf', 'I');
}
?>
