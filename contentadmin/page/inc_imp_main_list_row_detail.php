<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysListMain = new SysListMain();
$SysConversion = new SysConversion();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $qs_result_detail = $SysListMain->getRowImprisResult($_GET["fromtrans"],$r_uniquenum_pri,'','','',$tag_audit_yn = 'n');
            while($r_detail = $qs_result_detail->fetch(PDO::FETCH_ASSOC)){
        ?>
        <tr>
            <td colspan="5" width="90%">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="5%">
                            <span style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #e1fce0;width:250px;height:200px;" title="ภูมิลำเนา"><i class="fa fa-home" aria-hidden="true"></i> <i> <?php echo wordwrap($r_detail["imp_addrr"]." ".$r_detail["imp_subdistrict"]." ".$r_detail["imp_district"]." ".$r_detail["imp_province"]." ".$r_detail["imp_zipcode"]); if($r_detail["imp_telephone"] != ""){ echo "<br><i class='fa fa-tty' aria-hidden='true'></i> ".$r_detail["imp_telephone"];}?></i>
                            </span>
                        </td>
                        <td width="95%" valign="top">&nbsp;&nbsp;&nbsp;
                            <span style="padding-bottom: 0px;padding-top: 0px;padding-right: 0px;padding-left: 0px;display: inline-block;border-radius:5px;background-color: #ffffff;width:20%;height:auto;">
                                <?php if($r_detail["sys_autonum"] != ""){echo "<span style='padding-bottom: 5px;padding-top: 5px;padding-right: 10px;padding-left: 10px;border-radius:3px;background-color:#060d93;color:#ffffff;' title='หมายเลขรายการ'><b><i class='fa fa-angle-double-right' aria-hidden='true'></i> ".$r_detail["sys_autonum"]."</b></span><br>";}?>
                                <?php
                                    if($r_detail["imp_gender"] != ""){
                                        if($r_detail["imp_gender"] == "f"){
                                            echo '<span style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;border-radius:3px;background-color:#ff1680;color:#ffffff;"><i class="fa fa-female" aria-hidden="true" title="เพศหญิง"></i></span>&nbsp;&nbsp;&nbsp;';
                                        }else{
                                            echo '<span style="padding-bottom: 5px;padding-top: 5px;padding-right: 5px;padding-left: 5px;border-radius:3px;background-color:#16aaff;color:#ffffff;"><i class="fa fa-male" aria-hidden="true" title="เพศชาย"></i></span>&nbsp;&nbsp;&nbsp;';
                                        }
                                    }
                                ?>
                                <b>อายุ:</b> <?php echo $r_detail["imp_age"];?> ปี<br>
                                <b>หมายเลขประจำตัวประชาชน:</b> <?php echo $r_detail["imp_idcard"];?><br>
                                <b>หมายเลขหนังสือเดินทาง:</b> <?php echo $r_detail["imp_passport"];?><br>&nbsp;
                            </span>
                            <span style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #fefde5;width:50%;height:auto;font-size:12px;">
                                <b>สังกัด:</b> <?php echo $r_detail["imp_dept_desc"];?><br>
                                <b>เลขที่เอกสาร:</b> <?php echo $r_detail["imp_refdoc"];?><br>
                                <b>วันที่ถูกจับกุม:</b> <?php echo $SysConversion->convertDateFormat($r_detail["imp_prisdate"],"d-m-Y");?><br>
                                <b>ประเภทข้อหาที่ถูกจับกุม:</b> <?php echo $r_detail["laws_cate_desc"];?>&nbsp;&nbsp;&nbsp;<b>ข้อหาที่ถูกจับกุม:</b> <?php echo $r_detail["imp_reg_desc"];?><br>
                                <b>ประเภทสถานที่ควบคุมตัว:</b> <?php echo $r_detail["pris_cate_desc"];?>&nbsp;&nbsp;&nbsp;<b>สถานที่ควบคุมตัว:</b> <?php echo $r_detail["imp_det_desc"].", ".$r_detail["state_desc"];?>
                            </span>
                            <span style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #ffffff;width:20%;height:auto;font-size:12px;">
                                <b>วันที่ยืม:</b> <?php echo $SysConversion->convertDateFormat($r_detail["imp_cshbrw_date"],"d-m-Y");?><br>
                                <b>จำนวนเงินทดรอง (RM):</b> <?php echo number_format($r_detail["imp_cshbrw_amt"],2);?><br>
                                <b>หมายเลขอ้างอิง:</b> <?php echo $r_detail["imp_cshbrw_ref"];?><br>
                                <?php
                                    if($r_detail["imp_cshbrw_stat"] == "y"){
                                        echo '<span style="padding-bottom: 0px;padding-top: 0px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #006600;color:#ffffff"><b>ได้รับคืนจากกระทรวงฯ แล้ว</b></span>';
                                    }elseif($r_detail["imp_cshbrw_stat"] == "s"){
                                        echo '<span style="padding-bottom: 0px;padding-top: 0px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #990000;color:#ffffff"><b>ยังไม่ได้รับคืนจากกระทรวงฯ</b></span>';
                                    }else{
                                        echo '<span style="padding-bottom: 0px;padding-top: 0px;padding-right: 10px;padding-left: 10px;display: inline-block;border-radius:5px;background-color: #e67300;color:#ffffff"><b>ไม่พบข้อมูลเงินทดรองราชการ</b></span>';
                                    }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php
            } /* End while loop $qs_result */
        ?>
    </body>
</html>
