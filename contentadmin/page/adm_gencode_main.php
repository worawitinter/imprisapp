<!-- adm_gencode_main.php -->
<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
        <script type="text/javascript">
            $(document).on("click", ".parse-frommode", function () {
                var frommode = $(this).data('frommode');
                var fromtrans = $("#fmi_fromtrans").val();
                $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                $('.modal-body').find('button').removeClass( "btn dropdown-toggle disabled btn-default" ).addClass( "btn dropdown-toggle btn-default" );
                $(".modal-content #fmi_frommode").val( frommode );
                $('.modal-body input').val('');
                $('.modal-body textarea').val('');
                $("#fmi_imp_doc").val("");
                $('#ico_dowload').hide();
                $('#ico_del').hide();
                $('#file_name').hide();
                $('#fmi_ori_'+ fromtrans +'_autonum').hide();
                $('#fmi_'+ fromtrans +'_autonum_list').show();
                $("#fmi_reg_cate").val("");
                $("#fmi_pris_cate").val("");
                parseModalHeaderDesc();
                getNewUniqueID();
                getAutoSysNum(fromtrans);
                chk_exist_record();
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_lock").hide();
                $("#frm_unlock").hide();
                $("#frm_cancel").show();
                $("#frm_submit").show();
                //$('#frm_submit').attr('disabled',false);
                //$('#frm_cancel').attr('disabled',false);
            });

            $(document).on("click", ".row_upd", function () {
                var frommode = $(this).data('frommode');
                var upd_id = $(this).data('id');
                var fromtrans = $("#fmi_fromtrans").val();
                $(".modal-content #fmi_frommode").val( frommode );
                $(".modal-content #fmi_uniquenum_pri").val( upd_id );
                //alert(document.getElementById('fmi_frommode').value);
                if(frommode == 'view'){
                    var tag_audit_yn = "y";
                }else{
                    var tag_audit_yn = "n";
                }
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                getRowUpdate(tag_audit_yn);
                parseModalHeaderDesc();
                chkLockRecord();
                autoChkLockRecord();
                $('#fmi_ori_'+ fromtrans +'_autonum').show();
                $('#fmi_'+ fromtrans +'_autonum_list').hide();
                setTimeout(function(){ if($('#fmi_file_name').val() == ""){$('#ico_dowload').hide();$('#ico_del').hide();} }, 180);
                if(frommode == 'view'){
                    $("#frm_footer").hide();
                    $("#menu-toggle").hide();
                    $("#sidebar-wrapper").hide();
                    $('.modal-body').find('input, textarea, button, select').attr('disabled','disabled');
                }else{
                    $("#frm_footer").show();
                    $("#sidebar-wrapper").show();
                    $("#menu-toggle").show();
                    $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                }

                $('#upload').prop('disabled', true);
                $('[type=file]').change(
                    function(){
                        if ($(this).val()) {
                            $('#upload').attr('disabled',false);
                        }else{
                            $('#upload').prop('disabled', true);
                        }
                    }
                );
            });

            function confirmDelete(row,id){
                var sys_autonum = $("#r_sys_autonum" + row).val();
                $.confirm({
                    title: '',
                    content: 'ต้องการลบหมายเลขรายการ ' + sys_autonum + '?',
                    icon: 'fa fa-trash',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก',
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-danger',
                            action: function(){
                                $(".modal-content #fmi_frommode").val('delete');
                                $(".modal-content #fmi_uniquenum_pri").val(id);
                                $.LoadingOverlay("show");
                                $.ajax({
                                    type: 'post',
                                    url: 'adm_gencode_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        toggleAlertBox('','ลบหมายเลขรายการ ' + sys_autonum + ' สำเร็จ!','fa fa-trash','green');
                                        jQuery("#target-content").load("inc_adm_gencode_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function frm_deletefile(){
                $.ajax({
                    type: 'post',
                    url: 'adm_gencode_oup.php?delete_file=y',
                    data: $('form').serialize(),
                    success: function(){
                        document.getElementById("fmi_file_name").value = "";
                    }
                 });
            }

            function getNewUniqueID(){
                var tag_usage = 'autogen_upri';
                $.ajax({
                    url: '../control/inc_ajax_text.php',
                    data: "tag_usage=" + tag_usage,
                    dataType: 'text',
                    success: function(data)
                    {
                        document.getElementById('fmi_uniquenum_pri').value = data;
                    }
                });
            }

            function getRowUpdate(tag_audit_yn){
                var fromlink = document.getElementById('fmi_fromlink').value;
                var fromtrans = $("#fmi_fromtrans").val();
                var uniquenum_pri = document.getElementById('fmi_uniquenum_pri').value;
                $.ajax({
                    url: '../control/inc_ajax_fetch_row_update.php',
                    data: "fromlink=" + fromlink + "&tag_usage=" + fromtrans + "&uniquenum_pri=" + uniquenum_pri + "&tag_audit_yn=" + tag_audit_yn,
                    dataType: 'json',
                    success: function(data)
                    {
                        document.getElementById('fmi_ori_'+ fromtrans +'_autonum').value = data[0].gencode_code;
                        document.getElementById('fmi_gencode_desc').value = data[0].gencode_desc;
                        document.getElementById('fmi_gencode_remark').value = data[0].gencode_remark;
                        document.getElementById('fmi_state_code').value = data[0].state_code;
                        document.getElementById('fmi_state_desc').value = data[0].state_desc;
                        document.getElementById('fmi_state_disp').value = data[0].state_code + '@@@' + data[0].state_desc;
                        document.getElementById('file_name').textContent = data[0].file_name;
                        document.getElementById('fmi_file_name').value = data[0].file_name;
                        document.getElementById('fmi_date_created').value = data[0].date_created;
                        document.getElementById('fmi_imp_tot_male').value = data[0].imp_tot_male;
                        document.getElementById('fmi_imp_tot_female').value = data[0].imp_tot_female;
                        document.getElementById('fmi_imp_vist_date').value = moment(data[0].imp_vist_date).format('DD/MM/YYYY');

                        if(data[0].file_name != ''){
                            $('#ico_dowload').show();
                            $('#file_name').show();
                            $('#ico_del').show();
                        }
                        $('#fmi_state_disp').selectpicker('refresh');

                        $('select[name=fmi_reg_cate]').val(data[0].reg_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_reg_cate option[value='"+ data[0].reg_cate_uniq +"']");
                        $("select[name=fmi_reg_cate]").selectpicker("refresh");

                        $('select[name=fmi_pris_cate]').val(data[0].pris_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_pris_cate option[value='"+ data[0].pris_cate_uniq +"']");
                        $("select[name=fmi_pris_cate]").selectpicker("refresh");

                        $('select[name=fmi_imp_det]').val(data[0].imp_det_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_det option[value='"+ data[0].imp_det_uniq +"']").text(data[0].imp_det_desc);
                        $("select[name=fmi_imp_det]").selectpicker("refresh");

                        $('select[name=fmi_imp_cate]').val(data[0].imp_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_cate option[value='"+ data[0].imp_cate_uniq +"']");
                        $("select[name=fmi_imp_cate]").selectpicker("refresh");

                        if(data[0].chk_imps == 'y'){
                            document.getElementById('chk_imps').checked = true;
                        }else{
                            document.getElementById('chk_imps').checked = false;
                        }
                    }
                });
            }

            function formSearch(){
                if(typeof document.getElementById('form_search') === 'object'){
                    $.LoadingOverlay("show");
                    var fmi_form_search = document.getElementById('form_search').value;
    				jQuery("#target-content").load("inc_adm_gencode_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1&search_query="+fmi_form_search);
                    jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&search_query="+fmi_form_search);
                    $.LoadingOverlay("hide");
                }
            }

            function chk_exist_record(){
                var tablename = '<?php echo $tablename?>';
                var fromtrans = $("#fmi_fromtrans").val();
                $('#fmi_gencode_desc').blur(function(){
                    var src_fld = $('#fmi_gencode_desc').val();
                    var chk_fld = 'desc_lang01';
                    chkExistField(tablename,fromtrans,src_fld,chk_fld);
                });
            }

            $(document).ready(function() {
                $('#frm_adm_gencode').on('hidden.bs.modal', function () {
                    if(IntChkLock != null){
                        clearInterval(IntChkLock);
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="card" style="width:<?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <?php require_once 'inc_adm_gencode_main_form.php';
                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_imp_attchmt_main_form.php';
            ?>
            <div class="container">
                <?php
                    require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6"></div>
                                    <div class="col col-xs-6 text-right">
                                        <a <?php if($_SESSION["cookies_sys_create_yn"] != 'y'){ ?>style="display:none"<?php }?>href="main_screen<?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"]."_sys_autonum";?>&fromlink=sys_autonum&fromtarget=list&frommode=view" title="ตั้งค่าหมายเลขอัตโนมัติ" class="parse-frommode btn btn-primary" style="color:#ffffff"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                                        <a data-toggle="modal" data-frommode="new" data-target="#frm_adm_gencode" title="เพิ่ม" class="parse-frommode btn btn-primary" href="#frm_adm_gencode" style="color:#ffffff"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" id="target-content"></div>
                            <?php
                                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_pagination_main.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
