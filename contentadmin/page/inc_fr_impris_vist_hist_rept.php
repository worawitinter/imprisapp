<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';

$SysReport = new SysReport();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$stly_lt = "border-left: 1px solid #000000;border-top: 1px solid #000000;";
$stly_lb = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lbr = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltr = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lrtb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000;";

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
    ob_start();
}

if(isset($_GET["fromtrans"])){
    $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
}

$fmi_imp_date_from = "";
$fmi_imp_date_to = "";
$fmi_imp_det = "";
$fmi_date_type = "";
$fmi_multi_state = "";
if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
if(!empty($_GET["imp_det"])){$fmi_imp_det = $_GET["imp_det"];}
if(!empty($_GET["date_type"])){$fmi_date_type = $_GET["date_type"];}
if(!empty($_GET["multi_state"])){$fmi_multi_state = $_GET["multi_state"];}

$search_query = "";
if($fmi_date_type == 'date_impris'){
    $fld_date_col = "date_001";
}else{
    $fld_date_col = "date_created";
}
if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
    $search_query = $search_query . " AND vist.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
}else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
    $search_query = $search_query . " AND vist.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
}
if(!empty($fmi_imp_det)){
    $search_query = $search_query . " AND vist.var_25_005 = '".$fmi_imp_det."'";
}
if(!empty($fmi_multi_state)){
    $search_query = $search_query . " AND det.var_25_001 in (".$fmi_multi_state.")";
}

$qs_result = $SysReport->getImprisVistHistByState($search_query);

$rept_name = $rep_header_desc;
$rept_range = "ระหว่างวันที่ ".$_GET["datefrom"]." จนถึง ".$_GET["dateto"];
$rept_filename = $rept_name."_".$_GET["datefrom"]."_".$_GET["dateto"];
$rept_header = "|".$rept_name."<br>".$rept_range."|";
$rept_footer = $_SESSION["cookies_username"]." : ".$SysMainActivity->getClientIpAddress()."|หน้า {PAGENO} / {nb}|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding=0 cellspacing=0>
                    <thead>
                        <tr>
                            <td width="50%" rowspan="2" align="center" valign="top" style="<?php echo $stly_lt;?>"><b>รัฐ / สถานที่ควบคุมตัว</b></td>
                            <td width="10%" rowspan="2" align="center" valign="top" style="<?php echo $stly_lt;?>"><b>ครั้งที่</b></td>
                            <td width="10%" rowspan="2" align="center" valign="top" style="<?php echo $stly_lt;?>"><b>วันที่เข้าเยี่ยม</b></td>
                            <td width="20%" colspan="2" align="center" style="<?php echo $stly_lt;?>"><b>จำนวนผู้ตกทุกข์</b></td>
                            <td width="10%" rowspan="2" align="center" valign="top" style="<?php echo $stly_ltr;?>"><b>รวมทั้งหมด</b></td>
                        </tr>
                        <tr>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>ชาย</b></td>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>หญิง</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 0;
                            $total_imp = 0;
                            $grd_total_male = 0;
                            $grd_total_female = 0;
                            $grd_total = 0;
                            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                                $num = $num + 1;
                                $total_imp = $rows["tot_male"] + $rows["tot_female"];
                                $grd_total_male = $grd_total_male + $rows["tot_male"];
                                $grd_total_female = $grd_total_female + $rows["tot_female"];
                                $grd_total = $grd_total + $total_imp;
                                $state_code = $rows["state_code"];
                                $qs_result_detail = $SysReport->getImprisVistHist($search_query,$state_code);
                        ?>
                        <tr style="background-color:#A9A9A9;">
                            <td align="left" colspan="3" style="<?php echo $stly_lt;?>"><b><?php echo $num.". ".$rows["state_desc"];?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo number_format($rows["tot_male"]);?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo number_format($rows["tot_female"]);?></b></td>
                            <td align="right" style="<?php echo $stly_ltr;?>"><b><?php echo $total_imp;?></b></td>
                        </tr>
                            <?php
                                $s_num = 1;
                                $total_imp = 0;
                                $imp_det_desc = "";
                                $tr_rowspan = 1;
                                while($rows1 = $qs_result_detail->fetch(PDO::FETCH_ASSOC)){
                                    if($imp_det_desc == $rows1["imp_det_desc"]){
                                        $s_num = $s_num + 1;
                                        $num_vist = $num_vist + 1;
                                        $tr_style = "";
                                    }else{
                                        $s_num = $s_num--;
                                        $num_vist = 1;
                                        $tr_style = "";
                                    }

                                    $total_imp = $rows1["imp_tot_male"] + $rows1["imp_tot_female"];
                            ?>
                                <tr style="<?php echo $tr_style;?>">
                                    <td rowspan="<?php echo $tr_rowspan;?>" align="left" style="<?php echo $stly_lt;?>"><b><?php if($imp_det_desc != $rows1["imp_det_desc"]){echo "&nbsp;&nbsp;&nbsp;&nbsp;".$num.".".$s_num." ".$rows1["imp_det_desc"];}?></b></td>
                                    <td align="center" style="<?php echo $stly_lt;?>"><?php echo $num_vist;?></td>
                                    <td align="center" style="<?php echo $stly_lt;?>"><?php echo $SysConversion->convertDateFormat($rows1["imp_vist_date"],"d/m/Y");?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo number_format($rows1["imp_tot_male"]);?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo number_format($rows1["imp_tot_female"]);?></td>
                                    <td align="right" style="<?php echo $stly_ltr;?>"><?php echo $total_imp;?></td>
                                </tr>
                        <?php
                                $imp_det_desc = $rows1["imp_det_desc"];
                                } /* End while loop $qs_result_detail */
                            } /* End while loop $qs_result */
                        ?>
                        <tr style="background-color:#A9A9A9;">
                            <td align="center" colspan="3" style="<?php echo $stly_ltb;?>"><b>รวมทั้งหมด</b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $grd_total_male;?></b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $grd_total_female;?></b></td>
                            <td align="right" style="<?php echo $stly_lrtb;?>"><b><?php echo $grd_total;?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_hidden_values.php';?>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    $pdf = new mPDF('th', 'A4-L', '0', 'THSaraban','15','15','22','15');
    $pdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('../../folder_script/css_report_generator.css'); // external css
    $pdf->SetTitle($rept_name);
    $pdf->defaultheaderfontsize=10;
    $pdf->defaultheaderfontstyle='B';
    $pdf->SetHeader($rept_header);
    $pdf->defaultfooterfontstyle='I';
    $pdf->setFooter($rept_footer);
    $pdf->WriteHTML($stylesheet,1);
    $pdf->WriteHTML($html, 2);
    ob_end_clean();
    $pdf->Output($rept_filename.'.pdf', 'I');
}
?>
