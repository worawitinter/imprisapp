<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';

$SysReport = new SysReport();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
    ob_start();
}

if(isset($_GET["fromtrans"])){
    $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
}
$fmi_imp_date_from = "";
$fmi_imp_date_to = "";
$fmi_imp_laws = "";
$fmi_search_string = "";
$uniquenum_pri = "";
$tag_audit_yn = "n";
$fmi_date_type = "";
$fmi_imp_cate = "";
if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
if(!empty($_GET["imp_laws"])){$fmi_imp_laws = $_GET["imp_laws"];}
if(!empty($_GET["search_string"])){$fmi_search_string = $_GET["search_string"];}
if(!empty($_GET["uniquenum_pri"])){$uniquenum_pri = $_GET["uniquenum_pri"];}
if(isset($_GET["tag_audit_yn"])){$tag_audit_yn = $_GET["tag_audit_yn"];}
if(!empty($_GET["date_type"])){$fmi_date_type = $_GET["date_type"];}
if(!empty($_GET["imp_cate"])){$fmi_imp_cate = $_GET["imp_cate"];}

$search_query = "";
if($fmi_date_type == 'date_impris'){
    $fld_date_col = "date_002";
}else{
    $fld_date_col = "date_created";
}
if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
}else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
}
if(!empty($fmi_imp_laws)){
    $search_query = $search_query . " AND det.var_25_002 = '".$fmi_imp_laws."'";
}
if(!empty($uniquenum_pri)){
    $search_query = $search_query . " AND imp.uniquenum_pri = '".$uniquenum_pri."'";
}
if($tag_audit_yn == "y"){
    $search_query = $search_query . " AND imp.tag_deleted_yn = 'ed'";
}else{
    $search_query = $search_query . " AND imp.tag_deleted_yn = 'n'";
}

if(!empty($fmi_search_string)){
    $search_query = $search_query . " AND (lower(imp.desc_lang01) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang02) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.var_25_003) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($fmi_search_string))."%'";
    $search_query = $search_query . " OR lower(imp.notes_memo) like '%".trim(strtolower($fmi_search_string))."%')";
}

$todate = $SysConversion->convertDateFormat($today_date,"d");
$tomth = $SysConversion->convertThMonth($SysConversion->convertDateFormat($today_date,"m"));
$toyear = $SysConversion->convertThYear($SysConversion->convertDateFormat($today_date,"Y"));

$qs_result = $SysReport->getImprisWithinReg($search_query);

$rept_name = $rep_header_desc;
$rept_range = "ระหว่างวันที่ ".$_GET["datefrom"]." จนถึง ".$_GET["dateto"];
$rept_filename = $rept_name."_".$_GET["datefrom"]."_".$_GET["dateto"];
$rept_header = "|".$rept_name."<br>".$rept_range."|";
$rept_footer = $_SESSION["cookies_username"]."|".$SysMainActivity->getClientIpAddress()."|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding="0" cellspacing="0">
                <?php
                    $cnt = 0;
                    $whitespace = "&nbsp;";
                    while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                        $cnt = $cnt + 1;
                        $imp_name = $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];
                        $imp_age = $rows["imp_age"];
                        if($rows["usr_dept_desc"] <> ""){
                            $usr_dept_desc = $rows["usr_dept_desc"];
                        }else{
                            $usr_dept_desc = "สอท.ณ กรุงกัวลาลัมเปอร์";
                        }
                        if($rows["imp_cshbrw_date"] != "0000-00-00 00:00:00"){
                            $todate = $SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"d");
                            $tomth = $SysConversion->convertThMonth($SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"m"));
                            $def_tomth = $SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"m");
                            $toyear = $SysConversion->convertThYear($SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"Y"));
                            $def_toyear = $SysConversion->convertDateFormat($rows["imp_cshbrw_date"],"Y");
                        }else{
                            $todate = $SysConversion->convertDateFormat($today_date,"d");
                            $tomth = $SysConversion->convertThMonth($SysConversion->convertDateFormat($today_date,"m"));
                            $def_tomth = $SysConversion->convertDateFormat($today_date,"m");
                            $toyear = $SysConversion->convertThYear($SysConversion->convertDateFormat($today_date,"Y"));
                            $def_toyear = $SysConversion->convertDateFormat($today_date,"Y");
                        }
                        $date_payment = $def_toyear."-".$def_tomth."-".$todate;
                        $next_month_ts = strtotime($date_payment.' +1 month');
                        $next_month = date('d-m-Y', $next_month_ts);
                        $paymth = $SysConversion->convertThMonth($SysConversion->convertDateFormat($next_month,"m"));
                        $payyear = $SysConversion->convertThYear($SysConversion->convertDateFormat($next_month,"Y"));
                ?>
                <tr>
                    <td width="100%" colspan="2" align="center"><b><?php echo $rep_header_desc;?></b></td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="right" style="border: none; line-height: 2;text-align: justify;">ทำที่<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"60");?></span></td>
                </tr>
                <tr>
                    <td width="100%" colspan="2" align="right">วันที่<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5").$todate.$SysConversion->insertWhiteSpace($whitespace,"5");?></span>เดือน<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5").$tomth.$SysConversion->insertWhiteSpace($whitespace,"5");?></span>พ.ศ.<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5").$toyear.$SysConversion->insertWhiteSpace($whitespace,"5");?></span></td>
                </tr>
                <tr>
                    <td width="100%" align="left" style="border: none; line-height: 2;text-align: justify;">
                        <?php echo $SysConversion->insertWhiteSpace($whitespace,"10");?>ข้าพเจ้า<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"10").$imp_name.$SysConversion->insertWhiteSpace($whitespace,"10");?></span>หมายเลขประจำตัวประชาชน<span class="formtd"><?php if ($rows["imp_idcard"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"5").$rows["imp_idcard"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?></span>อายุ<span class="formtd"><?php if ($rows["imp_age"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"5").$rows["imp_age"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"10");}?></span>ปี<br>ตั้งบ้านเรือนอยู่เลขที่<span class="formtd"><?php if($rows["imp_addrr"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"5").$rows["imp_addrr"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?>
                        </span>ตำบล<span class="formtd"><?php if($rows["imp_subdistrict"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"0").$rows["imp_subdistrict"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"10");}?></span>อำเภอ<span class="formtd"><?php if($rows["imp_district"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"0").$rows["imp_district"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?>
                        </span>จังหวัด<span class="formtd"><?php if($rows["imp_province"] != ''){ echo $SysConversion->insertWhiteSpace($whitespace,"0").$rows["imp_province"].$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?></span><br>ขอทำสัญญารับสภาพหนี้ให้ไว้ต่อ<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"3").$usr_dept_desc.$SysConversion->insertWhiteSpace($whitespace,"3");?></span>โดย<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"3").$imp_name.$SysConversion->insertWhiteSpace($whitespace,"3");?></span>เป็นผู้แทน<br>ดังมีข้อความต่อไปนี้
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="left" style="border: none; line-height: 2;text-align: justify;text-indent: 50px;"><?php echo $SysConversion->insertWhiteSpace($whitespace,"10");?>ข้อ 1. ข้าพเจ้ายินยอมชดใช้เงินจำนวน<span class="formtd"><?php if($rows["imp_cshbrw_amt"] > 0){ echo $SysConversion->insertWhiteSpace($whitespace,"5").number_format($rows["imp_cshbrw_amt"],2).$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?></span>ริงกิตมาเลเซีย ที่ <?php echo $usr_dept_desc;?> ได้สำรองจ่ายเป็นค่าใช้จ่ายในการเดินทางกลับประเทศไทยของข้าพเจ้า รายละเอียดปรากฏตามสำเนาหนังสือรับรองการชดใช้เงินคืนที่แนบท้ายสัญญานี้ โดยขอผ่อนชำระให้แล้วเสร็จภายในกำหนด<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5");?>24<?php echo $SysConversion->insertWhiteSpace($whitespace,"5");?></span>เดือน
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="left" style="border: none; line-height: 2;text-align: justify;text-indent: 50px;"><?php echo $SysConversion->insertWhiteSpace($whitespace,"10");?>ข้อ 2. จำนวนเงินที่ข้าพเจ้าต้องรับผิดชอบ ตามข้อ 1. ข้าพเจ้าขอผ่อนชำระเป็นรายเดือนในอัตรา<br>เดือนละ<span class="formtd"><?php if($_SESSION["cookies_acpt_mth_amt"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"5").number_format($_SESSION["cookies_acpt_mth_amt"],2).$SysConversion->insertWhiteSpace($whitespace,"5");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"20");}?></span>บาท (<?php if($_SESSION["cookies_acpt_mth_amt"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$SysConversion->convertNumberToThaiBaht($_SESSION["cookies_acpt_mth_amt"],"THB").$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"80");}?></span>)<br>จนกว่าจะครบจำนวน และรับรองว่าจะไม่เรียกร้องเพื่อระงับหรืองดเว้นการชำระหนี้รายนี้เป็นอันขาด ทั้งนี้จะเริ่มผ่อนชำระตั้งแต่ เดือน<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5").$paymth.$SysConversion->insertWhiteSpace($whitespace,"5");?></span>พ.ศ.<span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"5").$payyear.$SysConversion->insertWhiteSpace($whitespace,"5");?></span>เป็นต้นไป
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="left" style="border: none; line-height: 2;text-align: justify;text-indent: 50px;"><?php echo $SysConversion->insertWhiteSpace($whitespace,"10");?>ข้อ 3. การชำระตามข้อ 2. ข้าพเจ้าจะนำมาชำระภายในวันที่ 3 ของทุกเดือน หากไม่สามารถนำมาชำระด้วยตนเองได้ ข้าพเจ้าจะส่งเงินมาชำระโดย<span class="formtd"><?php if($_SESSION["cookies_acpt_paymt_method"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$_SESSION["cookies_acpt_paymt_method"].$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"70");}?></span>และถ้าต้องเสียค่าธรรมเนียมในการส่งเงิน <br>ข้าพเจ้ายินยอมจะเป็นผู้ออกค่าใช้จ่ายเองทั้งสิ้น
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="left" style="border: none; line-height: 2;text-align: justify;text-indent: 50px;"><?php echo $SysConversion->insertWhiteSpace($whitespace,"10");?>สัญญานี้ทำขึ้น 2 ฉบับ ข้อความตรงกัน ข้าพเจ้าได้อ่านและรับฟังข้อความในสัญญานี้โดยตลอดแล้ว เห็นว่าเป็นการถูกต้องแล้ว จึงได้ลงลายมือชื่อให้ไว้ต่อหน้าพยานข้างท้ายนี้เป็นหลักฐาน
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="right" style="border: none;height:20;"></td>
                </tr>
                <tr>
                    <td width="100%" align="right" style="border: none;height:100;">
                        <span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"50");?></span>ผู้ให้สัญญา<br><br>
                        (<span class="formtd"><?php if($_SESSION["cookies_acpt_issue"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$_SESSION["cookies_acpt_issue"].$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"63");}?></span>)
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="right" style="border: none;height:100;">
                        <span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"50");?></span>ผู้รับสัญญา<br><br>
                        (<span class="formtd"><?php if($_SESSION["cookies_acpt_recv"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$_SESSION["cookies_acpt_recv"].$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"63");}?></span>)
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="right" style="border: none;height:100;">
                        <span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"50");?></span>พยาน<br><br>
                        (<span class="formtd"><?php if($_SESSION["cookies_acpt_witness1"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$_SESSION["cookies_acpt_witness1"].$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"63");}?></span>)
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="right" style="border: none;height:100;">
                        <table width="100%" cellpadding=0 cellspacing=0 autosize="1">
                            <tr>
                                <td width="50%" align="left" style="border: none;">
                                    ข้างซ้าย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้างขวา<br><br>
                                    &nbsp;&nbsp;&nbsp;ลายนิ้วหัวแม่มือของผู้ให้สัญญา&nbsp;&nbsp;&nbsp;
                                </td>
                                <td width="50%" align="right" style="border: none;">
                                    <span class="formtd"><?php echo $SysConversion->insertWhiteSpace($whitespace,"50");?></span>พยาน<br><br>
                                    (<span class="formtd"><?php if($_SESSION["cookies_acpt_witness2"] != ""){ echo $SysConversion->insertWhiteSpace($whitespace,"2").$_SESSION["cookies_acpt_witness2"].$SysConversion->insertWhiteSpace($whitespace,"2");}else{ echo $SysConversion->insertWhiteSpace($whitespace,"61");}?></span>)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
                    } /* End while loop $qs_result */
                ?>
                </table>
            </div>
        </div>
        <?php require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_hidden_values.php';?>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    $pdf = new mPDF('th', 'A4-P', '10', 'THSaraban','15','15','15','10');
    $pdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('../../folder_script/css_report_generator.css'); // external css
    $pdf->SetTitle($rept_name);
    $pdf->defaultfooterfontstyle='I';
    $pdf->WriteHTML($stylesheet,1);
    $pdf->WriteHTML($html, 2);
    $pdf->SetJS('window.print(); window.close();');
    ob_end_clean();
    $pdf->Output($rept_filename.'.pdf', 'I');
}
?>
