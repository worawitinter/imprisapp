<!-- rep_main.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
    </head>
    <body>
        <div class="card" style="width:<?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <div class="container-liquid" style="margin:0px; padding: 0px">
                <?php
                    require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
                ?>
                <div class="list-group">
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_rec_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายชื่อผู้ตกทุกข์ในมาเลเซีย</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_by_state_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานสถิติผู้ถูกจับกุมแยกตามรัฐ</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_by_reg_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานสถิติผู้ถูกจับกุมแยกตามข้อหาที่ถูกจับกุม</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_within_reg_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานประวัติผู้กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_protn_benft_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานสถิติการให้ความช่วยเหลือ คุ้มครองและดูแลผลประโยชน์คนไทยในต่างประเทศ</a>
                   <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_vist_hist_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานประวัติการเยี่ยมผู้ตกทุกข์</a>
                   <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_debt_contrct_status_rept&fromtarget=report&frommode=search" class="list-group-item"><i class="fa fa-angle-double-right" aria-hidden="true"></i> รายงานสถานะการให้ยืมเงินทดรองราชการ</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_accpt_debt_contrct_rept&fromtarget=report&frommode=search" class="list-group-item" style="display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> หนังสือสัญญารับสภาพหนี้</a>
                  <a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_debt_paymt_contrct_rept&fromtarget=report&frommode=search" class="list-group-item" style="display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> หนังสือสัญญารับรองการชดใช้เงินคืน</a>
                </div>
            </div>
        </div>
    </body>
</html>
