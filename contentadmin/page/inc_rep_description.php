<?php
$rep_header_desc = "";
if(isset($_GET["fromlink"])){
    if($_GET["fromlink"] == "fr_impris_rec_rept"){
        $rep_header_desc = "รายชื่อผู้ตกทุกข์ในมาเลเซีย";
    }else if($_GET["fromlink"] == "fr_impris_by_state_rept"){
        $rep_header_desc = "รายงานสถิติผู้ถูกจับกุมแยกตามรัฐ";
    }else if($_GET["fromlink"] == "fr_impris_by_reg_rept"){
        $rep_header_desc = "รายงานสถิติผู้ถูกจับกุมแยกตามข้อหาที่ถูกจับกุม";
    }else if($_GET["fromlink"] == "fr_impris_within_reg_rept"){
        $rep_header_desc = "รายงานประวัติผู้กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ";
    }else if($_GET["fromlink"] == "fr_accpt_debt_contrct_rept"){
        $rep_header_desc = "หนังสือสัญญารับสภาพหนี้";
    }else if($_GET["fromlink"] == "fr_debt_paymt_contrct_rept"){
        if(isset($_GET["papersize"]) && $_GET["papersize"] == "A5"){
            $rep_header_desc = "ใบเสร็จรับรองการชดใช้เงินคืน";
        }else{
            $rep_header_desc = "หนังสือสัญญารับรองการชดใช้เงินคืน";
        }
    }else if($_GET["fromlink"] == "fr_impris_protn_benft_rept"){
        $rep_header_desc = "รายงานสถิติการให้ความช่วยเหลือ คุ้มครองและดูแลผลประโยชน์คนไทยในต่างประเทศ";
    }else if($_GET["fromlink"] == "fr_impris_vist_hist_rept"){
        $rep_header_desc = "รายงานประวัติการเยี่ยมผู้ตกทุกข์";
    }else if($_GET["fromlink"] == "fr_debt_contrct_status_rept"){
        $rep_header_desc = "รายงานสถานะการให้ยืมเงินทดรองราชการ";
    }
}
?>
