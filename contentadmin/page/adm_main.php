<!-- adm_main.php -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>จัดการระบบ</title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 50px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
    </head>
    <body>
        <div class="card" style="width:<?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <h6>&nbsp;</h6>
            <div class="container-liquid" style="margin:0px; padding: 0px">
                <!--- START: user management setting section --->
                <div class="row">&nbsp;&nbsp;&nbsp;
                    <div class="chip">
                        <i class="fa fa-user-circle-o fa-2x" aria-hidden="true" style=" vertical-align: middle;"></i>
                        &nbsp;<b>การจัดการผู้ใช้ระบบ</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="one"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=sys_user&fromlink=usr&fromtarget=list&frommode=view"><i class="fa fa-user fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">ผู้ใช้งานระบบ</h5></a></div></span></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="two"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=sys_user_dept&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-users fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">สังกัด</h5></a></div></div>
                </div>
                <!--- END: user management setting section --->

                <div class="row"><div class="col-xs-12"><hr></div></div>

                <!--- START: Gencode setting section --->
                <div class="row">&nbsp;&nbsp;&nbsp;
                    <div class="chip">
                        <i class="fa fa-list-ul fa-2x" aria-hidden="true" style="vertical-align: middle;"></i>
                        &nbsp;<b>การจัดการข้อมูลระบบ</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="one"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=pris_cate&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-university fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">ประเภทสถานที่<br>ควบคุมตัว</h5></a></div></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="one"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=pris_det&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-map-marker fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">สถานที่ควบคุมตัว</h5></a></div></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="two"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=reg_cate&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-gavel fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">ประเภทข้อหา<br>ที่ถูกจับกุม</h5></a></div></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="two"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=reg_list&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-balance-scale fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">ข้อหาที่ถูกจับกุม</h5></a></div></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="three"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=imp_cate&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-tags fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">ประเภทงาน</h5></a></div></div>
                    <div class="col-xs-6 col-sm-2 text-center leftspan" id="three"><br><div class="badge-icon"><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=imp_stat&fromlink=adm_gencode&fromtarget=list&frommode=view"><i class="fa fa-vcard fa-3x" aria-hidden="true"></i><h5 class="badge-icon-text">สถานะผู้ตกทุกข์</h5></a></div></div>
                </div>
                <!--- END: Gencode setting section --->
            </div>
        </div>
    </body>
</html>
