<!--- inc_main_form_footer.php --->
<div class="modal-footer" id="frm_footer">
    <div id="div_lock_msg" class="alert alert-danger" style="display:none">
      <span id="lock_msg" />
    </div>
    <button type="button" id="frm_unlock" title="ปลดล็อครายการ" class="btn btn-warning" <?php if(($_GET["frommode"] == 'view' || $_GET["frommode"] == 'auditlog') && $_GET["fromtrans"] == 'impris_rep'){ echo "style='display:none';";}?> onclick="unlockRecord();"><i class="fa fa-unlock-alt"></i> ปลดล็อครายการ</button>
    <button type="button" id="frm_lock" title="ล็อครายการ" class="btn btn-warning" <?php if(($_GET["frommode"] == 'view' || $_GET["frommode"] == 'auditlog') && $_GET["fromtrans"] == 'impris_rep'){ echo "style='display:none';";}?> onclick="lockRecord()";><i class="fa fa-lock"></i> ล็อครายการ</button>
    <button type="button" id="frm_cancel" title="ยกเลิก" class="btn btn-danger" onclick="confirmCloseModal();" <?php if(($_GET["frommode"] == 'view' || $_GET["frommode"] == 'auditlog') && $_GET["fromtrans"] == 'impris_rep'){ echo "style='display:none';";}?>><i class="fa fa-times-circle"></i> ยกเลิก</button>
    <div class="btn-group dropup" id="frm_print"<?php if($_GET["fromtrans"] != 'impris' && $_GET["fromlink"] != 'imp'){ echo "style='display:none';";}?>>
      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-print"></i> พิมพ์
      </button>
      <div class="dropdown-menu" style="width:300px;background-color:rgba(0, 0, 0, 0.5)">
          <button class="dropdown-item btn-success" id="frm_print_impris" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ฟอร์มผู้ตกทุกข์</button>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item btn-success" id="frm_print_fr_impris_within_reg_rept" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ประวัติผู้กระทำความผิด/เข้าข่ายกระทำความผิดในต่างประเทศ</button>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item btn-success" id="frm_print_fr_accpt_debt_contrct_rept" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> หนังสือสัญญารับสภาพหนี้</button>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item btn-success" id="frm_print_fr_debt_paymt_contrct_rept" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> หนังสือสัญญารับรองการชดใช้เงินคืน</button>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item btn-success" id="frm_print_fr_debt_paymt_contrct_recpt" type="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ใบเสร็จรับรองการชดใช้เงินคืน</button>
      </div>
    </div>
    <button type="submit" id="frm_submit" title="บันทึก" class="btn btn-primary" <?php if(($_GET["frommode"] == 'view' || $_GET["frommode"] == 'auditlog') && $_GET["fromtrans"] == 'impris_rep'){ echo "style='display:none';";}?>><i class="fa fa-floppy-o"></i> บันทึก</button>
    <button type="submit" id="rep_print" title="พิมพ์รายงาน" class="btn btn-primary" <?php if($_GET["fromtrans"] != 'impris_rep'){ echo "style='display:none';";}?>><i class="fa fa-print"></i> พิมพ์รายงาน</button>
</div>
<input type="hidden" name="fmi_frommode" id="fmi_frommode" value="">
<input type="hidden" name="fmi_uniquenum_pri" id="fmi_uniquenum_pri" value="">
<input type="hidden" name="fmi_fromtrans" id="fmi_fromtrans" value="<?php echo $_GET["fromtrans"]; ?>">
<input type="hidden" name="fmi_fromlink" id="fmi_fromlink" value="<?php echo $_GET["fromlink"]; ?>">
<input type="hidden" name="fmi_userid" id="fmi_userid" value="<?php echo $_SESSION["cookies_username"]; ?>">
<input type="hidden" name="fmi_date_created" id="fmi_date_created" value="">
<input type="hidden" name="chk_lock_record" id="chk_lock_record" value="n">
<input type="hidden" name="chk_audit_yn" id="chk_audit_yn" value="n">
<input type="hidden" name="fmi_tablename" id="fmi_tablename" value="<?php echo $tablename; ?>">
<input type="hidden" name="fmi_tablename_data" id="fmi_tablename_data" value="<?php echo $tablename_data; ?>">
<input type="hidden" name="fmi_user_dept_uniq" id="fmi_user_dept_uniq" value="<?php echo $_SESSION["cookies_sys_user_dept_uniq"]; ?>">
<input type="hidden" name="fmi_user_dept_desc" id="fmi_user_dept_desc" value="<?php echo $_SESSION["cookies_sys_user_dept_desc"]; ?>">
<span id="ajax_auto_reload" />

<script type="text/javascript">
    $(document).on("click", ".row_upd", function () {
        var frommode = $(this).data('frommode');
        $('#div_lock_msg').hide();
        $('#chk_lock_record').val('n');
        if(frommode == 'auditlog'){
            $("#chk_audit_yn").val('y');
            $("#frm_unlock").hide();
            $("#frm_lock").hide();
            $("#frm_cancel").hide();
            $("#frm_submit").hide();
            $("#div_collapseLog").hide();
            $("#div_menu-toggle").hide();
        }else{
            $("#chk_audit_yn").val('n');
            $("#frm_unlock").hide();
            $("#frm_lock").show();
            $("#frm_cancel").show();
            $("#frm_submit").show();
            $("#div_collapseLog").show();
            $("#div_menu-toggle").show();
        }
    });

    function autoChkLockRecord(){
        var interval = null;
        var uniquenum_pri = $('#fmi_uniquenum_pri').val();
        var fromtrans = $('#fmi_fromtrans').val();
        var userid = $('#fmi_userid').val();
        var frommode = $('#fmi_frommode').val();
        if(uniquenum_pri != "" && frommode != "auditlog"){
            IntChkLock = setInterval(function(){
               $('#ajax_auto_reload').load("../control/inc_ajax_auto_reload.php?tag_usage=chk_lockrecord&uniquenum_pri=" + uniquenum_pri + "&fromtrans=" + fromtrans + "&fld_name=" + userid);
           }, 5000) /* time in milliseconds (ie 5 seconds)*/
        }
    }

    function chkLockRecord(){
        $('#frm_submit').prop('disabled', true);
        $('#frm_cancel').prop('disabled', true);
        var uniquenum_pri = $('#fmi_uniquenum_pri').val();
        var fromtrans = $('#fmi_fromtrans').val();
        var userid = $('#fmi_userid').val();
        var frommode = $('#fmi_frommode').val();
        if(uniquenum_pri != "" && frommode != "auditlog"){
            $.ajax({
                url: '../control/inc_ajax_text.php',
                data: "tag_usage=chk_lockrecord&uniquenum_pri=" + uniquenum_pri + "&fromtrans=" + fromtrans,
                dataType: 'text',
                success: function(data)
                {
                    if(data){
                        var rs = data.split("@@@");
                        var chk_userid = rs[0];
                        var date_lastupdate = rs[1];
                        if(userid.trim() != chk_userid.trim()){
                            var msg = "รายการนี้ถูกล็อคโดย " + chk_userid + " เมื่อวันที่/เวลา " + date_lastupdate + " โปรดดำเนินการปลดล็อค";
                            $('#div_lock_msg').show();
                            $('#lock_msg').html(msg);
                            $('#frm_unlock').show();
                            $('#frm_lock').hide();
                            $('#frm_submit').prop('disabled', true);
                            $('#frm_cancel').prop('disabled', true);
                            $('#chk_lock_record').val('y');
                        }else{
                            lockRecord();
                            $('#frm_unlock').hide();
                            $('#frm_lock').show();
                            $('#frm_submit').attr('disabled',false);
                            $('#frm_cancel').attr('disabled',false);
                            $('#chk_lock_record').val('n');
                        }
                    }else{
                        lockRecord();
                        $('#frm_unlock').hide();
                        $('#frm_lock').show();
                        $('#frm_submit').attr('disabled',false);
                        $('#frm_cancel').attr('disabled',false);
                        $('#chk_lock_record').val('n');
                    }
                }
            });
        }
    }

    function unlockRecord(){
        $.confirm({
            title: '',
            content: 'ทำการปลดล็อครายการนี้?',
            icon: 'fa fa-unlock-alt',
            theme: 'modern',
            type: 'orange',
            buttons: {
                cancel: {
                    text: 'ยกเลิก',
                },
                confirm: {
                    text: 'ยืนยัน',
                    btnClass: 'btn-orange',
                    action: function(){
                        $.LoadingOverlay("show");
                        var uniquenum_pri = $('#fmi_uniquenum_pri').val();
                        var fromtrans = $('#fmi_fromtrans').val();
                        $.ajax({
                            url: '../control/inc_ajax_text.php',
                            data: "tag_usage=unlockrecord&uniquenum_pri=" + uniquenum_pri + "&fromtrans=" + fromtrans,
                            dataType: 'text',
                            success: function(data)
                            {
                                if(data){
                                    toggleAlertBox('','ปลดล็อครายการสำเร็จ!','fa fa-unlock-alt','green');
                                    $('#div_lock_msg').hide();
                                    $('#frm_unlock').hide();
                                    $('#frm_lock').show();
                                    $('#frm_submit').attr('disabled',false);
                                    $('#frm_cancel').attr('disabled',false);
                                    $('#chk_lock_record').val('n');
                                }else{
                                    toggleAlertBox('','ขออภัย! ระบบไม่สามารถทำการปลดล็อครายการนี้ โปรดลองอีกครั้ง','fa fa-warning','red');
                                    $('#div_lock_msg').show();
                                    $('#frm_unlock').hide();
                                    $('#frm_lock').show();
                                    $('#frm_submit').prop('disabled', true);
                                    $('#frm_cancel').prop('disabled', true);
                                }
                                $.LoadingOverlay("hide");
                            }
                        });
                    }
                }
            }
        });
    }

    function lockRecord(){
        $.confirm({
            title: '',
            content: 'ทำการล็อครายการนี้?',
            icon: 'fa fa-lock',
            theme: 'modern',
            type: 'orange',
            buttons: {
                cancel: {
                    text: 'ยกเลิก',
                },
                confirm: {
                    text: 'ยืนยัน',
                    btnClass: 'btn-warning',
                    action: function(){
                        $.LoadingOverlay("show");
                        var uniquenum_pri = $('#fmi_uniquenum_pri').val();
                        var fromtrans = $('#fmi_fromtrans').val();
                        var fromlink = $('#fmi_fromlink').val();
                        var userid = $('#fmi_userid').val();
                        $.ajax({
                            url: '../control/inc_ajax_text.php',
                            data: "tag_usage=lockrecord&uniquenum_pri=" + uniquenum_pri + "&fromtrans=" + fromtrans + "&fld_name=" + userid + "&fromlink=" + fromlink,
                            dataType: 'text',
                            success: function(data)
                            {
                                if(data){
                                    toggleAlertBox('','ล็อครายการสำเร็จ!','fa fa-lock','green');
                                    $('#div_lock_msg').hide();
                                    $('#frm_unlock').show();
                                    $('#frm_lock').hide();
                                }else{
                                    toggleAlertBox('','ขออภัย! ระบบไม่สามารถทำการล็อครายการนี้ โปรดลองอีกครั้ง','fa fa-warning','red');
                                    $('#div_lock_msg').show();
                                    $('#frm_unlock').hide();
                                    $('#frm_lock').show();
                                }
                                $.LoadingOverlay("hide");
                            }
                        });
                    }
                }
            }
        });
    }
</script>
