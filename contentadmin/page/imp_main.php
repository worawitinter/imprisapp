<!-- imp_main.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
        <script type="text/javascript">
            $(document).on("click", ".parse-frommode", function () {
                var frommode = $(this).data('frommode');
                var fromtrans = $("#fmi_fromtrans").val();
                var js_function = "alert(this)";
                $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                $('.modal-body').find('button').removeClass( "btn dropdown-toggle disabled btn-default" ).addClass( "btn dropdown-toggle btn-default" );
                $(".modal-content #fmi_frommode").val( frommode );
                $('.modal-body input').val('');
                $('.modal-body textarea').val('');
                $("#imp_refdoc").html('');
                $('#fmi_ori_'+ fromtrans +'_autonum').hide();
                $('#fmi_'+ fromtrans +'_autonum_list').show();
                $("#frm_print").hide();
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_lock").hide();
                $("#frm_unlock").hide();
                $("#frm_cancel").show();
                $("#frm_submit").show();
                $('#frm_submit').attr('disabled',false);
                $('#frm_cancel').attr('disabled',false);
                parseModalHeaderDesc();
                getNewUniqueID();
                getAutoSysNum(fromtrans,js_function);
                showRemark(frommode);
                chkImprisonCategory();
                document.getElementById('frm_footer').style.display = '';
                toggleSboxList("fmi_imp_det","var_25_003","pris_det","fmi_pris_cate");
                toggleSboxList("fmi_imp_stat","var_25_004","imp_stat","fmi_imp_cate");
                toggleSboxList("fmi_imp_laws","var_25_002","reg_list","fmi_reg_cate");
                if($("#fmi_user_dept_uniq").val() != ''){
                    $('select[name=fmi_imp_dept]').val($("#fmi_user_dept_uniq").val());
                    $('.selectpicker').selectpicker('refresh')
                    $("#fmi_imp_dept option[value='"+ $("#fmi_user_dept_uniq").val() +"']").text($("#fmi_user_dept_desc").val());
                    $("select[name=fmi_imp_dept]").selectpicker("refresh");
                }
            });

            $(document).on("click", ".row_upd", function () {
                var frommode = $(this).data('frommode');
                var upd_id = $(this).data('id');
                propmtFrmUpdate(upd_id,frommode);
                //toggleSboxList("fmi_imp_det","var_25_003","pris_det","fmi_pris_cate");
                //toggleSboxList("fmi_imp_stat","var_25_004","imp_stat","fmi_imp_cate");
                //toggleSboxList("fmi_imp_laws","var_25_002","reg_list","fmi_reg_cate");
            });

            $(document).ready(function() {
                var form_search = document.getElementById("form_search");
                form_search.onkeyup = function(e){
                    if(e.keyCode == 13){
                       formSearch();
                    }
                }
            });

            function propmtFrmUpdate(upd_id,frommode){
                var fromtrans = $("#fmi_fromtrans").val();
                $(".modal-content #fmi_frommode").val( frommode );
                $(".modal-content #fmi_uniquenum_pri").val( upd_id );
                if(frommode == 'view'){
                    var tag_audit_yn = "y";
                }else{
                    var tag_audit_yn = "n";
                }
                getRowUpdate(tag_audit_yn);
                parseModalHeaderDesc();
                showRemark(frommode);
                chkImprisonCategory();
                if(frommode != 'upload' && frommode != 'view'  && frommode != 'new' && frommode != 'auditlog'){
                    chkLockRecord();
                    autoChkLockRecord();
                }
                $('#fmi_ori_'+ fromtrans +'_autonum').show();
                $('#fmi_'+ fromtrans +'_autonum_list').hide();
                $("#frm_print").show();
                if(frommode == 'view'){
                    $("#frm_submit").hide();
                    $("#frm_cancel").hide();
                    $("#menu-toggle").hide();
                    $("#sidebar-wrapper").hide();
                    $('.modal-body').find('input, textarea, button, select').attr('disabled','disabled');
                }else{
                    $("#frm_submit").show();
                    $("#frm_cancel").show();
                    $("#sidebar-wrapper").show();
                    $("#menu-toggle").show();
                    $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                }
            }

            function showRemark(frommode){
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();

                if(frommode == 'new'){
                    $('#imp_remark1').hide();
                    $('#imp_remark2').hide();
                }else{
                    $('#imp_remark1').show();
                    $('#imp_remark2').show();

                    jQuery("#remark_content").load("inc_imp_remark_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&uniquenum_pri=" + uniquenum_pri);
                }
            }

            function confirmDelete(row,id){
                var sys_autonum = $("#r_sys_autonum" + row).val();
                $.confirm({
                    title: '',
                    content: 'ต้องการลบหมายเลขรายการ ' + sys_autonum + '?',
                    icon: 'fa fa-trash',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก',
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-danger',
                            action: function(){
                                $(".modal-content #fmi_frommode").val('delete');
                                $(".modal-content #fmi_uniquenum_pri").val(id);
                                $.LoadingOverlay("show");
                                $.ajax({
                                    type: 'post',
                                    url: 'imp_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        toggleAlertBox('','ลบหมายเลขรายการ ' + sys_autonum + ' สำเร็จ!','fa fa-trash','green');
                                        jQuery("#target-content").load("inc_imp_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function getNewUniqueID(){
                var tag_usage = 'autogen_upri';
                $.ajax({
                    url: '../control/inc_ajax_text.php',
                    data: "tag_usage=" + tag_usage,
                    dataType: 'text',
                    success: function(data)
                    {
                        document.getElementById('fmi_uniquenum_pri').value = data;
                    }
                });
            }

            function getRowUpdate(tag_audit_yn){
                var fromlink = $("#fmi_fromlink").val();
                var fromtrans = $("#fmi_fromtrans").val();
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                $.ajax({
                    url: '../control/inc_ajax_fetch_row_update.php',
                    data: "fromlink=" + fromlink + "&tag_usage=" + fromtrans + "&uniquenum_pri=" + uniquenum_pri + "&tag_audit_yn=" + tag_audit_yn,
                    dataType: 'json',
                    success: function(data)
                    {
                        document.getElementById('fmi_ori_'+ fromtrans +'_autonum').value = data[0].sys_autonum;
                        document.getElementById('fmi_imp_refdoc').value = data[0].imp_refdoc;
                        document.getElementById('fmi_imp_remark').value = data[0].imp_remark;
                        document.getElementById('fmi_imp_firstname').value = data[0].imp_firstname;
                        document.getElementById('fmi_imp_lastname').value = data[0].imp_lastname;
                        document.getElementById('fmi_imp_firstname_2').value = data[0].imp_firstname_2;
                        document.getElementById('fmi_imp_lastname_2').value = data[0].imp_lastname_2;
                        document.getElementById('fmi_imp_birthdate').value = moment(data[0].imp_birthdate).format('DD/MM/YYYY');
                        document.getElementById('fmi_imp_prisdate').value = moment(data[0].imp_prisdate).format('DD/MM/YYYY');
                        document.getElementById('fmi_imp_age').value = data[0].imp_age;
                        document.getElementById('fmi_imp_idcard').value = data[0].imp_idcard;
                        document.getElementById('fmi_imp_addrr').value = data[0].imp_addrr;
                        document.getElementById('fmi_imp_subdistrict').value = data[0].imp_subdistrict;
                        document.getElementById('fmi_imp_district').value = data[0].imp_district;
                        document.getElementById('fmi_imp_province').value = data[0].imp_province;
                        document.getElementById('fmi_imp_zipcode').value = data[0].imp_zipcode;
                        document.getElementById('fmi_imp_telephone').value = data[0].imp_telephone;
                        document.getElementById('fmi_imp_passport').value = data[0].imp_passport;
                        document.getElementById('fmi_date_created').value = data[0].date_created;
                        document.getElementById('fmi_imp_temp_passport').value = data[0].imp_temp_passport;
                        document.getElementById('fmi_imp_ci').value = data[0].imp_ci;
                        document.getElementById('fmi_imp_cate').value = data[0].imp_cate_uniq;
                        document.getElementById('fmi_imp_bdr_pass').value = data[0].imp_bdr_pass;
                        document.getElementById('fmi_imp_cshbrw_date').value = moment(data[0].imp_cshbrw_date).format('DD/MM/YYYY');
                        document.getElementById('fmi_imp_cshbrw_amt').value = data[0].imp_cshbrw_amt;
                        document.getElementById('fmi_imp_cshbrw_ref').value = data[0].imp_cshbrw_ref;

                        if(data[0].chk_drf == 'y'){
                            document.getElementById('chk_drf').checked = true;
                        }else{
                            document.getElementById('chk_drf').checked = false;
                        }

                        var title_desc = '';
                        var title_code = '';
                        if(data[0].imp_title == 'นาย'){
                            title_code = 'mr';
                            title_desc = 'นาย';
                        }else if(data[0].imp_title == 'นางสาว'){
                            title_code = 'ms';
                            title_desc = 'นางสาว';
                        }else if(data[0].imp_title == 'นาง'){
                            title_code = 'mrs';
                            title_desc = 'นาง';
                        }else if(data[0].imp_title == 'เด็กชาย'){
                            title_code = 'master';
                            title_desc = 'เด็กชาย';
                        }else if(data[0].imp_title == 'เด็กหญิง'){
                            title_code = 'miss';
                            title_desc = 'เด็กหญิง';
                        }
                        $('select[name=fmi_imp_title]').val(title_code);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_title option[value='"+ title_code +"']").text(title_desc);
                        $("select[name=fmi_imp_title]").selectpicker("refresh");

                        var gender_desc = '';
                        if(data[0].imp_gender == 'm'){
                            gender_desc = 'ชาย';
                        }else{
                            gender_desc = 'หญิง';
                        }
                        $('select[name=fmi_imp_gender]').val(data[0].imp_gender);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_gender option[value='"+ data[0].imp_gender +"']").text(gender_desc);
                        $("select[name=fmi_imp_gender]").selectpicker("refresh");

                        $('select[name=fmi_imp_det]').val(data[0].imp_det_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_det option[value='"+ data[0].imp_det_uniq +"']").text(data[0].imp_det_desc);
                        $("select[name=fmi_imp_det]").selectpicker("refresh");

                        $('select[name=fmi_imp_laws]').val(data[0].imp_reg_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_laws option[value='"+ data[0].imp_reg_uniq +"']").text(data[0].imp_reg_desc);
                        $("select[name=fmi_imp_laws]").selectpicker("refresh");

                        $('select[name=fmi_imp_stat]').val(data[0].imp_stat_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_stat option[value='"+ data[0].imp_stat_uniq +"']").text(data[0].imp_stat_desc);
                        $("select[name=fmi_imp_stat]").selectpicker("refresh");

                        $('select[name=fmi_imp_cate]').val(data[0].imp_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_cate option[value='"+ data[0].imp_cate_uniq +"']");
                        $("select[name=fmi_imp_cate]").selectpicker("refresh");

                        $('select[name=fmi_pris_cate]').val(data[0].pris_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_pris_cate option[value='"+ data[0].pris_cate_uniq +"']").text(data[0].pris_cate_desc);
                        $("select[name=fmi_pris_cate]").selectpicker("refresh");

                        $('select[name=fmi_reg_cate]').val(data[0].laws_cate_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_reg_cate option[value='"+ data[0].laws_cate_uniq +"']").text(data[0].laws_cate_desc);
                        $("select[name=fmi_reg_cate]").selectpicker("refresh");

                        $('select[name=fmi_imp_refdoc]').val(data[0].imp_refdoc_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_refdoc option[value='"+ data[0].imp_refdoc_uniq +"']").text(data[0].imp_refdoc_desc);
                        $("select[name=fmi_imp_refdoc]").selectpicker("refresh");

                        $('select[name=fmi_imp_dept]').val(data[0].imp_dept_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_dept option[value='"+ data[0].imp_dept_uniq +"']").text(data[0].imp_dept_desc);
                        $("select[name=fmi_imp_dept]").selectpicker("refresh");

                        //document.getElementById('fmi_imp_gender').value = data[0].imp_gender;
                        //$('#fmi_imp_gender').selectpicker('refresh');
                        //document.getElementById('fmi_imp_det').value = data[0].imp_det_uniq;
                        //$('#fmi_imp_det').selectpicker('refresh');
                        //document.getElementById('fmi_imp_laws').value = data[0].imp_reg_uniq;
                        //$('#fmi_imp_laws').selectpicker('refresh');
                        //document.getElementById('fmi_imp_stat').value = data[0].imp_stat_uniq;
                        //$('#fmi_imp_stat').selectpicker('refresh');
                        toggleRefDoc(data[0].imp_refdoc_uniq);

                        var cshbrw_stat_desc = 'n';
                        if(data[0].imp_cshbrw_stat == 'y'){
                            cshbrw_stat_desc = 'ได้รับคืนจากกระทรวงฯ แล้ว';
                        }else if(data[0].imp_cshbrw_stat == 's'){
                                cshbrw_stat_desc = 'ยังไม่ได้รับคืนจากกระทรวงฯ';
                        }else{
                            cshbrw_stat_desc = 'กรุณาเลือก';
                        }
                        $('select[name=fmi_imp_cshbrw_stat]').val(data[0].imp_cshbrw_stat);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_imp_cshbrw_stat option[value='"+ data[0].imp_cshbrw_stat +"']").text(cshbrw_stat_desc);
                        $("select[name=fmi_imp_cshbrw_stat]").selectpicker("refresh");
                    }
                });
            }

            function formSearch(){
                $.LoadingOverlay("show");
				var fmi_form_search = document.getElementById('form_search').value;
				jQuery("#target-content").load("inc_imp_main_list<?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1&search_query="+fmi_form_search+"&tag_audit_yn=n");
                jQuery("#target-pagination").load("inc_pagination_main_list<?php echo $url_file_ext; ?>?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&search_query="+fmi_form_search);
                $.LoadingOverlay("hide");
            }

            function chkExistImpris(fld_obj){
                var search_query = fld_obj.value;
                if(search_query.trim() != ""){
                    var tag_usage = 'imp_exist';
                    $.ajax({
                        url: '../control/inc_ajax_text.php',
                        data: "tag_usage=" + tag_usage + "&search_query=" + search_query.trim(),
                        dataType: 'text',
                        success: function(data)
                        {
                            if(data != ""){
                                $.confirm({
                                    title: '',
                                    content: "บุคคลดังกล่าวมีอยู่ในฐานข้อมูลแล้ว (หมายเลขอ้างอิง: "+ search_query.trim() +") โปรดดำเนินการแก้ไขข้อมูล",
                                    icon: 'fa fa-id-card-o',
                                    theme: 'modern',
                                    type: 'green',
                                    buttons: {
                                        cancel: {
                                            text: 'ยกเลิก',
                                        },
                                        confirm: {
                                            text: 'ยืนยัน',
                                            btnClass: 'btn-success',
                                            action: function(){
                                                $.LoadingOverlay("show");
                                                if($("#fmi_frommode").val() == "new"){
                                                    var upd_id = data;
                                                    var frommode = "edit";
                                                    propmtFrmUpdate(upd_id,frommode);
                                                }
                                                $.LoadingOverlay("hide");
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }

            $(document).ready(function() {
                $('#frm_print_impris').on('click', function() {
                    var fromlink = $("#fmi_fromlink").val();
                    var fromtrans = $("#fmi_fromtrans").val();
                    var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                    var tag_audit_yn = $("#chk_audit_yn").val();
                    var target_url = '<?php echo $_GET["fromlink"];?>_view001<?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=print&frommode=<?php echo $_GET["frommode"];?>&uniquenum_pri=' + uniquenum_pri + '&tag_audit_yn=' + tag_audit_yn + '&papersize=A4';
                    window.open(target_url);
                });

                $('#frm_print_fr_impris_within_reg_rept').on('click', function() {
                    var fromlink = $("#fmi_fromlink").val();
                    var fromtrans = $("#fmi_fromtrans").val();
                    var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                    var tag_audit_yn = $("#chk_audit_yn").val();
                    var target_url = 'inc_fr_impris_within_reg_rept<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_impris_within_reg_rept&fromtarget=report&frommode=search&datefrom=&dateto=&state_code=&imp_laws=&imp_gender=&imp_det=&imp_stat=&search_string=&provn_class=&rept_output=pdf&uniquenum_pri=' + uniquenum_pri + '&tag_audit_yn=' + tag_audit_yn + '&papersize=A4';
                    window.open(target_url);
                });

                $('#frm_print_fr_accpt_debt_contrct_rept').on('click', function() {
                    var fromlink = $("#fmi_fromlink").val();
                    var fromtrans = $("#fmi_fromtrans").val();
                    var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                    var tag_audit_yn = $("#chk_audit_yn").val();
                    var target_url = 'inc_fr_accpt_debt_contrct_rept<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_accpt_debt_contrct_rept&fromtarget=report&frommode=search&datefrom=&dateto=&state_code=&imp_laws=&imp_gender=&imp_det=&imp_stat=&search_string=&provn_class=&rept_output=pdf&uniquenum_pri=' + uniquenum_pri + '&tag_audit_yn=' + tag_audit_yn + '&papersize=A4';
                    write_sys_session();
                    window.open(target_url);
                });

                $('#frm_print_fr_debt_paymt_contrct_rept').on('click', function() {
                    var fromlink = $("#fmi_fromlink").val();
                    var fromtrans = $("#fmi_fromtrans").val();
                    var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                    var tag_audit_yn = $("#chk_audit_yn").val();
                    var target_url = 'inc_fr_debt_paymt_contrct_rept<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_debt_paymt_contrct_rept&fromtarget=report&frommode=search&datefrom=&dateto=&state_code=&imp_laws=&imp_gender=&imp_det=&imp_stat=&search_string=&provn_class=&rept_output=pdf&uniquenum_pri=' + uniquenum_pri + '&tag_audit_yn=' + tag_audit_yn + '&papersize=A4';
                    write_sys_session();
                    window.open(target_url);
                });

                $('#frm_print_fr_debt_paymt_contrct_recpt').on('click', function() {
                    var fromlink = $("#fmi_fromlink").val();
                    var fromtrans = $("#fmi_fromtrans").val();
                    var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                    var tag_audit_yn = $("#chk_audit_yn").val();
                    var target_url = 'inc_fr_debt_paymt_contrct_rept<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=fr_debt_paymt_contrct_rept&fromtarget=report&frommode=search&datefrom=&dateto=&state_code=&imp_laws=&imp_gender=&imp_det=&imp_stat=&search_string=&provn_class=&rept_output=pdf&uniquenum_pri=' + uniquenum_pri + '&tag_audit_yn=' + tag_audit_yn + '&papersize=A5';
                    write_sys_session();
                    window.open(target_url);
                });
            });

            $(document).ready(function() {
                $('#frm_imp').on('hidden.bs.modal', function () {
                    if(IntChkLock != null){
                        clearInterval(IntChkLock);
                    }
                });
            });

            function write_sys_session(){
                $.ajax({
                    type: 'post',
                    url: '../control/inc_sys_session_write.php',
                    data: $('form').serialize(),
                    success: function () {
                    }
                });
            }
        </script>
    </head>
    <body>
        <?php
            //require_once DOCUMENT_ROOT . '/contentadmin/page/inc_form_search_panel.php';
        ?>
        <div id="pnl_list" class="card" style="width: <?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <?php require_once 'inc_imp_main_form.php';
                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_imp_attchmt_main_form.php';
            ?>
            <div class="container" style="width:auto">
                <?php
                    require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6"></div>
                                    <div class="col col-xs-6 text-right">
                                        <a <?php if($_SESSION["cookies_sys_create_yn"] != 'y'){ ?>style="display:none"<?php }?>href="main_screen<?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"]."_sys_autonum";?>&fromlink=sys_autonum&fromtarget=list&frommode=view" title="ตั้งค่าหมายเลขอัตโนมัติ" class="parse-frommode btn btn-primary" style="color:#ffffff"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                                        <a <?php if($_SESSION["cookies_sys_create_yn"] == 'y'){ ?>data-toggle="modal" data-frommode="new" data-target="#frm_imp" title="เพิ่ม"<?php }else{ echo "disabled='disabled'"; } ?> class="parse-frommode btn btn-primary" href="#frm_imp" style="color:#ffffff"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" id="target-content"></div>
                            <?php
                                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_pagination_main.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
