<!DOCTYPE html>
<!-- inc_pagination_main_list.php -->
<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20190302             wrwt                  fixed maxVisible to 20
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';

$DatabaseOperation = new DatabaseOperation();
$SysListMain = new SysListMain();
$uniquenum_pri = "";
if(isset($_GET["page"])){
    $page = $_GET["page"];
}else{
    $page = 1;
}
if(isset($_GET["search_query"])){
    $search_query = $_GET["search_query"];
}else{
    $search_query = "";
}
$limit = PAGE_LIMIT;
$page_start = 1;
if($search_query != ""){
    if($_GET["fromlink"] == "imp"){
        $qs_result = $SysListMain->getRowImprisResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
    }elseif($_GET["fromlink"] == "usr"){
        $qs_result = $SysListMain->getRowUserResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
    }elseif($_GET["fromlink"] == "adm_gencode"){
        $qs_result = $SysListMain->getRowGencodeResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
    }elseif($_GET["fromlink"] == "sys_autonum"){
        $qs_result = $SysListMain->getRowSysAutoNumResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
    }
    $total_records = $qs_result->rowCount();
}else{
    $total_records = $DatabaseOperation->getTotalRecords($_GET["tablename"],$_GET['fromtrans']);
}
$total_pages = ceil($total_records / $limit);

if($total_pages < 1){
    $total_pages = 1;
}
?>
<script type="text/javascript">
    function pageClick(pageNumber){
        var page_id = document.getElementById(pageNumber);
        $.LoadingOverlay("show");
        jQuery("#target-content").load("inc_<?php echo $_GET["fromlink"];?>_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=" + pageNumber);
        $("li.active").removeClass("active");
        $(page_id).addClass("active");
        $.LoadingOverlay("hide");
    }

    $(document).ready(function() {
        var total_records = "<?php echo $total_records; ?>";
        $("#total_records").html(total_records);
        $('.pagination').bootpag({
            total: <?php echo $total_pages;?>,
            page: 1,
            maxVisible: 20,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, num){
            pageClick(num);
            $(this).bootpag({total: <?php echo $total_pages;?>, maxVisible: 20});

        });
    });
</script>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="col col-xs-10">
            <div class="pagination"/>
        </div>
        <div class="col col-xs-2">
            <span class="pull-right">
                <h4><span><?php if($search_query <> ""){ echo "พบทั้งหมด";}else{ echo "ทั้งหมด";}?> <span id="total_records"/> รายการ</span></h4>
            </span>
        </div>
    </body>
</html>
