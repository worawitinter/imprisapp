<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20171201             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysMainActivity = new SysMainActivity();

$tablename = "tran_record_data";
$imp_remark = $_POST["fmi_imp_remark"];
$date_created = $TransactionGenUnique->generate_trans_datetime();
$new_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
$tag_type = "remark_list";
$data = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_sec"=>$uniquenum_pri,
                    "uniquenum_pri"=>$new_uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "var_25_001"=>$tag_type,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "notes_memo"=>$imp_remark
                );
$result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
?>
