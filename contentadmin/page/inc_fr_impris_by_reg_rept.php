<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_description.php';

$SysReport = new SysReport();
$SysConversion = new SysConversion();
$SysMainActivity = new SysMainActivity();

$stly_lt = "border-left: 1px solid #000000;border-top: 1px solid #000000;";
$stly_lb = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lbr = "border-left: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltr = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;";
$stly_ltb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-bottom: 1px solid #000000;";
$stly_lrtb = "border-left: 1px solid #000000;border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000;";

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mPDF-v6.1.0/mpdf.php';
    ob_start();
}

if(isset($_GET["fromtrans"])){
    $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
}

$fmi_imp_date_from = "";
$fmi_imp_date_to = "";
$fmi_imp_laws = "";
$fmi_provn_class = "'ปัตตานี','ยะลา','นราธิวาส','สตูล','สงขลา'";
$fmi_date_type = "";
$fmi_imp_cate = "";
if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
if(!empty($_GET["imp_laws"])){$fmi_imp_laws = $_GET["imp_laws"];}
if(!empty($_GET["provn_class"])){$fmi_provn_class = $_GET["provn_class"];}
if(!empty($_GET["date_type"])){$fmi_date_type = $_GET["date_type"];}
if(!empty($_GET["imp_cate"])){$fmi_imp_cate = $_GET["imp_cate"];}

$search_query = "";
if($fmi_date_type == 'date_impris'){
    $fld_date_col = "imp_prisdate";
}else{
    $fld_date_col = "imp_date_created";
}
if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
    $field_condition = $field_condition . " AND ".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
}else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
    $search_query = $search_query . " AND imp.".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
    $field_condition = $field_condition . " AND ".$fld_date_col." between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
}
if(!empty($fmi_imp_laws)){
    $search_query = $search_query . " AND imp.imp_reg_uniq = '".$fmi_imp_laws."'";
    $field_condition = $field_condition . " AND imp_reg_uniq = '".$fmi_imp_laws."'";
}

if(!empty($fmi_imp_cate)){
    $search_query = $search_query . " AND imp.reg_cate_unique = '".$fmi_imp_cate."'";
    $field_condition = $field_condition . " AND reg_cate_unique = '".$fmi_imp_cate."'";
}

if(isset($_GET["chk_rec_drf"]) && $_GET["chk_rec_drf"] == "n"){
    $search_query = $search_query . "  and imp.imp_rec_type = 'n'";
}

$qs_result = $SysReport->getImprisByRegCate($field_condition,$search_query,$fmi_provn_class);
$imp_male = 0;
$imp_female = 0;
$oth_male = 0;
$oth_female = 0;
$grd_total = 0;

$rept_name = $rep_header_desc;
$rept_range = "ระหว่างวันที่ ".$_GET["datefrom"]." จนถึง ".$_GET["dateto"];
$rept_filename = $rept_name."_".$_GET["datefrom"]."_".$_GET["dateto"];
$rept_header = "|".$rept_name."<br>".$rept_range."|";
$rept_footer = $_SESSION["cookies_username"]." : ".$SysMainActivity->getClientIpAddress()."|หน้า {PAGENO} / {nb}|{DATE j/m/Y h:m}";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="panel panel-default panel-table">
            <div class="panel-body">
                <table id="rept_style" width="100%" cellpadding=0 cellspacing=0>
                    <thead>
                        <tr>
                            <td width="40%" rowspan="2" align="center" valign="top" style="<?php echo $stly_lt;?>"><b>ข้อหาที่ถูกจับกุม</b></td>
                            <td width="20%" colspan="2" align="center" style="<?php echo $stly_lt;?>"><b>คนในพื้นที่ ศอ.บต.<?php if($fmi_provn_class != ""){ echo "<br>(".str_replace("'","", str_replace(","," ",$fmi_provn_class)).")";}?></b></td>
                            <td width="20%" colspan="2" align="center" style="<?php echo $stly_lt;?>"><b>คนจังหวัดอื่น</b></td>
                            <td width="20%" rowspan="2" align="center" valign="top" style="<?php echo $stly_ltr;?>"><b>รวมทั้งสิ้น</b></td>
                        </tr>
                        <tr>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>ชาย</b></td>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>หญิง</b></td>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>ชาย</b></td>
                            <td width="10%" align="center" style="<?php echo $stly_lt;?>"><b>หญิง</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 0;
                            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                                $num = $num + 1;
                                $imp_male = $imp_male + $rows["imp_male"];
                                $imp_female = $imp_female + $rows["imp_female"];
                                $oth_male = $oth_male + $rows["oth_male"];
                                $oth_female = $oth_female + $rows["oth_female"];
                                $grd_total = $grd_total + $rows["total"];
                                $reg_cate_unique = $rows["reg_cate_unique"];
                                $qs_result_detail = $SysReport->getImprisByReg($field_condition,$search_query,$fmi_provn_class,$reg_cate_unique);
                        ?>
                        <tr style="background-color:#A9A9A9;">
                            <td align="left" style="<?php echo $stly_lt;?>"><b><?php echo $num.". ".$rows["reg_cate_desc"];?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo $rows["imp_male"];?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo $rows["imp_female"];?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo $rows["oth_male"];?></b></td>
                            <td align="right" style="<?php echo $stly_lt;?>"><b><?php echo $rows["oth_female"];?></b></td>
                            <td align="right" style="<?php echo $stly_ltr;?>"><b><?php echo $rows["total"];?></b></td>
                        </tr>
                            <?php
                                $s_num = 0;
                                while($rows1 = $qs_result_detail->fetch(PDO::FETCH_ASSOC)){
                                    $s_num = $s_num + 1;
                            ?>
                                <tr>
                                    <td align="left" style="<?php echo $stly_lt;?>"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;".$num.".".$s_num." ".$rows1["reg_list"];?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo $rows1["imp_male"];?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo $rows1["imp_female"];?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo $rows1["oth_male"];?></td>
                                    <td align="right" style="<?php echo $stly_lt;?>"><?php echo $rows1["oth_female"];?></td>
                                    <td align="right" style="<?php echo $stly_ltr;?>"><?php echo $rows1["total"];?></td>
                                </tr>
                        <?php
                                } /* End while loop $qs_result_detail */
                            } /* End while loop $qs_result */
                        ?>
                        <tr style="background-color:#A9A9A9;">
                            <td align="left" style="<?php echo $stly_ltb;?>"><b>รวมทั้งสิ้น</b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $imp_male;?></b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $imp_female;?></b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $oth_male;?></b></td>
                            <td align="right" style="<?php echo $stly_ltb;?>"><b><?php echo $oth_female;?></b></td>
                            <td align="right" style="<?php echo $stly_lrtb;?>"><b><?php echo $grd_total;?></b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php require_once DOCUMENT_ROOT . '/contentadmin/page/inc_rep_hidden_values.php';?>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    $pdf = new mPDF('th', 'A4-L', '0', 'THSaraban','15','15','22','15');
    $pdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('../../folder_script/css_report_generator.css'); // external css
    $pdf->SetTitle($rept_name);
    $pdf->defaultheaderfontsize=10;
    $pdf->defaultheaderfontstyle='B';
    $pdf->SetHeader($rept_header);
    $pdf->defaultfooterfontstyle='I';
    $pdf->setFooter($rept_footer);
    $pdf->WriteHTML($stylesheet,1);
    $pdf->WriteHTML($html, 2);
    ob_end_clean();
    $pdf->Output($rept_filename.'.pdf', 'I');
}
?>
