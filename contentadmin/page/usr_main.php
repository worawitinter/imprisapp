<!-- usr_main.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysTransNum.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .chip {
                display: inline-block;
                padding: 0 25px;
                height: 80px;
                font-size: 16px;
                line-height: 50px;
                border-radius: 25px;
                background-color: #d8f6ff;
            }

            .chip img {
                float: left;
                margin: 0 10px 0 -25px;
                height: 50px;
                width: 50px;
                border-radius: 50%;
            }
        </style>
        <script type="text/javascript">
            $(document).on("click", ".parse-frommode", function () {
                var frommode = $(this).data('frommode');
                var fromtrans = $("#fmi_fromtrans").val();
                $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                $('.modal-body').find('button').removeClass( "btn dropdown-toggle disabled btn-default" ).addClass( "btn dropdown-toggle btn-default" );
                $(".modal-content #fmi_frommode").val( frommode );
                $('.modal-body input').val('');
                $('.modal-body textarea').val('');
                $('#fmi_ori_'+ fromtrans +'_autonum').hide();
                $('#fmi_'+ fromtrans +'_autonum_list').show();
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                $("#frm_lock").hide();
                $("#frm_unlock").hide();
                $("#frm_cancel").show();
                $("#frm_submit").show();
                $('#frm_submit').attr('disabled',false);
                $('#frm_cancel').attr('disabled',false);
                parseModalHeaderDesc();
                getNewUniqueID();
                getAutoSysNum(fromtrans);
                chk_exist_record();
            });

            $(document).on("click", ".row_upd", function () {
                var frommode = $(this).data('frommode');
                var upd_id = $(this).data('id');
                var fromtrans = $("#fmi_fromtrans").val();
                $(".modal-content #fmi_frommode").val( frommode );
                $(".modal-content #fmi_uniquenum_pri").val( upd_id );
                //alert(document.getElementById('fmi_frommode').value);
                if(frommode == 'view'){
                    var tag_audit_yn = "y";
                }else{
                    var tag_audit_yn = "n";
                }
                $('#div_lock_msg').hide();
                $('#chk_lock_record').val('n');
                getRowUpdate(tag_audit_yn);
                parseModalHeaderDesc();
                chkLockRecord();
                autoChkLockRecord();
                $('#fmi_ori_'+ fromtrans +'_autonum').show();
                $('#fmi_'+ fromtrans +'_autonum_list').hide();
                document.getElementById('fmi_usr_firstname').autofocus;
                if(frommode == 'view'){
                    $("#frm_footer").hide();
                    $("#menu-toggle").hide();
                    $("#sidebar-wrapper").hide();
                    $('.modal-body').find('input, textarea, button, select').attr('disabled','disabled');
                }else{
                    $("#frm_footer").show();
                    $("#sidebar-wrapper").show();
                    $("#menu-toggle").show();
                    $('.modal-body').find('input, textarea, button, select').removeAttr('disabled');
                }
            });

            function confirmDelete(row,id){
                var sys_autonum = $("#r_sys_autonum" + row).val();
                $.confirm({
                    title: '',
                    content: 'ต้องการลบหมายเลขรายการ ' + sys_autonum + '?',
                    icon: 'fa fa-trash',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก',
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-danger',
                            action: function(){
                                $(".modal-content #fmi_frommode").val('delete');
                                $(".modal-content #fmi_uniquenum_pri").val(id);
                                $.LoadingOverlay("show");
                                $.ajax({
                                    type: 'post',
                                    url: 'usr_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        toggleAlertBox('','ลบหมายเลขรายการ ' + sys_autonum + ' สำเร็จ!','fa fa-trash','green');
                                        jQuery("#target-content").load("inc_usr_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function getNewUniqueID(){
                var tag_usage = 'autogen_upri';
                $.ajax({
                    url: '../control/inc_ajax_text.php',
                    data: "tag_usage=" + tag_usage,
                    dataType: 'text',
                    success: function(data)
                    {
                        document.getElementById('fmi_uniquenum_pri').value = data;
                    }
                });
            }

            function getRowUpdate(tag_audit_yn){
                var fromlink = $("#fmi_fromlink").val();
                var fromtrans = $("#fmi_fromtrans").val();
                var uniquenum_pri = $("#fmi_uniquenum_pri").val();
                $.ajax({
                    url: '../control/inc_ajax_fetch_row_update.php',
                    data: "fromlink=" + fromlink + "&tag_usage=" + fromtrans + "&uniquenum_pri=" + uniquenum_pri + "&tag_audit_yn=" + tag_audit_yn,
                    dataType: 'json',
                    success: function(data)
                    {
                        document.getElementById('fmi_ori_'+ fromtrans +'_autonum').value = data[0].sys_autonum;
                        document.getElementById('fmi_usr_firstname').value = data[0].usr_firstname;
                        document.getElementById('fmi_usr_lastname').value = data[0].usr_lastname;
                        document.getElementById('fmi_usr_contact').value = data[0].usr_contact;
                        document.getElementById('fmi_usr_email').value = data[0].usr_email;
                        document.getElementById('fmi_usr_remark').value = data[0].usr_remark;
                        document.getElementById('fmi_usr_username').value = data[0].usr_username;
                        //document.getElementById('fmi_usr_password').value = data[0].usr_password;
                        document.getElementById('fmi_usr_enddate').value = moment(data[0].usr_enddate).format('DD/MM/YYYY');
                        document.getElementById('fmi_date_created').value = data[0].date_created;
                        document.getElementById('fmi_usr_last_access').value = moment(data[0].user_last_access).format('DD/MM/YYYY hh:mm:ss');

                        $('select[name=fmi_usr_dept]').val(data[0].usr_dept_uniq);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_usr_dept option[value='"+ data[0].usr_dept_uniq +"']").text(data[0].usr_dept_desc);
                        $("select[name=fmi_usr_dept]").selectpicker("refresh");

                        var title_desc = '';
                        if(data[0].usr_title == 'mr'){
                            title_desc = 'นาย';
                        }else if(data[0].usr_title == 'ms'){
                            title_desc = 'นางสาว';
                        }else if(data[0].usr_title == 'mrs'){
                            title_desc = 'นาง';
                        }else if(data[0].usr_title == 'master'){
                            title_desc = 'เด็กชาย';
                        }else if(data[0].usr_title == 'miss'){
                            title_desc = 'เด็กหญิง';
                        }
                        $('select[name=fmi_usr_title]').val(data[0].usr_title);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_usr_title option[value='"+ data[0].usr_title +"']").text(title_desc);
                        $("select[name=fmi_usr_title]").selectpicker("refresh");

                        var role_desc = '';
                        if(data[0].usr_role == 'admin'){
                            role_desc = 'ผู้ดูแลระบบ';
                        }else if(data[0].usr_role == 'user'){
                            role_desc = 'ผู้ใช้งาน';
                        }
                        $('select[name=fmi_usr_role]').val(data[0].usr_role);
                        $('.selectpicker').selectpicker('refresh')
                        $("#fmi_usr_role option[value='"+ data[0].usr_role +"']").text(role_desc);
                        $("select[name=fmi_usr_role]").selectpicker("refresh");

                        var temp = data[0].usr_right;
                        var arr_right = new Array();
                        arr_right = temp.split(",");
                        $('#fmi_usr_right').selectpicker('val', arr_right);
                    }
                });
            }

            function formSearch(){
                if(typeof document.getElementById('form_search') === 'object'){
                    var fmi_form_search = document.getElementById('form_search').value;
    				jQuery("#target-content").load("inc_usr_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1&search_query="+fmi_form_search);
                    jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&search_query="+fmi_form_search);
                }
            }

            function chk_exist_record(){
                var tablename = '<?php echo $tablename?>';
                var fromtrans = $("#fmi_fromtrans").val();
                $('#fmi_usr_username').blur(function(){
                    var src_fld = $('#fmi_usr_username').val();
                    var chk_fld = 'username';
                    chkExistField(tablename,fromtrans,src_fld,chk_fld);
                });
            }

            $(document).ready(function() {
                $('#frm_usr').on('hidden.bs.modal', function () {
                    if(IntChkLock != null){
                        clearInterval(IntChkLock);
                    }
                });
            });

            function confirmLogout(username,id){
                $.confirm({
                    title: '',
                    content: 'ชื่อผู้ใช้ ' + username + ' อยู่ในสถานะออนไลน์ ต้องการออกจากระบบ?',
                    icon: 'fa fa-power-off',
                    theme: 'modern',
                    type: 'red',
                    buttons: {
                        cancel: {
                            text: 'ยกเลิก',
                        },
                        confirm: {
                            text: 'ยืนยัน',
                            btnClass: 'btn-danger',
                            action: function(){
                                $(".modal-content #fmi_frommode").val('logout');
                                $(".modal-content #fmi_uniquenum_pri").val(id);
                                $.LoadingOverlay("show");
                                $.ajax({
                                    type: 'post',
                                    url: 'usr_logout_oup.php',
                                    data: $('form').serialize(),
                                    success: function () {
                                        toggleAlertBox('','ชื่อผู้ใช้ ' + username + ' ถูกตัดออกจากระบบแล้ว!','fa fa-power-off','green');
                                        jQuery("#target-content").load("inc_usr_main_list.php?fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>&page=1");
                                        jQuery("#target-pagination").load("inc_pagination_main_list.php?tablename=<?php echo $tablename;?>&fromtrans=<?php echo $_GET["fromtrans"];?>&fromlink=<?php echo $_GET["fromlink"];?>&fromtarget=<?php echo $_GET["fromtarget"];?>&frommode=<?php echo $_GET["frommode"];?>");
                                        $.LoadingOverlay("hide");
                                    }
                                });
                            }
                        }
                    }
                });
            }
        </script>
    </head>
    <body>
        <div id="pnl_list" class="card" style="width: <?php echo PANEL_WIDTH;?>;height:<?php echo PANEL_HEIGHT;?>">
            <?php require_once 'inc_usr_main_form.php';?>
            <div class="container" style="width:auto">
                <?php
                    require_once DOCUMENT_ROOT . '/contentadmin/page/inc_header_listmain.php';
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6"></div>
                                    <div class="col col-xs-6 text-right">
                                        <a <?php if($_SESSION["cookies_sys_create_yn"] != 'y'){ ?>style="display:none"<?php }?>href="main_screen<?php echo $url_file_ext; ?>?fromtrans=<?php echo $_GET["fromtrans"]."_sys_autonum";?>&fromlink=sys_autonum&fromtarget=list&frommode=view" title="ตั้งค่าหมายเลขอัตโนมัติ" class="parse-frommode btn btn-primary" style="color:#ffffff"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                                        <a data-toggle="modal" data-frommode="new" data-target="#frm_usr" title="เพิ่ม" class="parse-frommode btn btn-primary" href="#frm_usr" style="color:#ffffff"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" id="target-content"></div>
                            <?php
                                require_once DOCUMENT_ROOT . '/contentadmin/page/inc_pagination_main.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
