<?php
@session_start();
require_once '../config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainCookies.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/Authentication.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysMainCookies = new SysMainCookies();
$Authentication = new Authentication();
$SysMainActivity = new SysMainActivity();

$usr_uniquenum_pri = $_POST["fmi_uniquenum_pri"];
$tablename = "sys_user_main";
$qs_login = $Authentication->verifyUser($tablename,$DatabaseOperation,$usr_uniquenum_pri);
$log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
$log_tablename = "sys_activity_main";
$log_fromtrans = "usraccess";
$date_created = $TransactionGenUnique->generate_trans_datetime();
$location = LOCATION;
if($qs_login->rowCount() > 0){
    while($row=$qs_login->fetch(PDO::FETCH_OBJ)) {
        $log_data = array   (   "tag_table_usage"=>$log_fromtrans,
                            "uniquenum_pri"=>$log_uniquenum_pri,
                            "uniquenum_sec"=>$row->var_25_002,
                            "sys_link"=>"sys_logout",
                            "userid_cookie"=>$row->username,
                            "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                            "date_created"=>$date_created,
                            "desc_lang01"=>$_SERVER['HTTP_USER_AGENT'],
                            "notes_memo"=>$location,
                            "desc_lang10"=>getHostByName(getHostName()),
                            "setgen_code"=>$_SESSION["cookies_username"]
                        );
        $DatabaseOperation->insertRecordIntoTable($log_tablename,$log_data);
        $SysMainCookies->clearUserCookies($usr_uniquenum_pri);
    }
}
?>
