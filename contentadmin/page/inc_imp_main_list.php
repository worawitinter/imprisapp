<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysListMain.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$TransactionGenUnique = new TransactionGenUnique();
$DatabaseOperation = new DatabaseOperation();
$SysListMain = new SysListMain();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table class="table table-striped table-bordered table-list">
            <tr>
                <th class="text-center"><em class="fa fa-cog"></em></th>
                <th>วันที่รับแจ้ง</th>
                <th>ประเภทงาน</th>
                <th>ชื่อ-สกุล</th>
                <th>สถานะ</th>
            </tr>
        <?php
            $uniquenum_pri = '';
            if(isset($_GET["page"])){
                $page = $_GET["page"];
            }else{
                $page = 1;
            }
            if(isset($_GET["search_query"])){
                $search_query = $_GET["search_query"];
            }else{
                $search_query = "";
            }
            $limit = PAGE_LIMIT;
            $page_start = ($page-1) * $limit;
            $cnt = 0;
            $qs_result = $SysListMain->getRowImprisResult($_GET["fromtrans"],$uniquenum_pri,$page_start,$limit,$search_query,$tag_audit_yn = 'n');
            if($search_query != ""){
                $total_records = $qs_result->rowCount();
            }else{
                $total_records = $DatabaseOperation->getTotalRecords($tablename,$_GET['fromtrans']);
            }
            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                $cnt++;
                $r_uniquenum_pri = $rows["uniquenum_pri"];
        ?>
        <tr>
            <td align="center" width="15%">
                <input type="hidden" id="r_sys_autonum<?php echo $cnt;?>" name="r_sys_autonum<?php echo $cnt;?>" value="<?php echo $rows["sys_autonum"];?>">
                <a data-toggle="modal" data-target="#frm_imp" class="row_upd btn btn-success" title="เรียกดูหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="view" href="#frm_imp" <?php if($_SESSION["cookies_sys_view_yn"] == 'n'){ echo "style='display:none'"; } ?>><em class="fa fa-eye" style="color: #FFFFFF"></em></a>
                <a data-toggle="modal" data-target="#frm_imp" class="row_upd btn btn-success" title="แก้ไขหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="edit" href="#frm_imp" <?php if($_SESSION["cookies_sys_create_yn"] == 'n'){ echo "style='display:none'"; } ?>><em class="fa fa-pencil" style="color: #FFFFFF"></em></a>
                <a class="row_del btn btn-danger" title="ลบหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="delete" onclick="confirmDelete('<?php echo $cnt;?>','<?php echo $rows["uniquenum_pri"];?>');" <?php if($_SESSION["cookies_sys_delete_yn"] == 'n'){ echo "style='display:none'"; } ?>><em class="fa fa-trash"></em></a>
                <a data-toggle="modal" data-target="#frm_imp_attchmt" class="row_upd btn btn-warning" title="แนบเอกสารหมายเลขรายการ <?php echo $rows["sys_autonum"];?>" data-id="<?php echo $rows["uniquenum_pri"];?>" data-frommode="upload" data-impname="<?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?>" <?php if($_SESSION["cookies_sys_create_yn"] == 'n'){ echo "style='display:none'"; } ?>><em class="fa fa-paperclip" style="color: #FFFFFF"></em></a>
            </td>
            <td width="15%"><?php echo $rows["imp_date_created"];?></td>
            <td width="15%"><?php echo $rows["imp_cate_desc"];?></td>
            <td width="40%"><?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?>
                <br><?php if($rows["imp_firstname_2"] != ""){echo " (".strtoupper($rows["imp_title_2"])." ".strtoupper($rows["imp_firstname_2"])." ".strtoupper($rows["imp_lastname_2"]).")";}?>
            </td>
            <td width="15%"><?php echo $rows["imp_stat_desc"];?></td>
        </tr>
        <?php
            require DOCUMENT_ROOT . '/contentadmin/page/inc_imp_main_list_row_detail.php';
            } /* End while loop $qs_result */
        ?>
        </table>
    </body>
</html>
