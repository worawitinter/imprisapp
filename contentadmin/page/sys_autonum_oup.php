<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170813             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/AuditTrailLog.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/TransactionGenUnique.php';

$TransactionGenUnique = new TransactionGenUnique();
$SysMainActivity = new SysMainActivity();
$SysConversion = new SysConversion();
$DatabaseOperation = new DatabaseOperation();
$AuditTrailLog = new AuditTrailLog();

$sys_prefix_code = $_POST["fmi_prefix_code"];
$sys_run_number = $_POST["fmi_run_number"];
$sys_suffix_code = $_POST["fmi_suffix_code"];
if(isset($_POST["chk_mth_year"])){
    $sys_chk_mth_year = "y";
    $sys_mth_year = date("mY");
}else{
    $sys_chk_mth_year = "n";
    $sys_mth_year = "";
}
if(isset($_POST["chk_active"])){
    $sys_chk_active = "y";
}else{
    $sys_chk_active = "n";
}
if(isset($_POST["chk_default"])){
    $sys_chk_default = "y";
}else{
    $sys_chk_default = "n";
}
if($frommode == "new"){
    $date_created = $TransactionGenUnique->generate_trans_datetime();
    $log_uniquenum_pri = $TransactionGenUnique->autogen_uniquenum_pri();
}else{
    $date_created = $_POST["fmi_date_created"];
}
$gencode_code = $sys_prefix_code.$sys_mth_year.$sys_run_number.$sys_suffix_code;
$tablename = "sys_gencode_main";

$data = array   (   "tag_table_usage"=>$fromtrans,
                    "uniquenum_pri"=>$uniquenum_pri,
                    "sys_link"=>$fromlink,
                    "userid_cookie"=>$userid,
                    "ip_addr"=>$SysMainActivity->getClientIpAddress(),
                    "date_created"=>$date_created,
                    "date_lastupdate"=>$TransactionGenUnique->generate_trans_datetime(),
                    "setgen_unique"=>$uniquenum_pri,
                    "setgen_code"=>$sys_prefix_code,
                    "desc_lang01"=>$sys_run_number,
                    "desc_lang02"=>$sys_suffix_code,
                    "desc_lang03"=>$sys_mth_year,
                    "tag_other02_yn"=>$sys_chk_mth_year,
                    "tag_active_yn"=>$sys_chk_active,
                    "tag_other01_yn"=>$sys_chk_default
                );
if($frommode == "new"){
    $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
    $AuditTrailLog->createActivityLogs($frommode,$fromtrans,$fromlink,$gencode_code,$log_uniquenum_pri,$uniquenum_pri,$userid,$date_created);
}elseif($frommode == "edit"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$gencode_code,$tablename,$uniquenum_pri,$userid);
    $result = $DatabaseOperation->insertRecordIntoTable($tablename,$data);
}elseif($frommode == "delete"){
    $AuditTrailLog->resetRecord($frommode,$fromtrans,$fromlink,$gencode_code,$tablename,$uniquenum_pri,$userid);
}
?>
