<!-- home_main.php -->
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/GencodeSboxList.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <script language="javascript">
            function innitData(datefrom,dateto){
                $.LoadingOverlay("show");
                getTotalRecords(datefrom,dateto);
                getTodayTotalRecords(datefrom,dateto);
                getTotalGroupByMale(datefrom,dateto);
                getTotalGroupByFemale(datefrom,dateto);
                getPieChart(datefrom,dateto);
                getLineChart(datefrom,dateto);
                getBarChart(datefrom,dateto);
                getListByPrisDet(datefrom,dateto);
                getListByState(datefrom,dateto);
                getListByReg(datefrom,dateto);
                getListByPrisStatus(datefrom,dateto);
                $.LoadingOverlay("hide");
            }

            function getTotalRecords(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#summ_total').empty();
                $('#summ_total').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $('#summ_total_daterange').html("<br><font size='4'>" + datefrom + " ~ " + dateto + "</font>");
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_total_record&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     dataType: 'text',
                     success: function(data){
                          if(data){
                              $("#summ_total").html("<font class='dashbrd-total-number'><b>" + data.toLocaleString() + "</b></font>");
                          }else{
                              $("#summ_total").html("<font class='dashbrd-total-number'><b>0</b></font>");
                          }
                    }
                });
            }

            function getTodayTotalRecords(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#summ_today').empty();
                $('#summ_today').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $('#summ_total_todate').html("<br><font size='4'>ณ วันที่ " + moment().format('DD/MM/YYYY') + "</font>");
                var todaydate = moment();
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_total_today&datefrom=" + todaydate + "&imp_cate_uniq=" + imp_cate_uniq,
                     dataType: 'text',
                     success: function(data){
                          if(data){
                              $("#summ_today").html("<font class='dashbrd-total-number'><b>" + data.toLocaleString() + "</b></font>");
                          }else{
                              $("#summ_today").html("<font class='dashbrd-total-number'><b>0</b></font>");
                          }
                    }
                });
            }

            function getTotalGroupByMale(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#summ_male').empty();
                $('#summ_male').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $('#summ_total_daterange_male').html("<br><font size='4'>" + datefrom + " ~ " + dateto + "</font>");
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_total_male&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     dataType: 'text',
                     success: function(data){
                          if(data){
                              $("#summ_male").html("<font class='dashbrd-total-number'><b>" + data.toLocaleString() + "</b></font>");
                          }else{
                              $("#summ_male").html("<font class='dashbrd-total-number'><b>0</b></font>");
                          }
                    }
                });
            }

            function getTotalGroupByFemale(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#summ_female').empty();
                $('#summ_female').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $('#summ_total_daterange_female').html("<br><font size='4'>" + datefrom + " ~ " + dateto + "</font>");
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_total_female&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     dataType: 'text',
                     success: function(data){
                          if(data){
                              $("#summ_female").html("<font class='dashbrd-total-number'><b>" + data.toLocaleString() + "</b></font>");
                          }else{
                              $("#summ_female").html("<font class='dashbrd-total-number'><b>0</b></font>");
                          }
                    }
                });
            }

            function getListByPrisDet(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#pris_det_detail').empty();
                $('#pris_det_detail').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_list_pris_det&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        $('#pris_det_detail').empty();
                        var tableHTML = '';
                        tableHTML += '<div class="table-responsive">';
                        tableHTML += '<table class="table">';
                            tableHTML += '<thead>';
                              tableHTML += '<tr class="dashboard-text">';
                                tableHTML += '<th width="10%">#</th>';
                                tableHTML += '<th width="70%">สถานที่ควบคุมตัว</th>';
                                tableHTML += '<th width="20%">จำนวน</th>';
                              tableHTML += '</tr>';
                            tableHTML += '</thead>';
                       tableHTML += '<tbody>';
                        var cnt = 1;
                        var json = $.parseJSON(data);
                        if(json.length > 0){
                            for (var i=0;i<json.length;++i){
                                cnt = i + 1;
                                var det_desc = json[i].det_desc;
                                var total_pris = json[i].total_pris;
                                tableHTML += '<tr>';
                                tableHTML += '<th width="10%">' + cnt + '</th>';
                                tableHTML += '<th width="80%">' + det_desc + '</th>';
                                tableHTML += '<th width="10%"><span class="badge badge-inverse">' + total_pris.toLocaleString() + '</span></th>';
                                tableHTML += '</tr>';
                            }
                        }else{
                            tableHTML += '<tr>';
                            tableHTML += '<th colspan="3" width="100%"><i class="fa fa-frown-o" aria-hidden="true"></i> <i>ไม่พบข้อมูล...</i></th>';
                            tableHTML += '</tr>';
                        }
                        tableHTML += '</tbody>';
                        tableHTML += '</table>';
                        tableHTML += '</div>';
                        $('#pris_det_detail').append(tableHTML);
                    }
                });
            }

            function getListByState(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#pris_state_detail').empty();
                $('#pris_state_detail').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_list_state&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        $('#pris_state_detail').empty();
                        var tableHTML = '';
                        tableHTML += '<div class="table-responsive">';
                        tableHTML += '<table class="table">';
                            tableHTML += '<thead>';
                              tableHTML += '<tr class="dashboard-text">';
                                tableHTML += '<th width="10%">#</th>';
                                tableHTML += '<th width="70%">รัฐ</th>';
                                tableHTML += '<th width="20%">จำนวน</th>';
                              tableHTML += '</tr>';
                            tableHTML += '</thead>';
                       tableHTML += '<tbody>';
                        var cnt = 1;
                        var json = $.parseJSON(data);
                        if(json.length > 0){
                            for (var i=0;i<json.length;++i){
                                cnt = i + 1;
                                var state_desc = json[i].state_desc;
                                var total_pris = json[i].total_pris;
                                tableHTML += '<tr>';
                                tableHTML += '<th width="10%">' + cnt + '</th>';
                                tableHTML += '<th width="80%">' + state_desc + '</th>';
                                tableHTML += '<th width="10%"><span class="badge badge-inverse">' + total_pris.toLocaleString() + '</span></th>';
                                tableHTML += '</tr>';
                            }
                        }else{
                            tableHTML += '<tr>';
                            tableHTML += '<th colspan="3" width="100%"><i class="fa fa-frown-o" aria-hidden="true"></i> <i>ไม่พบข้อมูล...</i></th>';
                            tableHTML += '</tr>';
                        }
                        tableHTML += '</tbody>';
                        tableHTML += '</table>';
                        tableHTML += '</div>';
                        $('#pris_state_detail').append(tableHTML);
                    }
                });
            }

            function getListByReg(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#pris_reg_detail').empty();
                $('#pris_reg_detail').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_list_reg&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        $('#pris_reg_detail').empty();
                        var tableHTML = '';
                        tableHTML += '<div class="table-responsive">';
                        tableHTML += '<table class="table">';
                            tableHTML += '<thead>';
                              tableHTML += '<tr class="dashboard-text">';
                                tableHTML += '<th width="10%">#</th>';
                                tableHTML += '<th width="70%">ข้อหาที่ถูกจับกุม</th>';
                                tableHTML += '<th width="20%">จำนวน</th>';
                              tableHTML += '</tr>';
                            tableHTML += '</thead>';
                       tableHTML += '<tbody>';
                        var cnt = 1;
                        var json = $.parseJSON(data);
                        var last_imp_cate = "";
                        var last_reg_cate = "";
                        if(json.length > 0){
                            for (var i=0;i<json.length;++i){
                                cnt = i + 1;
                                var imp_cate_desc = json[i].imp_cate_desc;
                                var reg_cate_desc = json[i].reg_cate_desc;
                                var reg_desc = json[i].reg_desc;
                                var total_pris = json[i].total_pris;
                                if(last_imp_cate != imp_cate_desc){
                                    tableHTML += '<tr>';
                                    tableHTML += '<th width="10%"></th>';
                                    tableHTML += '<th colspan="2" width="90%"><span class="label label-primary"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <b>' + imp_cate_desc + '</b></span></th>';
                                    tableHTML += '</tr>';
                                }
                                if(last_reg_cate != reg_cate_desc){
                                    tableHTML += '<tr>';
                                    tableHTML += '<th width="10%"></th>';
                                    tableHTML += '<th colspan="2" width="90%"><span class="label label-danger"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <b>' + reg_cate_desc + '</b></span></th>';
                                    tableHTML += '</tr>';
                                }
                                tableHTML += '<tr>';
                                tableHTML += '<th width="10%">' + cnt + '</th>';
                                tableHTML += '<th width="80%">' + reg_desc + '</th>';
                                tableHTML += '<th width="10%"><span class="badge badge-inverse">' + total_pris.toLocaleString() + '</span></th>';
                                tableHTML += '</tr>';
                                last_imp_cate = imp_cate_desc;
                                last_reg_cate = reg_cate_desc;
                            }
                        }else{
                            tableHTML += '<tr>';
                            tableHTML += '<th colspan="3" width="100%"><i class="fa fa-frown-o" aria-hidden="true"></i> <i>ไม่พบข้อมูล...</i></th>';
                            tableHTML += '</tr>';
                        }
                        tableHTML += '</tbody>';
                        tableHTML += '</table>';
                        tableHTML += '</div>';
                        $('#pris_reg_detail').append(tableHTML);
                    }
                });
            }

            function getListByPrisStatus(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#pris_status_detail').empty();
                $('#pris_status_detail').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_list_status&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        $('#pris_status_detail').empty();
                        var tableHTML = '';
                        tableHTML += '<div class="table-responsive">';
                        tableHTML += '<table class="table">';
                            tableHTML += '<thead>';
                              tableHTML += '<tr class="dashboard-text">';
                                tableHTML += '<th width="10%">#</th>';
                                tableHTML += '<th width="70%">สถานะ</th>';
                                tableHTML += '<th width="20%">จำนวน</th>';
                              tableHTML += '</tr>';
                            tableHTML += '</thead>';
                       tableHTML += '<tbody>';
                        var cnt = 1;
                        var json = $.parseJSON(data);
                        var last_imp_cate = "";
                        if(json.length > 0){
                            for (var i=0;i<json.length;++i){
                                cnt = i + 1;
                                var imp_cate_desc = json[i].imp_cate_desc;
                                var state_desc = json[i].state_desc;
                                var total_pris = json[i].total_pris;
                                if(last_imp_cate != imp_cate_desc){
                                    tableHTML += '<tr>';
                                    tableHTML += '<th width="10%"></th>';
                                    tableHTML += '<th colspan="2" width="90%"><span class="label label-primary"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <b>' + imp_cate_desc + '</b></span></th>';
                                    tableHTML += '</tr>';
                                    last_imp_cate = imp_cate_desc;
                                }
                                tableHTML += '<tr>';
                                tableHTML += '<th width="10%">' + cnt + '</th>';
                                tableHTML += '<th width="80%">' + state_desc + '</th>';
                                tableHTML += '<th width="10%"><span class="badge badge-inverse">' + total_pris.toLocaleString() + '</span></th>';
                                tableHTML += '</tr>';
                            }
                        }else{
                            tableHTML += '<tr>';
                            tableHTML += '<th colspan="3" width="100%"><i class="fa fa-frown-o" aria-hidden="true"></i> <i>ไม่พบข้อมูล...</i></th>';
                            tableHTML += '</tr>';
                        }
                        tableHTML += '</tbody>';
                        tableHTML += '</table>';
                        tableHTML += '</div>';
                        $('#pris_status_detail').append(tableHTML);
                    }
                });
            }

            function getPieChart(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                $('#dshbrd_chart_age_group').empty();
                $('#dshbrd_chart_age_group').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_chart_age_group&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        var json = $.parseJSON(data);
                        if(json.length > 0){
                            var chart = c3.generate({
                                bindto: '#dshbrd_chart_age_group',
                                data: {
                                    // iris data from R
                                    columns: [
                                    ],
                                    type : 'pie',
                                    onclick: function (d, i) { console.log("onclick", d, i); },
                                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                                }
                            });

                            setTimeout(function () {
                                chart.load({
                                    columns: [
                                        ['< 20', json[0].below_20],
                                        ['20 - 29', json[0]._20_29],
                                        ['30 - 39', json[0]._30_39],
                                        ['40 - 49', json[0]._40_49],
                                        ['50 - 59', json[0]._50_59],
                                        ['60 - 69', json[0]._60_69],
                                        ['70 - 79', json[0]._70_79],
                                        ['> 80', json[0].above_80],
                                        ['ไม่ระบุ', json[0].na],
                                    ]
                                });
                            }, 1500);
                        }
                    }
                });
            }

            function getLineChart(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                var f_gender = "เพศหญิง";
                var f_jan = 0;
                var f_feb = 0;
                var f_mar = 0;
                var f_apr = 0;
                var f_may = 0;
                var f_jun = 0;
                var f_jul = 0;
                var f_aug = 0;
                var f_sep = 0;
                var f_oct = 0;
                var f_nov = 0;
                var f_dec = 0;
                var f_na = 0;

                var m_gender = "เพศชาย";
                var m_jan = 0;
                var m_feb = 0;
                var m_mar = 0;
                var m_apr = 0;
                var m_may = 0;
                var m_jun = 0;
                var m_jul = 0;
                var m_aug = 0;
                var m_sep = 0;
                var m_oct = 0;
                var m_nov = 0;
                var m_dec = 0;
                var m_na = 0;

                var t_gender = "รวมทั้งหมด";
                var t_jan = 0;
                var t_feb = 0;
                var t_mar = 0;
                var t_apr = 0;
                var t_may = 0;
                var t_jun = 0;
                var t_jul = 0;
                var t_aug = 0;
                var t_sep = 0;
                var t_oct = 0;
                var t_nov = 0;
                var t_dec = 0;
                var t_na = 0;

                $('#dshbrd_chart_prist_date_monthly').empty();
                $('#dshbrd_chart_prist_date_monthly').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_chart_prist_date_monthly&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        var json = $.parseJSON(data);
                        if(typeof json[0] != 'undefined'){
                            f_jan = json[0].jan;
                            f_feb = json[0].feb;
                            f_mar = json[0].mar;
                            f_apr = json[0].apr;
                            f_may = json[0].may;
                            f_jun = json[0].jun;
                            f_jul = json[0].jul;
                            f_aug = json[0].aug;
                            f_sep = json[0].sep;
                            f_oct = json[0].oct;
                            f_nov = json[0].nov;
                            f_dec = json[0].dec;
                            f_na = json[0].na;
                        }

                        if(typeof json[1] != 'undefined'){
                            m_jan = json[1].jan;
                            m_feb = json[1].feb;
                            m_mar = json[1].mar;
                            m_apr = json[1].apr;
                            m_may = json[1].may;
                            m_jun = json[1].jun;
                            m_jul = json[1].jul;
                            m_aug = json[1].aug;
                            m_sep = json[1].sep;
                            m_oct = json[1].oct;
                            m_nov = json[1].nov;
                            m_dec = json[1].dec;
                            m_na = json[1].na;
                        }

                        if(typeof json[2] != 'undefined'){
                            t_jan = json[2].jan;
                            t_feb = json[2].feb;
                            t_mar = json[2].mar;
                            t_apr = json[2].apr;
                            t_may = json[2].may;
                            t_jun = json[2].jun;
                            t_jul = json[2].jul;
                            t_aug = json[2].aug;
                            t_sep = json[2].sep;
                            t_oct = json[2].oct;
                            t_nov = json[2].nov;
                            t_dec = json[2].dec;
                            t_na = json[2].na;
                        }

                        var chart = c3.generate({
                            bindto: '#dshbrd_chart_prist_date_monthly',
                            data: {
                              columns: [
                                [m_gender, m_jan, m_feb, m_mar, m_apr, m_may, m_jun, m_jul, m_aug, m_sep, m_oct, m_nov, m_dec, m_na],
                                [f_gender, f_jan, f_feb, f_mar, f_apr, f_may, f_jun, f_jul, f_aug, f_sep, f_oct, f_nov, f_dec, f_na],
                                [t_gender, t_jan, t_feb, t_mar, t_apr, t_may, t_jun, t_jul, t_aug, t_sep, t_oct, t_nov, t_dec, t_na]

                              ]
                          },axis : {
                              x: {
                                      type: 'category',
                                      categories: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.', 'ไม่ระบุ']
                                  }
                          },
                          bar: {
                              width: {
                                  ratio: 0.5 // this makes bar width 50% of length between ticks
                              }
                              // or
                              //width: 100 // this makes bar width 100px
                          }
                        });

                        setTimeout(function () {
                            chart.load({
                                columns: [

                                ]
                            });
                        }, 1000);
                    }
                });
            }

            function getBarChart(datefrom,dateto){
                var imp_cate_uniq = $('#fmi_imp_cate_unique').val();
                var f_gender = "เพศหญิง";
                var m_gender = "เพศชาย";
                var t_gender = "รวมทั้งหมด";

                var f_MY_01 = 0;
                var f_MY_02 = 0;
                var f_MY_03 = 0;
                var f_MY_04 = 0;
                var f_MY_05 = 0;
                var f_MY_06 = 0;
                var f_MY_07 = 0;
                var f_MY_08 = 0;
                var f_MY_09 = 0;
                var f_MY_10 = 0;
                var f_MY_11 = 0;
                var f_MY_12 = 0;
                var f_MY_13 = 0;
                var f_MY_14 = 0;
                var f_MY_15 = 0;
                var f_MY_16 = 0;

                var m_MY_01 = 0;
                var m_MY_02 = 0;
                var m_MY_03 = 0;
                var m_MY_04 = 0;
                var m_MY_05 = 0;
                var m_MY_06 = 0;
                var m_MY_07 = 0;
                var m_MY_08 = 0;
                var m_MY_09 = 0;
                var m_MY_10 = 0;
                var m_MY_11 = 0;
                var m_MY_12 = 0;
                var m_MY_13 = 0;
                var m_MY_14 = 0;
                var m_MY_15 = 0;
                var m_MY_16 = 0;

                var t_MY_01 = 0;
                var t_MY_02 = 0;
                var t_MY_03 = 0;
                var t_MY_04 = 0;
                var t_MY_05 = 0;
                var t_MY_06 = 0;
                var t_MY_07 = 0;
                var t_MY_08 = 0;
                var t_MY_09 = 0;
                var t_MY_10 = 0;
                var t_MY_11 = 0;
                var t_MY_12 = 0;
                var t_MY_13 = 0;
                var t_MY_14 = 0;
                var t_MY_15 = 0;
                var t_MY_16 = 0;

                $.getJSON( "../../folder_script/countries/countries/malaysia.json", function( data ) {
                    var fld_label = new Array();
                    $.each( data, function( key, val ) {
                        fld_label.push(val.name)
                    });
                    var fld_MY_01 = fld_label[0];
                    var fld_MY_02 = fld_label[1];
                    var fld_MY_03 = fld_label[2];
                    var fld_MY_04 = fld_label[3];
                    var fld_MY_05 = fld_label[4];
                    var fld_MY_06 = fld_label[5];
                    var fld_MY_07 = fld_label[6];
                    var fld_MY_08 = fld_label[7];
                    var fld_MY_09 = fld_label[8];
                    var fld_MY_10 = fld_label[9];
                    var fld_MY_11 = fld_label[10];
                    var fld_MY_12 = fld_label[11];
                    var fld_MY_13 = fld_label[12];
                    var fld_MY_14 = fld_label[13];
                    var fld_MY_15 = fld_label[14];
                    var fld_MY_16 = fld_label[15];
                });

                $('#dshbrd_chart_prist_state').empty();
                $('#dshbrd_chart_prist_state').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">กำลังโหลด...</span>');
                $.ajax({
                     type: "GET",
                     cache: false,
                     url: "../control/inc_ajax_text.php",
                     data: "tag_usage=dshbrd_chart_prist_state&datefrom=" + datefrom + "&dateto=" + dateto + "&imp_cate_uniq=" + imp_cate_uniq,
                     success: function(data){
                        var json = $.parseJSON(data);
                        if(typeof json[0] != 'undefined'){
                            f_MY_01 = json[0].MY_01;
                            f_MY_02 = json[0].MY_02;
                            f_MY_03 = json[0].MY_03;
                            f_MY_04 = json[0].MY_04;
                            f_MY_05 = json[0].MY_05;
                            f_MY_06 = json[0].MY_06;
                            f_MY_07 = json[0].MY_07;
                            f_MY_08 = json[0].MY_08;
                            f_MY_09 = json[0].MY_09;
                            f_MY_10 = json[0].MY_10;
                            f_MY_11 = json[0].MY_11;
                            f_MY_12 = json[0].MY_12;
                            f_MY_13 = json[0].MY_13;
                            f_MY_14 = json[0].MY_14;
                            f_MY_15 = json[0].MY_15;
                            f_MY_16 = json[0].MY_16;

                            fld_MY_01 = json[0].fld_MY_01;
                            fld_MY_02 = json[0].fld_MY_02;
                            fld_MY_03 = json[0].fld_MY_03;
                            fld_MY_04 = json[0].fld_MY_04;
                            fld_MY_05 = json[0].fld_MY_05;
                            fld_MY_06 = json[0].fld_MY_06;
                            fld_MY_07 = json[0].fld_MY_07;
                            fld_MY_08 = json[0].fld_MY_08;
                            fld_MY_09 = json[0].fld_MY_09;
                            fld_MY_10 = json[0].fld_MY_10;
                            fld_MY_11 = json[0].fld_MY_11;
                            fld_MY_12 = json[0].fld_MY_12;
                            fld_MY_13 = json[0].fld_MY_13;
                            fld_MY_14 = json[0].fld_MY_14;
                            fld_MY_15 = json[0].fld_MY_15;
                            fld_MY_16 = json[0].fld_MY_16;
                        }

                        if(typeof json[1] != 'undefined'){
                            m_MY_01 = json[1].MY_01;
                            m_MY_02 = json[1].MY_02;
                            m_MY_03 = json[1].MY_03;
                            m_MY_04 = json[1].MY_04;
                            m_MY_05 = json[1].MY_05;
                            m_MY_06 = json[1].MY_06;
                            m_MY_07 = json[1].MY_07;
                            m_MY_08 = json[1].MY_08;
                            m_MY_09 = json[1].MY_09;
                            m_MY_10 = json[1].MY_10;
                            m_MY_11 = json[1].MY_11;
                            m_MY_12 = json[1].MY_12;
                            m_MY_13 = json[1].MY_13;
                            m_MY_14 = json[1].MY_14;
                            m_MY_15 = json[1].MY_15;
                            m_MY_16 = json[1].MY_16;
                        }

                        if(typeof json[2] != 'undefined'){
                            t_MY_01 = json[2].MY_01;
                            t_MY_02 = json[2].MY_02;
                            t_MY_03 = json[2].MY_03;
                            t_MY_04 = json[2].MY_04;
                            t_MY_05 = json[2].MY_05;
                            t_MY_06 = json[2].MY_06;
                            t_MY_07 = json[2].MY_07;
                            t_MY_08 = json[2].MY_08;
                            t_MY_09 = json[2].MY_09;
                            t_MY_10 = json[2].MY_10;
                            t_MY_11 = json[2].MY_11;
                            t_MY_12 = json[2].MY_12;
                            t_MY_13 = json[2].MY_13;
                            t_MY_14 = json[2].MY_14;
                            t_MY_15 = json[2].MY_15;
                            t_MY_16 = json[2].MY_16;
                        }

                        var chart = c3.generate({
                            bindto: '#dshbrd_chart_prist_state',
                            data: {
                              columns: [
                                [m_gender, m_MY_01, m_MY_02, m_MY_03, m_MY_04, m_MY_05, m_MY_06, m_MY_07, m_MY_08, m_MY_09, m_MY_10, m_MY_11, m_MY_12, m_MY_13, m_MY_14, m_MY_15, m_MY_16],
                                [f_gender, f_MY_01, f_MY_02, f_MY_03, f_MY_04, f_MY_05, f_MY_06, f_MY_07, f_MY_08, f_MY_09, f_MY_10, f_MY_11, f_MY_12, f_MY_13, f_MY_14, f_MY_15, f_MY_16],
                                [t_gender, t_MY_01, t_MY_02, t_MY_03, t_MY_04, t_MY_05, t_MY_06, t_MY_07, t_MY_08, t_MY_09, t_MY_10, t_MY_11, t_MY_12, t_MY_13, t_MY_14, t_MY_15, t_MY_16]

                              ],
                              type: 'bar'
                          },axis : {
                              x: {
                                      type: 'category',
                                      categories: [fld_MY_01, fld_MY_02, fld_MY_03, fld_MY_04, fld_MY_05, fld_MY_06, fld_MY_07, fld_MY_08, fld_MY_09, fld_MY_10, fld_MY_11, fld_MY_12, fld_MY_13, fld_MY_14, fld_MY_15, fld_MY_16]
                                  }
                          },
                          grid: {
                                y: {
                                    lines: [{value:0}]
                                }
                            }
                        });
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="row">
                            <div class="card-member col-lg-2 dashboard-text">
                                <br><span class="glyphicon glyphicon-user icon-size" style="font-size: 115px;"></span>
                            </div>
                            <div class="col-lg-6 dashboard-text" style="font-size: 20px;">
                                <?php $date = new DateTime($_SESSION["cookies_last_access"]);?>
                                <br><strong>ยินดีต้อนรับ คุณ <?php if($_SESSION["cookies_sys_user_firstname"] <> ''){ echo $_SESSION["cookies_sys_user_firstname"]." ".$_SESSION["cookies_sys_user_lastname"];}else{echo $_SESSION["cookies_username"];}?>
                                    <br><?php echo $lbl_usr_role;?>&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $_SESSION["cookies_sys_role"];?>
                                    <?php if($_SESSION["cookies_sys_user_dept_desc"] <> ""){echo "<br>สังกัด&nbsp;&nbsp;<i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;".$_SESSION["cookies_sys_user_dept_desc"];}?>
                                    <br><?php echo $lbl_last_access;?>&nbsp;&nbsp;<i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;<?php echo $date->format('d-m-Y H:i:s');?></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center" style="height:175px">
                        <div class="card-dashbrd-header dashboard-text"><h3><i class="fa fa-filter" aria-hidden="true"></i> <b>ตัวกรองข้อมูล </b></h3></div><br>
                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px;">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                        </div>
                        <div id="imp_cate" class="pull-right">
                            <input type="hidden" id="fmi_imp_cate_unique" name="fmi_imp_cate_unique" value="">
                            <?php $gencode = new GencodeSboxList(
                                                                  $gencode->fld_name = "fmi_imp_cate",
                                                                  $gencode->fld_usage = "imp_cate",
                                                                  $gencode->fld_label = "ประเภทงาน",
                                                                  $gencode->fld_unique = "",
                                                                  $gencode->fld_size = "",
                                                                  $gencode->fld_readmode = "",
                                                                  $gencode->fld_onclick = "",
                                                                  $gencode->fld_onblur = "",
                                                                  $gencode->fld_list = "desc",
                                                                  $gencode->fld_disp = "desc",
                                                                  $gencode->fld_js = "",
                                                                  $gencode->fld_ref_uniq = ""
                                                              );
                            ?>
                        </div>
                        <script type="text/javascript">
                            $(function() {
                                var start = moment().startOf('year');
                                var end = moment();

                                function cb(start, end) {
                                    $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                                    var datefrom = start.format('DD/MM/YYYY');
                                    var dateto = end.format('DD/MM/YYYY');
                                    innitData(datefrom,dateto);
                                }

                                $('select.selectpicker').change(function(){
                                    $('#fmi_imp_cate_unique').val($('#fmi_imp_cate').val());
                                    var datefrom = start.format('DD/MM/YYYY');
                                    var dateto = end.format('DD/MM/YYYY');
                                    innitData(datefrom,dateto);
                                });

                                $('#reportrange').daterangepicker({
                                    startDate: start,
                                    endDate: end,
                                    /*ranges: {
                                       'วันนี้': [moment(), moment()],
                                       'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                       '7 วันย้อนหลัง': [moment().subtract(6, 'days'), moment()],
                                       '30 วันย้อนหลัง': [moment().subtract(29, 'days'), moment()],
                                       'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
                                       'เดือนที่แล้ว': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                   }*/
                                }, cb);
                                cb(start, end);
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card-dashbrd-total card-body h-100 justify-content-center" style="height:250px">
                        <div class="dashbrd-total-number"><h1>รวมทั้งหมด <i class="fa fa-user" aria-hidden="true"></i> <span id="summ_total_daterange" /></h1></div>
                        <span id="summ_total"></span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card-dashbrd-total-today card-body h-100 justify-content-center" style="height:250px">
                        <div class="dashbrd-total-number"><h1>รวมวันนี้ <i class="fa fa-refresh fa-spin fa-fw"></i> <span id="summ_total_todate" /></h1></div>
                        <span id="summ_today"></span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card-dashbrd-total-male card-body h-100 justify-content-center" style="height:250px">
                        <div class="dashbrd-total-number"><h1>เพศชาย <i class="fa fa-male" aria-hidden="true"></i> <span id="summ_total_daterange_male" /></h1></div>
                        <span id="summ_male"></span>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card-dashbrd-total-female card-body h-100 justify-content-center" style="height:250px">
                        <div class="dashbrd-total-number"><h1>เพศหญิง <i class="fa fa-female" aria-hidden="true"></i> <span id="summ_total_daterange_female" /></h1></div>
                        <span id="summ_female"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><i class="fa fa-pie-chart" aria-hidden="true"></i> <b>สถิติผู้ตกทุกข์แบ่งตามช่วงอาย</b>ุ</h3></div><br>
                        <div id="dshbrd_chart_age_group"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><i class="fa fa-line-chart" aria-hidden="true"></i> <b>สถิติผู้ตกทุกข์แบ่งตามเดือน</b></h3></div><br>
                        <div id="dshbrd_chart_prist_date_monthly"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><i class="fa fa-bar-chart" aria-hidden="true"></i> <b>สถิติผู้ตกทุข์แบ่งตามรัฐ </b></h3></div><br>
                        <div id="dshbrd_chart_prist_state"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><b><i class="fa fa-tag" aria-hidden="true"></i> 10 อันดับ <i class="fa fa-angle-double-right" aria-hidden="true"></i> สถานที่ควบคุมตัว</b></h3></div>
                        <div id="pris_det_detail"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><b><i class="fa fa-tag" aria-hidden="true"></i> 10 อันดับ <i class="fa fa-angle-double-right" aria-hidden="true"></i> รัฐที่ถูกจับกุม</b></h3></div>
                        <div id="pris_state_detail"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="width:<?php echo PANEL_WIDTH;?>;height:auto;">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><b><i class="fa fa-tag" aria-hidden="true"></i> 10 อันดับ <i class="fa fa-angle-double-right" aria-hidden="true"></i> ข้อหาที่ถูกจับกุม</b></h3></div>
                        <div id="pris_reg_detail"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-body h-100 justify-content-center">
                        <div class="card-dashbrd-header dashboard-text"><h3><b><i class="fa fa-tag" aria-hidden="true"></i> ข้อมูลสรุป <i class="fa fa-angle-double-right" aria-hidden="true"></i> สถานะผู้ตกทุกข์</b></h3></div>
                        <div id="pris_status_detail"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
