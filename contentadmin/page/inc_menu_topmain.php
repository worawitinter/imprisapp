<!-- inc_menu_topmain.php -->
<?php require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';?>
<meta name="viewport" content="width=device-width,initial-scale=1">
<script type="text/javascript">
    function logout(){
        $.confirm({
            title: '',
            content: 'ยืนยันการออกจากระบบ?',
            icon: 'fa fa-power-off',
            theme: 'modern',
            type: 'red',
            buttons: {
                cancel: {
                    text: 'ยกเลิก',
                },
                confirm: {
                    text: 'ยืนยัน',
                    btnClass: 'btn-red',
                    action: function(){
                        $.LoadingOverlay("show");
                        window.open("logout.php","_self");
                        $.LoadingOverlay("hide");
                    }
                }
            }
        });
    }
</script>
<div class="container">
  <nav class="navbar-topmain navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-header col-lg-6">
              <div id="img_box_top_menu" class="col-lg-4"><img id="resize_horizontal_top_menu" src="<?php echo SERVER_URL_ROOT . '/folder_script/'.SYS_LOGO;?>" /></div>
              <div id="lbl_app_top_menu" class="col-lg-8"><?php echo "<b>".$lbl_app_name."<br>Consular Case Management System</b>";?></div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            </div>
            <div id="navbar" class="col-lg-6 navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=&fromlink=home&fromtarget=&frommode=view"><h4><i class="fa fa-tachometer" aria-hidden="true"></i> <?php echo $lbl_main_menu;?></h4></a></li>
                    <li <?php if($_SESSION["cookies_sys_admin_yn"] == 'n'){ echo "style='display:none'"; } ?>><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=&fromlink=adm&fromtarget=dashboard&frommode=view"><h4><i class="fa fa-cogs" aria-hidden="true"></i> <?php echo $lbl_setting;?></h4></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php echo $lbl_entry;?><span class="caret"></span></h4></a>
                        <ul class="dropdown-menu">
                            <li><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris&fromlink=imp&fromtarget=list&frommode=view"><h4><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo $lbl_entry_impris;?></h4></a></li>
                            <li><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=imp_vist&fromlink=adm_gencode&fromtarget=list&frommode=view"><h4><i class="fa fa-chevron-right" aria-hidden="true"></i> ประวัติการเยี่ยมผู้ตกทุกข์</h4></a></li>
                            <li <?php if($_SESSION["cookies_sys_admin_yn"] == 'n'){ echo "style='display:none'"; } ?>><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impr_doc&fromlink=adm_gencode&fromtarget=list&frommode=view"><h4><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo $lbl_entry_refdoc;?></h4></a></li>
                        </ul>
                    </li>
                    <li><a href="main_screen<?php echo $url_file_ext; ?>?fromtrans=impris_rep&fromlink=rep&fromtarget=list"><h4><i class="fa fa-print" aria-hidden="true"></i> <?php echo $lbl_rept;?></h4></a></li>
                    <li><a href="<?php echo SERVER_URL_ROOT . '/folder_manual/userguide.pdf';?>" target="_blank"><h4><i class="fa fa-commenting" aria-hidden="true"></i> คู่มือการใช้งาน</h4></a></li>
                    <li><a onclick="logout();" style="cursor: pointer;"><h4><i class="fa fa-power-off" aria-hidden="true"></i> <?php echo $lbl_usr_logout;?></h4></a></li>
                </ul>
            </div>
          <!--/.nav-collapse -->
        </div>
    </div>
    <!--/.container-fluid -->
  </nav>
</div>
