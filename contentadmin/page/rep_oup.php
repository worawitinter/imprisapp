<?php
/*###############################################################################
Version 1.0
No.     Modified Date        By                    Description
1.      20170813             wrwt                   create new file
###############################################################################*/

require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/DatabaseOperation.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysMainActivity.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_param_form_main.php';

$sys_prefix_code = $_POST["fmi_prefix_code"];
$sys_run_number = $_POST["fmi_run_number"];
$sys_suffix_code = $_POST["fmi_suffix_code"];
$tablename_main = "sys_gencode_main";

?>
