<!DOCTYPE html>
<?php
require_once '../../contentadmin/config/config.ini.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_set_sysheader.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysReport.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/class/SysConversion.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_fromtrans_parse_description.php';
require_once DOCUMENT_ROOT . '/contentadmin/control/inc_sys_db_config.php';

$SysConversion = new SysConversion();
$SysReport = new SysReport();

$rept_output = $_GET["rept_output"];

if($rept_output == "pdf"){
    require_once DOCUMENT_ROOT . '/folder_script/mpdf60/mpdf.php';
    ob_start();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php require_once DOCUMENT_ROOT . '/folder_script/inc_src_script.php';?>
    </head>
    <body>
        <table border="1" class="table table-striped table-bordered table-list" id="rep_main">
            <thead>
                <tr>
                    <th class="hidden-xs">ลำดับ</th>
                    <th>ชื่อ-สกุล</th>
                    <th>อายุ</th>
                    <th>หมายเลขประจำตัวประชาชน</th>
                    <th>ภูมิลำเนา</th>
                    <th>ข้อหาที่ถูกจับกุม</th>
                    <th>สถานที่ควบคุมตัว</th>
                    <th>วันที่ถูกจับกุม</th>
                    <th>สถานะ</th>
                </tr>
            </thead>
            <tbody>
        <?php
            $uniquenum_pri = '';
            if(isset($_GET["fromtrans"])){
                $fromtrans = str_replace("_rep", "", $_GET["fromtrans"]);
            }

            $fmi_imp_date_from = "";
            $fmi_imp_date_to = "";
            $fmi_state_code = "";
            $fmi_imp_gender = "";
            $fmi_imp_laws = "";
            $fmi_imp_det = "";
            $fmi_search_string = "";
            $fmi_imp_stat = "";

            if(!empty($_GET["datefrom"])){$fmi_imp_date_from = $SysConversion->convertDate($_GET["datefrom"]);}
            if(!empty($_GET["dateto"])){$fmi_imp_date_to = $SysConversion->convertDate($_GET["dateto"]);}
            if(!empty($_GET["state_code"])){$fmi_state_code = $_GET["state_code"];}
            if(!empty($_GET["imp_laws"])){$fmi_imp_laws = $_GET["imp_laws"];}
            if(!empty($_GET["imp_det"])){$fmi_imp_det = $_GET["imp_det"];}
            if(!empty($_GET["imp_gender"])){$fmi_imp_gender = $_GET["imp_gender"];}
            if(!empty($_GET["search_string"])){$fmi_search_string = $_GET["search_string"];}
            if(!empty($_GET["imp_stat"])){$fmi_imp_stat = $_GET["imp_stat"];}

            $search_query = "";
            if($fmi_imp_date_from !== "" && $fmi_imp_date_to == ""){
                $search_query = $search_query . " AND imp.date_002 between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_from."'";
            }else if($fmi_imp_date_from !== "" && $fmi_imp_date_to !== ""){
                $search_query = $search_query . " AND imp.date_002 between '".$fmi_imp_date_from."' AND '".$fmi_imp_date_to."'";
            }
            if(!empty($fmi_state_code)){
                $search_query = $search_query . " AND det.var_25_001 = '".$fmi_state_code."'";
            }
            if(!empty($fmi_imp_laws)){
                $search_query = $search_query . " AND imp.var_25_002 = '".$fmi_imp_laws."'";
            }
            if(!empty($fmi_imp_det)){
                $search_query = $search_query . " AND imp.var_25_005 = '".$fmi_imp_det."'";
            }
            if(!empty($fmi_imp_gender)){
                $search_query = $search_query . " AND imp.desc_lang04 = '".$fmi_imp_gender."'";
            }
            if(!empty($fmi_imp_stat)){
                $search_query = $search_query . " AND imp.var_25_004 = '".$fmi_imp_stat."'";
            }
            if(!empty($fmi_search_string)){
                $search_query = $search_query . " AND lower(imp.desc_lang01) like '%".trim(strtolower($fmi_search_string))."%'";
                $search_query = $search_query . " OR lower(imp.desc_lang02) like '%".trim(strtolower($fmi_search_string))."%'";
                $search_query = $search_query . " OR lower(imp.var_25_003) like '%".trim(strtolower($fmi_search_string))."%'";
                $search_query = $search_query . " OR lower(imp.desc_lang10) like '%".trim(strtolower($fmi_search_string))."%'";
                $search_query = $search_query . " OR lower(imp.notes_memo) like '%".trim(strtolower($fmi_search_string))."%'";
            }

            $qs_result = $SysReport->getImprisRecord($search_query);
            while($rows = $qs_result->fetch(PDO::FETCH_ASSOC)){
                $cnt++;
        ?>
        <tr>
            <td class="hidden-xs"><?php echo $cnt;?></td>
            <td><?php echo $rows["imp_title"]." ".$rows["imp_firstname"]." ".$rows["imp_lastname"];?></td>
            <td><?php echo $rows["imp_age"];?></td>
            <td><?php echo $rows["imp_idcard"];?></td>
            <td><?php echo wordwrap($rows["imp_addrr"]." ".$rows["imp_subdistrict"]." ".$rows["imp_district"]." ".$rows["imp_province"]." ".$rows["imp_zipcode"]);?></td>
            <td><?php echo $rows["imp_reg_desc"];?></td>
            <td><?php echo $rows["imp_det_desc"];?></td>
            <td><?php echo $rows["imp_prisdate"];?></td>
            <td><?php echo $rows["imp_stat_desc"];?></td>
        </tr>
        <?php
            } /* End while loop $qs_result */
        ?>
            </tbody>
        </table>
    </body>
</html>
<?php
if($rept_output == "pdf"){
    $html = ob_get_contents();
    ob_end_clean();
    $pdf = new mPDF('th', 'A3-L', '0', 'THSaraban');
    $pdf->SetDisplayMode('fullpage');
    $pdf->WriteHTML($html, 2);
    ob_clean();
    $pdf->Output();
}
?>
