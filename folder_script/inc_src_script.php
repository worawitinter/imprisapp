<!-- inc_src_script.php -->

<link rel="stylesheet" type="text/css" href="../../folder_script/css_utility.css" />
<link rel="stylesheet" type="text/css" href="../../folder_script/css_report_generator.css" />
<link rel="stylesheet" href="../../folder_script/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../../folder_script/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="../../folder_script/simplePagination/simplePagination.css" rel="stylesheet">
<link href="../../folder_script/bootstrap-select-1.12.4/dist/css/bootstrap-select.min.css" rel="stylesheet">
<link href="../../folder_script/startbootstrap-simple-sidebar/css/simple-sidebar.css" rel="stylesheet">

<script data-cfasync="false" src="../../folder_script/jquery-3.2.1.min.js"></script>
<script data-cfasync="false" src="../../folder_script/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script data-cfasync="false" src='../../folder_script/simplePagination/jquery.simplePagination.js'></script>
<script data-cfasync="false" src='../../folder_script/bootstrap-select-1.12.4/dist/js/bootstrap-select.min.js'></script>

<!--- datetimepicker --->
<link href="../../folder_script/bootstrap-datetimepicker-4.17.47/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<script data-cfasync="false" src="../../folder_script/moment-2.1.0/moment.js"></script>
<script data-cfasync="false" src="../../folder_script/bootstrap-datetimepicker-4.17.47/build/js/bootstrap-datetimepicker.min.js"></script>
<!--- datetimepicker --->

<!--- jquery.thailand.js --->
<script type="text/javascript" src="../../folder_script/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="../../folder_script/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<link rel="stylesheet" href="../../folder_script/jquery.Thailand.js/dist/jquery.Thailand.min.css">
<script type="text/javascript" src="../../folder_script/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
<!--- jquery.thailand.js --->

<!--- bootstrapvalidator --->
<link rel="stylesheet" href="../../folder_script/bootstrapvalidator-0.5.2/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" src="../../folder_script/bootstrapvalidator-0.5.2/dist/js/bootstrapValidator.js"></script>
<!--- bootstrapvalidator --->

<!--- bootstrap uploader --->
<link rel="stylesheet" href="../../folder_script/drag-drop-file-upload/dist/bootstrap.fd.css"/>
<script type="text/javascript" src="../../folder_script/drag-drop-file-upload/src/bootstrap.fd.js"></script>
<!--- bootstrap uploader --->

<!--- Excel / PDF / Etc. table export --->
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/tableExport.js"></script>
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/jquery.base64.js"></script>
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/html2canvas.js"></script>
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../folder_script/tableExport.jquery.plugin/jspdf/libs/base64.js"></script>
<!--- Excel / PDF / Etc. table export --->

<script data-cfasync="false" type="text/javascript" src="../../folder_script/bootpag-1.0.7/lib/jquery.bootpag.js"></script>

<!-- Load c3.css -->
<link href="../../folder_script/c3/c3.css" rel="stylesheet">

<!-- Load d3.js and c3.js -->
<script type="text/javascript" src="../../folder_script/c3/d3.v3.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../../folder_script/c3/c3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="../../folder_script/bootstrap-daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="../../folder_script/bootstrap-daterangepicker/daterangepicker.css" />

<!-- Styled scroll bar -->
<script type="text/javascript" src="../../folder_script/jQuery-slimScroll/jquery.slimscroll.min.js"></script>

<!-- kartik-fileinput -->
<link href="../../folder_script/kartik-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../folder_script/kartik-fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
<!-- piexif.min.js is only needed for restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script data-cfasync="false" src="../../folder_script/kartik-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
    This must be loaded before fileinput.min.js -->
<script data-cfasync="false" src="../../folder_script/kartik-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
    HTML files. This must be loaded before fileinput.min.js -->
<script data-cfasync="false" src="../../folder_script/kartik-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js
   3.3.x versions without popper.min.js. -->
<!---<script src="../../folder_script/kartik-fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>
<script src="../../folder_script/kartik-fileinput/themes/fa/theme.js" type="text/javascript"></script>--->
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<!-- the main fileinput plugin file -->
<script data-cfasync="false" src="../../folder_script/kartik-fileinput/js/fileinput.min.js"></script>
<!-- optionally uncomment line below for loading your theme assets for a theme like Font Awesome (`fa`) -->
<!-- script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/themes/fa/theme.min.js"></script -->
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<script data-cfasync="false" src="../../folder_script/kartik-fileinput/js/locales/th.js"></script>

<script data-cfasync="false" src="../../folder_script/jquery-loading-overlay-1.6.0/src/loadingoverlay.min.js"></script>
<script data-cfasync="false" src="../../folder_script/jquery-loading-overlay-1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>
<script data-cfasync="false" src="../../folder_script/push.js/bin/push.min.js"></script>

<link rel="stylesheet" href="../../folder_script/jquery-confirm/css/jquery-confirm.css">
<script data-cfasync="false" src="../../folder_script/jquery-confirm/js/jquery-confirm.js"></script>
<script data-cfasync="false" src="../../folder_script/ThaiBath/thaibath.js"></script>
<script data-cfasync="false" src="../../folder_script/base64.js"></script>
<script type="text/javascript">
    function chkExistField(tablename,fromtrans,src_fld,chk_fld){
        if(src_fld.trim() != ""){
            var tag_usage = 'chk_exist_fld';
            $.ajax({
                url: '../control/inc_ajax_text.php',
                data: "tag_usage=" + tag_usage + "&search_query=" + src_fld.trim() + "&tablename=" + tablename + "&fromtrans=" + fromtrans + "&fld_name=" + chk_fld,
                dataType: 'text',
                success: function(data)
                {
                    if(data >= 1){
                        var msg = src_fld.trim() + " มีอยู่ในระบบแล้ว!";
                        alert(msg);
                        return false;
                    }
                }
            });
        }
    }

    function toggleAlertBox(title,text,icon,color){
        $.confirm({title: title,content: text,icon: icon,theme: 'modern',type: color,buttons: {close: {text: 'ปิด'}}});
    }

    function toggleSboxList(sbox_id,fld_name,tag_usage,fld_unique){
        var sbox_list = $("#" + sbox_id);
        var unique = $("#" + fld_unique).val();
        sbox_list.empty(); // remove old options
        sbox_list.selectpicker('refresh');
        $.ajax({
            type: "GET",
            cache: false,
            url: "../control/inc_ajax_text.php",
            data: "tag_usage=sbox_list&fld_name=" + fld_name + "&uniquenum_pri=" + unique + "&fromtrans=" + tag_usage,
            success: function(data){
                var json = $.parseJSON(data);
                if(json.length > 0){
                   for (var i=0;i<json.length;++i){
                        var unique = json[i].list_uniq;
                        var desc = json[i].list_desc;
                        var option=$('<option></option>').attr("value", unique).text(desc);
                        sbox_list.append(option);
                   }
                   sbox_list.selectpicker('refresh');
                }
            }
        });
    }
</script>
