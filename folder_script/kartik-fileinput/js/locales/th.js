/*!
 * FileInput Thai Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-fileinput
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";

    $.fn.fileinputLocales['th'] = {
        fileSingle: 'ไฟล์',
        filePlural: 'ไฟล์',
        browseLabel: 'เลือกดู &hellip;',
        removeLabel: 'ลบทิ้ง',
        removeTitle: 'ลบไฟล์ที่เลือกทิ้ง',
        cancelLabel: 'ยกเลิก',
        cancelTitle: 'ยกเลิกการอัพโหลด',
        uploadLabel: 'อัพโหลด',
        uploadTitle: 'อัพโหลดไฟล์ที่เลือก',
        msgNo: 'ไม่',
        msgNoFilesSelected: '',
        msgCancelled: 'ยกเลิก',
        msgPlaceholder: 'เลือก {files}...',
        msgZoomModalHeading: 'แสดงตัวอย่าง',
        msgFileRequired: 'กรุณาเลือกไฟล์ที่ต้องการอัพโหลด.',
        msgSizeTooSmall: 'ไฟล์ "{name}" (<b>{size} KB</b>) มีขนาดเล็กเกินกว่าที่ระบบอนุญาตที่ <b>{minSize} KB</b>.',
        msgSizeTooLarge: 'ไฟล์ "{name}" (<b>{size} KB</b>) มีขนาดเกินกว่าที่ระบบอนุญาตที่ <b>{maxSize} KB</b>, กรุณาลองใหม่อีกครั้ง!',
        msgFilesTooLess: 'คุณต้องเลือกไฟล์จำนวนอย่างน้อย <b>{n}</b> {files} เพื่ออัพโหลด, กรุณาลองใหม่อีกครั้ง!',
        msgFilesTooMany: 'ไฟล์ที่คุณเลือกมีจำนวน <b>({n})</b> ซึ่งเกินกว่าที่ระบบอนุญาตที่ <b>{m}</b>, กรุณาลองใหม่อีกครั้ง!',
        msgFileNotFound: 'ไม่พบไฟล์ "{name}" !',
        msgFileSecured: 'ระบบความปลอดภัยไม่อนุญาตให้อ่านไฟล์ "{name}".',
        msgFileNotReadable: 'ไม่สามารถอ่านไฟล์ "{name}" ได้',
        msgFilePreviewAborted: 'ไฟล์ "{name}" ไม่อนุญาตให้ดูตัวอย่าง',
        msgFilePreviewError: 'พบปัญหาในการดูตัวอย่างไฟล์ "{name}".',
        msgInvalidFileName: 'ชื่อไฟล์ไม่รอบรับอักขระพิเศษ "{name}".',
        msgInvalidFileType: 'ไฟล์ "{name}" เป็นประเภทไฟล์ที่ไม่ถูกต้อง, อนุญาตเฉพาะไฟล์ประเภท "{types}"',
        msgInvalidFileExtension: 'ไฟล์ "{name}" เป็นประเภทที่ไม่รองรับ, อนุญาตเฉพาะไฟล์ประเภท "{extensions}"',
        msgFileTypes: {
            'image': 'image',
            'html': 'HTML',
            'text': 'text',
            'video': 'video',
            'audio': 'audio',
            'flash': 'flash',
            'pdf': 'PDF',
            'object': 'object',
            'doc': 'DOC',
            'docx': 'DOCX',
            'xls': 'XLS',
            'xlsx': 'XLSX'
        },
        msgUploadAborted: 'อัปโหลดไฟล์ถูกยกเลิก',
        msgUploadThreshold: 'กำลังอัพโหลด...',
        msgUploadBegin: 'กำลังดำเนินการ...',
        msgUploadEnd: 'เสร็จสิ้น',
        msgUploadEmpty: 'ไม่พบไฟล์ที่จะอัพโหลด.',
        msgUploadError: 'เกิดความผิดพลาด',
        msgValidationError: 'ข้อผิดพลาดในการตรวจสอบ',
        msgLoading: 'กำลังโหลดไฟล์ {index} จาก {files} &hellip;',
        msgProgress: 'กำลังโหลดไฟล์ {index} จาก {files} - {name} - {percent}%',
        msgSelected: '{n} {files} ถูกเลือก',
        msgFoldersNotAllowed: 'ลาก & วาง เฉพาะไฟล์เท่านั้น! ข้าม dropped folder จำนวน {n}',
        msgImageWidthSmall: 'ความกว้างของภาพไฟล์ "{name}" ต้องมีอย่างน้อย {size} px.',
        msgImageHeightSmall: 'ความสูงของภาพไฟล์ "{name}" ต้องมีอย่างน้อย {size} px.',
        msgImageWidthLarge: 'ความกว้างของภาพไฟล์ "{name}" ไม่เกิน {size} พิกเซล.',
        msgImageHeightLarge: 'ความสูงของไฟล์ภาพ "{name}" ไม่เกิน {size} พิกเซล.',
        msgImageResizeError: 'ไม่สามารถรับขนาดภาพเพื่อปรับขนาด',
        msgImageResizeException: 'ข้อผิดพลาดขณะปรับขนาดภาพ<pre>{errors}</pre>',
        msgAjaxError: 'พบความผิดพลาด {operation}. โปรดลองอีกครั้ง!',
        msgAjaxProgressError: '{operation} เกิดความผิดพลาด',
        ajaxOperations: {
            deleteThumb: 'ไฟล์ถูกลบ',
            uploadThumb: 'ไฟล์ถูกอัพโหลด',
            uploadBatch: 'ชุดไฟล์ถูกอัพโหลด',
            uploadExtra: 'แบบฟอร์มข้อมูลถูกอัพโหลด'
        },
        dropZoneTitle: 'ลาก & วาง ไฟล์ตรงนี้ &hellip;',
        dropZoneClickTitle: '<br>(หรือคลิก เพื่อเลือกไฟล์ {files})',
        fileActionSettings: {
            removeTitle: 'ลบไฟล์',
            uploadTitle: 'อัพโหลดไฟล์',
            uploadRetryTitle: 'อัพโหลดอีกครั้ง',
            downloadTitle: 'ดาวน์โหลดไฟล์',
            zoomTitle: 'แสดงตัวอย่าง',
            dragTitle: 'ย้ายตำแหน่ง',
            indicatorNewTitle: 'ยังไม่ได้อัพโหลด',
            indicatorSuccessTitle: 'อัพโหลด',
            indicatorErrorTitle: 'ข้อผิดพลาดในการอัพโหลด',
            indicatorLoadingTitle: 'อัพโหลด ...'
        },
        previewZoomButtonTitles: {
            prev: 'เรียกดูไฟล์ก่อนหน้านี้',
            next: 'เรียกดูไฟล์ถัดไป',
            toggleheader: 'ไม่แสดงส่วนหัวเมนู',
            fullscreen: 'แสดงแบบเต็มจอ',
            borderless: 'แสดงแบบเต็มขอบหน้าจอ',
            close: 'ปิดการแสดงตัวอย่าง'
        }
    };
})(window.jQuery);
