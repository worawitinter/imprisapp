-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2018 at 01:21 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imprisapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_activity_main`
--

CREATE TABLE `sys_activity_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_unique` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang03` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang04` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang05` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang06` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang07` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang08` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang09` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang10` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sys_config_main`
--

CREATE TABLE `sys_config_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang03` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang04` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang05` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang06` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang07` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang08` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang09` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang10` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_nnn_10_01_yn` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_02_yn` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_03_yn` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_04_yn` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_05_yn` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nnnnnnnnnn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sys_gencode_data`
--

CREATE TABLE `sys_gencode_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uniquenum_uniq` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_unique` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `row_item_num` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT '0000000000',
  `item_unique` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_desc` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sys_gencode_main`
--

CREATE TABLE `sys_gencode_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_unique` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang03` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang04` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang05` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang06` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang07` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang08` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang09` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang10` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_data`
--

CREATE TABLE `sys_user_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uniquenum_uniq` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_firstname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_middlename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_lastname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_main_addr_01` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_main_addr_02` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_postcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_email_01` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_email_02` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_tel_01` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_tel_02` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_website_01` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_website_02` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_main`
--

CREATE TABLE `sys_user_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_role` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_acc_right` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang03` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang04` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang05` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang06` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang07` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang08` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang09` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang10` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_record_data`
--

CREATE TABLE `tran_record_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uniquenum_uniq` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `row_item_num` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT '0000000000',
  `item_unique` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_desc` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tran_record_main`
--

CREATE TABLE `tran_record_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_deleted_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang03` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang04` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang05` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang06` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang07` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang08` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang09` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang10` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_memo` longtext COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_002` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_003` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_004` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_25_005` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_001` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_002` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_003` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_004` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_50_005` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_001` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_002` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_003` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_004` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var_100_005` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_other01_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sys_activity_main`
--
ALTER TABLE `sys_activity_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD UNIQUE KEY `setgen_unique_UNIQUE` (`setgen_unique`),
  ADD KEY `tag_table_usage` (`tag_table_usage`),
  ADD KEY `uniquenum_sec` (`uniquenum_sec`),
  ADD KEY `sys_link` (`sys_link`);

--
-- Indexes for table `sys_config_main`
--
ALTER TABLE `sys_config_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD KEY `tag_table_usage` (`tag_table_usage`),
  ADD KEY `uniquenum_sec` (`uniquenum_sec`),
  ADD KEY `sys_link` (`sys_link`);

--
-- Indexes for table `sys_gencode_data`
--
ALTER TABLE `sys_gencode_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD UNIQUE KEY `setgen_unique_UNIQUE` (`setgen_unique`),
  ADD KEY `tag_table_usage` (`tag_table_usage`),
  ADD KEY `uniquenum_sec` (`uniquenum_sec`),
  ADD KEY `uniquenum_uniq` (`uniquenum_uniq`),
  ADD KEY `sys_link` (`sys_link`),
  ADD KEY `item_unique` (`item_unique`);

--
-- Indexes for table `sys_gencode_main`
--
ALTER TABLE `sys_gencode_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD KEY `uniquenum_pri` (`uniquenum_pri`,`setgen_unique`),
  ADD KEY `tag_table_usage` (`tag_table_usage`),
  ADD KEY `sys_link` (`sys_link`),
  ADD KEY `uniquenum_sec` (`uniquenum_sec`);

--
-- Indexes for table `sys_user_data`
--
ALTER TABLE `sys_user_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `sys_user_main`
--
ALTER TABLE `sys_user_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `tran_record_data`
--
ALTER TABLE `tran_record_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD KEY `tag_table_usage` (`tag_table_usage`),
  ADD KEY `uniquenum_sec` (`uniquenum_sec`),
  ADD KEY `uniquenum_uniq` (`uniquenum_uniq`),
  ADD KEY `sys_link` (`sys_link`);

--
-- Indexes for table `tran_record_main`
--
ALTER TABLE `tran_record_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sys_activity_main`
--
ALTER TABLE `sys_activity_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `sys_config_main`
--
ALTER TABLE `sys_config_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_gencode_data`
--
ALTER TABLE `sys_gencode_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_gencode_main`
--
ALTER TABLE `sys_gencode_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT for table `sys_user_data`
--
ALTER TABLE `sys_user_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `sys_user_main`
--
ALTER TABLE `sys_user_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tran_record_data`
--
ALTER TABLE `tran_record_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tran_record_main`
--
ALTER TABLE `tran_record_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=804;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
