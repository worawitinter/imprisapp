-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2017 at 07:30 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imprisapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_activity_main`
--

CREATE TABLE `sys_activity_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_unique` varchar(25) DEFAULT NULL,
  `setgen_code` varchar(50) DEFAULT NULL,
  `desc_lang01` varchar(100) DEFAULT NULL,
  `desc_lang02` varchar(100) DEFAULT NULL,
  `desc_lang03` varchar(100) DEFAULT NULL,
  `desc_lang04` varchar(100) DEFAULT NULL,
  `desc_lang05` varchar(100) DEFAULT NULL,
  `desc_lang06` varchar(100) DEFAULT NULL,
  `desc_lang07` varchar(100) DEFAULT NULL,
  `desc_lang08` varchar(100) DEFAULT NULL,
  `desc_lang09` varchar(100) DEFAULT NULL,
  `desc_lang10` varchar(100) DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_config_main`
--

CREATE TABLE `sys_config_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) DEFAULT NULL,
  `desc_lang02` varchar(100) DEFAULT NULL,
  `desc_lang03` varchar(100) DEFAULT NULL,
  `desc_lang04` varchar(100) DEFAULT NULL,
  `desc_lang05` varchar(100) DEFAULT NULL,
  `desc_lang06` varchar(100) DEFAULT NULL,
  `desc_lang07` varchar(100) DEFAULT NULL,
  `desc_lang08` varchar(100) DEFAULT NULL,
  `desc_lang09` varchar(100) DEFAULT NULL,
  `desc_lang10` varchar(100) DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_nnn_10_01_yn` varchar(10) NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_02_yn` varchar(10) NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_03_yn` varchar(10) NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_04_yn` varchar(10) NOT NULL DEFAULT 'nnnnnnnnnn',
  `tag_nnn_10_05_yn` varchar(10) NOT NULL DEFAULT 'nnnnnnnnnn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_gencode_data`
--

CREATE TABLE `sys_gencode_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `uniquenum_uniq` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `setgen_unique` varchar(25) DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `row_item_num` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT '0000000000',
  `item_unique` varchar(25) DEFAULT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_desc` varchar(200) DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_gencode_main`
--

CREATE TABLE `sys_gencode_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `tag_deleted_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) CHARACTER SET utf8 NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `companyfn` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `setgen_unique` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `setgen_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang01` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang02` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang03` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang04` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang05` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang06` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang07` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang08` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang09` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `desc_lang10` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `userid_cookie` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `notes_memo` longtext CHARACTER SET utf8,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `var_25_002` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `var_25_003` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `var_25_004` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `var_25_005` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `var_50_001` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `var_50_002` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `var_50_003` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `var_50_004` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `var_50_005` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `var_100_001` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `var_100_002` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `var_100_003` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `var_100_004` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `var_100_005` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tag_other01_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sys_gencode_main`
--

INSERT INTO `sys_gencode_main` (`idcode`, `tag_table_usage`, `tag_deleted_yn`, `tag_active_yn`, `uniquenum_pri`, `uniquenum_sec`, `companyfn`, `masterfn`, `sys_link`, `setgen_unique`, `setgen_code`, `desc_lang01`, `desc_lang02`, `desc_lang03`, `desc_lang04`, `desc_lang05`, `desc_lang06`, `desc_lang07`, `desc_lang08`, `desc_lang09`, `desc_lang10`, `userid_cookie`, `notes_memo`, `date_created`, `date_lastupdate`, `date_001`, `date_002`, `date_003`, `date_004`, `date_005`, `var_25_001`, `var_25_002`, `var_25_003`, `var_25_004`, `var_25_005`, `var_50_001`, `var_50_002`, `var_50_003`, `var_50_004`, `var_50_005`, `var_100_001`, `var_100_002`, `var_100_003`, `var_100_004`, `var_100_005`, `tag_other01_yn`, `tag_other02_yn`, `tag_other03_yn`, `tag_other04_yn`, `tag_other05_yn`, `num_02_d_001`, `num_02_d_002`, `num_02_d_003`, `num_02_d_004`, `num_02_d_005`, `num_04_d_001`, `num_04_d_002`, `num_04_d_003`, `num_04_d_004`, `num_04_d_005`) VALUES
(41, 'imp_stat', 'n', 'y', 'p20170813161502637796205', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170813161502637796205', '003', 'à¸à¸±à¸à¸à¸±à¸™', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-08-13 05:23:24', '2017-09-03 09:57:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(42, 'imp_stat', 'n', 'y', 'p20170813241502637804615', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170813241502637804615', '002', 'à¸žà¸´à¸ˆà¸²à¸£à¸“à¸²à¹ƒà¸™à¸Šà¸±à¹‰à¸™à¸¨à¸²à¸¥', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-08-13 05:23:46', '2017-09-03 09:57:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(43, 'imp_stat', 'n', 'y', 'p20170813001502637840354', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170813001502637840354', '001', 'à¸›à¸¥à¹ˆà¸­à¸¢à¸•à¸±à¸§', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-08-13 05:24:16', '2017-09-03 09:57:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(44, 'reg_list', 'n', 'y', 'p20170902081504355228845', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902081504355228845', '001', 'à¸«à¸¥à¸šà¸«à¸™à¸µà¹€à¸‚à¹‰à¸²à¹€à¸¡à¸·à¸­à¸‡', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:27:29', '2017-09-03 09:57:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(45, 'reg_list', 'n', 'y', 'p20170902341504355254243', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902341504355254243', '002', 'à¸—à¸³à¸‡à¸²à¸™à¹‚à¸”à¸¢à¹„à¸¡à¹ˆà¸¡à¸µà¹ƒà¸šà¸­à¸™à¸¸à¸à¸²à¸•', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:27:58', '2017-09-03 09:57:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(46, 'reg_list', 'n', 'y', 'p20170902591504355279491', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902591504355279491', '003', 'à¸žà¸³à¸™à¸±à¸à¹€à¸à¸´à¸™à¸£à¸°à¸¢à¸°à¹€à¸§à¸¥à¸²', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:28:11', '2017-09-03 09:57:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(47, 'reg_list', 'n', 'y', 'p20170902131504355293203', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902131504355293203', '004', 'à¸—à¸³à¸œà¸´à¸”à¸à¸à¸ˆà¸£à¸²à¸ˆà¸£', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:28:40', '2017-09-03 09:57:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(48, 'pris_det', 'n', 'y', 'p20170902231504355723841', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902231504355723841', '001', 'à¸ªà¸–à¸²à¸™à¸à¸±à¸à¸à¸±à¸™à¸£à¸±à¸à¸›à¸µà¸™à¸±à¸‡', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:35:39', '2017-09-03 09:56:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'MY-07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pulau Pinang', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(49, 'pris_det', 'n', 'y', 'p20170902421504355742236', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902421504355742236', '002', 'à¸ªà¸–à¸²à¸™à¸à¸±à¸à¸à¸±à¸™à¸£à¸±à¸à¸­à¸´à¹‚à¸›à¹Šà¸°', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-02 02:36:27', '2017-09-03 09:56:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'MY-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Perak', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(50, 'pris_det', 'n', 'y', 'p20170902291504355789702', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170902291504355789702', '003', 'à¸ªà¸–à¸²à¸™à¸à¸±à¸à¸à¸±à¸™à¸£à¸±à¸à¸à¸±à¸§à¸¥à¸²à¸£à¹Œà¸¥à¸±à¸¡à¹€à¸›à¸­à¸£à¹Œ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'worawitinter', '', '2017-09-02 02:36:49', '2017-09-03 12:39:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'MY-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Wilayah Persekutuan Kuala Lumpur', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(51, 'sys_user_dept', 'n', 'y', 'p20170903441504422824713', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170903441504422824713', '001', 'à¸à¹ˆà¸²à¸¢à¸«à¸™à¸±à¸‡à¸ªà¸·à¸­à¹€à¸”à¸´à¸™à¸—à¸²à¸‡', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-03 09:14:06', '2017-09-03 09:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000'),
(52, 'sys_user_dept', 'n', 'y', 'p20170903211504422861986', NULL, 'syscfn', 'sysmfn', 'adm_gencode', 'p20170903211504422861986', '002', 'à¸à¹ˆà¸²à¸¢à¸­à¸²à¸„à¸²à¸£/à¸ªà¸–à¸²à¸™à¸—à¸µà¹ˆ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '', '2017-09-03 09:14:43', '2017-09-03 09:57:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'undefined', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'undefined', NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_data`
--

CREATE TABLE `sys_user_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `uniquenum_uniq` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `usr_title` varchar(50) DEFAULT NULL,
  `usr_firstname` varchar(50) DEFAULT NULL,
  `usr_middlename` varchar(50) DEFAULT NULL,
  `usr_lastname` varchar(50) DEFAULT NULL,
  `usr_main_addr_01` varchar(50) DEFAULT NULL,
  `usr_main_addr_02` varchar(50) DEFAULT NULL,
  `usr_city` varchar(50) DEFAULT NULL,
  `usr_state` varchar(50) DEFAULT NULL,
  `usr_country` varchar(50) DEFAULT NULL,
  `usr_postcode` varchar(50) DEFAULT NULL,
  `usr_email_01` varchar(50) DEFAULT NULL,
  `usr_email_02` varchar(50) DEFAULT NULL,
  `usr_tel_01` varchar(50) DEFAULT NULL,
  `usr_tel_02` varchar(50) DEFAULT NULL,
  `usr_website_01` varchar(50) DEFAULT NULL,
  `usr_website_02` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_user_data`
--

INSERT INTO `sys_user_data` (`idcode`, `tag_table_usage`, `tag_deleted_yn`, `tag_active_yn`, `uniquenum_pri`, `uniquenum_sec`, `uniquenum_uniq`, `companyfn`, `masterfn`, `sys_link`, `userid_cookie`, `notes_memo`, `date_created`, `date_lastupdate`, `var_25_001`, `var_25_002`, `var_25_003`, `var_25_004`, `var_25_005`, `var_50_001`, `var_50_002`, `var_50_003`, `var_50_004`, `var_50_005`, `var_100_001`, `var_100_002`, `var_100_003`, `var_100_004`, `var_100_005`, `usr_title`, `usr_firstname`, `usr_middlename`, `usr_lastname`, `usr_main_addr_01`, `usr_main_addr_02`, `usr_city`, `usr_state`, `usr_country`, `usr_postcode`, `usr_email_01`, `usr_email_02`, `usr_tel_01`, `usr_tel_02`, `usr_website_01`, `usr_website_02`) VALUES
(1, 'sys_user', 'n', 'y', 'p20170903551504425595619', NULL, NULL, 'syscfn', 'sysmfn', 'usr', 'worawitinter', 'à¸—à¸”à¸ªà¸­à¸š', '2017-09-03 10:00:39', '2017-09-04 04:28:19', 'p20170903211504422861986', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mr', 'à¸§à¸£à¸§à¸´à¸—à¸¢à¹Œ', NULL, 'à¸šà¸´à¸™à¹€à¸”à¹‡à¸™', NULL, NULL, NULL, NULL, NULL, NULL, 'worawit.inter@gmail.com', NULL, '0850788825', NULL, NULL, NULL),
(2, 'sys_user', 'n', 'y', 'p20170904011504498321446', NULL, NULL, 'syscfn', 'sysmfn', 'usr', 'worawitinter', '', '2017-09-04 06:12:48', '2017-09-04 06:12:48', 'p20170903441504422824713', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mr', 'à¸œà¸¹à¹‰à¹ƒà¸Šà¹‰ 1', NULL, 'à¸œà¸¹à¹‰à¹ƒà¸Šà¹‰ 1', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_user_main`
--

CREATE TABLE `sys_user_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `sys_role` varchar(100) DEFAULT NULL,
  `sys_acc_right` varchar(100) DEFAULT NULL,
  `desc_lang01` varchar(100) DEFAULT NULL,
  `desc_lang02` varchar(100) DEFAULT NULL,
  `desc_lang03` varchar(100) DEFAULT NULL,
  `desc_lang04` varchar(100) DEFAULT NULL,
  `desc_lang05` varchar(100) DEFAULT NULL,
  `desc_lang06` varchar(100) DEFAULT NULL,
  `desc_lang07` varchar(100) DEFAULT NULL,
  `desc_lang08` varchar(100) DEFAULT NULL,
  `desc_lang09` varchar(100) DEFAULT NULL,
  `desc_lang10` varchar(100) DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_user_main`
--

INSERT INTO `sys_user_main` (`idcode`, `tag_table_usage`, `tag_deleted_yn`, `tag_active_yn`, `uniquenum_pri`, `uniquenum_sec`, `companyfn`, `masterfn`, `sys_link`, `username`, `password`, `sys_role`, `sys_acc_right`, `desc_lang01`, `desc_lang02`, `desc_lang03`, `desc_lang04`, `desc_lang05`, `desc_lang06`, `desc_lang07`, `desc_lang08`, `desc_lang09`, `desc_lang10`, `userid_cookie`, `notes_memo`, `date_created`, `date_lastupdate`, `date_001`, `date_002`, `date_003`, `date_004`, `date_005`, `var_25_001`, `var_25_002`, `var_25_003`, `var_25_004`, `var_25_005`, `var_50_001`, `var_50_002`, `var_50_003`, `var_50_004`, `var_50_005`, `var_100_001`, `var_100_002`, `var_100_003`, `var_100_004`, `var_100_005`, `tag_other01_yn`, `tag_other02_yn`, `tag_other03_yn`, `tag_other04_yn`, `tag_other05_yn`) VALUES
(2, 'sys_user', 'n', 'y', 'p20170903551504425595619', NULL, 'syscfn', 'sysmfn', 'usr', 'worawitinter', 'wor@wit2529', 'admin', 'create,view,delete', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'worawitinter', NULL, '2017-09-03 10:00:39', '2017-09-04 04:28:19', '2017-09-04 06:56:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '::1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n'),
(3, 'sys_user', 'n', 'y', 'p20170904011504498321446', NULL, 'syscfn', 'sysmfn', 'usr', 'user1', '123456', 'user', 'view', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'worawitinter', NULL, '2017-09-04 06:12:48', '2017-09-04 06:12:48', '2017-09-04 06:55:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '::1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `tran_record_data`
--

CREATE TABLE `tran_record_data` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `uniquenum_uniq` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `row_item_num` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT '0000000000',
  `item_unique` varchar(25) DEFAULT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_desc` varchar(200) DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tran_record_main`
--

CREATE TABLE `tran_record_main` (
  `idcode` int(11) NOT NULL,
  `tag_table_usage` varchar(25) DEFAULT NULL,
  `tag_deleted_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_active_yn` varchar(1) NOT NULL DEFAULT 'y',
  `uniquenum_pri` varchar(25) NOT NULL DEFAULT 'sys',
  `uniquenum_sec` varchar(25) DEFAULT NULL,
  `companyfn` varchar(45) NOT NULL DEFAULT 'syscfn',
  `masterfn` varchar(45) NOT NULL DEFAULT 'sysmfn',
  `sys_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_lang01` varchar(100) DEFAULT NULL,
  `desc_lang02` varchar(100) DEFAULT NULL,
  `desc_lang03` varchar(100) DEFAULT NULL,
  `desc_lang04` varchar(100) DEFAULT NULL,
  `desc_lang05` varchar(100) DEFAULT NULL,
  `desc_lang06` varchar(100) DEFAULT NULL,
  `desc_lang07` varchar(100) DEFAULT NULL,
  `desc_lang08` varchar(100) DEFAULT NULL,
  `desc_lang09` varchar(100) DEFAULT NULL,
  `desc_lang10` varchar(100) DEFAULT NULL,
  `userid_cookie` varchar(50) DEFAULT NULL,
  `notes_memo` longtext,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_001` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_002` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_003` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_004` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_005` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `var_25_001` varchar(25) DEFAULT NULL,
  `var_25_002` varchar(25) DEFAULT NULL,
  `var_25_003` varchar(25) DEFAULT NULL,
  `var_25_004` varchar(25) DEFAULT NULL,
  `var_25_005` varchar(25) DEFAULT NULL,
  `var_50_001` varchar(50) DEFAULT NULL,
  `var_50_002` varchar(50) DEFAULT NULL,
  `var_50_003` varchar(50) DEFAULT NULL,
  `var_50_004` varchar(50) DEFAULT NULL,
  `var_50_005` varchar(50) DEFAULT NULL,
  `var_100_001` varchar(100) DEFAULT NULL,
  `var_100_002` varchar(100) DEFAULT NULL,
  `var_100_003` varchar(100) DEFAULT NULL,
  `var_100_004` varchar(100) DEFAULT NULL,
  `var_100_005` varchar(100) DEFAULT NULL,
  `tag_other01_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other02_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other03_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other04_yn` varchar(1) NOT NULL DEFAULT 'n',
  `tag_other05_yn` varchar(1) NOT NULL DEFAULT 'n',
  `num_02_d_001` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_002` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_003` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_004` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_02_d_005` decimal(25,2) NOT NULL DEFAULT '0.00',
  `num_04_d_001` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_002` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_003` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_004` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `num_04_d_005` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tran_record_main`
--

INSERT INTO `tran_record_main` (`idcode`, `tag_table_usage`, `tag_deleted_yn`, `tag_active_yn`, `uniquenum_pri`, `uniquenum_sec`, `companyfn`, `masterfn`, `sys_link`, `desc_lang01`, `desc_lang02`, `desc_lang03`, `desc_lang04`, `desc_lang05`, `desc_lang06`, `desc_lang07`, `desc_lang08`, `desc_lang09`, `desc_lang10`, `userid_cookie`, `notes_memo`, `date_created`, `date_lastupdate`, `date_001`, `date_002`, `date_003`, `date_004`, `date_005`, `var_25_001`, `var_25_002`, `var_25_003`, `var_25_004`, `var_25_005`, `var_50_001`, `var_50_002`, `var_50_003`, `var_50_004`, `var_50_005`, `var_100_001`, `var_100_002`, `var_100_003`, `var_100_004`, `var_100_005`, `tag_other01_yn`, `tag_other02_yn`, `tag_other03_yn`, `tag_other04_yn`, `tag_other05_yn`, `num_02_d_001`, `num_02_d_002`, `num_02_d_003`, `num_02_d_004`, `num_02_d_005`, `num_04_d_001`, `num_04_d_002`, `num_04_d_003`, `num_04_d_004`, `num_04_d_005`) VALUES
(1, 'impris', 'n', 'y', 'p20170902561504356236325', NULL, 'syscfn', 'sysmfn', 'imp', 'à¸§à¸£à¸§à¸´à¸—à¸¢à¹Œ', 'à¸šà¸´à¸™à¹€à¸”à¹‡à¸™', '31', 'm', '54', 'à¸šà¹‰à¸²à¸™à¸™à¸²', 'à¸ˆà¸°à¸™à¸°', 'à¸ªà¸‡à¸‚à¸¥à¸²', '90130', '1909900090497', 'worawitinter', 'à¸—à¸”à¸ªà¸­à¸š', '2017-09-02 02:44:32', '2017-09-04 04:51:18', '2017-09-07 00:00:00', '2017-09-28 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0850788825', 'p20170902131504355293203', 'AA0001', 'p20170813161502637796205', 'p20170902421504355742236', 'mr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'n', 'n', 'n', 'n', 'n', '0.00', '0.00', '0.00', '0.00', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sys_activity_main`
--
ALTER TABLE `sys_activity_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD UNIQUE KEY `setgen_unique_UNIQUE` (`setgen_unique`);

--
-- Indexes for table `sys_config_main`
--
ALTER TABLE `sys_config_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `sys_gencode_data`
--
ALTER TABLE `sys_gencode_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD UNIQUE KEY `setgen_unique_UNIQUE` (`setgen_unique`);

--
-- Indexes for table `sys_gencode_main`
--
ALTER TABLE `sys_gencode_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`),
  ADD UNIQUE KEY `setgen_unique_UNIQUE` (`setgen_unique`);

--
-- Indexes for table `sys_user_data`
--
ALTER TABLE `sys_user_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `sys_user_main`
--
ALTER TABLE `sys_user_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `tran_record_data`
--
ALTER TABLE `tran_record_data`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- Indexes for table `tran_record_main`
--
ALTER TABLE `tran_record_main`
  ADD PRIMARY KEY (`idcode`,`uniquenum_pri`),
  ADD UNIQUE KEY `uniquenum_pri_UNIQUE` (`uniquenum_pri`),
  ADD UNIQUE KEY `idcode_UNIQUE` (`idcode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sys_activity_main`
--
ALTER TABLE `sys_activity_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_config_main`
--
ALTER TABLE `sys_config_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_gencode_data`
--
ALTER TABLE `sys_gencode_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_gencode_main`
--
ALTER TABLE `sys_gencode_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `sys_user_data`
--
ALTER TABLE `sys_user_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_user_main`
--
ALTER TABLE `sys_user_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tran_record_data`
--
ALTER TABLE `tran_record_data`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tran_record_main`
--
ALTER TABLE `tran_record_main`
  MODIFY `idcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
